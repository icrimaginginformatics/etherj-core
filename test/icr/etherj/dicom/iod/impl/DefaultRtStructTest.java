/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.module.ClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.ClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.CommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralReferenceModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.PatientStudyModule;
import icr.etherj.dicom.iod.module.RoiContourModule;
import icr.etherj.dicom.iod.module.RtRoiObservationsModule;
import icr.etherj.dicom.iod.module.RtSeriesModule;
import icr.etherj.dicom.iod.module.SopCommonModule;
import icr.etherj.dicom.iod.module.StructureSetModule;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class DefaultRtStructTest
{
	private RtStruct instance;
	
	public DefaultRtStructTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
		instance = new DefaultRtStruct(false);
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of getClinicalTrialStudyModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetClinicalTrialStudyModule()
	{
		ClinicalTrialStudyModule result = instance.getClinicalTrialStudyModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getClinicalTrialSubjectModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetClinicalTrialSubjectModule()
	{
		ClinicalTrialSubjectModule result = instance.getClinicalTrialSubjectModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getCommonInstanceReferenceModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetCommonInstanceReferenceModule()
	{
		CommonInstanceReferenceModule result = instance.getCommonInstanceReferenceModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getFrameOfReferenceModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetFrameOfReferenceModule()
	{
		FrameOfReferenceModule result = instance.getFrameOfReferenceModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getGeneralEquipmentModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetGeneralEquipmentModule()
	{
		GeneralEquipmentModule result = instance.getGeneralEquipmentModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getGeneralReferenceModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetGeneralReferenceModule()
	{
		GeneralReferenceModule result = instance.getGeneralReferenceModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getGeneralStudyModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetGeneralStudyModule()
	{
		GeneralStudyModule result = instance.getGeneralStudyModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getPatientModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetPatientModule()
	{
		PatientModule result = instance.getPatientModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getPatientStudyModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetPatientStudyModule()
	{
		PatientStudyModule result = instance.getPatientStudyModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getRoiContourModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetRoiContourModule()
	{
		RoiContourModule result = instance.getRoiContourModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getRtRoiObservationsModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetRtRoiObservationsModule()
	{
		RtRoiObservationsModule result = instance.getRtRoiObservationsModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getRtSeriesModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetRtSeriesModule()
	{
		RtSeriesModule result = instance.getRtSeriesModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getSopCommonModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetSopCommonModule()
	{
		SopCommonModule result = instance.getSopCommonModule();
		assertFalse(result.isStrict());
	}
	/**
	 * Test of getStructureSetModule method, of class DefaultRtStruct.
	 */
	@Test
	public void testGetStructureSetModule()
	{
		StructureSetModule result = instance.getStructureSetModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of isStrict method, of class DefaultRtStruct.
	 */
	@Test
	public void testIsStrict()
	{
		assertFalse(instance.isStrict());
	}

}
