/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.dicom.iod.module.ClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.ClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.CommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.EnhancedGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralImageModule;
import icr.etherj.dicom.iod.module.GeneralReferenceModule;
import icr.etherj.dicom.iod.module.GeneralSeriesModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import icr.etherj.dicom.iod.module.MultiframeDimensionModule;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.PatientStudyModule;
import icr.etherj.dicom.iod.module.SegmentationImageModule;
import icr.etherj.dicom.iod.module.SegmentationSeriesModule;
import icr.etherj.dicom.iod.module.SopCommonModule;
import junit.framework.TestCase;

/**
 *
 * @author jamesd
 */
public class DefaultSegmentationTest extends TestCase
{
	private Segmentation instance;
	
	public DefaultSegmentationTest(String testName)
	{
		super(testName);
	}
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
		instance = new DefaultSegmentation(false);
	}
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	/**
	 * Test of getClinicalTrialStudyModule method, of class DefaultSegmentation.
	 */
	public void testGetClinicalTrialStudyModule()
	{
		ClinicalTrialStudyModule result = instance.getClinicalTrialStudyModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getClinicalTrialSubjectModule method, of class DefaultSegmentation.
	 */
	public void testGetClinicalTrialSubjectModule()
	{
		ClinicalTrialSubjectModule result = instance.getClinicalTrialSubjectModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getCommonInstanceReferenceModule method, of class DefaultSegmentation.
	 */
	public void testGetCommonInstanceReferenceModule()
	{
		CommonInstanceReferenceModule result = instance.getCommonInstanceReferenceModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getEnhancedEquipmentModule method, of class DefaultSegmentation.
	 */
	public void testGetEnhancedEquipmentModule()
	{
		EnhancedGeneralEquipmentModule result = instance.getEnhancedEquipmentModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getFrameOfReferenceModule method, of class DefaultSegmentation.
	 */
	public void testGetFrameOfReferenceModule()
	{
		FrameOfReferenceModule result = instance.getFrameOfReferenceModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getGeneralEquipmentModule method, of class DefaultSegmentation.
	 */
	public void testGetGeneralEquipmentModule()
	{
		GeneralEquipmentModule result = instance.getGeneralEquipmentModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getGeneralImageModule method, of class DefaultSegmentation.
	 */
	public void testGetGeneralImageModule()
	{
		GeneralImageModule result = instance.getGeneralImageModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getGeneralReferenceModule method, of class DefaultSegmentation.
	 */
	public void testGetGeneralReferenceModule()
	{
		GeneralReferenceModule result = instance.getGeneralReferenceModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getGeneralSeriesModule method, of class DefaultSegmentation.
	 */
	public void testGetGeneralSeriesModule()
	{
		GeneralSeriesModule result = instance.getGeneralSeriesModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getGeneralStudyModule method, of class DefaultSegmentation.
	 */
	public void testGetGeneralStudyModule()
	{
		GeneralStudyModule result = instance.getGeneralStudyModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getImagePixelModule method, of class DefaultSegmentation.
	 */
	public void testGetImagePixelModule()
	{
		ImagePixelModule result = instance.getImagePixelModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getMultiframeFunctionalGroupsModule method, of class DefaultSegmentation.
	 */
	public void testGetMultiframeFunctionalGroupsModule()
	{
		MultiframeFunctionalGroupsModule result = instance.getMultiframeFunctionalGroupsModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getMultiframeDimensionModule method, of class DefaultSegmentation.
	 */
	public void testGetMultiframeDimensionModule()
	{
		MultiframeDimensionModule result = instance.getMultiframeDimensionModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getPatientModule method, of class DefaultSegmentation.
	 */
	public void testGetPatientModule()
	{
		PatientModule result = instance.getPatientModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getPatientStudyModule method, of class DefaultSegmentation.
	 */
	public void testGetPatientStudyModule()
	{
		PatientStudyModule result = instance.getPatientStudyModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getSegmentationImageModule method, of class DefaultSegmentation.
	 */
	public void testGetSegmentationImageModule()
	{
		SegmentationImageModule result = instance.getSegmentationImageModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getSegmentationSeriesModule method, of class DefaultSegmentation.
	 */
	public void testGetSegmentationSeriesModule()
	{
		SegmentationSeriesModule result = instance.getSegmentationSeriesModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of getSopCommonModule method, of class DefaultSegmentation.
	 */
	public void testGetSopCommonModule()
	{
		SopCommonModule result = instance.getSopCommonModule();
		assertFalse(result.isStrict());
	}

	/**
	 * Test of isStrict method, of class DefaultSegmentation.
	 */
	public void testIsStrict()
	{
		assertFalse(instance.isStrict());
	}
	
}
