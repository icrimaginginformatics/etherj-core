/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class DefaultGeneralStudyModuleTest
{
	private final String bad = "bad";
	private final String empty = "";
	private final String value = "value";
	
	public DefaultGeneralStudyModuleTest()
	{
	}

	@BeforeClass
	public static void setUpClass()
	{
	}

	@AfterClass
	public static void tearDownClass()
	{
	}

	@Before
	public void setUp()
	{
	}

	@After
	public void tearDown()
	{
	}

	/**
	 * Test of isStrict method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of setAccessionNumber method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetAccessionNumber()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule();
		instance.setAccessionNumber(value);
		assertEquals(value, instance.getAccessionNumber());
		instance.setAccessionNumber(null);
		assertEquals(empty, instance.getAccessionNumber());
	}

	/**
	 * Test of setStudyDate method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetStudyDateLax()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule(false);
		String date = "20200319";
		instance.setStudyDate(date);
		assertEquals(date, instance.getStudyDate());
		instance.setStudyDate(empty);
		assertEquals(empty, instance.getStudyDate());
		instance.setStudyDate(null);
		assertEquals(empty, instance.getStudyDate());
		instance.setStudyDate(bad);
		assertEquals(bad, instance.getStudyDate());
	}

	/**
	 * Test of setStudyDate method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetStudyDateStrict()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule();
		String date = "20200319";
		instance.setStudyDate(date);
		assertEquals(date, instance.getStudyDate());
		instance.setStudyDate(empty);
		assertEquals(empty, instance.getStudyDate());
		instance.setStudyDate(null);
		assertEquals(empty, instance.getStudyDate());
	}

	/**
	 * Test of setStudyDate method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetStudyDateStrictBad()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule();
		try
		{
			instance.setStudyDate(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setStudyId method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetStudyId()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule();
		instance.setStudyId(value);
		assertEquals(value, instance.getStudyId());
		instance.setStudyId(empty);
		assertEquals(empty, instance.getStudyId());
		instance.setStudyId(null);
		assertEquals(empty, instance.getStudyId());
	}

	/**
	 * Test of setStudyInstanceUid method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetStudyInstanceUidLax()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule(false);
		try
		{
			instance.setStudyInstanceUid(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setStudyInstanceUid(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setStudyInstanceUid method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetStudyInstanceUidStrict()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule();
		instance.setStudyInstanceUid(value);
		assertEquals(value, instance.getStudyInstanceUid());
	}

	/**
	 * Test of setStudyInstanceUid method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetStudyInstanceUidStrictBad()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule();
		try
		{
			instance.setStudyInstanceUid(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setStudyInstanceUid(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setReferringPhysician method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetReferringPhysician()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule();
		instance.setReferringPhysician(value);
		assertEquals(value, instance.getReferringPhysicianName());
		instance.setReferringPhysician(empty);
		assertEquals(empty, instance.getReferringPhysicianName());
		instance.setReferringPhysician(null);
		assertEquals(empty, instance.getReferringPhysicianName());
	}

	/**
	 * Test of setStudyTime method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetStudyTimeLax()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule(false);
		String time = "153745";
		instance.setStudyTime(time);
		assertEquals(time, instance.getStudyTime());
		instance.setStudyTime(empty);
		assertEquals(empty, instance.getStudyTime());
		instance.setStudyTime(null);
		assertEquals(empty, instance.getStudyTime());
		instance.setStudyTime(bad);
		assertEquals(bad, instance.getStudyTime());
	}

	/**
	 * Test of setStudyTime method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetStudyTimeStrict()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule();
		String time = "153745";
		instance.setStudyTime(time);
		assertEquals(time, instance.getStudyTime());
		instance.setStudyTime(empty);
		assertEquals(empty, instance.getStudyTime());
		instance.setStudyTime(null);
		assertEquals(empty, instance.getStudyTime());
	}

	/**
	 * Test of setStudyTime method, of class DefaultGeneralStudyModule.
	 */
	@Test
	public void testSetStudyTimeStrictBad()
	{
		DefaultGeneralStudyModule instance = new DefaultGeneralStudyModule();
		try
		{
			instance.setStudyTime(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

}
