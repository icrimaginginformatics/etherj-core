/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class DefaultClinicalTrialSubjectModuleTest
{
	private final String empty = "";
	private final String value = "value";
	
	public DefaultClinicalTrialSubjectModuleTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of isStrict method, of class DefaultClinicalTrialSubjectModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultClinicalTrialSubjectModule instance =
			new DefaultClinicalTrialSubjectModule(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of setClinicalTrialProtocolId method, of class DefaultClinicalTrialSubjectModule.
	 */
	@Test
	public void testSetClinicalTrialProtocolId()
	{
		DefaultClinicalTrialSubjectModule instance =
			new DefaultClinicalTrialSubjectModule();
		setupTest(instance);
		instance.setClinicalTrialProtocolId(value);
		checkTest(instance);
		setupTest(instance);
		instance.setClinicalTrialProtocolId(null);
		checkTestReset(instance);
		setupTest(instance);
		instance.setClinicalTrialProtocolId(empty);
		checkTestReset(instance);
	}

	/**
	 * Test of setClinicalTrialProtocolName method, of class DefaultClinicalTrialSubjectModule.
	 */
	@Test
	public void testSetClinicalTrialProtocolName()
	{
		DefaultClinicalTrialSubjectModule instance =
			new DefaultClinicalTrialSubjectModule();
		instance.setClinicalTrialProtocolName(value);
		assertEquals(value, instance.getClinicalTrialProtocolName());
		instance.setClinicalTrialProtocolName(null);
		assertEquals(empty, instance.getClinicalTrialProtocolName());
	}

	/**
	 * Test of setClinicalTrialSiteId method, of class DefaultClinicalTrialSubjectModule.
	 */
	@Test
	public void testSetClinicalTrialSiteId()
	{
		DefaultClinicalTrialSubjectModule instance =
			new DefaultClinicalTrialSubjectModule();
		instance.setClinicalTrialSiteId(value);
		assertEquals(value, instance.getClinicalTrialSiteId());
		instance.setClinicalTrialSiteId(null);
		assertEquals(empty, instance.getClinicalTrialSiteId());
	}

	/**
	 * Test of setClinicalTrialSiteName method, of class DefaultClinicalTrialSubjectModule.
	 */
	@Test
	public void testSetClinicalTrialSiteName()
	{
		DefaultClinicalTrialSubjectModule instance =
			new DefaultClinicalTrialSubjectModule();
		instance.setClinicalTrialSiteName(value);
		assertEquals(value, instance.getClinicalTrialSiteName());
		instance.setClinicalTrialSiteName(null);
		assertEquals(empty, instance.getClinicalTrialSiteName());
	}

	/**
	 * Test of setClinicalTrialSponsorName method, of class DefaultClinicalTrialSubjectModule.
	 */
	@Test
	public void testSetClinicalTrialSponsorName()
	{
		DefaultClinicalTrialSubjectModule instance =
			new DefaultClinicalTrialSubjectModule();
		setupTest(instance);
		instance.setClinicalTrialSponsorName(value);
		checkTest(instance);
		setupTest(instance);
		instance.setClinicalTrialSponsorName(null);
		checkTestReset(instance);
		setupTest(instance);
		instance.setClinicalTrialSponsorName(empty);
		checkTestReset(instance);
	}

	/**
	 * Test of setClinicalTrialSubjectId method, of class DefaultClinicalTrialSubjectModule.
	 */
	@Test
	public void testSetClinicalTrialSubjectId()
	{
		DefaultClinicalTrialSubjectModule instance =
			new DefaultClinicalTrialSubjectModule();
		instance.setClinicalTrialSubjectId(value);
		assertEquals(value, instance.getClinicalTrialSubjectId());
		assertNull(instance.getClinicalTrialSubjectReadingId());
		instance.setClinicalTrialSubjectReadingId(value);
		instance.setClinicalTrialSubjectId(empty);
		instance.setClinicalTrialSubjectReadingId(value);
		instance.setClinicalTrialSubjectId(null);
	}

	/**
	 * Test of setClinicalTrialSubjectId method, of class DefaultClinicalTrialSubjectModule.
	 */
	@Test
	public void testSetClinicalTrialSubjectIdBad()
	{
		DefaultClinicalTrialSubjectModule instance =
			new DefaultClinicalTrialSubjectModule();
		try
		{
			instance.setClinicalTrialSubjectId(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setClinicalTrialSubjectId(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setClinicalTrialSubjectReadingId method, of class DefaultClinicalTrialSubjectModule.
	 */
	@Test
	public void testSetClinicalTrialSubjectReadingId()
	{
		DefaultClinicalTrialSubjectModule instance =
			new DefaultClinicalTrialSubjectModule();
		instance.setClinicalTrialSubjectReadingId(value);
		assertEquals(value, instance.getClinicalTrialSubjectReadingId());
		assertNull(instance.getClinicalTrialSubjectId());
		instance.setClinicalTrialSubjectId(value);
		instance.setClinicalTrialSubjectReadingId(empty);
		instance.setClinicalTrialSubjectId(value);
		instance.setClinicalTrialSubjectReadingId(null);
	}

	/**
	 * Test of setClinicalTrialSubjectId method, of class DefaultClinicalTrialSubjectModule.
	 */
	@Test
	public void testSetClinicalTrialSubjectReadingIdBad()
	{
		DefaultClinicalTrialSubjectModule instance =
			new DefaultClinicalTrialSubjectModule();
		try
		{
			instance.setClinicalTrialSubjectReadingId(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setClinicalTrialSubjectReadingId(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	private void checkTest(DefaultClinicalTrialSubjectModule instance)
	{
		assertEquals(value, instance.getClinicalTrialProtocolId());
		assertEquals(value, instance.getClinicalTrialProtocolName());
		assertEquals(value, instance.getClinicalTrialSiteId());
		assertEquals(value, instance.getClinicalTrialSiteName());
		assertEquals(value, instance.getClinicalTrialSponsorName());
		assertNull(instance.getClinicalTrialSubjectId());
		assertEquals(value, instance.getClinicalTrialSubjectReadingId());
	}

	private void checkTestReset(DefaultClinicalTrialSubjectModule instance)
	{
		assertNull(instance.getClinicalTrialProtocolId());
		assertEquals(empty, instance.getClinicalTrialProtocolName());
		assertNull(instance.getClinicalTrialSiteId());
		assertEquals(empty, instance.getClinicalTrialSiteName());
		assertNull(instance.getClinicalTrialSponsorName());
		assertNull(instance.getClinicalTrialSubjectId());
		assertNull(instance.getClinicalTrialSubjectReadingId());
	}

	private void setupTest(DefaultClinicalTrialSubjectModule instance)
	{
		instance.setClinicalTrialProtocolId(value);
		instance.setClinicalTrialProtocolName(value);
		instance.setClinicalTrialSiteId(value);
		instance.setClinicalTrialSiteName(value);
		instance.setClinicalTrialSponsorName(value);
		instance.setClinicalTrialSubjectId(value);
		instance.setClinicalTrialSubjectReadingId(value);
	}
	
}
