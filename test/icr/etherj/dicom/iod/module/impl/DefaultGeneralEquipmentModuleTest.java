/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import junit.framework.TestCase;

/**
 *
 * @author jamesd
 */
public class DefaultGeneralEquipmentModuleTest extends TestCase
{
	
	public DefaultGeneralEquipmentModuleTest(String testName)
	{
		super(testName);
	}
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	/**
	 * Test of isStrict method, of class DefaultGeneralEquipmentModule.
	 */
	public void testIsStrict()
	{
		DefaultGeneralEquipmentModule instance =
			new DefaultGeneralEquipmentModule(false);
		boolean expResult = false;
		boolean result = instance.isStrict();
		assertEquals(expResult, result);
	}

	/**
	 * Test of setManufacturer method, of class DefaultGeneralEquipmentModule.
	 */
	public void testSetManufacturer()
	{
		String empty = "";
		String manufacturer = "Manufacturer";
		DefaultGeneralEquipmentModule instance = new DefaultGeneralEquipmentModule();
		instance.setManufacturer(null);
		assertEquals(empty, instance.getManufacturer());
		instance.setManufacturer(manufacturer);
		assertEquals(manufacturer, instance.getManufacturer());
	}

	/**
	 * Test of setPixelPaddingValue method, of class DefaultGeneralEquipmentModule.
	 */
	public void testSetPixelPaddingValue()
	{
		int value = Short.MIN_VALUE;
		DefaultGeneralEquipmentModule instance = new DefaultGeneralEquipmentModule();
		instance.setPixelPaddingValue(value);
		assertEquals(value, instance.getPixelPaddingValue());
		value = Short.MIN_VALUE-1;
		instance.setPixelPaddingValue(value);
		assertEquals(Integer.MIN_VALUE, instance.getPixelPaddingValue());
		value = (2 << 15)-1;
		instance.setPixelPaddingValue(value);
		assertEquals(value, instance.getPixelPaddingValue());
		value = (2 << 15);
		instance.setPixelPaddingValue(value);
		assertEquals(Integer.MIN_VALUE, instance.getPixelPaddingValue());
	}

	/**
	 * Test of setSpatialResolution method, of class DefaultGeneralEquipmentModule.
	 */
	public void testSetSpatialResolution()
	{
		double resolution = 2.0;
		DefaultGeneralEquipmentModule instance = new DefaultGeneralEquipmentModule();
		instance.setSpatialResolution(resolution);
		assertEquals(resolution, instance.getSpatialResolution());
		resolution = 0.0;
		instance.setSpatialResolution(resolution);
		assertTrue(Double.isNaN(instance.getSpatialResolution()));
		resolution = Double.POSITIVE_INFINITY;
		instance.setSpatialResolution(resolution);
		assertTrue(Double.isNaN(instance.getSpatialResolution()));
	}

	/**
	 * Test of validate method, of class DefaultGeneralEquipmentModule.
	 */
	public void testValidate()
	{
		DefaultGeneralEquipmentModule instance = new DefaultGeneralEquipmentModule();
		assertTrue(instance.validate());
	}
	
}
