/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.dicom.iod.SegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SegmentationSharedFunctionalGroups;
import icr.etherj.dicom.iod.impl.DefaultSegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.impl.DefaultSegmentationSharedFunctionalGroups;
import junit.framework.TestCase;

/**
 *
 * @author jamesd
 */
public class DefaultSegmentationMultiframeFunctionalGroupsModuleTest extends TestCase
{
	private SegmentationPerFrameFunctionalGroups perFrameGroups;
	private SegmentationMultiModuleCore segMultiModCore;
	private SegmentationSharedFunctionalGroups sharedGroups;
	
	public DefaultSegmentationMultiframeFunctionalGroupsModuleTest(String testName)
	{
		super(testName);
	}
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
		segMultiModCore = new SegmentationMultiModuleCore();
		sharedGroups = new DefaultSegmentationSharedFunctionalGroups();
		perFrameGroups = new DefaultSegmentationPerFrameFunctionalGroups();
	}
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	/**
	 * Test of setNumberOfFrames method, of class DefaultSegmentationMultiframeFunctionalGroupsModule.
	 */
	public void testSetNumberOfFramesLax()
	{
		int frameCount = 10;
		DefaultSegmentationMultiframeFunctionalGroupsModule instance =
			new DefaultSegmentationMultiframeFunctionalGroupsModule(
				segMultiModCore, sharedGroups, perFrameGroups, false);
		instance.setNumberOfFrames(frameCount);
		assertEquals(frameCount, instance.getNumberOfFrames());
		instance.setNumberOfFrames(-1);
	}
	
	/**
	 * Test of setNumberOfFrames method, of class DefaultSegmentationMultiframeFunctionalGroupsModule.
	 */
	public void testSetNumberOfFramesStrict()
	{
		int frameCount = 10;
		DefaultSegmentationMultiframeFunctionalGroupsModule instance =
			new DefaultSegmentationMultiframeFunctionalGroupsModule(
				segMultiModCore, sharedGroups, perFrameGroups);
		instance.setNumberOfFrames(frameCount);
		assertEquals(frameCount, instance.getNumberOfFrames());
	}
	
	/**
	 * Test of setNumberOfFrames method, of class DefaultSegmentationMultiframeFunctionalGroupsModule.
	 */
	public void testSetNumberOfFramesStrictBad()
	{
		int frameCount = -1;
		DefaultSegmentationMultiframeFunctionalGroupsModule instance =
			new DefaultSegmentationMultiframeFunctionalGroupsModule(
				segMultiModCore, sharedGroups, perFrameGroups);
		try
		{
			instance.setNumberOfFrames(frameCount);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}
	
}
