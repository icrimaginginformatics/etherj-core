/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author roban
 */
public class DefaultPatientStudyModuleTest
{
	private final String age = "044Y";
	private final String bad = "bad";
	private final double delta = 1e-3;
	private final double value = 10.0;
	
	public DefaultPatientStudyModuleTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of isStrict method, of class DefaultPatientStudyModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultPatientStudyModule instance = new DefaultPatientStudyModule(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of setPatientAge method, of class DefaultPatientStudyModule.
	 */
	@Test
	public void testSetPatientAgeLax()
	{
		DefaultPatientStudyModule instance = new DefaultPatientStudyModule(false);
		instance.setPatientAge(null);
		assertNull(instance.getPatientAge());
		instance.setPatientAge("");
		assertNull(instance.getPatientAge());
		instance.setPatientAge(age);
		assertEquals(age, instance.getPatientAge());
		instance.setPatientAge(bad);
		assertEquals(bad, instance.getPatientAge());
	}

	/**
	 * Test of setPatientAge method, of class DefaultPatientStudyModule.
	 */
	@Test
	public void testSetPatientAgeStrict()
	{
		DefaultPatientStudyModule instance = new DefaultPatientStudyModule();
		instance.setPatientAge(null);
		assertNull(instance.getPatientAge());
		instance.setPatientAge("");
		assertNull(instance.getPatientAge());
		instance.setPatientAge(age);
		assertEquals(age, instance.getPatientAge());
	}

	/**
	 * Test of setPatientAge method, of class DefaultPatientStudyModule.
	 */
	@Test
	public void testSetPatientAgeStrictBad()
	{
		DefaultPatientStudyModule instance = new DefaultPatientStudyModule();
		try
		{
			instance.setPatientAge(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setPatientSize method, of class DefaultPatientStudyModule.
	 */
	@Test
	public void testSetPatientSize()
	{
		DefaultPatientStudyModule instance = new DefaultPatientStudyModule();
		instance.setPatientSize(value);
		assertEquals(value, instance.getPatientSize(), delta);
		instance.setPatientSize(-1);
		assertTrue(Double.isNaN(instance.getPatientSize()));
		instance.setPatientSize(Double.POSITIVE_INFINITY);
		assertTrue(Double.isNaN(instance.getPatientSize()));
	}

	/**
	 * Test of setPatientWeight method, of class DefaultPatientStudyModule.
	 */
	@Test
	public void testSetPatientWeight()
	{
		DefaultPatientStudyModule instance = new DefaultPatientStudyModule();
		instance.setPatientWeight(value);
		assertEquals(value, instance.getPatientWeight(), delta);
		instance.setPatientWeight(-1);
		assertTrue(Double.isNaN(instance.getPatientWeight()));
		instance.setPatientWeight(Double.POSITIVE_INFINITY);
		assertTrue(Double.isNaN(instance.getPatientWeight()));
	}
	
}
