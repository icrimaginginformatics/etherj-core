/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.ReferencedFrameOfReference;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.impl.DefaultReferencedFrameOfReference;
import icr.etherj.dicom.iod.impl.DefaultStructureSetRoi;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author roban
 */
public class DefaultStructureSetModuleTest
{
	private final String bad = "bad";
	private RtStructMultiModuleCore core;
	private final String date = "20200323";
	private final String empty = "";
	private final ReferencedFrameOfReference rfor1 =
		new DefaultReferencedFrameOfReference();
	private final ReferencedFrameOfReference rfor2 =
		new DefaultReferencedFrameOfReference();
	private final StructureSetRoi ssRoi1 = new DefaultStructureSetRoi();
	private final StructureSetRoi ssRoi2 = new DefaultStructureSetRoi();
	private final String time = "185407";
	private final String value = "value";
	
	public DefaultStructureSetModuleTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
		core = new RtStructMultiModuleCore();
		ssRoi1.setRoiNumber(1);
		ssRoi2.setRoiNumber(2);
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of addReferencedFrameOfReference method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testAddReferencedFrameOfReference()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		assertFalse(instance.addReferencedFrameOfReference(null));
		assertTrue(instance.addReferencedFrameOfReference(rfor1));
	}

	/**
	 * Test of addStructureSetRoi method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testAddStructureSetRoi()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		assertFalse(instance.addStructureSetRoi(null));
		assertTrue(instance.addStructureSetRoi(ssRoi1));
	}

	/**
	 * Test of getReferencedFrameOfReferenceList method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testGetReferencedFrameOfReferenceList()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		instance.addReferencedFrameOfReference(rfor1);
		instance.addReferencedFrameOfReference(rfor2);
		List<ReferencedFrameOfReference> list =
			instance.getReferencedFrameOfReferenceList();
		assertNotNull(list);
		assertEquals(2, list.size());
	}

	/**
	 * Test of getStructureSetRoi method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testGetStructureSetRoi()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		assertNull(instance.getStructureSetRoi(2));
		instance.addStructureSetRoi(ssRoi2);
		assertEquals(ssRoi2, instance.getStructureSetRoi(2));
	}

	/**
	 * Test of getStructureSetRoiList method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testGetStructureSetRoiList()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		instance.addStructureSetRoi(ssRoi1);
		instance.addStructureSetRoi(ssRoi2);
		List<StructureSetRoi> list = instance.getStructureSetRoiList();
		assertNotNull(list);
		assertEquals(2, list.size());
	}

	/**
	 * Test of isStrict method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core,
			false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of removeStructureSetRoi method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testRemoveStructureSetRoi()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		instance.addStructureSetRoi(ssRoi1);
		instance.addStructureSetRoi(ssRoi2);
		assertTrue(instance.removeStructureSetRoi(2));
		assertFalse(instance.removeStructureSetRoi(2));
		List<StructureSetRoi> list = instance.getStructureSetRoiList();
		assertFalse(list.contains(ssRoi2));
	}

	/**
	 * Test of removeStructureSetRoi method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testRemoveStructureSetRoi_StructureSetRoi()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		instance.addStructureSetRoi(ssRoi1);
		instance.addStructureSetRoi(ssRoi2);
		assertTrue(instance.removeStructureSetRoi(ssRoi2));
		assertFalse(instance.removeStructureSetRoi(ssRoi2));
		List<StructureSetRoi> list = instance.getStructureSetRoiList();
		assertFalse(list.contains(ssRoi2));
	}

	/**
	 * Test of setStructureSetDate method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testSetStructureSetDateLax()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core,
			false);
		instance.setStructureSetDate(date);
		assertEquals(date, instance.getStructureSetDate());
		instance.setStructureSetDate(null);
		assertEquals(empty, instance.getStructureSetDate());
		instance.setStructureSetDate(empty);
		assertEquals(empty, instance.getStructureSetDate());
		instance.setStructureSetDate(bad);
		assertEquals(bad, instance.getStructureSetDate());
	}

	/**
	 * Test of setStructureSetDate method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testSetStructureSetDateStrict()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		instance.setStructureSetDate(date);
		assertEquals(date, instance.getStructureSetDate());
		instance.setStructureSetDate(null);
		assertEquals(empty, instance.getStructureSetDate());
		instance.setStructureSetDate(empty);
		assertEquals(empty, instance.getStructureSetDate());
	}

	/**
	 * Test of setStructureSetDate method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testSetStructureSetDateStrictBad()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		try
		{
			instance.setStructureSetDate(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setStructureSetLabel method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testSetStructureSetLabelLax()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core,
			false);
		instance.setStructureSetLabel(value);
		assertEquals(value, instance.getStructureSetLabel());
		instance.setStructureSetLabel(null);
		assertFalse(StringUtils.isNullOrEmpty(instance.getStructureSetLabel()));
		instance.setStructureSetLabel(empty);
		assertFalse(StringUtils.isNullOrEmpty(instance.getStructureSetLabel()));
	}

	/**
	 * Test of setStructureSetLabel method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testSetStructureSetLabelStrict()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		instance.setStructureSetLabel(value);
		assertEquals(value, instance.getStructureSetLabel());
	}

	/**
	 * Test of setStructureSetLabel method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testSetStructureSetLabelStrictBad()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		try
		{
			instance.setStructureSetLabel(null);
			fail("Expected IllegalArgumentExcption not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setStructureSetLabel(empty);
			fail("Expected IllegalArgumentExcption not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setStructureSetTime method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testSetStructureSetTimeLax()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core,
			false);
		instance.setStructureSetTime(time);
		assertEquals(time, instance.getStructureSetTime());
		instance.setStructureSetTime(null);
		assertEquals(empty, instance.getStructureSetTime());
		instance.setStructureSetTime(empty);
		assertEquals(empty, instance.getStructureSetTime());
		instance.setStructureSetTime(bad);
		assertEquals(bad, instance.getStructureSetTime());
	}

	/**
	 * Test of setStructureSetTime method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testSetStructureSetTimeStrict()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		instance.setStructureSetTime(time);
		assertEquals(time, instance.getStructureSetTime());
		instance.setStructureSetTime(null);
		assertEquals(empty, instance.getStructureSetTime());
		instance.setStructureSetTime(empty);
		assertEquals(empty, instance.getStructureSetTime());
	}

	/**
	 * Test of setStructureSetTime method, of class DefaultStructureSetModule.
	 */
	@Test
	public void testSetStructureSetTimeStrictBad()
	{
		DefaultStructureSetModule instance = new DefaultStructureSetModule(core);
		try
		{
			instance.setStructureSetTime(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

}
