/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import junit.framework.TestCase;

/**
 *
 * @author jamesd
 */
public class SegmentationImagePixelModuleTest extends TestCase
{
	private final SegmentationMultiModuleCore segMultiModCore =
		new SegmentationMultiModuleCore();
	
	public SegmentationImagePixelModuleTest(String testName)
	{
		super(testName);
	}
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	/**
	 * Test of getPlanarConfiguration method, of class SegmentationImagePixelModule.
	 */
	public void testGetPlanarConfiguration()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore);
		try
		{
			instance.getPlanarConfiguration();
			fail("Expected UnsupportedOperationException not thrown");
		}
		catch (UnsupportedOperationException ex)
		{}
	}

	/**
	 * Test of isStrict method, of class SegmentationImagePixelModule.
	 */
	public void testIsStrict()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore, false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of setColumnCount method, of class SegmentationImagePixelModule.
	 */
	public void testSetColumnCountLax()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore, false);
		int columns = 128;
		instance.setColumnCount(columns);
		assertEquals(columns, instance.getColumnCount());
		columns = 0;
		instance.setColumnCount(columns);
		assertEquals(columns, instance.getColumnCount());
	}

	/**
	 * Test of setColumnCount method, of class SegmentationImagePixelModule.
	 */
	public void testSetColumnCountStrict()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore);
		int columns = 128;
		instance.setColumnCount(columns);
		assertEquals(columns, instance.getColumnCount());
	}

	/**
	 * Test of setColumnCount method, of class SegmentationImagePixelModule.
	 */
	public void testSetColumnCountStrictBad()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore);
		try
		{
			instance.setColumnCount(0);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setLargestPixelValue method, of class SegmentationImagePixelModule.
	 */
	public void testSetLargestPixelValue()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore);
		int value = 240;
		instance.setLargestPixelValue(value);
		assertEquals(value, instance.getLargestPixelValue());
		instance.setLargestPixelValue(-240);
		assertEquals(-1, instance.getLargestPixelValue());
	}

	/**
	 * Test of setPixelData method, of class SegmentationImagePixelModule.
	 */
	public void testSetPixelData()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore);
		byte[] pixels = null;
		try
		{
			instance.setPixelData(pixels);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setPlanarConfiguration method, of class SegmentationImagePixelModule.
	 */
	public void testSetPlanarConfiguration()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore);
		try
		{
			instance.setPlanarConfiguration(0);
			fail("Expected UnsupportedOperationException not thrown");
		}
		catch (UnsupportedOperationException ex)
		{}
	}

	/**
	 * Test of setRowCount method, of class SegmentationImagePixelModule.
	 */
	public void testSetRowCountLax()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore, false);
		int rows = 128;
		instance.setRowCount(rows);
		assertEquals(rows, instance.getRowCount());
		rows = 0;
		instance.setRowCount(rows);
		assertEquals(rows, instance.getRowCount());
	}

	/**
	 * Test of setRowCount method, of class SegmentationImagePixelModule.
	 */
	public void testSetRowCountStrict()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore);
		int rows = 128;
		instance.setRowCount(rows);
		assertEquals(rows, instance.getRowCount());
	}

	/**
	 * Test of setRowCount method, of class SegmentationImagePixelModule.
	 */
	public void testSetRowCountStrictBad()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore);
		try
		{
			instance.setRowCount(-1);
			fail("Expected IllegalArguemntException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setSmallestPixelValue method, of class SegmentationImagePixelModule.
	 */
	public void testSetSmallestPixelValue()
	{
		SegmentationImagePixelModule instance = new SegmentationImagePixelModule(
			segMultiModCore);
		int value = 240;
		instance.setSmallestPixelValue(value);
		assertEquals(value, instance.getSmallestPixelValue());
		instance.setSmallestPixelValue(-240);
		assertEquals(-1, instance.getSmallestPixelValue());
	}
	
}
