/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.Uids;
import junit.framework.TestCase;

/**
 *
 * @author jamesd
 */
public class DefaultFrameOfReferenceModuleTest extends TestCase
{
	private final String empty = "";
	private final String uid = Uids.generateDicomUid();

	public DefaultFrameOfReferenceModuleTest(String testName)
	{
		super(testName);
	}
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	/**
	 * Test of isStrict method, of class DefaultFrameOfReferenceModule.
	 */
	public void testIsStrict()
	{
		DefaultFrameOfReferenceModule instance =
			new DefaultFrameOfReferenceModule(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of setFrameOfReferenceUid method, of class DefaultFrameOfReferenceModule.
	 */
	public void testSetFrameOfReferenceUidLax()
	{
		DefaultFrameOfReferenceModule instance =
			new DefaultFrameOfReferenceModule(false);
		instance.setFrameOfReferenceUid(null);
		assertNull(instance.getFrameOfReferenceUid());
		instance.setFrameOfReferenceUid(uid);
		assertEquals(uid, instance.getFrameOfReferenceUid());
		instance.setFrameOfReferenceUid(empty);
		assertEquals(empty, instance.getFrameOfReferenceUid());
	}

	/**
	 * Test of setFrameOfReferenceUid method, of class DefaultFrameOfReferenceModule.
	 */
	public void testSetFrameOfReferenceUidStrict()
	{
		DefaultFrameOfReferenceModule instance = new DefaultFrameOfReferenceModule();
		instance.setFrameOfReferenceUid(null);
		assertNull(instance.getFrameOfReferenceUid());
		instance.setFrameOfReferenceUid(uid);
		assertEquals(uid, instance.getFrameOfReferenceUid());
	}

	/**
	 * Test of setFrameOfReferenceUid method, of class DefaultFrameOfReferenceModule.
	 */
	public void testSetFrameOfReferenceUidStrictBad()
	{
		DefaultFrameOfReferenceModule instance = new DefaultFrameOfReferenceModule();
		try
		{
			instance.setFrameOfReferenceUid(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setPositionReferenceIndicator method, of class DefaultFrameOfReferenceModule.
	 */
	public void testSetPositionReferenceIndicator()
	{
		DefaultFrameOfReferenceModule instance = new DefaultFrameOfReferenceModule();
		instance.setPositionReferenceIndicator(null);
		assertEquals(empty, instance.getPositionReferenceIndicator());
		instance.setPositionReferenceIndicator(empty);
		assertEquals(empty, instance.getPositionReferenceIndicator());
		instance.setPositionReferenceIndicator(uid);
		assertEquals(uid, instance.getPositionReferenceIndicator());
	}
	
}
