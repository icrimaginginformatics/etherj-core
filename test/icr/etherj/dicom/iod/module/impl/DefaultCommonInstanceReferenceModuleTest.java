/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.dicom.iod.ReferencedSeries;
import icr.etherj.dicom.iod.impl.DefaultReferencedSeries;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class DefaultCommonInstanceReferenceModuleTest
{
	private ReferencedSeries series1 = new DefaultReferencedSeries();
	private ReferencedSeries series2 = new DefaultReferencedSeries();

	
	public DefaultCommonInstanceReferenceModuleTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of addReferencedSeries method, of class DefaultCommonInstanceReferenceModule.
	 */
	@Test
	public void testAddReferencedSeries()
	{
		DefaultCommonInstanceReferenceModule instance =
			new DefaultCommonInstanceReferenceModule();
		assertTrue(instance.addReferencedSeries(series1));
		List<ReferencedSeries> list = instance.getReferencedSeriesList();
		assertTrue(list.contains(series1));
		assertEquals(1, list.size());
	}

	/**
	 * Test of addReferencedSeries method, of class DefaultCommonInstanceReferenceModule.
	 */
	@Test
	public void testAddReferencedSeriesBad()
	{
		DefaultCommonInstanceReferenceModule instance =
			new DefaultCommonInstanceReferenceModule();
		try
		{
			instance.addReferencedSeries(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of getReferencedSeriesList method, of class DefaultCommonInstanceReferenceModule.
	 */
	@Test
	public void testGetReferencedSeriesList()
	{
		DefaultCommonInstanceReferenceModule instance =
			new DefaultCommonInstanceReferenceModule();
		List<ReferencedSeries> list = instance.getReferencedSeriesList();
		assertNotNull(list);
		assertTrue(list.isEmpty());
	}

	/**
	 * Test of isStrict method, of class DefaultCommonInstanceReferenceModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultCommonInstanceReferenceModule instance =
			new DefaultCommonInstanceReferenceModule(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of removeReferencedSeries method, of class DefaultCommonInstanceReferenceModule.
	 */
	@Test
	public void testRemoveReferencedSeries()
	{
		DefaultCommonInstanceReferenceModule instance =
			new DefaultCommonInstanceReferenceModule();
		instance.addReferencedSeries(series1);
		instance.addReferencedSeries(series2);
		assertTrue(instance.removeReferencedSeries(series1));
		assertFalse(instance.removeReferencedSeries(series1));
		List<ReferencedSeries> list = instance.getReferencedSeriesList();
		assertTrue(list.contains(series2));
		assertFalse(list.contains(series1));
		assertEquals(1, list.size());
	}
	
}
