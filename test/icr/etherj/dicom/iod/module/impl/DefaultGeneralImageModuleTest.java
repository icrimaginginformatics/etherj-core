/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.dicom.iod.Constants;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class DefaultGeneralImageModuleTest
{
	private final String bad = "bad";
	private final String date = "20200320";
	private final String empty = "";
	private final String time = "145137";
	
	public DefaultGeneralImageModuleTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of isStrict method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of setAcquisitionDate method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetAcquisitionDateLax()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule(false);
		instance.setAcquisitionDate(date);
		assertEquals(date, instance.getAcquisitionDate());
		instance.setAcquisitionDate(empty);
		assertEquals(empty, instance.getAcquisitionDate());
		instance.setAcquisitionDate(null);
		assertNull(instance.getAcquisitionDate());
		instance.setAcquisitionDate(bad);
		assertEquals(bad, instance.getAcquisitionDate());
	}

	/**
	 * Test of setAcquisitionDate method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetAcquisitionDateStrict()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		instance.setAcquisitionDate(date);
		assertEquals(date, instance.getAcquisitionDate());
		instance.setAcquisitionDate(empty);
		assertEquals(empty, instance.getAcquisitionDate());
		instance.setAcquisitionDate(null);
		assertNull(instance.getAcquisitionDate());
	}

	/**
	 * Test of setAcquisitionDate method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetAcquisitionDateStrictBad()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		try
		{
			instance.setAcquisitionDate(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setAcquisitionTime method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetAcquisitionTimeLax()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule(false);
		instance.setAcquisitionTime(time);
		assertEquals(time, instance.getAcquisitionTime());
		instance.setAcquisitionTime(empty);
		assertEquals(empty, instance.getAcquisitionTime());
		instance.setAcquisitionTime(null);
		assertEquals(null, instance.getAcquisitionTime());
		instance.setAcquisitionTime(bad);
		assertEquals(bad, instance.getAcquisitionTime());
	}


	/**
	 * Test of setAcquisitionTime method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetAcquisitionTimeStrict()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		instance.setAcquisitionTime(time);
		assertEquals(time, instance.getAcquisitionTime());
		instance.setAcquisitionTime(empty);
		assertEquals(empty, instance.getAcquisitionTime());
		instance.setAcquisitionTime(null);
		assertEquals(null, instance.getAcquisitionTime());
	}


	/**
	 * Test of setAcquisitionTime method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetAcquisitionTimeStrictBad()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		try
		{
			instance.setAcquisitionTime(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setBurnedInAnnotation method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetBurnedInAnnotationLax()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule(false);
		instance.setBurnedInAnnotation(null);
		assertNull(instance.getBurnedInAnnotation());
		instance.setBurnedInAnnotation(Constants.Yes);
		assertEquals(Constants.Yes, instance.getBurnedInAnnotation());
		instance.setBurnedInAnnotation(Constants.No);
		assertEquals(Constants.No, instance.getBurnedInAnnotation());
		instance.setBurnedInAnnotation(bad);
		assertEquals(bad, instance.getBurnedInAnnotation());
	}

	/**
	 * Test of setBurnedInAnnotation method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetBurnedInAnnotationStrict()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		instance.setBurnedInAnnotation(null);
		assertNull(instance.getBurnedInAnnotation());
		instance.setBurnedInAnnotation(Constants.Yes);
		assertEquals(Constants.Yes, instance.getBurnedInAnnotation());
		instance.setBurnedInAnnotation(Constants.No);
		assertEquals(Constants.No, instance.getBurnedInAnnotation());
	}

	/**
	 * Test of setBurnedInAnnotation method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetBurnedInAnnotationStrictBad()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		try
		{
			instance.setBurnedInAnnotation(bad);
			fail("Expected IllegalArgumentException not found");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setAcquisitionDate method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetContentDateLax()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule(false);
		instance.setContentDate(date);
		assertEquals(date, instance.getContentDate());
		instance.setContentDate(empty);
		assertEquals(empty, instance.getContentDate());
		instance.setContentDate(null);
		assertNull(instance.getContentDate());
		instance.setContentDate(bad);
		assertEquals(bad, instance.getContentDate());
	}

	/**
	 * Test of setAcquisitionDate method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetContentDateStrict()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		instance.setContentDate(date);
		assertEquals(date, instance.getContentDate());
		instance.setContentDate(empty);
		assertEquals(empty, instance.getContentDate());
		instance.setContentDate(null);
		assertNull(instance.getContentDate());
	}

	/**
	 * Test of setAcquisitionDate method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetContentDateStrictBad()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		try
		{
			instance.setContentDate(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setAcquisitionTime method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetContentTimeLax()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule(false);
		instance.setContentTime(time);
		assertEquals(time, instance.getContentTime());
		instance.setContentTime(empty);
		assertEquals(empty, instance.getContentTime());
		instance.setContentTime(null);
		assertEquals(null, instance.getContentTime());
		instance.setContentTime(bad);
		assertEquals(bad, instance.getContentTime());
	}


	/**
	 * Test of setAcquisitionTime method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetContentTimeStrict()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		instance.setContentTime(time);
		assertEquals(time, instance.getContentTime());
		instance.setContentTime(empty);
		assertEquals(empty, instance.getContentTime());
		instance.setContentTime(null);
		assertEquals(null, instance.getContentTime());
	}


	/**
	 * Test of setAcquisitionTime method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetContentTimeStrictBad()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		try
		{
			instance.setContentTime(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setInstanceNumber method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetInstanceNumber()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		instance.setInstanceNumber(0);
		assertEquals(0, instance.getInstanceNumber());
		instance.setInstanceNumber(1);
		assertEquals(1, instance.getInstanceNumber());
		instance.setInstanceNumber(-1);
		assertEquals(0, instance.getInstanceNumber());
	}

	/**
	 * Test of setPatientOrientation method, of class DefaultGeneralImageModule.
	 */
	@Test
	public void testSetPatientOrientation()
	{
		DefaultGeneralImageModule instance = new DefaultGeneralImageModule();
		String[] orientation = null;
		instance.setPatientOrientation(orientation);
		String[] result = instance.getPatientOrientation();
		assertNotNull(result);
		assertEquals(0, result.length);
		orientation = new String[3];
		instance.setPatientOrientation(orientation);
		result = instance.getPatientOrientation();
		assertNotNull(result);
		assertEquals(0, result.length);
		orientation = new String[] {"Fish", "Banana"};
		instance.setPatientOrientation(orientation);
		result = instance.getPatientOrientation();
		assertTrue(Arrays.equals(orientation, result));
	}
	
}
