/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.dicom.iod.RoiContour;
import icr.etherj.dicom.iod.impl.DefaultRoiContour;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class DefaultRoiContourModuleTest
{
	private final RoiContour rc1 = new DefaultRoiContour();
	private final RoiContour rc2 = new DefaultRoiContour();

	public DefaultRoiContourModuleTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
		rc1.setReferencedRoiNumber(1);
		rc2.setReferencedRoiNumber(2);
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of addRoiContour method, of class DefaultRoiContourModule.
	 */
	@Test
	public void testAddRoiContour()
	{
		DefaultRoiContourModule instance = new DefaultRoiContourModule();
		assertFalse(instance.addRoiContour(null));
		assertTrue(instance.addRoiContour(rc1));
		instance.addRoiContour(rc1);
		List<RoiContour> list = instance.getRoiContourList();
		assertEquals(1, list.size());
	}

	/**
	 * Test of getRoiContour method, of class DefaultRoiContourModule.
	 */
	@Test
	public void testGetRoiContour()
	{
		DefaultRoiContourModule instance = new DefaultRoiContourModule();
		instance.addRoiContour(rc1);
		RoiContour result = instance.getRoiContour(rc1.getReferencedRoiNumber());
		assertEquals(rc1, result);
	}

	/**
	 * Test of getRoiContourList method, of class DefaultRoiContourModule.
	 */
	@Test
	public void testGetRoiContourList()
	{
		DefaultRoiContourModule instance = new DefaultRoiContourModule();
		instance.addRoiContour(rc1);
		instance.addRoiContour(rc2);
		List<RoiContour> list = instance.getRoiContourList();
		assertNotNull(list);
		assertEquals(2, list.size());
	}

	/**
	 * Test of isStrict method, of class DefaultRoiContourModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultRoiContourModule instance = new DefaultRoiContourModule(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of removeRoiContour method, of class DefaultRoiContourModule.
	 */
	@Test
	public void testRemoveRoiContour_int()
	{
		DefaultRoiContourModule instance = new DefaultRoiContourModule();
		instance.addRoiContour(rc1);
		instance.addRoiContour(rc2);
		assertTrue(instance.removeRoiContour(rc1.getReferencedRoiNumber()));
		assertFalse(instance.removeRoiContour(rc1.getReferencedRoiNumber()));
		List<RoiContour> list = instance.getRoiContourList();
		assertEquals(1, list.size());
		assertFalse(list.contains(rc1));
		assertTrue(list.contains(rc2));
	}

	/**
	 * Test of removeRoiContour method, of class DefaultRoiContourModule.
	 */
	@Test
	public void testRemoveRoiContour_RoiContour()
	{
		DefaultRoiContourModule instance = new DefaultRoiContourModule();
		instance.addRoiContour(rc1);
		instance.addRoiContour(rc2);
		assertTrue(instance.removeRoiContour(rc1));
		assertFalse(instance.removeRoiContour(rc1));
		List<RoiContour> list = instance.getRoiContourList();
		assertEquals(1, list.size());
		assertFalse(list.contains(rc1));
		assertTrue(list.contains(rc2));
	}

}
