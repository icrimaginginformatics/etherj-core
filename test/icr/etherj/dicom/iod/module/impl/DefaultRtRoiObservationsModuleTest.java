/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.dicom.iod.RtRoiObservation;
import icr.etherj.dicom.iod.impl.DefaultRtRoiObservation;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class DefaultRtRoiObservationsModuleTest
{
	private final RtRoiObservation obs1 = new DefaultRtRoiObservation();
	private final RtRoiObservation obs2 = new DefaultRtRoiObservation();
	
	public DefaultRtRoiObservationsModuleTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
		obs1.setReferencedRoiNumber(1);
		obs2.setReferencedRoiNumber(2);
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of addRtRoiObservation method, of class DefaultRtRoiObservationsModule.
	 */
	@Test
	public void testAddRtRoiObservation()
	{
		DefaultRtRoiObservationsModule instance =
			new DefaultRtRoiObservationsModule();
		assertFalse(instance.addRtRoiObservation(null));
		assertTrue(instance.addRtRoiObservation(obs1));
	}

	/**
	 * Test of getRtRoiObservationList method, of class DefaultRtRoiObservationsModule.
	 */
	@Test
	public void testGetRtRoiObservationList()
	{
		DefaultRtRoiObservationsModule instance =
			new DefaultRtRoiObservationsModule();
		instance.addRtRoiObservation(obs1);
		instance.addRtRoiObservation(obs2);
		List<RtRoiObservation> list = instance.getRtRoiObservationList();
		assertNotNull(list);
		assertEquals(2, list.size());
	}

	/**
	 * Test of isStrict method, of class DefaultRtRoiObservationsModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultRtRoiObservationsModule instance =
			new DefaultRtRoiObservationsModule(false);
		assertFalse(instance.isStrict());
	}

}
