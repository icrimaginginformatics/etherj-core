/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.StringUtils;
import junit.framework.TestCase;

/**
 *
 * @author jamesd
 */
public class DefaultEnhancedGeneralEquipmentModuleTest extends TestCase
{
	private final String empty = "";
	private final String manu = "Manufacturer";
	private final String name = "ModelName";
	private final String serial = "DeviceSerial";
	private final String version = "SoftwareVersion";
	
	public DefaultEnhancedGeneralEquipmentModuleTest(String testName)
	{
		super(testName);
	}
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	/**
	 * Test of setDeviceSerialNumber method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetDeviceSerialNumberLax()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule(false);
		instance.setDeviceSerialNumber(null);
		assertTrue(!StringUtils.isNullOrEmpty(instance.getDeviceSerialNumber()));
		instance.setDeviceSerialNumber(empty);
		assertTrue(!StringUtils.isNullOrEmpty(instance.getDeviceSerialNumber()));
		instance.setDeviceSerialNumber(serial);
		assertEquals(serial, instance.getDeviceSerialNumber());
	}

	/**
	 * Test of setDeviceSerialNumber method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetDeviceSerialNumberStrict()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule();
		instance.setDeviceSerialNumber(serial);
		assertEquals(serial, instance.getDeviceSerialNumber());
	}

	/**
	 * Test of setDeviceSerialNumber method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetDeviceSerialNumberStrictBad()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule();
		try
		{
			instance.setDeviceSerialNumber(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setDeviceSerialNumber(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setManufacturer method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetManufacturerLax()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule(false);
		instance.setManufacturer(null);
		assertTrue(!StringUtils.isNullOrEmpty(instance.getManufacturer()));
		instance.setManufacturer(empty);
		assertTrue(!StringUtils.isNullOrEmpty(instance.getManufacturer()));
		instance.setManufacturer(manu);
		assertEquals(manu, instance.getManufacturer());
	}

	/**
	 * Test of setManufacturer method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetManufacturerStrict()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule();
		instance.setManufacturer(manu);
		assertEquals(manu, instance.getManufacturer());
	}

	/**
	 * Test of setManufacturer method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetManufacturerStrictBad()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule();
		try
		{
			instance.setManufacturer(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setManufacturer(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setManufacturer method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetManufacturerModelNameLax()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule(false);
		instance.setManufacturersModelName(null);
		assertTrue(!StringUtils.isNullOrEmpty(instance.getManufacturersModelName()));
		instance.setManufacturersModelName(empty);
		assertTrue(!StringUtils.isNullOrEmpty(instance.getManufacturersModelName()));
		instance.setManufacturersModelName(manu);
		assertEquals(manu, instance.getManufacturersModelName());
	}

	/**
	 * Test of setManufacturer method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetManufacturerModelNameStrict()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule();
		instance.setManufacturersModelName(manu);
		assertEquals(manu, instance.getManufacturersModelName());
	}

	/**
	 * Test of setManufacturer method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetManufacturerModelNameStrictBad()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule();
		try
		{
			instance.setManufacturersModelName(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setManufacturersModelName(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setManufacturer method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetSoftwareVersionLax()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule(false);
		instance.setSoftwareVersion(null);
		assertTrue(!StringUtils.isNullOrEmpty(instance.getSoftwareVersion()));
		instance.setSoftwareVersion(empty);
		assertTrue(!StringUtils.isNullOrEmpty(instance.getSoftwareVersion()));
		instance.setSoftwareVersion(manu);
		assertEquals(manu, instance.getSoftwareVersion());
	}

	/**
	 * Test of setManufacturer method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetSoftwareVersionStrict()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule();
		instance.setSoftwareVersion(manu);
		assertEquals(manu, instance.getSoftwareVersion());
	}

	/**
	 * Test of setManufacturer method, of class DefaultEnhancedGeneralEquipmentModule.
	 */
	public void testSetSoftwareVersionStrictBad()
	{
		DefaultEnhancedGeneralEquipmentModule instance =
			new DefaultEnhancedGeneralEquipmentModule();
		try
		{
			instance.setSoftwareVersion(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setSoftwareVersion(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

}
