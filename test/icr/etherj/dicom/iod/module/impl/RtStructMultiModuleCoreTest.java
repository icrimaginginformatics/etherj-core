/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.Uids;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author roban
 */
public class RtStructMultiModuleCoreTest
{
	
	public RtStructMultiModuleCoreTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of isStrict method, of class RtStructMultiModuleCore.
	 */
	@Test
	public void testIsStrict()
	{
		RtStructMultiModuleCore instance = new RtStructMultiModuleCore(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of setInstanceNumber method, of class RtStructMultiModuleCore.
	 */
	@Test
	public void testSetInstanceNumber()
	{
		RtStructMultiModuleCore instance = new RtStructMultiModuleCore();
		instance.setInstanceNumber(10);
		assertEquals(10, instance.getInstanceNumber());
		instance.setInstanceNumber(-1);
		assertEquals(-1, instance.getInstanceNumber());
	}

	/**
	 * Test of setSopInstanceUid method, of class RtStructMultiModuleCore.
	 */
	@Test
	public void testSetSopInstanceUid()
	{
		RtStructMultiModuleCore instance = new RtStructMultiModuleCore();
		String uid = Uids.generateDicomUid();
		instance.setSopInstanceUid(uid);
		assertEquals(uid, instance.getSopInstanceUid());
	}
	
	/**
	 * Test of setSopInstanceUid method, of class RtStructMultiModuleCore.
	 */
	@Test
	public void testSetSopInstanceUidBad()
	{
		RtStructMultiModuleCore instance = new RtStructMultiModuleCore();
		try
		{
			instance.setSopInstanceUid(null);
			fail("Expected IlleaglArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setSopInstanceUid("");
			fail("Expected IlleaglArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}
	
}
