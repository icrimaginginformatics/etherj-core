/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.dicom.iod.PerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SharedFunctionalGroups;
import icr.etherj.dicom.iod.impl.DefaultSegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.impl.DefaultSegmentationSharedFunctionalGroups;
import junit.framework.TestCase;
import org.dcm4che2.data.UID;

/**
 *
 * @author jamesd
 */
public class DefaultMultiframeFunctionalGroupsModuleTest extends TestCase
{
	private final String bad = "Bad";
	private final String date = "20200306";
	private final String time = "165003";
	
	private PerFrameFunctionalGroups perFrameGroups;
	private SharedFunctionalGroups sharedGroups;
	private final String sopClassUid = UID.SegmentationStorage;
	
	public DefaultMultiframeFunctionalGroupsModuleTest(String testName)
	{
		super(testName);
	}
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
		perFrameGroups = new DefaultSegmentationPerFrameFunctionalGroups();
		sharedGroups = new DefaultSegmentationSharedFunctionalGroups();
	}
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	/**
	 * Test of getSopClassUid method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testGetSopClassUid()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups);
		String result = instance.getSopClassUid();
		assertEquals(sopClassUid, result);
	}

	/**
	 * Test of isStrict method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testIsStrict()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups, false);
		boolean result = instance.isStrict();
		assertEquals(false, result);
	}

	/**
	 * Test of setContentDate method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetContentDateLax()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups, false);
		instance.setContentDate(date);
		instance.setContentDate(bad);
	}

	/**
	 * Test of setContentDate method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetContentDateStrict()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups);
		instance.setContentDate(date);
	}

	/**
	 * Test of setContentDate method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetContentDateStrictBad()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups);
		try
		{
			instance.setContentDate(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setContentTime method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetContentTimeLax()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups, false);
		instance.setContentTime(time);
		instance.setContentTime(bad);
	}

	/**
	 * Test of setContentTime method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetContentTimeStrict()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups);
		instance.setContentTime(time);
	}

	/**
	 * Test of setContentTime method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetContentTimeStrictBad()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups);
		try
		{
			instance.setContentTime(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setInstanceNumber method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetInstanceNumberLax()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups, false);
		instance.setInstanceNumber(3);
		instance.setInstanceNumber(-1);
	}

	/**
	 * Test of setInstanceNumber method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetInstanceNumberStrict()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups);
		instance.setInstanceNumber(3);
	}

	/**
	 * Test of setInstanceNumber method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetInstanceNumberStrictBad()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups);
		try
		{
			instance.setInstanceNumber(-1);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setNumberOfFrames method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetNumberOfFramesLax()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups, false);
		instance.setNumberOfFrames(10);
		instance.setNumberOfFrames(-1);
	}
	
	/**
	 * Test of setNumberOfFrames method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetNumberOfFramesStrict()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups);
		instance.setNumberOfFrames(10);
	}
	
	/**
	 * Test of setNumberOfFrames method, of class DefaultMultiframeFunctionalGroupsModule.
	 */
	public void testSetNumberOfFramesStrictBad()
	{
		DefaultMultiframeFunctionalGroupsModule instance =
			new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
				sharedGroups, perFrameGroups);
		try
		{
			instance.setNumberOfFrames(-1);
			fail("Expected IllegaArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}
	
}
