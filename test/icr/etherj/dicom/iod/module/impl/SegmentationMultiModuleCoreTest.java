/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.Uids;
import icr.etherj.dicom.iod.Constants;
import junit.framework.TestCase;
import org.dcm4che2.data.UID;

/**
 *
 * @author jamesd
 */
public class SegmentationMultiModuleCoreTest extends TestCase
{
	private final String bad = "Bad";
	private final String date = "20200306";
	private final String time = "165003";
	
	public SegmentationMultiModuleCoreTest(String testName)
	{
		super(testName);
	}
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	/**
	 * Test of getSopClassUid method, of class SegmentationMultiModuleCore.
	 */
	public void testGetSopClassUid()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		String result = instance.getSopClassUid();
		assertEquals(UID.SegmentationStorage, result);
	}

	/**
	 * Test of isStrict method, of class SegmentationMultiModuleCore.
	 */
	public void testIsStrict()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of setBitsAllocated method, of class SegmentationMultiModuleCore.
	 */
	public void testSetBitsAllocatedLax()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setBitsAllocated(1);
		instance.setBitsAllocated(8);
		instance.setBitsAllocated(2);
	}

	/**
	 * Test of setBitsAllocated method, of class SegmentationMultiModuleCore.
	 */
	public void testSetBitsAllocatedStrict()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		instance.setBitsAllocated(1);
		instance.setBitsAllocated(8);
	}

	/**
	 * Test of setBitsAllocated method, of class SegmentationMultiModuleCore.
	 */
	public void testSetBitsAllocatedStrictBad()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		try
		{
			instance.setBitsAllocated(2);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setBitsStored method, of class SegmentationMultiModuleCore.
	 */
	public void testSetBitsStoredLax()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setBitsStored(1);
		instance.setBitsStored(8);
		instance.setBitsStored(2);
	}

	/**
	 * Test of setBitsStored method, of class SegmentationMultiModuleCore.
	 */
	public void testSetBitsStoredStrict()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		instance.setBitsStored(1);
		instance.setBitsStored(8);
	}

	/**
	 * Test of setBitsStored method, of class SegmentationMultiModuleCore.
	 */
	public void testSetBitsStoredStrictBad()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		try
		{
			instance.setBitsStored(2);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setContentDate method, of class SegmentationMultiModuleCore.
	 */
	public void testSetContentDateLax()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setContentDate(date);
		instance.setContentDate(bad);
	}

	/**
	 * Test of setContentDate method, of class SegmentationMultiModuleCore.
	 */
	public void testSetContentDateStrict()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		instance.setContentDate(date);
	}

	/**
	 * Test of setContentDate method, of class SegmentationMultiModuleCore.
	 */
	public void testSetContentDateStrictBad()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		try
		{
			instance.setContentDate(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setContentTime method, of class SegmentationMultiModuleCore.
	 */
	public void testSetContentTimeLax()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setContentTime(time);
		instance.setContentTime(bad);
	}

	/**
	 * Test of setContentTime method, of class SegmentationMultiModuleCore.
	 */
	public void testSetContentTimeStrict()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		instance.setContentTime(time);
	}

	/**
	 * Test of setContentTime method, of class SegmentationMultiModuleCore.
	 */
	public void testSetContentTimeStrictBad()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		try
		{
			instance.setContentTime(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setFrameOfReferenceUid method, of class SegmentationMultiModuleCore.
	 */
	public void testSetFrameOfReferenceUidLax()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setFrameOfReferenceUid(null);
		instance.setFrameOfReferenceUid(Uids.generateDicomUid());
		instance.setFrameOfReferenceUid("");
	}

	/**
	 * Test of setFrameOfReferenceUid method, of class SegmentationMultiModuleCore.
	 */
	public void testSetFrameOfReferenceUidStrict()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		instance.setFrameOfReferenceUid(null);
		instance.setFrameOfReferenceUid(Uids.generateDicomUid());
	}

	/**
	 * Test of setFrameOfReferenceUid method, of class SegmentationMultiModuleCore.
	 */
	public void testSetFrameOfReferenceUidStrictBad()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		try
		{
			instance.setFrameOfReferenceUid("");
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setHighBit method, of class SegmentationMultiModuleCore.
	 */
	public void testSetHighBitLax()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setHighBit(0);
		instance.setHighBit(7);
		instance.setHighBit(1);
	}

	/**
	 * Test of setHighBit method, of class SegmentationMultiModuleCore.
	 */
	public void testSetHighBitStrict()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		instance.setHighBit(0);
		instance.setHighBit(7);
	}

	/**
	 * Test of setHighBit method, of class SegmentationMultiModuleCore.
	 */
	public void testSetHighBitStrictBad()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		try
		{
			instance.setHighBit(1);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setInstanceNumber method, of class SegmentationMultiModuleCore.
	 */
	public void testSetInstanceNumberLax()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setInstanceNumber(1);
		instance.setInstanceNumber(0);
	}

	/**
	 * Test of setInstanceNumber method, of class SegmentationMultiModuleCore.
	 */
	public void testSetInstanceNumberStrict()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		instance.setInstanceNumber(1);
	}

	/**
	 * Test of setInstanceNumber method, of class SegmentationMultiModuleCore.
	 */
	public void testSetInstanceNumberStrictBad()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		try
		{
			instance.setInstanceNumber(0);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setPhotometricInterpretation method, of class SegmentationMultiModuleCore.
	 */
	public void testSetPhotometricInterpretationLax()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setPhotometricInterpretation(Constants.Monochrome2);
		instance.setPhotometricInterpretation(bad);
	}

	/**
	 * Test of setPhotometricInterpretation method, of class SegmentationMultiModuleCore.
	 */
	public void testSetPhotometricInterpretationStrict()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		instance.setPhotometricInterpretation(Constants.Monochrome2);
	}

	/**
	 * Test of setPhotometricInterpretation method, of class SegmentationMultiModuleCore.
	 */
	public void testSetPhotometricInterpretationStrictBad()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		try
		{
			instance.setPhotometricInterpretation(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setPixelRepresentation method, of class SegmentationMultiModuleCore.
	 */
	public void testSetPixelRepresentationLax()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setPixelRepresentation(0);
		instance.setPixelRepresentation(1);
	}

	/**
	 * Test of setPixelRepresentation method, of class SegmentationMultiModuleCore.
	 */
	public void testSetPixelRepresentationStrict()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		instance.setPixelRepresentation(0);
	}

	/**
	 * Test of setPixelRepresentation method, of class SegmentationMultiModuleCore.
	 */
	public void testSetPixelRepresentationStrictBad()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		try
		{
			instance.setPixelRepresentation(1);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setSamplesPerPixel method, of class SegmentationMultiModuleCore.
	 */
	public void testSetSamplesPerPixelLax()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setSamplesPerPixel(1);
		instance.setSamplesPerPixel(2);
	}

	/**
	 * Test of setSamplesPerPixel method, of class SegmentationMultiModuleCore.
	 */
	public void testSetSamplesPerPixelStrict()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		instance.setSamplesPerPixel(1);
	}

	/**
	 * Test of setSamplesPerPixel method, of class SegmentationMultiModuleCore.
	 */
	public void testSetSamplesPerPixelStrictBad()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		try
		{
			instance.setSamplesPerPixel(2);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setSopInstanceUid method, of class SegmentationMultiModuleCore.
	 */
	public void testSetSopInstanceUidLax()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setSopInstanceUid(Uids.generateDicomUid());
		try
		{
			instance.setSopInstanceUid(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setSopInstanceUid("");
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}
	
	/**
	 * Test of setSopInstanceUid method, of class SegmentationMultiModuleCore.
	 */
	public void testSetSopInstanceUidLaxBad()
	{
		SegmentationMultiModuleCore instance =
			new SegmentationMultiModuleCore(false);
		instance.setSopInstanceUid(Uids.generateDicomUid());
	}
	
	/**
	 * Test of setSopInstanceUid method, of class SegmentationMultiModuleCore.
	 */
	public void testSetSopInstanceUidStrict()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		instance.setSopInstanceUid(Uids.generateDicomUid());
	}
	
	/**
	 * Test of setSopInstanceUid method, of class SegmentationMultiModuleCore.
	 */
	public void testSetSopInstanceUidStrictBad()
	{
		SegmentationMultiModuleCore instance = new SegmentationMultiModuleCore();
		try
		{
			instance.setSopInstanceUid(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setSopInstanceUid("");
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}
	
}
