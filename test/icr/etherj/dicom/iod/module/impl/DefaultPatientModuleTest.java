/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.dicom.iod.Constants;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author roban
 */
public class DefaultPatientModuleTest
{
	private final String bad = "bad";
	private final String date = "20200320";
	private final String empty = "";
	private final String value = "value";
	
	public DefaultPatientModuleTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of isStrict method, of class DefaultPatientModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultPatientModule instance = new DefaultPatientModule(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of setPatientBirthDate method, of class DefaultPatientModule.
	 */
	@Test
	public void testSetPatientBirthDateLax()
	{
		DefaultPatientModule instance = new DefaultPatientModule(false);
		instance.setPatientBirthDate(date);
		assertEquals(date, instance.getPatientBirthDate());
		instance.setPatientBirthDate(empty);
		assertEquals(empty, instance.getPatientBirthDate());
		instance.setPatientBirthDate(null);
		assertEquals(empty, instance.getPatientBirthDate());
		instance.setPatientBirthDate(bad);
		assertEquals(bad, instance.getPatientBirthDate());
	}

	/**
	 * Test of setPatientBirthDate method, of class DefaultPatientModule.
	 */
	@Test
	public void testSetPatientBirthDateStrict()
	{
		DefaultPatientModule instance = new DefaultPatientModule();
		instance.setPatientBirthDate(date);
		assertEquals(date, instance.getPatientBirthDate());
		instance.setPatientBirthDate(empty);
		assertEquals(empty, instance.getPatientBirthDate());
		instance.setPatientBirthDate(null);
		assertEquals(empty, instance.getPatientBirthDate());
	}

	/**
	 * Test of setPatientBirthDate method, of class DefaultPatientModule.
	 */
	@Test
	public void testSetPatientBirthDateStrictBad()
	{
		DefaultPatientModule instance = new DefaultPatientModule();
		try
		{
			instance.setPatientBirthDate(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setPatientId method, of class DefaultPatientModule.
	 */
	@Test
	public void testSetPatientId()
	{
		DefaultPatientModule instance = new DefaultPatientModule();
		instance.setPatientId(value);
		assertEquals(value, instance.getPatientId());
		instance.setPatientId(empty);
		assertEquals(empty, instance.getPatientId());
		instance.setPatientId(null);
		assertEquals(empty, instance.getPatientId());
	}

	/**
	 * Test of setPatientName method, of class DefaultPatientModule.
	 */
	@Test
	public void testSetPatientName()
	{
		DefaultPatientModule instance = new DefaultPatientModule();
		instance.setPatientName(value);
		assertEquals(value, instance.getPatientName());
		instance.setPatientName(empty);
		assertEquals(empty, instance.getPatientName());
		instance.setPatientName(null);
		assertEquals(empty, instance.getPatientName());
	}

	/**
	 * Test of setPatientSex method, of class DefaultPatientModule.
	 */
	@Test
	public void testSetPatientSexLax()
	{
		DefaultPatientModule instance = new DefaultPatientModule(false);
		instance.setPatientSex(Constants.Male);
		assertEquals(Constants.Male, instance.getPatientSex());
		instance.setPatientSex(Constants.Female);
		assertEquals(Constants.Female, instance.getPatientSex());
		instance.setPatientSex(Constants.Other);
		assertEquals(Constants.Other, instance.getPatientSex());
		instance.setPatientSex(null);
		assertEquals(empty, instance.getPatientSex());
		instance.setPatientSex(empty);
		assertEquals(empty, instance.getPatientSex());
		instance.setPatientSex(bad);
		assertEquals(bad, instance.getPatientSex());
	}

	/**
	 * Test of setPatientSex method, of class DefaultPatientModule.
	 */
	@Test
	public void testSetPatientSexStrict()
	{
		DefaultPatientModule instance = new DefaultPatientModule();
		instance.setPatientSex(Constants.Male);
		assertEquals(Constants.Male, instance.getPatientSex());
		instance.setPatientSex(Constants.Female);
		assertEquals(Constants.Female, instance.getPatientSex());
		instance.setPatientSex(Constants.Other);
		assertEquals(Constants.Other, instance.getPatientSex());
		instance.setPatientSex(null);
		assertEquals(empty, instance.getPatientSex());
		instance.setPatientSex(empty);
		assertEquals(empty, instance.getPatientSex());
		instance.setPatientSex(bad);
		assertEquals(empty, instance.getPatientSex());
	}

}
