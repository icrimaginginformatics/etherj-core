/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.Uids;
import icr.etherj.dicom.iod.Modality;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class DefaultRtSeriesModuleTest
{
	private final String bad = "bad";
	private final String date = "20200324";
	private final String empty = "";
	private final String time = "154259";
	private final String value = "value";

	public DefaultRtSeriesModuleTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of isStrict method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of setModality method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetModality()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule();
		instance.setModality(Modality.RTDOSE);
		assertEquals(Modality.RTDOSE, instance.getModality());
		instance.setModality(Modality.RTIMAGE);
		instance.setModality(Modality.RTPLAN);
		instance.setModality(Modality.RTRECORD);
		instance.setModality(Modality.RTSTRUCT);
	}

	/**
	 * Test of setModality method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetModalityBad()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule();
		try
		{
			instance.setModality(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setModality("");
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setModality("bad");
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setOperatorsName method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetOperatorsName()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule();
		instance.setOperatorsName(value);
		assertEquals(value, instance.getOperatorsName());
		instance.setOperatorsName(null);
		assertEquals(empty, instance.getOperatorsName());
		instance.setOperatorsName(empty);
		assertEquals(empty, instance.getOperatorsName());
	}

	/**
	 * Test of setSeriesDate method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetSeriesDateLax()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule(false);
		instance.setSeriesDate(date);
		assertEquals(date, instance.getSeriesDate());
		instance.setSeriesDate(null);
		assertNull(instance.getSeriesDate());
		instance.setSeriesDate(empty);
		assertEquals(empty, instance.getSeriesDate());
		instance.setSeriesDate(bad);
		assertEquals(bad, instance.getSeriesDate());
	}

	/**
	 * Test of setSeriesDate method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetSeriesDateStrict()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule();
		instance.setSeriesDate(date);
		assertEquals(date, instance.getSeriesDate());
		instance.setSeriesDate(null);
		assertNull(instance.getSeriesDate());
		instance.setSeriesDate(empty);
		assertEquals(empty, instance.getSeriesDate());
	}

	/**
	 * Test of setSeriesDate method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetSeriesDateStrictBad()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule();
		try
		{
			instance.setSeriesDate(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setSeriesDescription method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetSeriesDescription()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule();
		instance.setSeriesDescription(value);
		assertEquals(value, instance.getSeriesDescription());
		instance.setSeriesDescription(null);
		assertNull(instance.getSeriesDescription());
		instance.setSeriesDescription(empty);
		assertEquals(empty, instance.getSeriesDescription());
	}

	/**
	 * Test of setSeriesInstanceUid method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetSeriesInstanceUid()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule();
		String uid = Uids.generateDicomUid();
		instance.setSeriesInstanceUid(uid);
		assertEquals(uid, instance.getSeriesInstanceUid());
	}

	/**
	 * Test of setSeriesInstanceUid method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetSeriesInstanceUidBad()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule();
		try
		{
			instance.setSeriesInstanceUid(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setSeriesInstanceUid(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setSeriesNumber method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetSeriesNumber()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule();
		instance.setSeriesNumber(10);
		assertEquals(10, instance.getSeriesNumber());
		instance.setSeriesNumber(-10);
		assertEquals(-1, instance.getSeriesNumber());
	}

	/**
	 * Test of setSeriesTime method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetSeriesTimeLax()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule(false);
		instance.setSeriesTime(time);
		assertEquals(time, instance.getSeriesTime());
		instance.setSeriesTime(null);
		assertNull(instance.getSeriesTime());
		instance.setSeriesTime(empty);
		assertEquals(empty, instance.getSeriesTime());
		instance.setSeriesTime(bad);
		assertEquals(bad, instance.getSeriesTime());
	}

	/**
	 * Test of setSeriesTime method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetSeriesTimeStrict()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule();
		instance.setSeriesTime(time);
		assertEquals(time, instance.getSeriesTime());
		instance.setSeriesTime(null);
		assertNull(instance.getSeriesTime());
		instance.setSeriesTime(empty);
		assertEquals(empty, instance.getSeriesTime());
	}

	/**
	 * Test of setSeriesTime method, of class DefaultRtSeriesModule.
	 */
	@Test
	public void testSetSeriesTimeStrictBad()
	{
		DefaultRtSeriesModule instance = new DefaultRtSeriesModule();
		try
		{
			instance.setSeriesTime(bad);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

}
