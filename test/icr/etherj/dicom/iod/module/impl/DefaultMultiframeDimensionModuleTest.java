/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.StringUtils;
import icr.etherj.Uids;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.DimensionIndex;
import icr.etherj.dicom.iod.impl.DefaultDimensionIndex;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class DefaultMultiframeDimensionModuleTest
{
	private final String empty = "";
	private final DimensionIndex index1 = new DefaultDimensionIndex();
	private final DimensionIndex index2 = new DefaultDimensionIndex();
	private final String uid1 = Uids.generateDicomUid();
	private final String uid2 = Uids.generateDicomUid();

	public DefaultMultiframeDimensionModuleTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of addDimensionIndex method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testAddDimensionIndex()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule();
		assertFalse(instance.addDimensionIndex(null));
		assertTrue(instance.addDimensionIndex(index1));
		List<DimensionIndex> list = instance.getDimensionIndexList();
		assertEquals(1, list.size());
		assertTrue(list.contains(index1));
	}

	/**
	 * Test of addDimensionOrganisationUid method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testAddDimensionOrganisationUidLax()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule(false);
		assertTrue(instance.addDimensionOrganisationUid(uid1));
		assertTrue(instance.addDimensionOrganisationUid(empty));
		assertTrue(instance.addDimensionOrganisationUid(null));
		List<String> list = instance.getDimensionOrganisationUidList();
		assertEquals(3, list.size());
		assertTrue(list.contains(uid1));
		list.forEach((uid) -> assertFalse(StringUtils.isNullOrEmpty(uid)));
	}

	/**
	 * Test of addDimensionOrganisationUid method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testAddDimensionOrganisationUidStrict()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule();
		assertTrue(instance.addDimensionOrganisationUid(uid1));
		List<String> list = instance.getDimensionOrganisationUidList();
		assertEquals(1, list.size());
		assertTrue(list.contains(uid1));
	}

	/**
	 * Test of addDimensionOrganisationUid method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testAddDimensionOrganisationUidStrictBad()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule();
		try
		{
			instance.addDimensionOrganisationUid(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.addDimensionOrganisationUid(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of getDimensionIndexList method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testGetDimensionIndexList()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule();
		List<DimensionIndex> list = instance.getDimensionIndexList();
		assertNotNull(list);
		assertTrue(list.isEmpty());
	}

	/**
	 * Test of getDimensionOrganisationUidList method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testGetDimensionOrganisationUidList()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule();
		List<String> list = instance.getDimensionOrganisationUidList();
		assertNotNull(list);
		assertTrue(list.isEmpty());
	}

	/**
	 * Test of isStrict method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule(false);
		assertFalse(instance.isStrict());
		
	}

	/**
	 * Test of removeDimensionIndex method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testRemoveDimensionIndex()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule();
		instance.addDimensionIndex(index1);
		instance.addDimensionIndex(index2);
		assertTrue(instance.removeDimensionIndex(index2));
		assertFalse(instance.removeDimensionIndex(index2));
		List<DimensionIndex> list = instance.getDimensionIndexList();
		assertFalse(list.contains(index2));
	}

	/**
	 * Test of removeDimensionOrganisationUid method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testRemoveDimensionOrganisationUid()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule();
		instance.addDimensionOrganisationUid(uid1);
		instance.addDimensionOrganisationUid(uid2);
		assertTrue(instance.removeDimensionOrganisationUid(uid2));
		assertFalse(instance.removeDimensionOrganisationUid(uid2));
		List<String> list = instance.getDimensionOrganisationUidList();
		assertFalse(list.contains(uid2));
	}

	/**
	 * Test of setDimensionOrganisationType method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testSetDimensionOrganisationTypeLax()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule(false);
		instance.setDimensionOrganisationType(null);
		assertNull(instance.getDimensionOrganisationType());
		instance.setDimensionOrganisationType(Constants.ThreeD);
		assertEquals(Constants.ThreeD, instance.getDimensionOrganisationType());
		instance.setDimensionOrganisationType(Constants.ThreeDTemporal);
		assertEquals(Constants.ThreeDTemporal,
			instance.getDimensionOrganisationType());
		instance.setDimensionOrganisationType(Constants.TiledFull);
		assertEquals(Constants.TiledFull,
			instance.getDimensionOrganisationType());
		instance.setDimensionOrganisationType(Constants.TiledSparse);
		assertEquals(Constants.TiledSparse,
			instance.getDimensionOrganisationType());
		String bad = "bad";
		instance.setDimensionOrganisationType(bad);
		assertEquals(bad, instance.getDimensionOrganisationType());
	}
	
	/**
	 * Test of setDimensionOrganisationType method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testSetDimensionOrganisationTypeStrict()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule();
		instance.setDimensionOrganisationType(null);
		assertNull(instance.getDimensionOrganisationType());
		instance.setDimensionOrganisationType(Constants.ThreeD);
		assertEquals(Constants.ThreeD, instance.getDimensionOrganisationType());
		instance.setDimensionOrganisationType(Constants.ThreeDTemporal);
		assertEquals(Constants.ThreeDTemporal,
			instance.getDimensionOrganisationType());
		instance.setDimensionOrganisationType(Constants.TiledFull);
		assertEquals(Constants.TiledFull,
			instance.getDimensionOrganisationType());
		instance.setDimensionOrganisationType(Constants.TiledSparse);
		assertEquals(Constants.TiledSparse,
			instance.getDimensionOrganisationType());
	}
	
	/**
	 * Test of setDimensionOrganisationType method, of class DefaultMultiframeDimensionModule.
	 */
	@Test
	public void testSetDimensionOrganisationTypeStrictBad()
	{
		DefaultMultiframeDimensionModule instance =
			new DefaultMultiframeDimensionModule();
		try
		{
			instance.setDimensionOrganisationType("bad");
			fail("Expected IllegalArgumentExcpetion not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}
	
}
