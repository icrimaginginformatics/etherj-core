/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.Uids;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.ReferencedImage;
import icr.etherj.dicom.iod.SourceImage;
import icr.etherj.dicom.iod.impl.DefaultCode;
import icr.etherj.dicom.iod.impl.DefaultSourceImage;
import java.util.List;
import org.dcm4che2.data.UID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jamesd
 */
public class DefaultGeneralReferenceModuleTest
{
	private final Code code1 = new DefaultCode();
	private final Code code2 = new DefaultCode();
	private final SourceImage image1 = new DefaultSourceImage(false);
	private final SourceImage image2 = new DefaultSourceImage(false);
	
	public DefaultGeneralReferenceModuleTest()
	{
	}
	
	@BeforeClass
	public static void setUpClass()
	{
	}
	
	@AfterClass
	public static void tearDownClass()
	{
	}
	
	@Before
	public void setUp()
	{
		image1.setReferencedSopClassUid(UID.MRImageStorage);
		image1.setReferencedSopInstanceUid(Uids.generateDicomUid());
		image2.setReferencedSopClassUid(UID.MRImageStorage);
		image2.setReferencedSopInstanceUid(Uids.generateDicomUid());
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of addDerivationCode method, of class DefaultGeneralReferenceModule.
	 */
	@Test
	public void testAddDerivationCode()
	{
		DefaultGeneralReferenceModule instance =
			new DefaultGeneralReferenceModule();
		assertFalse(instance.addDerivationCode(null));
		assertTrue(instance.addDerivationCode(code1));
	}

	/**
	 * Test of addReferencedImage method, of class DefaultGeneralReferenceModule.
	 */
	@Test
	public void testAddReferencedImage()
	{
		DefaultGeneralReferenceModule instance =
			new DefaultGeneralReferenceModule();
		assertFalse(instance.addReferencedImage(null));
		assertTrue(instance.addReferencedImage(image1));
	}

	/**
	 * Test of addSourceImage method, of class DefaultGeneralReferenceModule.
	 */
	@Test
	public void testAddSourceImage()
	{
		DefaultGeneralReferenceModule instance =
			new DefaultGeneralReferenceModule();
		assertFalse(instance.addSourceImage(null));
		assertTrue(instance.addSourceImage(image1));
	}

	/**
	 * Test of getDerivationCodeList method, of class DefaultGeneralReferenceModule.
	 */
	@Test
	public void testGetDerivationCodeList()
	{
		DefaultGeneralReferenceModule instance =
			new DefaultGeneralReferenceModule();
		List<Code> list = instance.getDerivationCodeList();
		assertNotNull(list);
		assertTrue(list.isEmpty());
		instance.addDerivationCode(code1);
		list = instance.getDerivationCodeList();
		assertTrue(list.contains(code1));
	}

	/**
	 * Test of getReferencedImageList method, of class DefaultGeneralReferenceModule.
	 */
	@Test
	public void testGetReferencedImageList()
	{
		DefaultGeneralReferenceModule instance =
			new DefaultGeneralReferenceModule();
		List<ReferencedImage> list = instance.getReferencedImageList();
		assertNotNull(list);
		assertTrue(list.isEmpty());
		instance.addReferencedImage(image1);
		instance.addReferencedImage(image1);
		list = instance.getReferencedImageList();
		assertTrue(list.contains(image1));
		assertEquals(1, list.size());
	}

	/**
	 * Test of getSourceImageList method, of class DefaultGeneralReferenceModule.
	 */
	@Test
	public void testGetSourceImageList()
	{
		DefaultGeneralReferenceModule instance =
			new DefaultGeneralReferenceModule();
		List<SourceImage> list = instance.getSourceImageList();
		assertNotNull(list);
		assertTrue(list.isEmpty());
		instance.addSourceImage(image1);
		instance.addSourceImage(image1);
		list = instance.getSourceImageList();
		assertTrue(list.contains(image1));
		assertEquals(1, list.size());
	}

	/**
	 * Test of isStrict method, of class DefaultGeneralReferenceModule.
	 */
	@Test
	public void testIsStrict()
	{
		DefaultGeneralReferenceModule instance =
			new DefaultGeneralReferenceModule(false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of removeDerivationCode method, of class DefaultGeneralReferenceModule.
	 */
	@Test
	public void testRemoveDerivationCode()
	{
		DefaultGeneralReferenceModule instance =
			new DefaultGeneralReferenceModule();
		instance.addDerivationCode(code1);
		instance.addDerivationCode(code2);
		assertTrue(instance.removeDerivationCode(code1));
		assertFalse(instance.removeDerivationCode(code1));
		List<Code> list = instance.getDerivationCodeList();
		assertFalse(list.contains(code1));
		assertTrue(list.contains(code2));
	}

	/**
	 * Test of removeReferencedImage method, of class DefaultGeneralReferenceModule.
	 */
	@Test
	public void testRemoveReferencedImage()
	{
		DefaultGeneralReferenceModule instance =
			new DefaultGeneralReferenceModule();
		instance.addReferencedImage(image1);
		instance.addReferencedImage(image2);
		assertTrue(instance.removeReferencedImage(image1));
		assertFalse(instance.removeReferencedImage(image1));
		List<ReferencedImage> list = instance.getReferencedImageList();
		assertFalse(list.contains(image1));
		assertTrue(list.contains(image2));
	}

	/**
	 * Test of removeSourceImage method, of class DefaultGeneralReferenceModule.
	 */
	@Test
	public void testRemoveSourceImage()
	{
		DefaultGeneralReferenceModule instance =
			new DefaultGeneralReferenceModule();
		instance.addSourceImage(image1);
		instance.addSourceImage(image2);
		assertTrue(instance.removeSourceImage(image1));
		assertFalse(instance.removeSourceImage(image1));
		List<SourceImage> list = instance.getSourceImageList();
		assertFalse(list.contains(image1));
		assertTrue(list.contains(image2));
	}

}
