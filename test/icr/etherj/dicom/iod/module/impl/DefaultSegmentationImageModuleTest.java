/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.Segment;
import icr.etherj.dicom.iod.impl.DefaultSegment;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author jamesd
 */
public class DefaultSegmentationImageModuleTest extends TestCase
{
	private final String empty = "";
	private final SegmentationMultiModuleCore segMultiModCore =
		new SegmentationMultiModuleCore();
	private final String test = "Test";

	public DefaultSegmentationImageModuleTest(String testName)
	{
		super(testName);
	}
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}

	/**
	 * Test of addSegment method, of class DefaultSegmentationImageModule.
	 */
	public void testAddSegment()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		assertFalse(instance.addSegment(null));
		assertTrue(instance.addSegment(new DefaultSegment()));
		assertEquals(1, instance.getSegmentList().size());
	}

	/**
	 * Test of getMaximumFractionalValue method, of class DefaultSegmentationImageModule.
	 */
	public void testGetMaximumFractionalValue()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		int maxFrac = 240;
		instance.setSegmentationType(Constants.Fractional);
		instance.setMaximumFractionalValue(maxFrac);
		int result = instance.getMaximumFractionalValue();
		assertEquals(maxFrac, result);
		instance.setSegmentationType(Constants.Binary);
		result = instance.getMaximumFractionalValue();
		assertEquals(-1, result);
	}

	/**
	 * Test of getSegmentList method, of class DefaultSegmentationImageModule.
	 */
	public void testGetSegmentList()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		List<Segment> result = instance.getSegmentList();
		assertTrue(result.isEmpty());
		Segment segment01 = new DefaultSegment();
		Segment segment02 = new DefaultSegment();
		instance.addSegment(segment01);
		instance.addSegment(segment02);
		result = instance.getSegmentList();
		assertEquals(2, result.size());
		assertTrue(result.contains(segment01));
		assertTrue(result.contains(segment02));
	}

	/**
	 * Test of getSegmentationFractionalType method, of class DefaultSegmentationImageModule.
	 */
	public void testGetSegmentationFractionalType()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		instance.setSegmentationType(Constants.Binary);
		assertNull(instance.getSegmentationFractionalType());
		instance.setSegmentationType(Constants.Fractional);
		instance.setSegmentationFractionalType(Constants.Occupancy);
		assertEquals(Constants.Occupancy,
			instance.getSegmentationFractionalType());
		instance.setSegmentationFractionalType(Constants.Probability);
		assertEquals(Constants.Probability,
			instance.getSegmentationFractionalType());
	}

	/**
	 * Test of isStrict method, of class DefaultSegmentationImageModule.
	 */
	public void testIsStrict()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore, false);
		assertFalse(instance.isStrict());
	}

	/**
	 * Test of removeSegment method, of class DefaultSegmentationImageModule.
	 */
	public void testRemoveSegment()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		List<Segment> result = instance.getSegmentList();
		assertTrue(result.isEmpty());
		Segment segment01 = new DefaultSegment();
		Segment segment02 = new DefaultSegment();
		Segment segment03 = new DefaultSegment();
		instance.addSegment(segment01);
		instance.addSegment(segment02);
		instance.addSegment(segment03);
		assertTrue(instance.removeSegment(segment02));
		result = instance.getSegmentList();
		assertEquals(2, result.size());
		assertTrue(result.contains(segment01));
		assertFalse(result.contains(segment02));
		assertTrue(result.contains(segment03));
	}

	/**
	 * Test of setContentCreatorsName method, of class DefaultSegmentationImageModule.
	 */
	public void testSetContentCreatorsName()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		instance.setContentCreatorsName(null);
		assertEquals("", instance.getContentCreatorsName());
		instance.setContentCreatorsName(test);
		assertEquals(test, instance.getContentCreatorsName());
	}

	/**
	 * Test of setContentDescription method, of class DefaultSegmentationImageModule.
	 */
	public void testSetContentDescription()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		instance.setContentCreatorsName(null);
		assertEquals("", instance.getContentCreatorsName());
		instance.setContentCreatorsName(test);
		assertEquals(test, instance.getContentCreatorsName());
	}

	/**
	 * Test of setContentLabel method, of class DefaultSegmentationImageModule.
	 */
	public void testSetContentLabelLax()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore, false);
		instance.setContentLabel(test);
		instance.setContentLabel(null);
		instance.setContentLabel(empty);
	}

	/**
	 * Test of setContentLabel method, of class DefaultSegmentationImageModule.
	 */
	public void testSetContenLabelStrict()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		instance.setContentLabel(test);
		assertEquals(test, instance.getContentLabel());
	}

	/**
	 * Test of setContentLabel method, of class DefaultSegmentationImageModule.
	 */
	public void testSetContentLabelStrictBad()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		try
		{
			instance.setContentLabel(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setContentLabel(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setMaximumFractionalValue method, of class DefaultSegmentationImageModule.
	 */
	public void testSetMaximumFractionalValueLax()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore, false);
		instance.setSegmentationType(Constants.Fractional);
		int value = 240;
		instance.setMaximumFractionalValue(value);
		assertEquals(value, instance.getMaximumFractionalValue());
		value = -1;
		instance.setMaximumFractionalValue(value);
		assertEquals(value, instance.getMaximumFractionalValue());
		value = 256;
		instance.setMaximumFractionalValue(value);
		assertEquals(value, instance.getMaximumFractionalValue());

	}

	/**
	 * Test of setMaximumFractionalValue method, of class DefaultSegmentationImageModule.
	 */
	public void testSetMaximumFractionalValueStrict()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		instance.setSegmentationType(Constants.Fractional);
		int value = 240;
		instance.setMaximumFractionalValue(value);
		assertEquals(value, instance.getMaximumFractionalValue());
	}

	/**
	 * Test of setMaximumFractionalValue method, of class DefaultSegmentationImageModule.
	 */
	public void testSetMaximumFractionalValueStrictBad()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		instance.setSegmentationType(Constants.Fractional);
		try
		{
			instance.setMaximumFractionalValue(-1);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setMaximumFractionalValue(256);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setSegmentationFractionalType method, of class DefaultSegmentationImageModule.
	 */
	public void testSetSegmentationFractionalTypeLax()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore, false);
		instance.setSegmentationType(Constants.Fractional);
		instance.setSegmentationFractionalType(Constants.Probability);
		assertEquals(Constants.Probability, instance.getSegmentationFractionalType());
		instance.setSegmentationFractionalType(Constants.Occupancy);
		assertEquals(Constants.Occupancy, instance.getSegmentationFractionalType());
		instance.setSegmentationFractionalType(null);
		assertEquals(null, instance.getSegmentationFractionalType());
		instance.setSegmentationFractionalType(test);
		assertEquals(test, instance.getSegmentationFractionalType());
	}

	/**
	 * Test of setSegmentationFractionalType method, of class DefaultSegmentationImageModule.
	 */
	public void testSetSegmentationFractionalTypeStrict()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		instance.setSegmentationType(Constants.Fractional);
		instance.setSegmentationFractionalType(Constants.Probability);
		assertEquals(Constants.Probability, instance.getSegmentationFractionalType());
		instance.setSegmentationFractionalType(Constants.Occupancy);
		assertEquals(Constants.Occupancy, instance.getSegmentationFractionalType());
	}

	/**
	 * Test of setSegmentationFractionalType method, of class DefaultSegmentationImageModule.
	 */
	public void testSetSegmentationFractionalTypeStrictBad()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		instance.setSegmentationType(Constants.Fractional);
		try
		{
			instance.setSegmentationFractionalType(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setSegmentationFractionalType(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setSegmentationFractionalType(test);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}

	/**
	 * Test of setSegmentationType method, of class DefaultSegmentationImageModule.
	 */
	public void testSetSegmentationTypeLax()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore, false);
		instance.setSegmentationType(Constants.Binary);
		assertEquals(Constants.Binary, instance.getSegmentationType());
		instance.setSegmentationType(Constants.Fractional);
		assertEquals(Constants.Fractional, instance.getSegmentationType());
		instance.setSegmentationType(null);
		assertEquals(null, instance.getSegmentationType());
		instance.setSegmentationType(test);
		assertEquals(test, instance.getSegmentationType());
	}
	
	/**
	 * Test of setSegmentationType method, of class DefaultSegmentationImageModule.
	 */
	public void testSetSegmentationTypeStrict()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		instance.setSegmentationType(Constants.Binary);
		assertEquals(Constants.Binary, instance.getSegmentationType());
		instance.setSegmentationType(Constants.Fractional);
		assertEquals(Constants.Fractional, instance.getSegmentationType());
	}
	
	/**
	 * Test of setSegmentationType method, of class DefaultSegmentationImageModule.
	 */
	public void testSetSegmentationTypeStrictBad()
	{
		DefaultSegmentationImageModule instance =
			new DefaultSegmentationImageModule(segMultiModCore);
		try
		{
			instance.setSegmentationType(null);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setSegmentationType(empty);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
		try
		{
			instance.setSegmentationType(test);
			fail("Expected IllegalArgumentException not thrown");
		}
		catch (IllegalArgumentException ex)
		{}
	}
	
}
