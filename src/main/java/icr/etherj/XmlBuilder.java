/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj;

import java.util.ArrayDeque;
import java.util.Deque;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * A class for easy construction of simple XML documents. Modelled after the
 * StringBuilder class.
 * @author jamesd
 */
public class XmlBuilder
{
	private final Document doc;
	private final Deque<Element> stack = new ArrayDeque<>();

	/**
	 * Factory method for creating a new <code>XmlBuilder</code> with an XML
	 * version of "1.0".
	 * @return a new <code>XmlBuilder</code>
	 * @throws XmlException if an error occurs
	 */
	public static XmlBuilder create() throws XmlException
	{
		return create("1.0");
	}

	/**
	 * Factory method for creating a new <code>XmlBuilder</code> with the
	 * supplied XML version.
	 * @param version the XML version
	 * @return a new <code>XmlBuilder</code>
	 * @throws XmlException if an error occurs
	 */
	public static XmlBuilder create(String version) throws XmlException
	{
		return new XmlBuilder(version);
	}

	/**
	 * Close the current element and set its parent as the current element.
	 * @return the XmlBuilder
	 */
	public XmlBuilder endEntity()
	{
		stack.pop();
		return this;
	}

	/**
	 * Returns the constructed document.
	 * @return the constructed document
	 */
	public Document getDocument()
	{
		// TODO: Dupe the doc not return it
		doc.normalize();
		return doc;
	}

	/**
	 * Sets the value as the attribute of the current element.
	 * @param name name of the attribute
	 * @param value value of the attribute
	 * @return the XmlBuilder
	 */
	public XmlBuilder setAttribute(String name, int value)
	{
		return setAttribute(name, Integer.toString(value));
	}

	/**
	 * Sets the value as the attribute of the current element.
	 * @param name name of the attribute
	 * @param value value of the attribute
	 * @return the XmlBuilder
	 */
	public XmlBuilder setAttribute(String name, long value)
	{
		return setAttribute(name, Long.toString(value));
	}

	/**
	 * Sets the value as the attribute of the current element.
	 * @param name name of the attribute
	 * @param value value of the attribute
	 * @return the XmlBuilder
	 */
	public XmlBuilder setAttribute(String name, String value)
	{
		if ((name != null) && !name.isEmpty() && (value != null))
		{
			stack.peek().setAttribute(name, value);
		}
		return this;
	}

	/**
	 * Create a new element and add it to the most recently created element or
	 * the document root.
	 * @param name the name of the element
	 * @return the XmlBuilder
	 */
	public XmlBuilder startEntity(String name)
	{
		Element element = doc.createElement(name);
		Element top = stack.peek();
		if (top != null)
		{
			top.appendChild(element);
		}
		else
		{
			doc.appendChild(element);
		}
		stack.push(element);
		return this;
	}

	/**
	 * Create a new text element and add it to the most recently created element or
	 * the document root.
	 * @param name the name of the element
	 * @param value the value to set as the text content
	 * @return the XmlBuilder
	 */
	public XmlBuilder startTextEntity(String name, int value)
	{
		startEntity(name);
		stack.peek().setTextContent(Integer.toString(value));
		return this;
	}

	/**
	 * Create a new text element and add it to the most recently created element or
	 * the document root.
	 * @param name the name of the element
	 * @param value the value to set as the text content
	 * @return the XmlBuilder
	 */
	public XmlBuilder startTextEntity(String name, long value)
	{
		startEntity(name);
		stack.peek().setTextContent(Long.toString(value));
		return this;
	}

	/**
	 * Create a new text element and add it to the most recently created element or
	 * the document root.
	 * @param name the name of the element
	 * @param text the string to set as the text content
	 * @return the XmlBuilder
	 */
	public XmlBuilder startTextEntity(String name, String text)
	{
		startEntity(name);
		stack.peek().setTextContent(text);
		return this;
	}

	private XmlBuilder(String version) throws XmlException
	{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;
		try
		{
			docBuilder = docFactory.newDocumentBuilder();
		}
		catch (ParserConfigurationException ex)
		{
			throw new XmlException("Document creation failed", ex);
		}
		doc = docBuilder.newDocument();
		doc.setXmlVersion(version);
	}
}
