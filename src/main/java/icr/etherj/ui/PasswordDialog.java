/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author jamesd
 */
public final class PasswordDialog extends JDialog
{
	public static final int OK = 0;
	public static final int CANCEL = 1;

	private int choice = CANCEL;
	private JPasswordField passField;
	private JPasswordField newPassField;
	private JPasswordField confirmPassField;

	public static String[] showChangePasswordDialog(JFrame parent)
	{
		PasswordDialog dialog = new PasswordDialog(parent, "Old Password",
				"New Password", "Confirm New Password", "Enter Password");
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
		return (dialog.choice == OK)
			? dialog.getPasswords()
			: null;
	}

	public static String showPasswordDialog(JFrame parent)
	{
		return showPasswordDialog(parent, "Password");
	}

	public static String showPasswordDialog(JFrame parent, String message)
	{
		return showPasswordDialog(parent, message, "Enter Password");
	}

	/**
	 *
	 * @param parent
	 * @param message
	 * @param title
	 * @return the chosen password or null on cancellation
	 */
	public static String showPasswordDialog(JFrame parent, String message,
		String title)
	{
		PasswordDialog dialog = new PasswordDialog(parent, message, title);
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
		return (dialog.choice == OK)
			? new String(dialog.passField.getPassword())
			: null;
	}

	public static String showResetPasswordDialog(JFrame parent)
	{
		PasswordDialog dialog = new PasswordDialog(parent,
				"New Password", "Confirm New Password", "Reset Password");
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
		return (dialog.choice == OK)
			? dialog.getPassword()
			: null;
	}

	private PasswordDialog(JFrame parent, String message, String title)
	{
		super(parent, title, true);
		layoutSimpleUi(message);
	}

	private PasswordDialog(JFrame parent, String message1, String message2,
		String message3, String title)
	{
		super(parent, title, true);
		layoutChangeUi(message1, message2, message3);
	}

	private PasswordDialog(JFrame parent, String message1, String message2,
		String title)
	{
		super(parent, title, true);
		layoutResetUi(message1, message2);
	}

	private String getPassword()
	{
		return new String(newPassField.getPassword());
	}

	private String[] getPasswords()
	{
		return new String[]
		{
			new String(passField.getPassword()),
			new String(newPassField.getPassword())
		};
	}

	private void layoutChangeUi(String message1, String message2, String message3)
	{
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		JPanel content = new JPanel(new GridBagLayout());
		contentPane.add(content, BorderLayout.CENTER);
		content.setBorder(new EmptyBorder(3, 3, 3, 3));
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.FIRST_LINE_START;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(2, 2, 2, 2);
		gbc.gridwidth = 3;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		JLabel label = new JLabel(message1);
		content.add(label, gbc);
		passField = new JPasswordField(24);
		passField.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onChangeOk();
			}
		});
		gbc.gridy = 1;
		content.add(passField, gbc);
		label = new JLabel(message2);
		gbc.gridy = 2;
		content.add(label, gbc);
		newPassField = new JPasswordField(24);
		newPassField.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onChangeOk();
			}
		});
		gbc.gridy = 3;
		content.add(newPassField, gbc);
		label = new JLabel(message3);
		gbc.gridy = 4;
		content.add(label, gbc);
		confirmPassField = new JPasswordField(24);
		confirmPassField.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onChangeOk();
			}
		});
		gbc.gridy = 5;
		content.add(confirmPassField, gbc);
		JPanel buttonPanel = new JPanel(new GridLayout(1, 2));
		gbc.gridx = 1;
		gbc.gridy = 6;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.CENTER;
		content.add(buttonPanel, gbc);
		JButton ok = new JButton("OK");
		ok.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onChangeOk();
			}
		});
		buttonPanel.add(ok);
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onCancel();
			}
		});
		buttonPanel.add(cancel);
	}

	private void layoutResetUi(String message1, String message2)
	{
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		JPanel content = new JPanel(new GridBagLayout());
		contentPane.add(content, BorderLayout.CENTER);
		content.setBorder(new EmptyBorder(3, 3, 3, 3));
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.FIRST_LINE_START;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(2, 2, 2, 2);
		gbc.gridwidth = 3;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		JLabel label = new JLabel(message1);
		content.add(label, gbc);
		newPassField = new JPasswordField(24);
		newPassField.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onResetOk();
			}
		});
		gbc.gridy = 1;
		content.add(newPassField, gbc);
		label = new JLabel(message2);
		gbc.gridy = 2;
		content.add(label, gbc);
		confirmPassField = new JPasswordField(24);
		confirmPassField.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onResetOk();
			}
		});
		gbc.gridy = 3;
		content.add(confirmPassField, gbc);
		JPanel buttonPanel = new JPanel(new GridLayout(1, 2));
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.CENTER;
		content.add(buttonPanel, gbc);
		JButton ok = new JButton("OK");
		ok.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onResetOk();
			}
		});
		buttonPanel.add(ok);
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onCancel();
			}
		});
		buttonPanel.add(cancel);
	}

	private void layoutSimpleUi(String message)
	{
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		JPanel content = new JPanel(new GridBagLayout());
		contentPane.add(content, BorderLayout.CENTER);
		content.setBorder(new EmptyBorder(3, 3, 3, 3));
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.FIRST_LINE_START;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(2, 2, 2, 2);
		gbc.gridwidth = 3;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		JLabel label = new JLabel(message);
		content.add(label, gbc);
		passField = new JPasswordField(24);
		passField.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onSimpleOk();
			}
		});
		gbc.gridy = 1;
		content.add(passField, gbc);
		JPanel buttonPanel = new JPanel(new GridLayout(1, 2));
		gbc.gridx = 1;
		gbc.gridy = 10;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.CENTER;
		content.add(buttonPanel, gbc);
		JButton ok = new JButton("OK");
		ok.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onSimpleOk();
			}
		});
		buttonPanel.add(ok);
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onCancel();
			}
		});
		buttonPanel.add(cancel);
	}

	private void onCancel()
	{
		choice = CANCEL;
		dispose();
	}

	private void onChangeOk()
	{
		if (!(new String(newPassField.getPassword())).equals(
			 new String(confirmPassField.getPassword())))
		{
			JOptionPane.showMessageDialog(this, "New passwords don't match",
				"Password Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		choice = OK;
		dispose();
	}

	private void onResetOk()
	{
		if (!(new String(newPassField.getPassword())).equals(
			 new String(confirmPassField.getPassword())))
		{
			JOptionPane.showMessageDialog(this, "Passwords don't match",
				"Password Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		choice = OK;
		dispose();
	}

	private void onSimpleOk()
	{
		choice = OK;
		dispose();
	}
}
