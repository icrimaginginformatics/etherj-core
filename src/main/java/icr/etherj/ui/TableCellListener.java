/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.ui;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

/**
 *
 * @author jamesd
 */
public class TableCellListener implements PropertyChangeListener
{
	// Adapted from Rob Camick's TableCellListener to avoid leaking this in ctor
	private Change change;
	private final JTable table;
	private final Action start;
	private final Action stop;

	/**
	 * Create a TableCellListener.
	 *
	 * @param table  the JTable to be monitored for data changes
	 * @param start
	 * @param stop
	 * @return the new TableCellListener
	 */
	public static TableCellListener newInstance(JTable table, Action start,
		Action stop)
	{
		TableCellListener tcl = new TableCellListener(table, start, stop);
		table.addPropertyChangeListener("tableCellEditor", tcl);
		return tcl;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt)
	{
		//  A cell has started/stopped editing
		if (table.isEditing())
		{
			processEditingStarted();
		}
		else
		{
			processEditingStopped();
		}
	}

	/*
	 * Save information of the cell about to be edited
	 */
	private void processEditingStarted()
	{
		//  The invokeLater is necessary because the editing row and editing
		//  column of the table have not been set when the "tableCellEditor"
		//  PropertyChangeEvent is fired.
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				int row = table.convertRowIndexToModel(table.getEditingRow());
				int column = table.convertColumnIndexToModel(
					table.getEditingColumn());
				if ((row < 0) || (column < 0))
				{
					return;
				}
				change = new Change(table, row, column,
					table.getModel().getValueAt(row, column),
					null);
				ActionEvent event = new ActionEvent(
					new Change(table, row, column, 
						table.getModel().getValueAt(row, column), null),
					ActionEvent.ACTION_PERFORMED, "");
				start.actionPerformed(event);
			}
		});
	}

	/*
	 *	Update the Cell history when necessary
	 */
	private void processEditingStopped()
	{
		if (change == null)
		{
			return;
		}
		Object newValue = table.getModel().getValueAt(change.row, change.column);
		//  Make a copy of the data in case another cell starts editing
		//  while processing this change
		Change eventSource = new Change(table, change.row, change.column,
			change.oldValue, newValue);
		ActionEvent event = new ActionEvent(eventSource,
			ActionEvent.ACTION_PERFORMED, "");
		stop.actionPerformed(event);
	}

	private TableCellListener(JTable table, Action start, Action stop)
	{
		if (table == null)
		{
			throw new IllegalArgumentException("Table cannot be null");
		}
		if (start == null)
		{
			throw new IllegalArgumentException("Action cannot be null");
		}
		this.table = table;
		this.start = start;
		this.stop = stop;
	}

	public static class Change
	{
		private final JTable table;
		private final int row;
		private final int column;
		private final Object oldValue;
		private final Object newValue;

		/**
		 *  Create a TableCellListener with a copy of all the data relevant to
		 *  the change of data for a given cell.
		 *
		 *  @param row  the row of the changed cell
		 *  @param column  the column of the changed cell
		 *  @param oldValue  the old data of the changed cell
		 *  @param newValue  the new data of the changed cell
		 */
		private Change(JTable table, int row, int column, Object oldValue,
			Object newValue)
		{
			this.table = table;
			this.row = row;
			this.column = column;
			this.oldValue = oldValue;
			this.newValue = newValue;
		}

		/**
		 *  Get the column that was last edited
		 *
		 *  @return the column that was edited
		 */
		public int getColumn()
		{
			return column;
		}

		/**
		 * Get the new value in the cell
		 *
		 * @return the new value in the cell
		 */
		public Object getNewValue()
		{
			return newValue;
		}

		/**
		 * Get the old value of the cell
		 *
		 * @return the old value of the cell
		 */
		public Object getOldValue()
		{
			return oldValue;
		}

		/**
		 * Get the row that was last edited
		 *
		 * @return the row that was edited
		 */
		public int getRow()
		{
			return row;
		}

		/**
		 * Get the table of the cell that was changed
		 *
		 * @return the table of the cell that was changed
		 */
		public JTable getTable()
		{
			return table;
		}

	}
}
