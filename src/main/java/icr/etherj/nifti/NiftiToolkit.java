/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.nifti;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory and factory locator class for <code>etherj.nifti</code> package.
 * @author jamesd
 */
public abstract class NiftiToolkit
{
	/** Key for default toolkit. */
	public static final String Default = "default";

	private static final Logger logger = LoggerFactory.getLogger(NiftiToolkit.class);
	private static final Map<String,NiftiToolkit> toolkitMap = new HashMap<>();

	static
	{
		toolkitMap.put(Default, new DefaultNiftiToolkit());
	}

	/**
	 * Returns the default toolkit, equivalent to <code>getToolkit(Default)</code>.
	 * @return the toolkit
	 */
	public static NiftiToolkit getToolkit()
	{
		return getToolkit(Default);
	}

	/**
	 * Returns the toolkit associated with the supplied key or null.
	 * @param key the key
	 * @return the toolkit
	 */
	public static NiftiToolkit getToolkit(String key)
	{
		return toolkitMap.get(key);
	}

	/**
	 * Associates the <code>NiftiToolkit</code> with the key.
	 * @param key the key
	 * @param toolkit the toolkit
	 * @return the previous toolkit associated with the key or null
	 */
	public static NiftiToolkit setToolkit(String key, NiftiToolkit toolkit)
	{
		NiftiToolkit tk = toolkitMap.put(key, toolkit);
		logger.info(toolkit.getClass().getName()+" set with key '"+key+"'");
		return tk;
	}

	/**
	 * Returns a new <code>Nifti</code> object.
	 * @param header the NIfTI header
	 * @return the NIfTI object
	 */
	public abstract Nifti createNifti(NiftiHeader header);

	/**
	 * Returns a new <code>NiftiHeader</code>.
	 * @return the NIfTI header
	 */
	public abstract NiftiHeader createNiftiHeader();

	/**
	 * Returns a new <code>NiftiReader</code>.
	 * @return the NIfTI reader
	 */
	public abstract NiftiReader createReader();

	/**
	 * Returns a new <code>NiftiWriter</code>.
	 * @return the NIfTI reader
	 */
	public abstract NiftiWriter createWriter();

	/*
	 *	Protected constructor to prevent direct instantiation
	 */
	protected NiftiToolkit()
	{}

}
