/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.nifti;

import com.google.common.io.LittleEndianDataInputStream;
import icr.etherj.IoUtils;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.GZIPInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
class DefaultNiftiReader implements NiftiReader
{
	private final static Logger logger = LoggerFactory.getLogger(
		DefaultNiftiReader.class);

	@Override
	public Nifti read(InputStream is) throws IOException
	{
		return read(is, false);
	}

	@Override
	public Nifti read(InputStream is, boolean headerOnly) throws IOException
	{
		return readStream(decompressStream(is), headerOnly);
	}

	@Override
	public Nifti read(String filePath) throws IOException
	{
		return read(filePath, false);
	}

	@Override
	public Nifti read(String filePath, boolean headerOnly) throws IOException
	{
		Path path = Paths.get(filePath);
		File file = path.toFile();
		if (!file.isFile() || !file.canRead())
		{
			throw new IOException("Not a regular, readable file: "+filePath);
		}
		try (InputStream is = Files.newInputStream(path))
		{
			return readStream(decompressStream(is), headerOnly);
		}
	}

	private InputStream decompressStream(InputStream is) throws IOException
	{
		PushbackInputStream pbs = new PushbackInputStream(is, 2);
		byte[] magic = new byte[2];
		int nRead = pbs.read(magic);
		if (nRead != 2)
		{
			IoUtils.safeClose(pbs);
			throw new IOException("Error reading stream");
		}
		pbs.unread(magic, 0, nRead);
		int gzipMagic = (int) ((magic[0] & 0xff) | (magic[1] << 8) & 0xff00);
		if (gzipMagic == GZIPInputStream.GZIP_MAGIC)
		{
			return new GZIPInputStream(pbs);
		}
		return pbs;
	}

	private void readDataBytes(Nifti nifti, InputStream is) throws IOException
	{
		LittleEndianDataInputStream dis = new LittleEndianDataInputStream(is);
		NiftiHeader header = nifti.getHeader();
		long[] dims = header.getDimensions();
		int nDims = (int) dims[0];
		long nValues = dims[1];
		for (int i=1; i<nDims; i++)
		{
			nValues *= dims[i+1];
		}
		if (nValues > Integer.MAX_VALUE)
		{
			throw new IllegalArgumentException(
				"Data array has too many elements: "+nValues);
		}
		short dataType = header.getDataType();
		switch (dataType)
		{
			case Nifti.Int16:
			case Nifti.Uint16:
				short[] shortData = readShortArray(dis, (int) nValues);
				logger.debug("Read "+NiftiUtils.getTypeName(dataType)+" data ("+
					shortData.length+" items)");
				nifti.setShortData(shortData);
				break;
			case Nifti.Float32:
				float[] float32Data = readFloat32Array(dis, (int) nValues);
				logger.debug("Read "+NiftiUtils.getTypeName(dataType)+" data ("+
					float32Data.length+" items)");
				nifti.setFloat32Data(float32Data);
				break;
			default:
				throw new IllegalArgumentException(
					"Unsupported data type: "+NiftiUtils.getTypeName(dataType));
		}
	}

	private void readExtensions(NiftiHeader header, LittleEndianDataInputStream dis)
	{
		logger.debug("Nifti header extensions found");
	}

	private float[] readFloat32Array(LittleEndianDataInputStream dis, int length)
		throws IOException
	{
		float[] array = new float[length];
		for (int i=0; i<length; i++)
		{
			array[i] = dis.readFloat();
		}
		return array;
	}

	private double[] readFloatArrayAsDouble(LittleEndianDataInputStream dis,
		int length)
		throws IOException
	{
		double[] array = new double[length];
		for (int i=0; i<length; i++)
		{
			array[i] = dis.readFloat();
		}
		return array;
	}

	private NiftiHeader readHeader(InputStream is) throws IOException
	{
		LittleEndianDataInputStream dis = new LittleEndianDataInputStream(is);
		int length = dis.readInt();
		switch (length)
		{
			case NiftiHeader.Length_v1_1:
				return readHeader_v1p1(dis);
			case NiftiHeader.Length_v2:
				return readHeader_v2(dis);
			default:
				throw new IOException("Nifti header length invalid: "+length);
		}
	}

	private NiftiHeader readHeader_v1p1(LittleEndianDataInputStream dis)
		throws IOException
	{
		NiftiHeader header = new DefaultNiftiHeader();
		header.setLength(NiftiHeader.Length_v1_1);
		logger.debug("Nifti header format v1.1 found");
		dis.skip(35);
		header.setDimensionInfo(dis.readByte());
		long[] dims = new long[8];
		for (int i=0; i<8; i++)
		{
			dims[i] = dis.readShort();
		}
		header.setDimensions(dims);
		header.setIntentParameters(readFloat32Array(dis, 3));
		header.setIntentCode(dis.readShort());
		header.setDataType(dis.readShort());
		short bpp = dis.readShort();
		if (bpp != header.getBitsPerPixel())
		{
			throw new IOException(
				"Invalid NIfTI header. BPP does not match data type");
		}
		header.setFirstSliceIndex(dis.readShort());
		header.setPixelSpacing(readFloatArrayAsDouble(dis, 8));
		header.setVoxelOffset(dis.readFloat());
		header.setSlope(dis.readFloat());
		header.setIntercept(dis.readFloat());
		header.setLastSliceIndex(dis.readShort());
		header.setSliceTimingOrder(dis.readByte());
		header.setXyztUnits(dis.readByte());
		header.setMaxDisplay(dis.readFloat());
		header.setMinDisplay(dis.readFloat());
		header.setSliceDuration(dis.readFloat());
		header.setTimeOffset(dis.readFloat());
		dis.skip(8);
		header.setDescription(readString(dis, 80));
		header.setAuxFileName(readString(dis, 24));
		header.setQFormCode(dis.readShort());
		header.setSFormCode(dis.readShort());
		header.setQuaternionB(dis.readFloat());
		header.setQuaternionC(dis.readFloat());
		header.setQuaternionD(dis.readFloat());
		header.setQuaternionXShift(dis.readFloat());
		header.setQuaternionYShift(dis.readFloat());
		header.setQuaternionZShift(dis.readFloat());
		header.setAffineX(readFloatArrayAsDouble(dis, 4));
		header.setAffineY(readFloatArrayAsDouble(dis, 4));
		header.setAffineZ(readFloatArrayAsDouble(dis, 4));
		header.setIntentName(readString(dis, 16));
		header.setMagic(readString(dis, 4));
		byte[] extender = new byte[4];
		dis.readFully(extender);
		if (extender[0] == 1)
		{
			readExtensions(header, dis);
		}

		return header;
	}

	private NiftiHeader readHeader_v2(LittleEndianDataInputStream dis)
	{
		NiftiHeader header = new DefaultNiftiHeader();
		header.setLength(NiftiHeader.Length_v2);
		logger.debug("Nifti header format v2 found");
		return header;
	}

	private short[] readShortArray(LittleEndianDataInputStream dis, int length)
		throws IOException
	{
		short[] array = new short[length];
		for (int i=0; i<length; i++)
		{
			array[i] = dis.readShort();
		}
		return array;
	}

	private Nifti readStream(InputStream is, boolean headerOnly) throws IOException
	{
		BufferedInputStream bis = new BufferedInputStream(is);
		NiftiHeader header = readHeader(bis);
		Nifti nifti = new DefaultNifti(header);
		if (!headerOnly)
		{
			readDataBytes(nifti, bis);
		}
		return nifti;
	}

	private String readString(LittleEndianDataInputStream dis, int length)
		throws IOException
	{
		byte[] bytes = new byte[length];
		dis.readFully(bytes);
		int nullIdx = 0;
		for (int i=0; i<length; i++)
		{
			if (bytes[i] == 0)
			{
				break;
			}
			nullIdx++;
		}

		return new String(bytes, 0, nullIdx);
	}
	
}
