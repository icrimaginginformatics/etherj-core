/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom;

/**
 *
 * @author jamesd
 */
import com.google.common.collect.ImmutableList;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class DicomDiff
{
	public static final int DefaultWidth = 64;

	private static final Logger logger =
		LoggerFactory.getLogger(DicomComparison.class);

	private final Deque<Integer> currPath = new ArrayDeque<>();
	private final List<DiffElement> elementList = new ArrayList<>();
	private final String leftDesc;
	private final String rightDesc;
	private final String separator = "\\";

	/**
	 *
	 * @param left
	 * @param right
	 */
	public DicomDiff(DicomObject left, DicomObject right)
	{
		this("Left", left, "Right", right);
	}

	/**
	 *
	 * @param leftDesc
	 * @param left
	 * @param rightDesc
	 * @param right
	 */
	public DicomDiff(String leftDesc, DicomObject left, String rightDesc,
		DicomObject right)
	{
		this.leftDesc = leftDesc;
		if (left == null)
		{
			left = new BasicDicomObject();
		}
		this.rightDesc = rightDesc;
		if (right == null)
		{
			right = new BasicDicomObject();
		}

		diff(left, right);
	}

	/**
	 *
	 */
	public void dump()
	{
		dump(System.out, DefaultWidth, new HashSet<>());
	}

	/**
	 *
	 * @param excludes
	 */
	public void dump(Set<Integer> excludes)
	{
		dump(System.out, DefaultWidth, excludes);
	}

	/**
	 *
	 * @param ps
	 */
	public void dump(PrintStream ps)
	{
		dump(ps, DefaultWidth, new HashSet<>());
	}

	/**
	 *
	 * @param ps
	 * @param excludes
	 */
	public void dump(PrintStream ps, Set<Integer> excludes)
	{
		dump(ps, DefaultWidth, excludes);
	}

	/**
	 *
	 * @param ps
	 * @param width
	 */
	public void dump(PrintStream ps, int width)
	{
		dump(ps, width, new HashSet<>());
	}

	/**
	 *
	 * @param ps
	 * @param width
	 * @param excludes
	 */
	public void dump(PrintStream ps, int width, Set<Integer> excludes)
	{
		ps.println("--- "+leftDesc);
		ps.println("+++ "+rightDesc);
		int[] insDels = getInsertsDeletes();
		ps.println("@@ -"+insDels[1]+", +"+insDels[0]+" @@");
		elementList.forEach((element) -> dump(element, ps, width, excludes));
	}

	/**
	 *
	 * @param index
	 * @return
	 */
	public DiffElement getElement(int index)
	{
		return elementList.get(index);
	}

	/**
	 *
	 * @return
	 */
	public int getElementCount()
	{
		return elementList.size();
	}

	/**
	 *
	 * @return
	 */
	public List<DiffElement> getElements()
	{
		return ImmutableList.copyOf(elementList);
	}

	/**
	 *
	 * @return
	 */
	public int size()
	{
		return elementList.size();
	}

	private void appendByteArrayValue(StringBuilder sb, DicomElement element,
		int width, int itemWidth)
	{
		// Handle the potentially huge byte arrays
		List<String> values = new ArrayList<>();
		int totalLength = 0;
		int idx = 0;
		byte[] bytes = element.getBytes();
		VR vr = element.vr();
		boolean bigEndian = element.bigEndian();
		while (idx < element.length())
		{
			byte[] itemBytes = Arrays.copyOf(bytes, itemWidth);
			String value = vr.toString(itemBytes, bigEndian, null);
			values.add(value);
			totalLength += value.length()+1;
			if (totalLength >= width-1)
			{
				break;
			}
			idx += itemWidth;
		}
		sb.append(trim(String.join(separator, values),  width));
	}

	private void appendElement(StringBuilder sb, DicomElement element,
		String tagName, int width)
	{

		sb.append(element.vr()).append(" ").append(element.length())
			.append(" ");
		appendValue(sb, element, width);
		sb.append(" ").append(tagName).append("\n");
	}

	private void appendValue(StringBuilder sb, DicomElement element,
		int width)
	{
		sb.append("[");
		VR vr = element.vr();
		if (vr == VR.SQ)
		{
			int nItems = element.countItems();
			sb.append((nItems == 1)
				? "1 item]"
				: String.format("%d items]", nItems));
			return;
		}
		switch (vr.toString())
		{
			case "OB":
				appendByteArrayValue(sb, element, width, 1);
				break;
			case "OW":
				appendByteArrayValue(sb, element, width, 2);
				break;
			case "OL":
			case "OF":
				appendByteArrayValue(sb, element, width, 4);
				break;
			case "OV":
			case "OD":
				appendByteArrayValue(sb, element, width, 8);
				break;
			case "UN":
				appendByteArrayValue(sb, element, width, element.length());
				break;
			default:
				try
				{
					if (element.vm(null) == 1)
					{
						String value = vr.toString(element.getBytes(),
							element.bigEndian(), null);
						sb.append(trim(value, width));
					}
					else
					{
						String[] values = element.vr().toStrings(element.getBytes(),
							element.bigEndian(), null);
						sb.append(trim(String.join(separator, values),  width));
					}
				}
				catch (UnsupportedOperationException ex)
				{
					ex.printStackTrace();
				}
				break;
		}
		sb.append("]");
	}

	private ListIterator<DicomElement> createListIterator(DicomObject dcm)
	{
		List<DicomElement> list = new ArrayList<>();
		Iterator<DicomElement> iter = dcm.iterator();
		while (iter.hasNext())
		{
			list.add(iter.next());
		}
		return list.listIterator();
	}

	private void deleteContainer(DicomElement de)
	{
		int nItems = de.countItems();
		if (de.hasDicomObjects())
		{
			for (int i=0; i<nItems; i++)
			{
				currPath.push(i);
				deleteObject(de.getDicomObject(i));
				currPath.pop();
			}
		}
		else
		{
			for (int i=0; i<nItems; i++)
			{
				//onFragment(element, i, dcm);
				logger.warn("DicomDiff - Frag insertion not yet supported");
			}
		}
	}

	private void deleteObject(DicomObject dcm)
	{
		Iterator<DicomElement> iter = dcm.iterator();
		while (iter.hasNext())
		{
			DicomElement next = iter.next();
			currPath.push(next.tag());
			elementList.add(new DiffElement(getTagPath(), next, null));
			if (isContainer(next))
			{
				processContainer(next, null);
			}
			currPath.pop();
		}
	}

	private void diff(DicomObject left, DicomObject right)
	{
		ListIterator<DicomElement> leftIter = createListIterator(left);
		ListIterator<DicomElement> rightIter = createListIterator(right);

		while (leftIter.hasNext())
		{
			DicomElement leftNext = leftIter.next();
			int leftNextTag = leftNext.tag();
			int rightNextTag = peekNextTag(rightIter);
			if (right.contains(leftNextTag))
			{
				if (rightNextTag == leftNextTag)
				{
					DicomElement rightNext = rightIter.next();
					handleMatchingTags(leftNext, rightNext);
				}
				else
				{
					handleDisparateTags(leftNext, rightIter);
					// Rewind to ensure we don't skip this unmatched tag from left.
					leftIter.previous();
				}
			}
			else
			{
				handleDisparateTags(leftNext, rightIter);
			}
		}
		// Handle anything remaining in the right object
		while (rightIter.hasNext())
		{
			DicomElement next = rightIter.next();
			currPath.push(next.tag());
			elementList.add(new DiffElement(getTagPath(), null, next));
			if (isContainer(next))
			{
				processContainer(null, next);
			}
			currPath.pop();
		}
	}

	private void dump(DiffElement element, PrintStream ps, int width,
		Set<Integer> excludes)
	{
		int[] tagPath = element.getTagPath();
		if (excludes.contains(tagPath[0]))
		{
			return;
		}
		String tagName = DicomUtils.tagName(tagPath[tagPath.length-1]);
		StringBuilder sb = new StringBuilder();
		DicomElement left = element.getLeft();
		if (left != null)
		{
			sb.append("- ").append(DicomUtils.formatTagPath(tagPath)).append(" ");
			appendElement(sb, left, tagName, width);
		}
		DicomElement right = element.getRight();
		if (right != null)
		{
			sb.append("+ ").append(DicomUtils.formatTagPath(tagPath)).append(" ");
			appendElement(sb, right, tagName, width);
		}
		ps.print(sb);
	}

	private int[] getInsertsDeletes()
	{
		int ins = 0;
		int dels = 0;
		for (DiffElement element : elementList)
		{
			if (element.getLeft() != null)
			{
				dels++;
			}
			if (element.getRight() != null)
			{
				ins++;
			}
		}
		return new int[] {ins, dels};
	}
		
	private int[] getTagPath()
	{
		int[] tagPath = new int[currPath.size()];
		Iterator<Integer> iter = currPath.descendingIterator();
		for (int i=0; i<tagPath.length; i++)
		{
			tagPath[i] = iter.next();
		}
		return tagPath;
	}

	private void handleDisparateTags(DicomElement left,
		ListIterator<DicomElement> rightIter)
	{
		int leftTag = left.tag();
		if (isTagBefore(left.tag(), peekNextTag(rightIter)))
		{
			// Deletion
			currPath.push(leftTag);
			elementList.add(new DiffElement(getTagPath(), left, null));
			if (isContainer(left))
			{
				processContainer(left, null);
			}
			currPath.pop();
		}
		else
		{
			// Insertion
			while (isTagBefore(peekNextTag(rightIter), leftTag))
			{
				DicomElement right = rightIter.next();
				currPath.push(right.tag());
				elementList.add(new DiffElement(getTagPath(), null, right));
				if (isContainer(right))
				{
					processContainer(null, right);
				}
				currPath.pop();
			}
		}
	}

	private void handleMatchingTags(DicomElement left, DicomElement right)
	{
		int tag = left.tag();
		if (left.equals(right))
		{
			return;
		}
		if (isContainer(left))
		{
			currPath.push(tag);
			processContainer(left, right);
			currPath.pop();
			return;
		}
		currPath.push(tag);
		elementList.add(new DiffElement(getTagPath(), left, right));
		currPath.pop();
	}

	private void insertContainer(DicomElement de)
	{
		int nItems = de.countItems();
		if (de.hasDicomObjects())
		{
			for (int i=0; i<nItems; i++)
			{
				currPath.push(i);
				insertObject(de.getDicomObject(i));
				currPath.pop();
			}
		}
		else
		{
			for (int i=0; i<nItems; i++)
			{
				//onFragment(element, i, dcm);
				logger.warn("DicomDiff - Frag insertion not yet supported");
			}
		}
	}

	private void insertObject(DicomObject dcm)
	{
		Iterator<DicomElement> iter = dcm.iterator();
		while (iter.hasNext())
		{
			DicomElement next = iter.next();
			currPath.push(next.tag());
			elementList.add(new DiffElement(getTagPath(), null, next));
			if (isContainer(next))
			{
				processContainer(null, next);
			}
			currPath.pop();
		}
	}

	private boolean isContainer(DicomElement element)
	{
		return (element.length() == -1 || element.vr() == VR.SQ);
	}

	private boolean isTagBefore(int first, int second)
	{
		return ((first & 0xffffffffL) < (second & 0xffffffffL));
	}

	private int peekNextTag(ListIterator<DicomElement> iter)
	{
		int nextTag = 0xffffffff;
		if (iter.hasNext())
		{
			// Peek at next element then rewind to not disturb iterator state
			nextTag = iter.next().tag();
			iter.previous();
		}
		return nextTag;
	}

	private void processContainer(DicomElement left, DicomElement right)
	{
		if (left == null)
		{
			insertContainer(right);
			return;
		}
		if (right == null)
		{
			deleteContainer(left);
			return;
		}

		int leftItems = left.countItems();
		int rightItems = right.countItems();
		int nItems = Math.min(leftItems, rightItems);
		// Process items where both parent elements have an item at that index
		if (left.hasDicomObjects())
		{
			for (int i=0; i<nItems; i++)
			{
				currPath.push(i);
				diff(left.getDicomObject(i), right.getDicomObject(i));
				currPath.pop();
			}
		}
		else
		{
			for (int i=0; i<nItems; i++)
			{
				//onFragment(element, i, dcm);
				logger.warn("DicomDiff - Frag encountered");
			}
		}
		// Process items where one parent element has an excess of items over the
		// other
		if (leftItems != rightItems)
		{
			if (leftItems > rightItems)
			{
				if (left.hasDicomObjects())
				{
					for (int i=nItems; i<leftItems; i++)
					{
						currPath.push(i);
						deleteObject(left.getDicomObject(i));
						currPath.pop();
					}
				}
				else
				{
					for (int i=nItems; i<leftItems; i++)
					{
						//onFragment(element, i, dcm);
						logger.warn("DicomDiff - Frag encountered");
					}
				}
			}
			else
			{
				if (right.hasDicomObjects())
				{
					for (int i=nItems; i<rightItems; i++)
					{
						currPath.push(i);
						insertObject(right.getDicomObject(i));
						currPath.pop();
					}
				}
				else
				{
					for (int i=nItems; i<rightItems; i++)
					{
						//onFragment(element, i, dcm);
						logger.warn("DicomDiff - Frag encountered");
					}
				}
			}
		}
	}

	private String trim(String value, int width)
	{
		if (value == null)
		{
			return "";
		}
		return (value.length() <= width)
			? value
			: value.substring(0, width-3)+"...";
	}

	public static class DiffElement
	{
		private final DicomElement left;
		private final DicomElement right;
		private final int[] tagPath;

		private DiffElement(int[] tagPath, DicomElement left, DicomElement right)
		{
			if ((tagPath == null) || (tagPath.length == 0))
			{
				throw new IllegalArgumentException(
					"Tag path must not be null or empty");
			}
			if ((left == null) && (right == null))
			{
				throw new IllegalArgumentException(
					"Both left and right elements cannot both be null");
			}
			this.tagPath = Arrays.copyOf(tagPath, tagPath.length);
			this.left = left;
			this.right = right;
		}

		public DicomElement getLeft()
		{
			return left;
		}

		public DicomElement getRight()
		{
			return right;
		}

		public int[] getTagPath()
		{
			return Arrays.copyOf(tagPath, tagPath.length);
		}
	}

}