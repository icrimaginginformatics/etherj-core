/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class SegUtils
{

	/**
	 * Creates a list of pixel data fragments, including the basic offset table,
	 * from natively encoded pixel data.
	 * @param pixelData pixel data array
	 * @param nRows rows in each frame
	 * @param nCols columns in each frame
	 * @return
	 */
	public static List<byte[]> createRleFragments(byte[] pixelData,
		int nRows, int nCols)
	{
		int bytesPerFrame = nRows*nCols;
		if ((pixelData.length % bytesPerFrame) != 0)
		{
			throw new IllegalArgumentException(
				"Array length not compatible with RLE: "+pixelData.length);
		}
		List<byte[]> frags = new ArrayList<>();
		// Empty basic offset table
		frags.add(new byte[8]);
		int nFrames = pixelData.length/bytesPerFrame;
		for (int i=0; i<nFrames; i++)
		{
			int start = i*bytesPerFrame;
			byte[] frame = Arrays.copyOfRange(pixelData, start, start+bytesPerFrame);
			byte[] frag = RLE.compressPixels(frame, nRows, nCols);
			frags.add(frag);
		}

		return frags;
	}

	/**
	 * Packs 8-bit per pixel data into 1-bit per pixel array. Last byte may be
	 * incomplete if the input array length is not a multiple of 8.
	 * @param input byte array
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static byte[] pack(byte[] input) throws IllegalArgumentException
	{
		if (input == null)
		{
			throw new IllegalArgumentException("Input array must not be null");
		}
		int outLen = input.length / 8;
		int overspill = input.length % 8;
		byte[] output = (overspill == 0) ? new byte[outLen] : new byte[outLen+1];
		int inIdx = 0;
		for (int i=0; i<outLen; i++)
		{
			byte value = (byte) ((input[inIdx] != 0) ? 1 : 0);
			inIdx++;
			value |= (byte) ((input[inIdx] != 0) ? 2 : 0);
			inIdx++;
			value |= (byte) ((input[inIdx] != 0) ? 4 : 0);
			inIdx++;
			value |= (byte) ((input[inIdx] != 0) ? 8 : 0);
			inIdx++;
			value |= (byte) ((input[inIdx] != 0) ? 16 : 0);
			inIdx++;
			value |= (byte) ((input[inIdx] != 0) ? 32 : 0);
			inIdx++;
			value |= (byte) ((input[inIdx] != 0) ? 64 : 0);
			inIdx++;
			value |= (byte) ((input[inIdx] != 0) ? 128 : 0);
			inIdx++;
			output[i] = value;
		}
		for (int i=0; i<overspill; i++)
		{
			int x = (input[inIdx] != 0) ? 1 : 0;
			output[outLen] |= (byte) (x << i);
			inIdx++;
		}

		return output;
	}

	/**
	 * Unpacks 1-bit per pixel data to 8-bit per pixel data, up to a maximum of
	 * <code>nBytes</code> pixels. Last byte of the input array may be incomplete
	 * if the total number of pixels per frame of the SEG was not a multiple of 8.
	 * @param input packed byte array
	 * @param nBytes output array length
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static byte[] unpack(byte[] input, int nBytes)
		throws IllegalArgumentException
	{
		return unpack(input, nBytes, (byte) 255);
	}
	/**
	 * Unpacks 1-bit per pixel data to 8-bit per pixel data, up to a maximum of
	 * <code>nBytes</code> pixels. Last byte of the input array may be incomplete
	 * if the total number of pixels per frame of the SEG was not a multiple of 8.
	 * @param input packed byte array
	 * @param nBytes output array length
	 * @param value value to put in each non-zero output byte
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static byte[] unpack(byte[] input, int nBytes, byte value)
		throws IllegalArgumentException
	{
		if (input == null)
		{
			throw new IllegalArgumentException("Input array must not be null");
		}
		byte[] output = new byte[nBytes];
		for (int i=0; i<nBytes; i++)
		{
			int byteIdx = i / 8;
			int bitIdx = i % 8;
			int mask = (1 << bitIdx);
			output[i] = (byte) (((input[byteIdx] & mask) == mask) ? value : 0);
		}

		return output;
	}

	/**
	 * Unpacks an individual frame of 8-bit per pixel data from 1-bit per pixel
	 * data. 
	 * @param input packed byte array
	 * @param nRows rows in the frame
	 * @param nCols columns in the frame
	 * @param frameNumber frame number in the pixel data
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static byte[] unpackFrame(byte[] input, int nRows, int nCols,
		int frameNumber) throws IllegalArgumentException
	{
		return unpackFrame(input, nRows, nCols, frameNumber, (byte) 255);
	}
	/**
	 *
	 * @param input
	 * @param nRows
	 * @param nCols
	 * @param frameNumber
	 * @param value
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static byte[] unpackFrame(byte[] input, int nRows, int nCols,
		int frameNumber, byte value) throws IllegalArgumentException
	{
		if (input == null)
		{
			throw new IllegalArgumentException("Input array must not be null");
		}
		if (frameNumber < 1)
		{
			throw new IllegalArgumentException("Frame number must be >= 1");
		}
		int frameIdx = frameNumber - 1;
		int frameBytes = nRows*nCols;
		byte[] output = new byte[frameBytes];
		int startBit = frameBytes*frameIdx;
		int endBit = startBit+frameBytes;
		if ((input.length * 8) < endBit)
		{
			throw new IllegalArgumentException(
				"Requested frame outside input array: "+frameNumber);
		}
		int outIdx = 0;
		for (int i=startBit; i<endBit; i++)
		{
			int byteIdx = i / 8;
			int bitIdx = i % 8;
			int mask = (1 << bitIdx);
			output[outIdx] = (byte) (((input[byteIdx] & mask) == mask) ? value : 0);
			outIdx++;
		}

		return output;
	}

	private SegUtils()
	{}
}
