/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.StringUtils;
import icr.etherj.Uids;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.impl.SegWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.DoublePredicate;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public final class IodUtils
{
	private static final Logger logger = LoggerFactory.getLogger(IodUtils.class);

	private static String defDate = "19000101";
	private static String defTime = "000000";
	private static final Map<String,String> dummyStrings = new HashMap<>();

	static
	{
		dummyStrings.put("AE", "UNKNOWN");
		dummyStrings.put("AS", "999Y");
		dummyStrings.put("CS", "UNKNOWN");
		dummyStrings.put("DA", defDate);
		dummyStrings.put("DS", "NaN");
		dummyStrings.put("DT", defDate+defTime);
		dummyStrings.put("IS", "-1");
		dummyStrings.put("LO", "UNKNOWN");
		dummyStrings.put("LT", "UNKNOWN");
		dummyStrings.put("PN", "UNKNOWN");
		dummyStrings.put("SH", "UNKNOWN");
		dummyStrings.put("ST", "UNKNOWN");
		dummyStrings.put("TM", defTime);
		dummyStrings.put("UR", "UNKNOWN");
		dummyStrings.put("UT", "UNKNOWN");
	}

	/**
	 *
	 * @param strict
	 * @param value
	 * @param predicate
	 * @param message
	 * @return
	 */
	public static String checkCondition(boolean strict, String value,
		Predicate<String> predicate, String message)
	{
		if (strict)
		{
			if (!predicate.test(value))
			{
				throw new IllegalArgumentException(message);
			}
			return value;
		}
		boolean testPass = false;
		// Testing the predicate may throw, need to accept bad value anyway
		try
		{
			testPass = predicate.test(value);
		}
		catch (IllegalArgumentException ex)
		{ /* Deliberate no-op */}
		if (!testPass)
		{
			logger.warn(message);
		}
		return value;
	}

	/**
	 *
	 * @param strict
	 * @param value
	 * @param predicate
	 * @param message
	 * @param defaultValue
	 * @return
	 */
	public static String checkCondition(boolean strict, String value,
		Predicate<String> predicate, String message, String defaultValue)
	{
		if (strict)
		{
			if (!predicate.test(value))
			{
				throw new IllegalArgumentException(message);
			}
			return value;
		}
		boolean testPass = false;
		// Testing the predicate may throw, need to accept bad value anyway
		try
		{
			testPass = predicate.test(value);
		}
		catch (IllegalArgumentException ex)
		{ /* Deliberate no-op */}
		if (!testPass)
		{
			logger.warn(message);
			return defaultValue;
		}
		return value;
	}

	/**
	 *
	 * @param strict
	 * @param value
	 * @param predicate
	 * @param message
	 * @return
	 */
	public static double checkCondition(boolean strict, double value,
		DoublePredicate predicate, String message)
	{
		if (strict)
		{
			if (!predicate.test(value))
			{
				throw new IllegalArgumentException(message);
			}
			return value;
		}
		if (!predicate.test(value))
		{
			logger.warn(message);
		}
		return value;
	}

	/**
	 *
	 * @param strict
	 * @param value
	 * @param predicate
	 * @param message
	 * @return
	 */
	public static float checkCondition(boolean strict, float value,
		Predicate<Float> predicate, String message)
	{
		if (strict)
		{
			if (!predicate.test(value))
			{
				throw new IllegalArgumentException(message);
			}
			return value;
		}
		if (!predicate.test(value))
		{
			logger.warn(message);
		}
		return value;
	}

	/**
	 *
	 * @param strict
	 * @param value
	 * @param predicate
	 * @param message
	 * @return
	 */
	public static int checkCondition(boolean strict, int value,
		IntPredicate predicate, String message)
	{
		if (strict)
		{
			if (!predicate.test(value))
			{
				throw new IllegalArgumentException(message);
			}
			return value;
		}
		if (!predicate.test(value))
		{
			logger.warn(message);
		}
		return value;
	}

	/**
	 *
	 * @param value
	 * @param predicate
	 * @param message
	 * @param defaultValue
	 * @return
	 */
	public static double checkConditionOrDefault(double value,
		DoublePredicate predicate, String message, double defaultValue)
	{
		if (predicate.test(value))
		{
			return value;
		}
		logger.info(message);
		return defaultValue;
	}

	/**
	 *
	 * @param value
	 * @param predicate
	 * @param message
	 * @param defaultValue
	 * @return
	 */
	public static int checkConditionOrDefault(int value,
		IntPredicate predicate, String message, int defaultValue)
	{
		if (predicate.test(value))
		{
			return value;
		}
		logger.info(message);
		return defaultValue;
	}

	/**
	 *
	 * @param strict
	 * @param value
	 * @param vr
	 * @param name
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static String checkType1(boolean strict, String value, VR vr,
		String name) throws IllegalArgumentException
	{
		if (strict)
		{
			if (StringUtils.isNullOrEmpty(value))
			{
				throw new IllegalArgumentException(
					name+" must not be null or empty");
			}
			return checkByVR(strict, value, name, vr);
		}
		if (StringUtils.isNullOrEmpty(value))
		{
			logger.warn(name+" must not be null or empty");
			return getDummyString(vr);
		}
		return value;
	}

	/**
	 *
	 * @param strict
	 * @param values
	 * @param vr
	 * @param name
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static String[] checkType1(boolean strict, String[] values, VR vr,
		String name) throws IllegalArgumentException
	{
		if (strict)
		{
			if ((values == null) ||
				 ((values.length == 1) && StringUtils.isNullOrEmpty(values[0])))
			{
				throw new IllegalArgumentException(
					name+" must not be null or empty");
			}
			String[] allChecked = new String[values.length];
			for (int i=0; i<values.length; i++)
			{
				String value = values[i];
				if (StringUtils.isNullOrEmpty(value))
				{
					throw new IllegalArgumentException(
						name+" must not be null or empty");
				}
				allChecked[i] = checkByVR(strict, value, name, vr);
			}
			return allChecked;
		}

		// Non-strict
		if ((values == null) ||
			 ((values.length == 1) && StringUtils.isNullOrEmpty(values[0])))
		{
			logger.warn(name+" must not be null or empty");
			return new String[] {getDummyString(vr)};
		}
		String[] allChecked = new String[values.length];
		for (int i=0; i<values.length; i++)
		{
			String value = values[i];
			if (StringUtils.isNullOrEmpty(value))
			{
				logger.warn(name+" must not be null or empty");
				allChecked[i] = getDummyString(vr);
			}
			allChecked[i] = value;
		}
		return allChecked;
	}

	/**
	 *
	 * @param strict
	 * @param value
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static String checkType2(boolean strict, String value)
	{
		return StringUtils.isNullOrEmpty(value) ? "" : value;
	}

	/**
	 *
	 * @param strict
	 * @param value
	 * @param name
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static String checkType2Date(boolean strict, String value,
		String name) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(value))
		{
			return "";
		}
		return checkDate(strict, value, name, (strict ? "" : value));
	}

	/**
	 *
	 * @param strict
	 * @param value
	 * @param name
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static String checkType2Time(boolean strict, String value,
		String name) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(value))
		{
			return "";
		}
		return checkTime(strict, value, name, (strict ? "" : value));
	}

	/**
	 *
	 * @param strict
	 * @param value
	 * @param name
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static String checkType3Date(boolean strict, String value,
		String name) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(value))
		{
			return value;
		}
		return checkDate(strict, value, name, value);
	}

	/**
	 *
	 * @param strict
	 * @param value
	 * @param name
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static String checkType3Time(boolean strict, String value,
		String name) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(value))
		{
			return value;
		}
		return checkTime(strict, value, name, value);
	}

	/**
	 *
	 * @param rtStruct
	 * @return
	 */
	public static String getDate(RtStruct rtStruct)
	{
		return IodUtils.getDate(rtStruct, "yyyyMMdd");
	}

	/**
	 *
	 * @param rtStruct
	 * @param format
	 * @return
	 */
	public static String getDate(RtStruct rtStruct, String format)
	{
		// Find the date from the most specific but optional to the least specific
		// but mandatory
		String date = rtStruct.getStructureSetModule().getStructureSetDate();
		if (date.isEmpty())
		{
			date = rtStruct.getRtSeriesModule().getSeriesDate();
			if (StringUtils.isNullOrEmpty(date))
			{
				date = rtStruct.getStudyDate();
				if (StringUtils.isNullOrEmpty(date))
				{
					return defDate;
				}
			}
		}
		Date dt = DicomUtils.parseDate(date);
		return formatDate(dt, format);
	}

	/**
	 *
	 * @param seg
	 * @return
	 */
	public static String getDate(Segmentation seg)
	{
		return getDate(seg, "yyyyMMdd");
	}

	/**
	 *
	 * @param seg
	 * @param format
	 * @return
	 */
	public static String getDate(Segmentation seg, String format)
	{
		String date = seg.getMultiframeFunctionalGroupsModule().getContentDate();
		if (StringUtils.isNullOrEmpty(date))
		{
			return defDate;
		}
		Date dt = DicomUtils.parseDate(date);
		return formatDate(dt, format);
	}

	public static String getDummyString(VR vr)
	{
		String vrs = vr.toString();
		if ("UI".equals(vrs))
		{
			return Uids.generateDicomUid();
		}
		String value = dummyStrings.get(vrs);
		return (value != null) ? value : "DUMMY";
	}

	/**
	 *
	 * @param seg
	 * @return
	 */
	public static String getName(Segmentation seg)
	{
		String desc = seg.getGeneralSeriesModule().getSeriesDescription();
		if (!StringUtils.isNullOrEmpty(desc))
		{
			return desc;	
		}
		return "Segmentation "+seg.getSegmentationSeriesModule().getSeriesNumber();
	}

	/**
	 * @param seg
	 * @return
	 * @deprecated 
	 */
	public static String getSegmentationDate(Segmentation seg)
	{
		return getDate(seg);
	}

	/**
	 * @param seg
	 * @param format
	 * @return
	 * @deprecated 
	 */
	public static String getSegmentationDate(Segmentation seg, String format)
	{
		return getDate(seg, format);
	}

	/**
	 * @param seg
	 * @return
	 * @deprecated 
	 */
	public static String getSegmentationName(Segmentation seg)
	{
		return getName(seg);
	}

	/**
	 *
	 * @param seg
	 * @return
	 * @deprecated 
	 */
	public static String getSegmentationTime(Segmentation seg)
	{
		return IodUtils.getTime(seg);
	}

	/**
	 *
	 * @param seg
	 * @param format
	 * @return
	 * @deprecated 
	 */
	public static String getSegmentationTime(Segmentation seg, String format)
	{
		return IodUtils.getTime(seg, format);
	}

	/**
	 *
	 * @param rtStruct
	 * @return
	 */
	public static String getTime(RtStruct rtStruct)
	{
		return IodUtils.getTime(rtStruct, "HHmmss");
	}

	/**
	 *
	 * @param rtStruct
	 * @param format
	 * @return
	 */
	public static String getTime(RtStruct rtStruct, String format)
	{
		// Find the date from the most specific but optional to the least specific
		// but mandatory
		String time = rtStruct.getStructureSetModule().getStructureSetTime();
		if (time.isEmpty())
		{
			time = rtStruct.getRtSeriesModule().getSeriesTime();
			if ((time == null) || time.isEmpty())
			{
				time = rtStruct.getStudyTime();
			}
		}
		Date dt = DicomUtils.parseTime(time);
		return formatTime(dt, format);
	}

	/**
	 *
	 * @param seg
	 * @return
	 */
	public static String getTime(Segmentation seg)
	{
		return IodUtils.getTime(seg, "HHmmss");
	}

	/**
	 *
	 * @param seg
	 * @param format
	 * @return
	 */
	public static String getTime(Segmentation seg, String format)
	{
		String time = seg.getMultiframeFunctionalGroupsModule().getContentTime();
		if (StringUtils.isNullOrEmpty(time))
		{
			return defTime;
		}
		Date dt = DicomUtils.parseTime(time);
		return formatTime(dt, format);
	}

	/**
	 *
	 * @param date
	 * @throws IllegalArgumentException
	 */
	public static void setDefaultDate(String date)
		throws IllegalArgumentException
	{
		if (DicomUtils.parseDate(date) == null)
		{
			throw new IllegalArgumentException("Invalid DICOM date: "+date);
		}
		defDate = date;
		dummyStrings.put("DA", defDate);
		dummyStrings.put("DT", defDate+defTime);
	}

	/**
	 *
	 * @param time
	 * @throws IllegalArgumentException
	 */
	public static void setDefaultTime(String time)
		throws IllegalArgumentException
	{
		if (DicomUtils.parseTime(time) == null)
		{
			throw new IllegalArgumentException("Invalid DICOM time: "+time);
		}
		defTime = time;
		dummyStrings.put("TM", defTime);
		dummyStrings.put("DT", defDate+defTime);
	}

	/**
	 *
	 * @param rtStruct
	 * @param path
	 * @throws IOException
	 */
	public static void write(RtStruct rtStruct, String path) throws IOException
	{
		write(rtStruct, new File(path), UID.ImplicitVRLittleEndian);
	}

	/**
	 *
	 * @param rtStruct
	 * @param path
	 * @param xferSyntax
	 * @throws IOException
	 */
	public static void write(RtStruct rtStruct, String path,
		String xferSyntax) throws IOException
	{
		write(rtStruct, new File(path), xferSyntax);
	}

	/**
	 *
	 * @param rtStruct
	 * @param file
	 * @throws IOException
	 */
	public static void write(RtStruct rtStruct, File file) throws IOException
	{
		write(rtStruct, file, UID.ImplicitVRLittleEndian);
	}

	/**
	 *
	 * @param rtStruct
	 * @param file
	 * @param xferSyntax
	 * @throws IOException
	 */
	public static void write(RtStruct rtStruct, File file,
		String xferSyntax) throws IOException
	{
		DicomObject dcm = new BasicDicomObject();
		Iods.pack(rtStruct, dcm);
		DicomUtils.writeDicomFile(dcm, file, xferSyntax);
	}

	/**
	 *
	 * @param rtStruct
	 * @param stream
	 * @param xferSyntax
	 * @throws IOException
	 */
	public static void write(RtStruct rtStruct, OutputStream stream,
		String xferSyntax) throws IOException
	{
		DicomObject dcm = new BasicDicomObject();
		Iods.pack(rtStruct, dcm);
		DicomUtils.writeDicomObject(dcm, stream, xferSyntax);
	}

	/**
	 *
	 * @param seg
	 * @param path
	 * @throws IOException
	 */
	public static void write(Segmentation seg, String path) throws IOException
	{
		write(seg, new File(path), null);
	}

	/**
	 *
	 * @param seg
	 * @param path
	 * @param xferSyntax
	 * @throws IOException
	 */
	public static void write(Segmentation seg, String path,
		String xferSyntax) throws IOException
	{
		write(seg, new File(path), xferSyntax);
	}

	/**
	 *
	 * @param seg
	 * @param file
	 * @throws IOException
	 */
	public static void write(Segmentation seg, File file) throws IOException
	{
		write(seg, file, null);
	}

	/**
	 *
	 * @param seg
	 * @param file
	 * @param xferSyntax
	 * @throws IOException
	 */
	public static void write(Segmentation seg, File file,
		String xferSyntax) throws IOException
	{
		SegWriter writer = new SegWriter();
		writer.write(seg, file, xferSyntax);
	}

	private static String checkByVR(boolean strict, String value, String name,
		VR vr)
	{
		String vrs = vr.toString();
		switch (vrs)
		{
			case "DA":
				return checkDate(strict, value, name, dummyStrings.get(vrs));
			case "TM":
				return checkTime(strict, value, name, dummyStrings.get(vrs));
			case "DT":
				return checkDateTime(strict, value, name, dummyStrings.get(vrs));
			default:
		}
		return value;
	}

	private static String checkDate(boolean strict, String value, String name,
		String defaultValue) throws IllegalArgumentException
	{
		Date date = null;
		try
		{
			date = DicomUtils.parseDate(value);
		}
		catch (IllegalArgumentException ex)
		{
			if (!strict)
			{
				logger.warn(name+" invalid: "+value);
				return defaultValue;
			}
		}
		if (strict)
		{
			if (date == null)
			{
				throw new IllegalArgumentException(name+" invalid: "+value);
			}
			return value;
		}
		if (date == null)
		{
			logger.warn(name+" invalid: "+value);
			return defaultValue;
		}
		return value;
	}

	private static String checkDateTime(boolean strict, String value, String name,
		String defaultValue) throws IllegalArgumentException
	{
		Date date = null;
		try
		{
			date = DicomUtils.parseDateTime(value);
		}
		catch (IllegalArgumentException ex)
		{
			if (!strict)
			{
				logger.warn(name+" invalid: "+value);
				return defaultValue;
			}
		}
		if (strict)
		{
			if (date == null)
			{
				throw new IllegalArgumentException(name+" invalid: "+value);
			}
			return value;
		}
		if (date == null)
		{
			logger.warn(name+" invalid: "+value);
			return defaultValue;
		}
		return value;
	}

	private static String checkTime(boolean strict, String value, String name,
		String defaultValue) throws IllegalArgumentException
	{
		Date date = null;
		try
		{
			date = DicomUtils.parseTime(value);
		}
		catch (IllegalArgumentException ex)
		{
			if (!strict)
			{
				logger.warn(name+" invalid: "+value);
				return defaultValue;
			}
		}
		if (strict)
		{
			if (date == null)
			{
				throw new IllegalArgumentException(name+" invalid: "+value);
			}
			return value;
		}
		if (date == null)
		{
			logger.warn(name+" invalid: "+value);
			return defaultValue;
		}
		return value;
	}

	private static String formatDate(Date dt, String format)
	{
		String date = defDate;
		try
		{
			if (dt != null)
			{
				DateFormat df = new SimpleDateFormat(format);
				date = df.format(dt);
			}
		}
		catch (NullPointerException | IllegalArgumentException ex)
		{
			logger.warn("Date format error", ex);
		}
		return date;
	}

	private static String formatTime(Date dt, String format)
	{
		String time = defTime;
		try
		{
			if (dt != null)
			{
				DateFormat df = new SimpleDateFormat(format);
				time = df.format(dt);
			}
		}
		catch (NullPointerException | IllegalArgumentException ex)
		{
			logger.warn("Time format error", ex);
		}
		return time;
	}

	private IodUtils()
	{}
}
