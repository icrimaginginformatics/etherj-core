/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;
import icr.etherj.dicom.iod.module.AcquisitionContextModule;
import icr.etherj.dicom.iod.module.ClinicalTrialSeriesModule;
import icr.etherj.dicom.iod.module.ClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.ClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.CommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.EnhancedGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralImageModule;
import icr.etherj.dicom.iod.module.GeneralReferenceModule;
import icr.etherj.dicom.iod.module.GeneralSeriesModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import icr.etherj.dicom.iod.module.MultiResolutionNavigationModule;
import icr.etherj.dicom.iod.module.MultiframeDimensionModule;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.OpticalPathModule;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.PatientStudyModule;
import icr.etherj.dicom.iod.module.SlideLabelModule;
import icr.etherj.dicom.iod.module.SopCommonModule;
import icr.etherj.dicom.iod.module.SpecimenModule;
import icr.etherj.dicom.iod.module.WholeSlideMicroscopyImageModule;
import icr.etherj.dicom.iod.module.WholeSlideMicroscopySeriesModule;

/**
 *
 * @author jamesd
 */
public interface WholeSlideMicroscopyImage extends Displayable
{
	/**
	 * Returns the acquisition context module.
	 * @return the module
	 */
	AcquisitionContextModule getAcquisitionContextModule();

	/**
	 * Returns the clinical trial series module.
	 * @return the module
	 */
	ClinicalTrialSeriesModule getClinicalTrialSeriesModule();

	/**
	 * Returns the clinical trial study module.
	 * @return the module
	 */
	ClinicalTrialStudyModule getClinicalTrialStudyModule();

	/**
	 * Returns the clinical trial subject module.
	 * @return the module
	 */
	ClinicalTrialSubjectModule getClinicalTrialSubjectModule();

	/**
	 * Returns the common instance reference module.
	 * @return the module
	 */
	CommonInstanceReferenceModule getCommonInstanceReferenceModule();

	/**
	 * Returns the enhanced general equipment module.
	 * @return the module
	 */
	EnhancedGeneralEquipmentModule getEnhancedEquipmentModule();

	/**
	 * Returns the frame of reference module.
	 * @return the module
	 */
	FrameOfReferenceModule getFrameOfReferenceModule();

	/**
	 * Returns the general equipment module.
	 * @return the module
	 */
	GeneralEquipmentModule getGeneralEquipmentModule();

	/**
	 * Returns the general image module.
	 * @return the module
	 */
	GeneralImageModule getGeneralImageModule();

	/**
	 * Returns the general reference module.
	 * @return the module
	 */
	GeneralReferenceModule getGeneralReferenceModule();

	/**
	 * Returns the general series module.
	 * @return the module
	 */
	GeneralSeriesModule getGeneralSeriesModule();

	/**
	 * Returns the general study module.
	 * @return the module
	 */
	GeneralStudyModule getGeneralStudyModule();

	/**
	 * Returns the image pixel module.
	 * @return the module
	 */
	ImagePixelModule getImagePixelModule();

	/**
	 * Returns the multiframe functional groups module.
	 * @return the module
	 */
	MultiframeFunctionalGroupsModule getMultiframeFunctionalGroupsModule();

	/**
	 * Returns the multiframe dimension module.
	 * @return the module
	 */
	MultiframeDimensionModule getMultiframeDimensionModule();

	/**
	 * Returns the multi-resolution navigation module.
	 * @return the module
	 */
	MultiResolutionNavigationModule getMultiResolutionNavigationModule();

	/**
	 * Returns the optical path module.
	 * @return the module
	 */
	OpticalPathModule getOpticalPathModule();

	/**
	 * Returns the patient module.
	 * @return the module
	 */
	PatientModule getPatientModule();

	/**
	 * Returns the patient study module.
	 * @return the module
	 */
	PatientStudyModule getPatientStudyModule();

	/**
	 * Returns the slide label module.
	 * @return the module
	 */
	SlideLabelModule getSlideLabelModule();

	/**
	 * Returns the SOP common module.
	 * @return the module
	 */
	SopCommonModule getSopCommonModule();

	/**
	 * Returns the SOP instance UID.
	 * @return the UID
	 */
	String getSopInstanceUid();

	/**
	 * Returns the specific character set.
	 * @return
	 */
	String getSpecificCharacterSet();

	/**
	 * Returns the specimen module.
	 * @return the module
	 */
	SpecimenModule getSpecimenModule();

	/**
	 * Returns the study date.
	 * @return the date
	 */
	String getStudyDate();

	/**
	 * Returns the study time.
	 * @return the time
	 */
	String getStudyTime();

	/**
	 * Returns the whole slide microscopy image module.
	 * @return the module
	 */
	WholeSlideMicroscopyImageModule getWholeSlideMicroscopyImageModule();

	/**
	 * Returns the whole slide microscopy series module.
	 * @return the module
	 */
	WholeSlideMicroscopySeriesModule getWholeSlideMicroscopySeriesModule();

	/**
	 * Returns whether the DICOM standard is strictly followed
	 * @return
	 */
	boolean isStrict();
	
	/**
	 * Sets the specific character set.
	 * @param scs
	 */
	void setSpecificCharacterSet(String scs);

}
