/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.EnhancedSr;
import icr.etherj.dicom.iod.module.ClinicalTrialSeriesModule;
import icr.etherj.dicom.iod.module.ClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.ClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.Modules;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.PatientStudyModule;
import icr.etherj.dicom.iod.module.SopCommonModule;
import icr.etherj.dicom.iod.module.SrDocumentContentModule;
import icr.etherj.dicom.iod.module.SrDocumentGeneralModule;
import icr.etherj.dicom.iod.module.SrDocumentSeriesModule;
import icr.etherj.dicom.iod.module.SynchronisationModule;
import java.io.PrintStream;
import org.dcm4che2.data.UID;

/**
 *
 * @author jamesd
 */
public class DefaultEnhancedSr extends AbstractDisplayable implements EnhancedSr
{
	private final ClinicalTrialSeriesModule clinTrialSeries;
	private final ClinicalTrialStudyModule clinTrialStudy;
	private final ClinicalTrialSubjectModule clinTrialSubj;
	private final GeneralEquipmentModule genEquip;
	private final GeneralStudyModule genStudy;
	private final PatientModule patient;
	private final PatientStudyModule patientStudy;
	private final SopCommonModule sopCommon;
	private final SrDocumentContentModule srDocContent;
	private final SrDocumentGeneralModule srDocGeneral;
	private final SrDocumentSeriesModule srDocSeries;
	private final SynchronisationModule sync;
	private final boolean strict;

	public DefaultEnhancedSr()
	{
		this(true);
	}

	public DefaultEnhancedSr(boolean strict)
	{
		// Strict adherence to standard
		this.strict = strict;

		// Standalone modules
		clinTrialSeries = Modules.clinicalTrialSeriesModule(strict);
		clinTrialSubj = Modules.clinicalTrialSubjectModule(strict);
		clinTrialStudy = Modules.clinicalTrialStudyModule(strict);
		genEquip = Modules.generalEquipmentModule(strict);
		genStudy = Modules.generalStudyModule(strict);
		patient = Modules.patientModule(strict);
		patientStudy = Modules.patientStudyModule(strict);
		sopCommon = Modules.sopCommonModule(UID.EnhancedSRStorage);
		srDocContent = Modules.srDocumentContentModule();
		srDocGeneral = Modules.srDocumentGeneralModule();
		srDocSeries = Modules.srDocumentSeriesModule();
		sync = Modules.synchronisationModule();
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		patient.display(ps, indent+"  ", recurse);
		clinTrialSubj.display(ps, indent+"  ", recurse);
		genStudy.display(ps, indent+"  ", recurse);
		patientStudy.display(ps, indent+"  ", recurse);
		clinTrialStudy.display(ps, indent+"  ", recurse);
		srDocSeries.display(ps, indent+"  ", recurse);
		clinTrialSeries.display(ps, indent+"  ", recurse);
		sync.display(ps, indent+"  ", recurse);
		genEquip.display(ps, indent+"  ", recurse);
		srDocGeneral.display(ps, indent+"  ", recurse);
		srDocContent.display(ps, indent+"  ", recurse);
		sopCommon.display(ps, indent+"  ", recurse);
	}

	@Override
	public ClinicalTrialSeriesModule getClinicalTrialSeriesModule()
	{
		return clinTrialSeries;
	}

	@Override
	public ClinicalTrialStudyModule getClinicalTrialStudyModule()
	{
		return clinTrialStudy;
	}

	@Override
	public ClinicalTrialSubjectModule getClinicalTrialSubjectModule()
	{
		return clinTrialSubj;
	}

	@Override
	public GeneralEquipmentModule getGeneralEquipmentModule()
	{
		return genEquip;
	}

	@Override
	public GeneralStudyModule getGeneralStudyModule()
	{
		return genStudy;
	}

	@Override
	public PatientModule getPatientModule()
	{
		return patient;
	}

	@Override
	public PatientStudyModule getPatientStudyModule()
	{
		return patientStudy;
	}

	@Override
	public SopCommonModule getSopCommonModule()
	{
		return sopCommon;
	}

	@Override
	public String getSopInstanceUid()
	{
		return sopCommon.getSopInstanceUid();
	}

	@Override
	public SrDocumentContentModule getSrDocumentContentModule()
	{
		return srDocContent;
	}

	@Override
	public SrDocumentGeneralModule getSrDocumentGeneralModule()
	{
		return srDocGeneral;
	}

	@Override
	public SrDocumentSeriesModule getSrDocumentSeriesModule()
	{
		return srDocSeries;
	}

	@Override
	public String getStudyDate()
	{
		return genStudy.getStudyDate();
	}

	@Override
	public String getStudyTime()
	{
		return genStudy.getStudyTime();
	}

	@Override
	public SynchronisationModule getSynchronisationModule()
	{
		return sync;
	}
	
	@Override
	public boolean isStrict()
	{
		return strict;
	}

}
