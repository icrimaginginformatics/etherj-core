/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.ReferencedImage;
import icr.etherj.dicom.iod.WsmReferencedImage;
import java.io.PrintStream;
import java.util.Arrays;

/**
 *
 * @author jamesd
 */
public class DefaultWsmReferencedImage extends DefaultReferencedImage
	implements WsmReferencedImage
{
	private int[] brhcOfLocaliser =
		new int[] {Integer.MIN_VALUE, Integer.MIN_VALUE};
	private int[] tlhcOfLocaliser =
		new int[] {Integer.MIN_VALUE, Integer.MIN_VALUE};
	private String optPathIdent;
	private double[] pixelSpacing = new double[] {Double.NaN, Double.NaN};
	private int samples = Integer.MIN_VALUE;
	private double zOffset = Double.NaN;
	
	@Override
	public void cloneTo(ReferencedImage target)
	{
		super.cloneTo(target);
		target.setReferencedFrameNumber(getReferencedFrameNumber());
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		String sopClassUid = getReferencedSopClassUid();
		ps.println(pad+"ReferencedSopClassUid: "+sopClassUid);
		ps.println(pad+"ReferencedSopInstanceUid: "+getReferencedSopInstanceUid());
		ps.println(pad+"ReferencedFrameNumber: "+getReferencedFrameNumber());
		ps.println(pad+"TopLeftHandCornerOfLocaliserArea: "+tlhcOfLocaliser[0]
			+"\\"+tlhcOfLocaliser[1]);
		ps.println(pad+"BottomRightHandCornerOfLocaliserArea: "+brhcOfLocaliser[0]
			+"\\"+brhcOfLocaliser[1]);
		ps.println(pad+"PixelSpacing: "+pixelSpacing[0]+"\\"+pixelSpacing[1]);
		ps.println(pad+"ZOffsetInSlideCoordinateSystem: "+zOffset);
		ps.println(pad+"SamplesPerPixel: "+samples);
		ps.println(pad+"OpticalPathIdentifier: "+optPathIdent);
	}

	@Override
	public int[] getBottomRightHandCornerOfLocaliserArea()
	{
		return Arrays.copyOf(brhcOfLocaliser, brhcOfLocaliser.length);
	}

	@Override
	public String getOpticalPathIdentifier()
	{
		return optPathIdent;
	}

	@Override
	public double[] getPixelSpacing()
	{
		return Arrays.copyOf(pixelSpacing, pixelSpacing.length);
	}

	@Override
	public int getSamplesPerPixel()
	{
		return samples;
	}

	@Override
	public int[] getTopLeftHandCornerOfLocaliserArea()
	{
		return Arrays.copyOf(tlhcOfLocaliser, tlhcOfLocaliser.length);
	}

	@Override
	public double getZOffsetInSlideCoordinateSystem()
	{
		return zOffset;
	}

	@Override
	public void setBottomRightHandCornerOfLocaliserArea(int[] brhc)
		throws IllegalArgumentException
	{
		if ((brhc == null) || (brhc.length != 2))
		{
			throw new IllegalArgumentException(
				"Bottom right hand corner must a two element int array");
		}
		if ((brhc[0] < 1) || (brhc[1] < 1))
		{
			throw new IllegalArgumentException(
				"Bottom right hand corner elements must be positive");
		}
		brhcOfLocaliser = Arrays.copyOf(brhc, brhc.length);
	}

	@Override
	public void setOpticalPathIdentifier(String ident)
		throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(ident))
		{
			throw new IllegalArgumentException(
				"Optical path identifier must not e null or empty");
		}
		optPathIdent = ident;
	}

	@Override
	public void setPixelSpacing(double[] spacing)
		throws IllegalArgumentException
	{
		if ((spacing == null) || (spacing.length != 2))
		{
			throw new IllegalArgumentException(
				"Pixel spacing must a two element double array");
		}
		if (!Double.isFinite(spacing[0]) || !Double.isFinite(spacing[1]) ||
			 (spacing[0] <= 0) || (spacing[1] <= 0))
		{
			throw new IllegalArgumentException(
				"Pixel spacing elements must be finite and positive");
		}
		pixelSpacing = Arrays.copyOf(spacing, spacing.length);
	}

	@Override
	public void setSamplesPerPixel(int samples) throws IllegalArgumentException
	{
		if ((samples != 1) && (samples != 3))
		{
			throw new IllegalArgumentException("Samples per pixel must be 1 or 3");
		}
		this.samples = samples;
	}

	@Override
	public void setTopLeftHandCornerOfLocaliserArea(int[] tlhc)
		throws IllegalArgumentException
	{
		if ((tlhc == null) || (tlhc.length != 2))
		{
			throw new IllegalArgumentException(
				"Top left hand corner must a two element int array");
		}
		if ((tlhc[0] < 1) || (tlhc[1] < 1))
		{
			throw new IllegalArgumentException(
				"Top left hand corner elements must be positive");
		}
		tlhcOfLocaliser = Arrays.copyOf(tlhc, tlhc.length);
	}

	@Override
	public void setZOffsetInSlideCoordinateSystem(double zOffset)
		throws IllegalArgumentException
	{
		if (!Double.isFinite(zOffset))
		{
			throw new IllegalArgumentException("Z offset must be finite");
		}
		this.zOffset = zOffset;
	}

}
