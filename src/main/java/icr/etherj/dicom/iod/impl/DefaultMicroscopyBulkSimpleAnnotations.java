/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.MicroscopyBulkSimpleAnnotations;
import icr.etherj.dicom.iod.module.ClinicalTrialSeriesModule;
import icr.etherj.dicom.iod.module.ClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.ClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.CommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.EnhancedGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralSeriesModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.MicroscopyBulkSimpleAnnotationsModule;
import icr.etherj.dicom.iod.module.MicroscopyBulkSimpleAnnotationsSeriesModule;
import icr.etherj.dicom.iod.module.Modules;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.PatientStudyModule;
import icr.etherj.dicom.iod.module.SopCommonModule;
import icr.etherj.dicom.iod.module.impl.DefaultMicroscopyBulkSimpleAnnotationsModule;
import icr.etherj.dicom.iod.module.impl.MicroscopyBulkSimpleAnnotationsMultiModuleCore;
import icr.etherj.dicom.iod.module.impl.MicroscopyBulkSimpleAnnotationsSopCommonModule;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultMicroscopyBulkSimpleAnnotations extends AbstractDisplayable
	implements MicroscopyBulkSimpleAnnotations
{
	private final ClinicalTrialStudyModule clinTrialStudy;
	private final ClinicalTrialSubjectModule clinTrialSubj;
	private final ClinicalTrialSeriesModule clinTrialSeries;
	private final CommonInstanceReferenceModule commonInstRef;
	private final EnhancedGeneralEquipmentModule enhGenEquip;
	private final FrameOfReferenceModule frameOfRef;
	private final GeneralStudyModule genStudy;
	private final MicroscopyBulkSimpleAnnotationsModule mbsa;
	private final MicroscopyBulkSimpleAnnotationsMultiModuleCore mbsaCore;
	private final MicroscopyBulkSimpleAnnotationsSeriesModule mbsaSeries;
	private final PatientModule patient;
	private final PatientStudyModule patientStudy;
	private final SopCommonModule sopCommon;
	private String scs = null;
	private final boolean strict;

	public DefaultMicroscopyBulkSimpleAnnotations()
	{
		this(true);
	}

	public DefaultMicroscopyBulkSimpleAnnotations(boolean strict)
	{
		// Strict adherence to standard
		this.strict = strict;

		// Interdependent modules
		mbsaCore = new MicroscopyBulkSimpleAnnotationsMultiModuleCore(strict);
		mbsa = new DefaultMicroscopyBulkSimpleAnnotationsModule(mbsaCore, strict);
		sopCommon = new MicroscopyBulkSimpleAnnotationsSopCommonModule(mbsaCore,
			strict);

		// Inheriting modules
		enhGenEquip = Modules.enhancedGeneralEquipmentModule(strict);
		
		// Standalone modules
		clinTrialSeries = Modules.clinicalTrialSeriesModule();
		clinTrialSubj = Modules.clinicalTrialSubjectModule();
		clinTrialStudy = Modules.clinicalTrialStudyModule();
		commonInstRef = Modules.commonInstanceReferenceModule(strict);
		frameOfRef = Modules.slideMicroscopyFrameOfReferenceModule(strict);
		genStudy = Modules.generalStudyModule();
		mbsaSeries = Modules.microscopyBulkSimpleAnnotationsSeriesModule(
			strict);
		patient = Modules.patientModule();
		patientStudy = Modules.patientStudyModule();
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		if (!StringUtils.isNullOrEmpty(scs))
		{
			ps.println(indent+"  * SpecificCharacterSet: "+scs);
		}
		String childIndent = indent+"  ";
		patient.display(ps, childIndent, recurse);
		clinTrialSubj.display(ps, childIndent, recurse);
		genStudy.display(ps, childIndent, recurse);
		patientStudy.display(ps, childIndent, recurse);
		clinTrialStudy.display(ps, childIndent, recurse);
		mbsaSeries.display(ps, childIndent, recurse);
		if (Constants.Coord3D.equals(mbsa.getAnnotationCoordinateType()))
		{
			frameOfRef.display(ps, childIndent, recurse);
		}
		enhGenEquip.display(ps, childIndent, recurse);
		mbsa.display(ps, childIndent, recurse);
		commonInstRef.display(ps, childIndent, recurse);
		sopCommon.display(ps, childIndent, recurse);
	}

	@Override
	public ClinicalTrialSeriesModule getClinicalTrialSeriesModule()
	{
		return clinTrialSeries;
	}

	@Override
	public ClinicalTrialStudyModule getClinicalTrialStudyModule()
	{
		return clinTrialStudy;
	}

	@Override
	public ClinicalTrialSubjectModule getClinicalTrialSubjectModule()
	{
		return clinTrialSubj;
	}

	@Override
	public CommonInstanceReferenceModule getCommonInstanceReferenceModule()
	{
		return commonInstRef;
	}

	@Override
	public EnhancedGeneralEquipmentModule getEnhancedEquipmentModule()
	{
		return enhGenEquip;
	}

	@Override
	public FrameOfReferenceModule getFrameOfReferenceModule()
	{
		return frameOfRef;
	}

	@Override
	public GeneralEquipmentModule getGeneralEquipmentModule()
	{
		return enhGenEquip;
	}

	@Override
	public GeneralSeriesModule getGeneralSeriesModule()
	{
		return mbsaSeries;
	}

	@Override
	public GeneralStudyModule getGeneralStudyModule()
	{
		return genStudy;
	}

	@Override
	public MicroscopyBulkSimpleAnnotationsModule getMicroscopyBulkSimpleAnnotationsModule()
	{
		return mbsa;
	}

	@Override
	public MicroscopyBulkSimpleAnnotationsSeriesModule getMicroscopyBulkSimpleAnnotationsSeriesModule()
	{
		return mbsaSeries;
	}

	@Override
	public PatientModule getPatientModule()
	{
		return patient;
	}

	@Override
	public PatientStudyModule getPatientStudyModule()
	{
		return patientStudy;
	}

	@Override
	public SopCommonModule getSopCommonModule()
	{
		return sopCommon;
	}

	@Override
	public String getSopInstanceUid()
	{
		return sopCommon.getSopInstanceUid();
	}

	@Override
	public String getSpecificCharacterSet()
	{
		return scs;
	}

	@Override
	public String getStudyDate()
	{
		return genStudy.getStudyDate();
	}

	@Override
	public String getStudyTime()
	{
		return genStudy.getStudyTime();
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setSpecificCharacterSet(String scs)
	{
		this.scs = StringUtils.isNullOrEmpty(scs) ? null : scs;
	}

}
