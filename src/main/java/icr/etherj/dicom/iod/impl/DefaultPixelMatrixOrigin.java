/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.PixelMatrixOrigin;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultPixelMatrixOrigin extends AbstractDisplayable
	implements PixelMatrixOrigin
{
	private double xOffset = Double.NaN;
	private double yOffset = Double.NaN;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"XOffsetInSlideCoordinateSystem: "+xOffset);
		ps.println(pad+"YOffsetInSlideCoordinateSystem: "+yOffset);
	}

	@Override
	public double getXOffsetInSlideCoordinateSystem()
	{
		return xOffset;
	}

	@Override
	public double getYOffsetInSlideCoordinateSystem()
	{
		return yOffset;
	}

	@Override
	public void setXOffsetInSlideCoordinateSystem(double offset)
	{
		if (!Double.isFinite(offset))
		{
			this.xOffset = Double.NaN;
			return;
		}
		this.xOffset = offset;
	}

	@Override
	public void setYOffsetInSlideCoordinateSystem(double offset)
	{
		if (!Double.isFinite(offset))
		{
			this.yOffset = Double.NaN;
			return;
		}
		this.yOffset = offset;
	}
	
}
