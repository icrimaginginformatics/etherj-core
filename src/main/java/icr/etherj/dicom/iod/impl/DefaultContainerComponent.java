/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.ContainerComponent;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultContainerComponent extends AbstractDisplayable
	implements ContainerComponent
{
	private String material = null;
	private Code typeCode = null;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ContainerComponentTypeCode: ");
		typeCode.display(ps, indent+"  ");
		if (material != null)
		{
			ps.println(pad+"ContainerComponentMaterial: "+material);
		}
	}

	@Override
	public String getContainerComponentMaterial()
	{
		return material;
	}

	@Override
	public Code getContainerComponentTypeCode()
	{
		return typeCode;
	}

	@Override
	public void setContainerComponentMaterial(String material) throws IllegalArgumentException
	{
		if (material == null)
		{
			this.material = material;
			return;
		}
		switch (material)
		{
			case Constants.Glass:
			case Constants.Metal:
			case Constants.Plastic:
				this.material = material;
				break;
			default:
				throw new IllegalArgumentException(
					"Container component material must be GLASS, METAL or PLASTIC");
		}
	}

	@Override
	public void setContainerComponentTypeCode(Code code)
		throws IllegalArgumentException
	{
		if (code == null)
		{
			throw new IllegalArgumentException(
				"Container component type code must not be null");
		}
		typeCode = code;
	}
	
}
