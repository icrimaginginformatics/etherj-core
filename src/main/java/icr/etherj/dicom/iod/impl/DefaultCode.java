/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Code;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class DefaultCode extends AbstractDisplayable
	implements Code
{
	private String designator = null;
	private String meaning = "CodeMeaning";
	private final List<Code> modifiers = new ArrayList<>();
	private String value = "CodeValue";
	private String version = null;

	@Override
	public boolean addModifier(Code code)
	{
		return (code != null) ? modifiers.add(code) : false;
	}

	@Override
	public void cloneTo(Code target)
	{
		target.setCodeMeaning(meaning);
		if (value.length() <= 16)
		{
			target.setCodeValue(value);
		}
		else
		{
			target.setLongCodeValue(value);
		}
		target.setCodingSchemeDesignator(designator);
		target.setCodingSchemeVersion(version);
		for (Code modifier : modifiers)
		{
			Code code = new DefaultCode();
			modifier.cloneTo(code);
			target.addModifier(code);
		}
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		if (value.length() <= 16)
		{
			ps.println(pad+"CodeValue: "+value);
		}
		if (designator != null)
		{
			ps.println(pad+"CodingSchemeDesignator: "+designator);
		}
		ps.println(pad+"CodeMeaning: "+meaning);
		if (value.length() > 16)
		{
			ps.println(pad+"LongCodeValue: "+value);
		}
		int nItems = modifiers.size();
		if (nItems > 0)
		{
			ps.println(pad+"ModifiersList: "+nItems+" item"+
				((nItems != 1) ? "s" : ""));
			for (Code code : modifiers)
			{
				code.display(ps, indent+"  ");
			}
		}
	}

	@Override
	public String getCodeMeaning()
	{
		return meaning;
	}

	@Override
	public String getCodeValue()
	{
		return (value.length() <= 16) ? value : null;
	}

	@Override
	public String getCodingSchemeDesignator()
	{
		return designator;
	}

	@Override
	public String getCodingSchemeVersion()
	{
		return version;
	}

	@Override
	public String getLongCodeValue()
	{
		return (value.length() > 16) ? value : null;
	}

	@Override
	public List<Code> getModifiers()
	{
		return ImmutableList.copyOf(modifiers);
	}

	@Override
	public boolean removeModifier(Code code)
	{
		return modifiers.remove(code);
	}

	@Override
	public void setCodeMeaning(String meaning) throws IllegalArgumentException
	{
		if ((meaning == null) || meaning.isEmpty())
		{
			throw new IllegalArgumentException(
				"CodeMeaning must not be null or empty");
		}
		this.meaning = meaning;
	}

	@Override
	public void setCodeValue(String value) throws IllegalArgumentException
	{
		if ((value == null) || value.isEmpty() || (value.length() > 16))
		{
			throw new IllegalArgumentException(
				"CodeValue must not be null, empty or long");
		}
		this.value = value;
	}

	@Override
	public void setCodingSchemeDesignator(String designator)
		throws IllegalArgumentException
	{
		if ((designator != null) && designator.isEmpty())
		{
			throw new IllegalArgumentException(
				"CodingSchemeDesignator must not be empty");
		}
		this.designator = designator;
	}

	@Override
	public void setCodingSchemeVersion(String version) throws IllegalArgumentException
	{
		if ((version != null) && version.isEmpty())
		{
			throw new IllegalArgumentException(
				"CodingSchemeVersion must not be empty");
		}
		this.version = version;
	}
	
	@Override
	public void setLongCodeValue(String value) throws IllegalArgumentException
	{
		if ((value == null) || value.isEmpty() || (value.length() <= 16))
		{
			throw new IllegalArgumentException(
				"LongCodeValue must not be null, empty or short");
		}
		this.value = value;
	}

}
