/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.FunctionalGroupsReferencedImage;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultFunctionalGroupsReferencedImage extends AbstractDisplayable
	implements FunctionalGroupsReferencedImage
{
	private Code purposeOfRefCode = null;
	private String refSopClassUid = null;
	private String refSopInstUid = null;
	private int refFrame = Integer.MIN_VALUE;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ReferencedSopClassUid: "+refSopClassUid);
		ps.println(pad+"ReferencedSopInstanceUid: "+refSopInstUid);
		if (refFrame > 0)
		{
			ps.println(pad+"ReferencedFrameNumber: "+refFrame);
		}
		if (purposeOfRefCode != null)
		{
			purposeOfRefCode.display(ps, indent+"  ", recurse);
		}
	}

	@Override
	public Code getPurposeOfReferenceCode()
	{
		return purposeOfRefCode;
	}

	@Override
	public int getReferencedFrameNumber()
	{
		return refFrame;
	}

	@Override
	public String getReferencedSopClassUid()
	{
		return refSopClassUid;
	}

	@Override
	public String getReferencedSopInstanceUid()
	{
		return refSopInstUid;
	}

	@Override
	public void setPurposeOfReferenceCode(Code code)
		throws IllegalArgumentException
	{
		if (code == null)
		{
			throw new IllegalArgumentException(
				"Purpose of Reference code must not be null");
		}
		purposeOfRefCode = code;
	}

	@Override
	public void setReferencedFrameNumber(int number)
		throws IllegalArgumentException
	{
		if (number < 1)
		{
			throw new IllegalArgumentException(
				"Referenced frame number must be >= 1");
		}
		refFrame = number;
	}

	@Override
	public void setReferencedSopClassUid(String uid)
		throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(uid))
		{
			throw new IllegalArgumentException(
				"ReferencedSopClassUid must not be null or empty");
		}
		refSopClassUid = uid;
	}

	@Override
	public void setReferencedSopInstanceUid(String uid)
		throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(uid))
		{
			throw new IllegalArgumentException(
				"ReferencedSopInstanceUid must not be null or empty");
		}
		refSopInstUid = uid;
	}
	
}
