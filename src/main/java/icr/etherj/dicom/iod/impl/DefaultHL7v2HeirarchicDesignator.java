/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.HL7v2HierarchicDesignator;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author jamesd
 */
public class DefaultHL7v2HeirarchicDesignator extends AbstractDisplayable
	implements HL7v2HierarchicDesignator
{
	private final static Set<String> types = new HashSet<>();

	private String localId = null;
	private String univId = null;
	private String type = null;

	static
	{
		types.add(Constants.DNS);
		types.add(Constants.EUI64);
		types.add(Constants.ISO);
		types.add(Constants.URI);
		types.add(Constants.UUID);
		types.add(Constants.X400);
		types.add(Constants.X500);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		
	}

	@Override
	public String getLocalNamespaceEntityId()
	{
		return localId;
	}

	@Override
	public String getUniversalEntityId()
	{
		return univId;
	}

	@Override
	public String getUniversalEntityIdType()
	{
		return type;
	}

	@Override
	public void setLocalNamespaceEntityId(String id) throws IllegalArgumentException
	{
		if (id == null)
		{
			localId = null;
			return;
		}
		if (id.isEmpty())
		{
			throw new IllegalArgumentException(
				"Local namespace entity ID type must not be empty");
		}
		localId = id;
	}

	@Override
	public void setUniversalEntityId(String id) throws IllegalArgumentException
	{
		if (id == null)
		{
			univId = null;
			return;
		}
		if (id.isEmpty())
		{
			throw new IllegalArgumentException(
				"Universal entity ID type must not be empty");
		}
		univId = id;
	}

	@Override
	public void setUniversalEntityIdType(String type) throws IllegalArgumentException
	{
		if (type == null)
		{
			this.type = null;
			return;
		}
		if (type.isEmpty())
		{
			throw new IllegalArgumentException(
				"Universal entity ID type must not be empty");
		}
		if (!types.contains(type))
		{
			throw new IllegalArgumentException(
				"Universal entity ID type must be DNS, EUI64, ISO, URI, UUID, X400 or X500");
		}
		this.type = type;
	}
	
}
