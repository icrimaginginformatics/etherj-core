/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.DimensionIndex;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultDimensionIndex extends AbstractDisplayable
	implements DimensionIndex
{
	private String dimOrgUid = "";
	private int indexPtr = 0;
	private String label;
	private int funcPtr = 0;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"DimensionOrganisationUid: "+dimOrgUid);
		ps.println(pad+"IndexPointer: "+String.format("%08x", indexPtr));
		if (funcPtr != 0)
		{
			ps.println(pad+"FunctionalGroupPointer: "+
				String.format("%08x", funcPtr));
		}
		if ((label != null) && !label.isEmpty())
		{
			ps.println(pad+"DescriptionLabel: "+label);
		}
	}	

	@Override
	public String getDescriptionLabel()
	{
		return label;
	}

	@Override
	public String getDimensionOrganisationUid()
	{
		return dimOrgUid;
	}

	@Override
	public int getFunctionalGroupPointer()
	{
		return funcPtr;
	}

	@Override
	public int getIndexPointer()
	{
		return indexPtr;
	}

	@Override
	public void setDescriptionLabel(String label)
	{
		this.label = label;
	}

	@Override
	public void setDimensionOrganisationUid(String uid)
		throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"DimensionOrganisationUid must not be null or empty");
		}
		dimOrgUid = uid;
	}

	@Override
	public void setFunctionalGroupPointer(int pointer)
	{
		funcPtr = pointer;
	}

	@Override
	public void setIndexPointer(int pointer)
	{
		indexPtr = pointer;
	}

}
