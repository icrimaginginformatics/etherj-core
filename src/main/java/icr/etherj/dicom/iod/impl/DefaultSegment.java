/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.Segment;
import java.util.Arrays;
import icr.etherj.dicom.iod.Code;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class DefaultSegment extends AbstractDisplayable implements Segment
{
	private String algoName = "AlgorithmName";
	private String algoType = "AlgorithmType";
	private final List<Code> anatomicCodes = new ArrayList<>();
	private Code categoryCode = new DefaultCode();
	private int[] cieValue = null;
	private String description = null;
	private String label = "SegmentLabel";
	private int number = 0;
	private String trackingId = null;
	private String trackingUid = null;
	private Code typeCode = new DefaultCode();

	@Override
	public boolean addAnatomicRegionCode(Code code)
	{
		return (code != null) ? anatomicCodes.add(code) : false;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"SegmentNumber: "+number);
		ps.println(pad+"SegmentLabel: "+label);
		if ((description != null) && !description.isEmpty())
		{
			ps.println(pad+"SegmentDescription: "+description);
		}
		ps.println(pad+"SegmentAlgorithmType: "+algoType);
		ps.println(pad+"SegmentAlgorithmName: "+algoName);
		if (cieValue != null)
		{
			ps.println(pad+"RecommendedDisplayCIELabValue: "+
				String.valueOf(cieValue[0])+"\\"+String.valueOf(cieValue[1])+"\\"+
				String.valueOf(cieValue[2]));
		}
		int nItems = anatomicCodes.size();
		ps.println(pad+"AnatomicRegionCodeList: "+nItems+" item"+
			((nItems != 1) ? "s" : ""));
		for (Code code : anatomicCodes)
		{
			code.display(ps, indent+"  ");
		}
		ps.println(pad+"SegmentedPropertyCategoryCode:");
		categoryCode.display(ps, indent+"  ", recurse);
		ps.println(pad+"SegmentedPropertyTypeCode:");
		typeCode.display(ps, indent+"  ", recurse);
		if (!StringUtils.isNullOrEmpty(trackingId) &&
			 !StringUtils.isNullOrEmpty(trackingUid))
		{
			ps.println(pad+"TrackingId: "+trackingId);
			ps.println(pad+"TrackingUid: "+trackingUid);
		}
	}

	@Override
	public List<Code> getAnatomicRegionCodeList()
	{
		return ImmutableList.copyOf(anatomicCodes);
	}

	@Override
	public int[] getRecommendedDisplayCIELabValue()
	{
		return cieValue;
	}

	@Override
	public String getSegmentAlgorithmName()
	{
		return algoName;
	}

	@Override
	public String getSegmentAlgorithmType()
	{
		return algoType;
	}

	@Override
	public String getSegmentDescription()
	{
		return description;
	}

	@Override
	public String getSegmentLabel()
	{
		return label;
	}

	@Override
	public int getSegmentNumber()
	{
		return number;
	}

	@Override
	public Code getSegmentedPropertyCategoryCode()
	{
		return categoryCode;
	}

	@Override
	public Code getSegmentedPropertyTypeCode()
	{
		return typeCode;
	}

	@Override
	public String getTrackingId()
	{
		boolean valid = ((trackingUid != null) && !trackingUid.isEmpty());
		return valid ? trackingId : null;
	}

	@Override
	public String getTrackingUid()
	{
		boolean valid = ((trackingId != null) && !trackingId.isEmpty());
		return valid ? trackingUid : null;
	}

	@Override
	public boolean removeAnatomicRegionCode(Code code)
	{
		return anatomicCodes.remove(code);
	}

	@Override
	public void setRecommendedDisplayCIELabValue(int[] value)
	{
		if (value == null)
		{
			cieValue = null;
			return;
		}
		if (value.length != 3)
		{
			throw new IllegalArgumentException(
				"RecommendedDisplayCIELabValue must be three element array");
		}
		this.cieValue = Arrays.copyOf(value, 3);
	}

	@Override
	public void setSegmentAlgorithmName(String name)
		throws IllegalArgumentException
	{
		if ((name != null) && name.isEmpty())
		{
			throw new IllegalArgumentException(
				"SegmentAlgorithmName must not be empty");
		}
		algoName = name;
	}

	@Override
	public void setSegmentAlgorithmType(String type)
		throws IllegalArgumentException
	{
		if ((type == null) || type.isEmpty())
		{
			throw new IllegalArgumentException(
				"SegmentAlgorithmType must not be null or empty");
		}
		switch (type)
		{
			case Constants.Automatic:
			case Constants.Manual:
			case Constants.SemiAutomatic:
				break;
			default:
				throw new IllegalArgumentException(
					"Invalid SegmentAlgorithmType: "+type);
		}
		algoType = type;
	}

	@Override
	public void setSegmentDescription(String description)
	{
		this.description = description;
	}

	@Override
	public void setSegmentLabel(String label) throws IllegalArgumentException
	{
		if ((label == null) || label.isEmpty())
		{
			throw new IllegalArgumentException(
				"SegmentLabel must not be null or empty");
		}
		this.label = label;
	}

	@Override
	public void setSegmentNumber(int number) throws IllegalArgumentException
	{
		if (number < 1)
		{
			throw new IllegalArgumentException(
				"SegmentNumber must be greater than zero");
		}
		this.number = number;
	}

	@Override
	public void setSegmentedPropertyCategoryCode(Code code)
		throws IllegalArgumentException
	{
		if (code == null)
		{
			throw new IllegalArgumentException(
				"SegmentedPropertyCategoryCode must not be null");
		}
		categoryCode = code;
	}
	
	@Override
	public void setSegmentedPropertyTypeCode(Code code)
		throws IllegalArgumentException
	{
		if (code == null)
		{
			throw new IllegalArgumentException(
				"SegmentedPropertyTypeCode must not be null");
		}
		typeCode = code;
	}

	@Override
	public void setTrackingId(String id)
	{
		this.trackingId = id;
	}

	@Override
	public void setTrackingUid(String uid)
	{
		this.trackingUid = uid;
	}
	
}
