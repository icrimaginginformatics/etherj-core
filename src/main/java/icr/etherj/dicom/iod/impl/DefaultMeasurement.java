/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Iods;
import icr.etherj.dicom.iod.Measurement;
import java.io.PrintStream;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class DefaultMeasurement extends AbstractDisplayable
	implements Measurement
{
	private static final Logger logger = LoggerFactory.getLogger(
		DefaultAlgorithmIdentification.class);

	private int[] annoIndices = new int[] {};
	private float[] floatValues = new float[] {};
	private Code nameCode = Iods.code();
	private boolean strict = true;
	private Code unitsCode = Iods.code();

	public DefaultMeasurement()
	{
		this(true);
	}

	public DefaultMeasurement(boolean strict)
	{
		this.strict = strict;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		String childIndent = indent+"  ";
		ps.println(pad+"ConceptNameCode:");
		nameCode.display(ps, childIndent, recurse);
		ps.println(pad+"MeasurementUnitsCode:");
		unitsCode.display(ps, childIndent, recurse);
		ps.println(pad+"FloatingPointValues: "+
			String.join(",", StringUtils.stringify(floatValues)));
		if (annoIndices.length > 0)
		{
			ps.println(pad+"AnnotationIndexList: "+
				String.join(",", StringUtils.stringify(annoIndices)));
		}
	}

	@Override
	public int[] getAnnotationIndexList()
	{
		return Arrays.copyOf(annoIndices, annoIndices.length);
	}

	@Override
	public Code getConceptNameCode()
	{
		return nameCode;
	}

	@Override
	public float[] getFloatingPointValues()
	{
		return Arrays.copyOf(floatValues, floatValues.length);
	}

	@Override
	public Code getMeasurementUnitsCode()
	{
		return unitsCode;
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setAnnotationIndexList(int[] indices)
	{
		if (indices == null)
		{
			annoIndices = new int[] {};
			return;
		}
		annoIndices = Arrays.copyOf(indices, indices.length);
	}

	@Override
	public void setConceptNameCode(Code code) throws IllegalArgumentException
	{
		if (code == null)
		{
			String msg = "Concept name code must not be null";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		nameCode = code;
	}

	@Override
	public void setFloatingPointValues(float[] values)
		throws IllegalArgumentException
	{
		if ((values == null) || (values.length < 1))
		{
			String msg = "FloatingPointValues must not be null or empty";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		floatValues = Arrays.copyOf(values, values.length);
	}

	@Override
	public void setMeasurementUnitsCode(Code code)
		throws IllegalArgumentException
	{
		if (code == null)
		{
			String msg = "Measurement units code must not be null";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		unitsCode = code;
	}
	
}
