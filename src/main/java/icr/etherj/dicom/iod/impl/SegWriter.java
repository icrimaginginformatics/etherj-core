/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.StringUtils;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.RLE;
import icr.etherj.dicom.SegUtils;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.Iods;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.SegmentationImageModule;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class SegWriter
{
	private static final Logger logger = LoggerFactory.getLogger(SegWriter.class);

	private boolean binary = false;
	private boolean isRle = false;
	private boolean useRle = false;
	private String xferSyntax;

	public void write(Segmentation seg, File file, String xs)
		throws IOException
	{
		determineXferSyntax(seg, xs);
		checkPixelData(seg);
		doWrite(seg, file);
	}

	private void checkBinaryPixelData(Segmentation seg)
	{
		ImagePixelModule ipm = seg.getImagePixelModule();
		int nRows = ipm.getRowCount();
		int nCols = ipm.getColumnCount();
		MultiframeFunctionalGroupsModule mffgm =
			seg.getMultiframeFunctionalGroupsModule();
		int nFrames = mffgm.getNumberOfFrames();
		byte[] pixelData = ipm.getPixelData();
		if (pixelData.length != 0)
		{
			handleBinaryNativePixels(seg, pixelData, nRows, nCols, nFrames);
			return;
		}
		throw new IllegalArgumentException("No native pixel data found");
	}

	private void checkFractionalPixelData(Segmentation seg)
	{
		ImagePixelModule ipm = seg.getImagePixelModule();
		int nRows = ipm.getRowCount();
		int nCols = ipm.getColumnCount();
		MultiframeFunctionalGroupsModule mffgm =
			seg.getMultiframeFunctionalGroupsModule();
		int nFrames = mffgm.getNumberOfFrames();

		if (isRle)
		{
			handleRlePixels(seg, ipm.getPixelDataFragments(), nRows, nCols, nFrames);
			return;
		}
		byte[] pixelData = ipm.getPixelData();
		if (pixelData.length != 0)
		{
			handleFractionalNativePixels(seg, pixelData, nRows, nCols, nFrames);
		}
	}

	private void checkPixelData(Segmentation seg)
	{
		if (binary)
		{
			checkBinaryPixelData(seg);
		}
		else
		{
			checkFractionalPixelData(seg);
		}
	}

	private int countUnique(int[] unique)
	{
		int count = 0;
		for (int i=0; i< unique.length; i++)
		{
			if (unique[i] != 0)
			{
				count++;
			}
		}
		return count;
	}

	private void determineBinaryXferSyntax(String xs)
	{
		binary = true;
		useRle = false;
		isRle = false;
		// Unspecified? Use DICOM default
		if (StringUtils.isNullOrEmpty(xs))
		{
			xferSyntax = UID.ImplicitVRLittleEndian;
			return;
		}
		switch (xs)
		{
			case UID.ImplicitVRLittleEndian:
			case UID.ExplicitVRLittleEndian:
			case UID.DeflatedExplicitVRLittleEndian:
			case UID.ExplicitVRBigEndian:
				xferSyntax = xs;
				break;
			case UID.RLELossless:
				xferSyntax = xs;
				useRle = true;
				break;
			default:
				logger.info("TransferSyntax "+xs+
					" not compatible with BINARY segmentation, using default: ImplicitVRLittleEndian");
				xferSyntax = UID.ImplicitVRLittleEndian;
		}
	}

	private void determineFractionalXferSyntax(Segmentation seg, String xs)
	{
		ImagePixelModule ipm = seg.getImagePixelModule();
		isRle = !ipm.getPixelDataFragments().isEmpty();
		if (!isRle)
		{
			// Unspecified and not RLE? Use DICOM default
			if (StringUtils.isNullOrEmpty(xs))
			{
				xferSyntax = UID.ImplicitVRLittleEndian;
				useRle = false;
				return;
			}
		}
		else
		{
			// Unspecified and RLE?
			xferSyntax = UID.RLELossless;
			useRle = true;
			return;
		}
		switch (xs)
		{
			case UID.ImplicitVRLittleEndian:
			case UID.ExplicitVRLittleEndian:
			case UID.DeflatedExplicitVRLittleEndian:
			case UID.ExplicitVRBigEndian:
				xferSyntax = xs;
				break;
			case UID.RLELossless:
				xferSyntax = xs;
				useRle = true;
				break;
			default:
				// Choose best default based existing RLE
				String impliedXferSyntax;
				if (isRle)
				{
					impliedXferSyntax = "RLELossless";
					xferSyntax = UID.RLELossless;
					useRle = true;
				}
				else
				{
					impliedXferSyntax = "ImplicitVRLittleEndian";
					xferSyntax = UID.ImplicitVRLittleEndian;
				}
				logger.info("TransferSyntax "+xs+" not supported, using default: "+
					impliedXferSyntax);
		}
	}

	private void determineXferSyntax(Segmentation seg, String xs)
	{
		if (Constants.Binary.equals(
			seg.getSegmentationImageModule().getSegmentationType()))
		{
			determineBinaryXferSyntax(xs);
		}
		else
		{
			determineFractionalXferSyntax(seg, xs);
		}
	}

	private void doWrite(Segmentation seg, File file) throws IOException
	{
		logger.debug("TransferSyntax: "+xferSyntax);
		DicomObject dcm = new BasicDicomObject();
		Iods.pack(seg, dcm);
		dcm.putString(Tag.TransferSyntaxUID, VR.UI, xferSyntax);
		DicomUtils.writeDicomFile(dcm, file, xferSyntax);
	}

	private void gatherUnique(byte[] frame, int[] unique)
	{
		for (int i=0; i<frame.length; i++)
		{
			int idx = frame[i] & 0x000000ff;
			unique[idx]++;
		}
	}

	private void handleBinaryNativePixels(Segmentation seg, byte[] pixelData,
		int nRows, int nCols, int nFrames)
	{
		int nPixels = nRows*nCols*nFrames;
		int nBytes = (nPixels/8) + (((nPixels % 8) == 0) ? 0 : 1);
		if (pixelData.length != nBytes)
		{
			if (pixelData.length == nPixels)
			{
				logger.info("Packing binary pixels");
				pixelData = SegUtils.pack(pixelData);
				ImagePixelModule ipm = seg.getImagePixelModule();
				ipm.setPixelData(pixelData, true);
				ipm.setBitsAllocated(1);
				ipm.setBitsStored(1);
				ipm.setHighBit(0);
			}
			else
			{
				throw new IllegalArgumentException(
					"Invalid pixel data array length for ("+nCols+"x"+nRows+"x"+
						nFrames+"): "+pixelData.length);
			}
		}
		if (useRle)
		{
			logger.info("Unpacking binary pixels");
			pixelData = SegUtils.unpack(pixelData, nPixels);
			logger.info("RLE compressing pixels");
			List<byte[]> frags = SegUtils.createRleFragments(pixelData,
				nRows, nCols);
			ImagePixelModule ipm = seg.getImagePixelModule();
			ipm.setPixelDataFragments(frags, true);
			SegmentationImageModule sim = seg.getSegmentationImageModule();
			sim.setSegmentationType(Constants.Fractional);
			sim.setSegmentationFractionalType(Constants.Probability);
			ipm.setBitsAllocated(8);
			ipm.setBitsStored(8);
			ipm.setHighBit(7);
		}
	}

	private void handleFractionalNativePixels(Segmentation seg,
		byte[] pixelData, int nRows, int nCols, int nFrames)
	{
		int nPixels = nRows*nCols*nFrames;
		if (pixelData.length == nPixels)
		{
			if (useRle)
			{
				logger.info("RLE compressing pixels");
				List<byte[]> frags = SegUtils.createRleFragments(pixelData,
					nRows, nCols);
				seg.getImagePixelModule().setPixelDataFragments(frags, true);
			}
		}
		else
		{
			throw new IllegalArgumentException(
				"Invalid pixel data array length for ("+nCols+"x"+nRows+"x"+
					nFrames+"): "+pixelData.length);
		}
	}

	private void handleRlePixels(Segmentation seg, List<byte[]> frags, int nRows,
		int nCols, int nFrames)
	{
		if (frags.size() != (nFrames+1))
		{
			throw new IllegalArgumentException(
				"Fragment count and frame count do not match");
		}
		if (useRle)
		{
			return;
		}
		int frameBytes = nCols*nRows;
		int nPixels = frameBytes*nFrames;
		byte[] pixelData = new byte[nPixels];
		int[] unique = new int[256];
		for (int i=1; i<frags.size(); i++)
		{
			byte[] frame = RLE.decompressPixels(frags.get(i));
			System.arraycopy(frame, 0, pixelData, (i-1)*frameBytes, frameBytes);
			gatherUnique(frame, unique);
		}
		// Native encoding if required
		if (countUnique(unique) > 2)
		{
			seg.getImagePixelModule().setPixelData(pixelData, true);
			return;
		}
		// Binary encoding
		ImagePixelModule ipm = seg.getImagePixelModule();
		logger.info("Packing binary pixels");
		pixelData = SegUtils.pack(pixelData);
		ipm.setPixelData(pixelData, true);
		ipm.setBitsAllocated(1);
		ipm.setBitsStored(1);
		ipm.setHighBit(0);
		SegmentationImageModule sim = seg.getSegmentationImageModule();
		sim.setSegmentationType(Constants.Binary);
	}
}
