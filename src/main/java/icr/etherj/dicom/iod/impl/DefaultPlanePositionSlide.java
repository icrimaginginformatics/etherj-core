/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.PlanePositionSlide;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultPlanePositionSlide extends AbstractDisplayable
	implements PlanePositionSlide
{
	private int colPos = Integer.MIN_VALUE;
	private int rowPos = Integer.MIN_VALUE;
	private double xOffset = Double.NaN;
	private double yOffset = Double.NaN;
	private double zOffset = Double.NaN;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ColumnPositionInTotalImagePixelMatrix: "+colPos);
		ps.println(pad+"RowPositionInTotalImagePixelMatrix: "+rowPos);
		ps.println(pad+"XOffsetInSlideCoordinateSystem: "+xOffset);
		ps.println(pad+"YOffsetInSlideCoordinateSystem: "+yOffset);
		ps.println(pad+"ZOffsetInSlideCoordinateSystem: "+zOffset);
	}

	@Override
	public int getColumnPositionInTotalImagePixelMatrix()
	{
		return colPos;
	}

	@Override
	public int getRowPositionInTotalImagePixelMatrix()
	{
		return rowPos;
	}

	@Override
	public double getXOffsetInSlideCoordinateSystem()
	{
		return xOffset;
	}

	@Override
	public double getYOffsetInSlideCoordinateSystem()
	{
		return yOffset;
	}

	@Override
	public double getZOffsetInSlideCoordinateSystem()
	{
		return zOffset;
	}

	@Override
	public void setColumnPositionInTotalImagePixelMatrix(int pos)
		 throws IllegalArgumentException
	{
		if (pos < 1)
		{
			throw new IllegalArgumentException("Column position must not be < 1");
		}
		colPos = pos;
	}

	@Override
	public void setRowPositionInTotalImagePixelMatrix(int pos)
		 throws IllegalArgumentException
	{
		if (pos < 1)
		{
			throw new IllegalArgumentException("Row position must not be < 1");
		}
		rowPos = pos;
	}

	@Override
	public void setXOffsetInSlideCoordinateSystem(double offset)
		 throws IllegalArgumentException
	{
		if (!Double.isFinite(offset))
		{
			throw new IllegalArgumentException("X offset must be finite");
		}
		xOffset = offset;
	}

	@Override
	public void setYOffsetInSlideCoordinateSystem(double offset)
		 throws IllegalArgumentException
	{
		if (!Double.isFinite(offset))
		{
			throw new IllegalArgumentException("Y offset must be finite");
		}
		yOffset = offset;
	}

	@Override
	public void setZOffsetInSlideCoordinateSystem(double offset)
		 throws IllegalArgumentException
	{
		if (!Double.isFinite(offset))
		{
			throw new IllegalArgumentException("Z offset must be finite");
		}
		zOffset = offset;
	}

}
