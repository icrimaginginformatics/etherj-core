/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.ContentItem;
import icr.etherj.dicom.iod.ContentRelationship;
import java.io.PrintStream;
import java.util.Arrays;

/**
 *
 * @author jamesd
 */
public class DefaultContentRelationship extends AbstractDisplayable
	implements ContentRelationship
{
	private int[] refId;
	private ContentItem relatedItem;
	private String type;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"RelationshipType: "+type);
		if ((refId != null) && (refId.length > 0))
		{
			StringBuilder sb = new StringBuilder(Integer.toString(refId[0]));
			for (int i=1; i<refId.length; i++)
			{
				sb.append("-").append(refId[i]);
			}
			ps.println(pad+"ReferencedContentItenIdentifier: "+
				sb.toString());
			return;
		}
		if (relatedItem != null)
		{
			ps.println(pad+"RelatedContentItem:");
			relatedItem.display(ps, indent+"  ", recurse);
		}
	}

	@Override
	public int[] getReferencedContentItemIdentifier()
	{
		if (refId == null)
		{
			return refId;
		}
		return Arrays.copyOf(refId, refId.length);
	}

	@Override
	public ContentItem getRelatedContentItem()
	{
		return relatedItem;
	}

	@Override
	public String getRelationshipType()
	{
		return type;
	}

	@Override
	public void setReferencedContentItemIdentifier(int[] id)
	{
		if (id == null)
		{
			refId = id;
			return;
		}
		refId = Arrays.copyOf(id, id.length);
	}

	@Override
	public void setRelatedContentItem(ContentItem item)
	{
		this.relatedItem = item;
	}

	@Override
	public void setRelationshipType(String type) throws IllegalArgumentException
	{
		this.type = type;
	}
	
}
