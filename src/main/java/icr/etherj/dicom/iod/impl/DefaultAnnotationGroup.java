/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.Uids;
import icr.etherj.dicom.iod.AlgorithmIdentification;
import icr.etherj.dicom.iod.AnnotationGroup;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.Measurement;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class DefaultAnnotationGroup extends AbstractDisplayable
	implements AnnotationGroup
{
	private static final Logger logger = LoggerFactory.getLogger(
		DefaultAnnotationGroup.class);

	private final List<AlgorithmIdentification> algoIdents = new ArrayList<>();
	private boolean allOpticalPaths = true;
	private boolean allZPlanes = true;
	private int annoCount = 0;
	private double[] commonZCoord = new double[] {};
	private String desc = null;
	private double[] doubleCoords = new double[] {};
	private float[] floatCoords = new float[] {};
	private String generationType = Constants.Manual;
	private String graphicType = Constants.Polygon;
	private int groupNumber = Integer.MIN_VALUE;
	private String label = "Label";
	private final List<Measurement> measurements = new ArrayList<>();
	private int[] primitivePointIndices = new int[] {};
	private Code propertyCategoryCode = new DefaultCode();
	private Code propertyTypeCode = new DefaultCode();
	private String[] refOpticalPathIds = new String[] {};
	private boolean strict = true;
	private String uid = Uids.generateDicomUid();

	public DefaultAnnotationGroup()
	{
		this(true);
	}

	public DefaultAnnotationGroup(boolean strict)
	{
		this.strict = strict;
	}

	@Override
	public boolean addAlgorithmIdentification(AlgorithmIdentification ident)
	{
		return (ident != null)
			? algoIdents.add(ident)
			: false;
	}

	@Override
	public boolean addMeasurement(Measurement measurement)
	{
		return (measurement != null)
			? measurements.add(measurement)
			: false;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		String childIndent = indent+"  ";
		ps.println(pad+"AnnotationGroupNumber: "+groupNumber);
		ps.println(pad+"AnnotationGroupUid: "+uid);
		ps.println(pad+"AnnotationGroupLabel: "+label);
		if (!StringUtils.isNullOrEmpty(desc))
		{
			ps.println(pad+"AnnotationGroupDescription: "+label);			
		}
		ps.println(pad+"AnnotationGroupGenerationType: "+generationType);
		int nAlgo = algoIdents.size();
		ps.println(pad+"AlgorithmIdentifiersList: "+nAlgo+" item"+
			((nAlgo != 1) ? "s" : ""));
		if (recurse)
		{
			algoIdents.forEach((x) -> x.display(ps, childIndent, recurse));
		}
		ps.println(pad+"AnnotationPropertyCategoryCode:");
		propertyCategoryCode.display(ps, childIndent, recurse);
		ps.println(pad+"AnnotationPropertyTypeCode:");
		propertyTypeCode.display(ps, childIndent, recurse);
		ps.println(pad+"NumberOfAnnotations: "+annoCount);
		ps.println(pad+"GraphicType: "+graphicType);
		if (allOpticalPaths)
		{
			ps.println(pad+"AnnotationAppliesToAllOpticalPaths: YES");
		}
		else
		{
			ps.println(pad+"AnnotationAppliesToAllOpticalPaths: NO");
			ps.println(pad+"ReferencedOpticalPathIdentifiers: "+
				String.join(",", refOpticalPathIds));
		}
		ps.println(pad+"AnnotationAppliesToAllZPlanes: "+
			(allZPlanes ? "YES" : "NO"));
		if (commonZCoord.length > 0)
		{
			ps.println(pad+"CommonZCoordinateValue: "+
				String.join(",", StringUtils.stringify(commonZCoord)));
		}
		if (floatCoords.length > 0)
		{
			ps.println(pad+"PointCoordinatesData: "+
				String.join(",", StringUtils.stringify(floatCoords)));
		}
		if (doubleCoords.length > 0)
		{
			ps.println(pad+"DoublePointCoordinatesData: "+
				String.join(",", StringUtils.stringify(doubleCoords)));
		}
		if (primitivePointIndices.length > 0)
		{
			ps.println(pad+"LongPrimitivePointIndexList: "+
				String.join(",", StringUtils.stringify(primitivePointIndices)));
		}
		int nMeasurements = measurements.size();
		if (nMeasurements > 0)
		{
			ps.println(pad+"MeasurementsList: "+nMeasurements+" item"+
				((nMeasurements != 1) ? "s" : ""));
			if (recurse)
			{
				measurements.forEach((x) -> x.display(ps, childIndent, recurse));
			}
		}
	}

	@Override
	public List<AlgorithmIdentification> getAlgorithmIdentificationList()
	{
		return ImmutableList.copyOf(algoIdents);
	}

	@Override
	public boolean getAnnotationAppliesToAllOpticalPaths()
	{
		return allOpticalPaths;
	}

	@Override
	public boolean getAnnotationAppliesToAllZPlanes()
	{
		return allZPlanes;
	}

	@Override
	public String getAnnotationGroupDescription()
	{
		return desc;
	}

	@Override
	public String getAnnotationGroupGenerationType()
	{
		return generationType;
	}

	@Override
	public String getAnnotationGroupLabel()
	{
		return label;
	}

	@Override
	public int getAnnotationGroupNumber()
	{
		return groupNumber;
	}

	@Override
	public String getAnnotationGroupUID()
	{
		return uid;
	}

	@Override
	public Code getAnnotationPropertyCategoryCode()
	{
		return propertyCategoryCode;
	}

	@Override
	public Code getAnnotationPropertyTypeCode()
	{
		return propertyTypeCode;
	}

	@Override
	public double[] getCommonZCoordinateValue()
	{
		return Arrays.copyOf(commonZCoord, commonZCoord.length);
	}

	@Override
	public double[] getDoublePointCoordinatesData()
	{
		return Arrays.copyOf(doubleCoords, doubleCoords.length);
	}

	@Override
	public String getGraphicType()
	{
		return graphicType;
	}

	@Override
	public int[] getLongPrimitivePointIndexList()
	{
		return Arrays.copyOf(primitivePointIndices, primitivePointIndices.length);
	}

	@Override
	public List<Measurement> getMeasurementList()
	{
		return ImmutableList.copyOf(measurements);
	}

	@Override
	public int getNumberOfAnnotations()
	{
		return annoCount;
	}

	@Override
	public float[] getPointCoordinatesData()
	{
		return Arrays.copyOf(floatCoords, floatCoords.length);
	}

	@Override
	public String[] getReferencedOpticalPathIdentifier()
	{
		return Arrays.copyOf(refOpticalPathIds, refOpticalPathIds.length);
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public boolean removeAlgorithmIdentification(AlgorithmIdentification ident)
	{
		return (ident != null)
			? algoIdents.remove(ident)
			: false;
	}

	@Override
	public boolean removeMeasurement(Measurement measurement)
	{
		return (measurement != null)
			? measurements.remove(measurement)
			: false;
	}

	@Override
	public void setAnnotationAppliesToAllOpticalPaths(boolean value)
	{
		allOpticalPaths = value;
	}

	@Override
	public void setAnnotationAppliesToAllZPlanes(boolean value)
	{
		allZPlanes = value;
	}

	@Override
	public void setAnnotationGroupDescription(String description)
	{
		desc = description;
	}

	@Override
	public void setAnnotationGroupGenerationType(String type)
		throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(type))
		{
			String msg = "Annotation group generation type must not be null or empty";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		switch (type)
		{
			case Constants.Automatic:
			case Constants.Manual:
			case Constants.SemiAutomatic:
				generationType = type;
				break;
			default:
				String msg = "Annotation group generation type must be MANUAL, AUTOMATIC or SEMIAUTOMATIC";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
		}
	}

	@Override
	public void setAnnotationGroupLabel(String label)
		throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(label))
		{
			String msg = "Annotation group label must not be null or empty";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		this.label = label;
	}

	@Override
	public void setAnnotationGroupNumber(int number)
		throws IllegalArgumentException
	{
		if (number < 1)
		{
			String msg = "Annotation group number must be >= 1";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		groupNumber = number;
	}

	@Override
	public void setAnnotationGroupUID(String uid)
		throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(uid))
		{
			String msg = "Annotation group UID must not be null or empty";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		this.uid = uid;
	}

	@Override
	public void setAnnotationPropertyCategoryCode(Code code)
		throws IllegalArgumentException
	{
		if (code == null)
		{
			String msg = "Annotation property category code must not be null";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		propertyCategoryCode = code;
	}

	@Override
	public void setAnnotationPropertyTypeCode(Code code)
		throws IllegalArgumentException
	{
		if (code == null)
		{
			String msg = "Annotation property type code must not be null";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		propertyTypeCode = code;
	}

	@Override
	public void setCommonZCoordinateValue(double[] value)
		throws IllegalArgumentException
	{
		if (value == null)
		{
			commonZCoord = new double[] {};
			return;
		}
		commonZCoord = Arrays.copyOf(value, value.length);
	}

	@Override
	public void setDoublePointCoordinatesData(double[] data)
		throws IllegalArgumentException
	{
		if (data == null)
		{
			doubleCoords = new double[] {};
			return;
		}
		doubleCoords = Arrays.copyOf(data, data.length);
	}

	@Override
	public void setGraphicType(String type) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(type))
		{
			String msg = "Graphic type must not be null or empty";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		switch (type)
		{
			case Constants.Point:
			case Constants.Polyline:
			case Constants.Polygon:
			case Constants.Ellipse:
			case Constants.Rectangle:
				graphicType = type;
				break;
			default:
				String msg = "Graphic type must be POINT, POLYLINE, POLYGON, ELLIPSE or RECTANGLE";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
		}
	}

	@Override
	public void setLongPrimitivePointIndexList(int[] list)
		throws IllegalArgumentException
	{
		if (list == null)
		{
			primitivePointIndices = new int[] {};
			return;
		}
		primitivePointIndices = Arrays.copyOf(list, list.length);
	}

	@Override
	public void setNumberOfAnnotations(int number)
		throws IllegalArgumentException
	{
		if (number < 1)
		{
			String msg = "Number of annotations must be >= 1";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
		}
		annoCount = number;
	}

	@Override
	public void setPointCoordinatesData(float[] data)
		throws IllegalArgumentException
	{
		if (data == null)
		{
			floatCoords = new float[] {};
			return;
		}
		floatCoords = Arrays.copyOf(data, data.length);
	}

	@Override
	public void setReferencedOpticalPathIdentifier(String[] identifiers)
		throws IllegalArgumentException
	{
		if (identifiers == null)
		{
			refOpticalPathIds = new String[] {};
			return;
		}
		refOpticalPathIds = Arrays.copyOf(identifiers, identifiers.length);
	}

}
