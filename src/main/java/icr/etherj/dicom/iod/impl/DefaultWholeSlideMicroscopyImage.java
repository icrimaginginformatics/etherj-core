/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.WholeSlideMicroscopyImage;
import icr.etherj.dicom.iod.WsmPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.WsmSharedFunctionalGroups;
import icr.etherj.dicom.iod.module.AcquisitionContextModule;
import icr.etherj.dicom.iod.module.ClinicalTrialSeriesModule;
import icr.etherj.dicom.iod.module.ClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.ClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.CommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.EnhancedGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralImageModule;
import icr.etherj.dicom.iod.module.GeneralReferenceModule;
import icr.etherj.dicom.iod.module.GeneralSeriesModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import icr.etherj.dicom.iod.module.Modules;
import icr.etherj.dicom.iod.module.MultiResolutionNavigationModule;
import icr.etherj.dicom.iod.module.MultiframeDimensionModule;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.OpticalPathModule;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.PatientStudyModule;
import icr.etherj.dicom.iod.module.SlideLabelModule;
import icr.etherj.dicom.iod.module.SopCommonModule;
import icr.etherj.dicom.iod.module.SpecimenModule;
import icr.etherj.dicom.iod.module.WholeSlideMicroscopyImageModule;
import icr.etherj.dicom.iod.module.WholeSlideMicroscopySeriesModule;
import icr.etherj.dicom.iod.module.impl.DefaultWholeSlideMicroscopyImageModule;
import icr.etherj.dicom.iod.module.impl.DefaultWsmMultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.impl.WholeSlideMicroscopyGeneralImageModule;
import icr.etherj.dicom.iod.module.impl.WholeSlideMicroscopyImagePixelModule;
import icr.etherj.dicom.iod.module.impl.WholeSlideMicroscopyMultiModuleCore;
import icr.etherj.dicom.iod.module.impl.WholeSlideMicroscopySopCommonModule;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultWholeSlideMicroscopyImage extends AbstractDisplayable
	implements WholeSlideMicroscopyImage
{
	private final AcquisitionContextModule acqContext;
	private final ClinicalTrialStudyModule clinTrialStudy;
	private final ClinicalTrialSubjectModule clinTrialSubj;
	private final ClinicalTrialSeriesModule clinTrialSeries;
	private final CommonInstanceReferenceModule commonInstRef;
	private final EnhancedGeneralEquipmentModule enhGenEquip;
	private final FrameOfReferenceModule frameOfRef;
	private final GeneralImageModule genImage;
	private final GeneralReferenceModule genRef;
	private final GeneralStudyModule genStudy;
	private final ImagePixelModule imagePixel;
	private final MultiframeFunctionalGroupsModule funcGroups;
	private final MultiframeDimensionModule dimension;
	private final MultiResolutionNavigationModule multiResNav;
	private final OpticalPathModule opticalPath;
	private final PatientModule patient;
	private final PatientStudyModule patientStudy;
	private final SlideLabelModule slideLabel;
	private final SopCommonModule sopCommon;
	private String scs = null;
	private final SpecimenModule specimen;
	private final WholeSlideMicroscopyMultiModuleCore wsmMultiModCore;
	private final boolean strict;
	private final WholeSlideMicroscopyImageModule wsmImage;
	private final WholeSlideMicroscopySeriesModule wsmSeries;

	public DefaultWholeSlideMicroscopyImage()
	{
		this(true);
	}

	public DefaultWholeSlideMicroscopyImage(boolean strict)
	{
		// Strict adherence to standard
		this.strict = strict;

		// Interdependent modules
		wsmMultiModCore = new WholeSlideMicroscopyMultiModuleCore(strict);
		genImage = new WholeSlideMicroscopyGeneralImageModule(wsmMultiModCore,
			strict);
		wsmImage = new DefaultWholeSlideMicroscopyImageModule(wsmMultiModCore,
			strict);
		imagePixel = new WholeSlideMicroscopyImagePixelModule(wsmMultiModCore,
			strict);
		WsmSharedFunctionalGroups sharedGroups =
			new DefaultWsmSharedFunctionalGroups();
		WsmPerFrameFunctionalGroups perFrameGroups =
			new DefaultWsmPerFrameFunctionalGroups();
		funcGroups = new DefaultWsmMultiframeFunctionalGroupsModule(
			wsmMultiModCore, sharedGroups, perFrameGroups, strict);
		sopCommon = new WholeSlideMicroscopySopCommonModule(wsmMultiModCore,
			strict);

		// Inheriting modules
		enhGenEquip = Modules.enhancedGeneralEquipmentModule(strict);
		wsmSeries = Modules.wholeSlideMicroscopySeriesModule(strict);

		// Standalone modules
		acqContext = Modules.acquisitionContextModule(strict);
		clinTrialSubj = Modules.clinicalTrialSubjectModule(strict);
		clinTrialStudy = Modules.clinicalTrialStudyModule(strict);
		clinTrialSeries = Modules.clinicalTrialSeriesModule(strict);
		commonInstRef = Modules.commonInstanceReferenceModule(strict);
		frameOfRef = Modules.slideMicroscopyFrameOfReferenceModule(strict);
		genRef = Modules.generalReferenceModule(strict);
		genStudy = Modules.generalStudyModule(strict);
		dimension = Modules.multiframeDimensionModule(strict);
		multiResNav = Modules.multiResolutionNavigationModule(strict);
		opticalPath = Modules.opticalPathModule(strict);
		patient = Modules.patientModule(strict);
		patientStudy = Modules.patientStudyModule(strict);
		specimen = Modules.specimenModule(strict);
		slideLabel = Modules.slideLabelModule(strict);
	}
	
	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		if (!StringUtils.isNullOrEmpty(scs))
		{
			ps.println(indent+"  * SpecificCharacterSet: "+scs);
		}
		String childIndent = indent+"  ";
		patient.display(ps, childIndent, recurse);
		clinTrialSubj.display(ps, childIndent, recurse);
		genStudy.display(ps, childIndent, recurse);
		patientStudy.display(ps, childIndent, recurse);
		clinTrialStudy.display(ps, childIndent, recurse);
		wsmSeries.display(ps, childIndent, recurse);
		frameOfRef.display(ps, childIndent, recurse);
		enhGenEquip.display(ps, childIndent, recurse);
		genImage.display(ps, childIndent, recurse);
		genRef.display(ps, childIndent, recurse);
		imagePixel.display(ps, childIndent, recurse);
		acqContext.display(ps, childIndent, recurse);
		wsmImage.display(ps, childIndent, recurse);
		dimension.display(ps, childIndent, recurse);
		specimen.display(ps, childIndent, recurse);
		opticalPath.display(ps, childIndent, recurse);
		multiResNav.display(ps, childIndent, recurse);
		slideLabel.display(ps, childIndent, recurse);
		funcGroups.display(ps, childIndent, recurse);
		commonInstRef.display(ps, childIndent, recurse);
		sopCommon.display(ps, childIndent, recurse);
	}

	@Override
	public AcquisitionContextModule getAcquisitionContextModule()
	{
		return acqContext;
	}

	@Override
	public ClinicalTrialSeriesModule getClinicalTrialSeriesModule()
	{
		return clinTrialSeries;
	}

	@Override
	public ClinicalTrialStudyModule getClinicalTrialStudyModule()
	{
		return clinTrialStudy;
	}

	@Override
	public ClinicalTrialSubjectModule getClinicalTrialSubjectModule()
	{
		return clinTrialSubj;
	}

	@Override
	public CommonInstanceReferenceModule getCommonInstanceReferenceModule()
	{
		return commonInstRef;
	}

	@Override
	public EnhancedGeneralEquipmentModule getEnhancedEquipmentModule()
	{
		return enhGenEquip;
	}

	@Override
	public FrameOfReferenceModule getFrameOfReferenceModule()
	{
		return frameOfRef;
	}

	@Override
	public GeneralEquipmentModule getGeneralEquipmentModule()
	{
		return enhGenEquip;
	}

	@Override
	public GeneralImageModule getGeneralImageModule()
	{
		return genImage;
	}

	@Override
	public GeneralReferenceModule getGeneralReferenceModule()
	{
		return genRef;
	}

	@Override
	public GeneralSeriesModule getGeneralSeriesModule()
	{
		return wsmSeries;
	}

	@Override
	public GeneralStudyModule getGeneralStudyModule()
	{
		return genStudy;
	}

	@Override
	public ImagePixelModule getImagePixelModule()
	{
		return imagePixel;
	}

	@Override
	public MultiResolutionNavigationModule getMultiResolutionNavigationModule()
	{
		return multiResNav;
	}

	@Override
	public MultiframeFunctionalGroupsModule getMultiframeFunctionalGroupsModule()
	{
		return funcGroups;
	}

	@Override
	public MultiframeDimensionModule getMultiframeDimensionModule()
	{
		return dimension;
	}

	@Override
	public OpticalPathModule getOpticalPathModule()
	{
		return opticalPath;
	}

	@Override
	public PatientModule getPatientModule()
	{
		return patient;
	}

	@Override
	public PatientStudyModule getPatientStudyModule()
	{
		return patientStudy;
	}

	@Override
	public SlideLabelModule getSlideLabelModule()
	{
		return slideLabel;
	}

	@Override
	public SopCommonModule getSopCommonModule()
	{
		return sopCommon;
	}

	@Override
	public String getSopInstanceUid()
	{
		return sopCommon.getSopInstanceUid();
	}

	@Override
	public String getSpecificCharacterSet()
	{
		return scs;
	}

	@Override
	public SpecimenModule getSpecimenModule()
	{
		return specimen;
	}

	@Override
	public String getStudyDate()
	{
		return genStudy.getStudyDate();
	}

	@Override
	public String getStudyTime()
	{
		return genStudy.getStudyTime();
	}

	@Override
	public WholeSlideMicroscopyImageModule getWholeSlideMicroscopyImageModule()
	{
		return wsmImage;
	}

	@Override
	public WholeSlideMicroscopySeriesModule getWholeSlideMicroscopySeriesModule()
	{
		return wsmSeries;
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setSpecificCharacterSet(String scs)
	{
		this.scs = StringUtils.isNullOrEmpty(scs) ? null : scs;
	}

}
