/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.OpticalPath;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class DefaultOpticalPath extends AbstractDisplayable
	implements OpticalPath
{
	private String desc;
	private byte[] iccProfile = null;
	private String ident = "UNKNOWN";
	private Code illuminatorTypeCode = null;
	private Code illuminationColourCode = null;
	private final List<Code> illuminationTypeCodes = new ArrayList<>();
	private float wavelength = Float.NaN;

	@Override
	public boolean addIlluminationTypeCode(Code code)
	{
		if (code == null)
		{
			return false;
		}
		return illuminationTypeCodes.add(code);
	}

	@Override
	public void clearIlluminationTypeCodes()
	{
		illuminationTypeCodes.clear();
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"OpticalPathIdentifier: "+ident);
		if (StringUtils.isNullOrEmpty(desc))
		{
			ps.println(pad+"OpticalPathDescription: "+desc);
		}
		if (illuminatorTypeCode != null)
		{
			ps.println(pad+"IlluminatorTypeCode:");
			illuminatorTypeCode.display(ps, indent+"  ", recurse);
		}
		if (Float.isFinite(wavelength))
		{
			ps.println(pad+"IlluminationWaveLength: "+wavelength);
		}
		if (illuminationColourCode != null)
		{
			ps.println(pad+"IlluminatorColourCode:");
			illuminationColourCode.display(ps, indent+"  ", recurse);
		}
		int nItems = illuminationTypeCodes.size();
		ps.println(pad+"IlluminationTypeCodes: "+nItems+" item"+
			((nItems != 1) ? "s" : ""));
		illuminationTypeCodes.forEach((code) -> code.display(ps, indent+"  "));
		if (iccProfile != null)
		{
			ps.println(pad+"IccProfile: "+iccProfile.length+" bytes");
		}
	}

	@Override
	public byte[] getIccProfile()
	{
		return (iccProfile != null)
			? Arrays.copyOf(iccProfile, iccProfile.length)
			: null;
	}

	@Override
	public Code getIlluminationColourCode()
	{
		return illuminationColourCode;
	}

	@Override
	public List<Code> getIlluminationTypeCodeList()
	{
		return ImmutableList.copyOf(illuminationTypeCodes);
	}

	@Override
	public float getIlluminationWaveLength()
	{
		return wavelength;
	}

	@Override
	public Code getIlluminatorTypeCode()
	{
		return illuminatorTypeCode;
	}

	@Override
	public String getOpticalPathDescription()
	{
		return desc;
	}

	@Override
	public String getOpticalPathIdentifier()
	{
		return ident;
	}

	@Override
	public void setIccProfile(byte[] profile)
	{
		if ((profile == null) || (profile.length == 0))
		{
			iccProfile = null;
			return;
		}
		iccProfile = Arrays.copyOf(profile, profile.length);
	}

	@Override
	public void setIlluminationColourCode(Code code)
		throws IllegalArgumentException
	{
		if (code == null)
		{
			throw new IllegalArgumentException(
				"Illumination colour code must not be null");
		}
		illuminationColourCode = code;
	}

	@Override
	public void setIlluminationWaveLength(float wavelength)
	{
		if (!Float.isFinite(wavelength) || (wavelength <= 0))
		{
			this.wavelength = Float.NaN;
			return;
		}
		this.wavelength = wavelength;
	}

	@Override
	public void setIlluminatorTypeCode(Code code)
	{
		this.illuminatorTypeCode = code;
	}

	@Override
	public void setOpticalPathDescription(String desc)
	{
		this.desc = desc;
	}

	@Override
	public void setOpticalPathIdentifier(String ident)
	{
		if (StringUtils.isNullOrEmpty(ident))
		{
			throw new IllegalArgumentException(
				"Optical path identifier must not be null or empty");
		}
		this.ident = ident;
	}
	
}
