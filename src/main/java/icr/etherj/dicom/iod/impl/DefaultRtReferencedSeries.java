/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.Uids;
import icr.etherj.dicom.Validation;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.RtReferencedSeries;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
public final class DefaultRtReferencedSeries extends AbstractDisplayable
	implements RtReferencedSeries
{
	private final List<ContourImage> imageList = new ArrayList<>();
	private String uid = Uids.generateDicomUid();

	@Override
	public boolean addContourImage(ContourImage ci)
	{
		return (ci != null) ? imageList.add(ci) : false;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"SeriesInstanceUid: "+uid);
		int nImages = imageList.size();
		ps.println(pad+"ContourImageList: "+nImages+" item"+
			((nImages != 1) ? "s" : ""));
		if (recurse)
		{
			for (ContourImage ci : imageList)
			{
				ci.display(ps, indent+"  ", recurse);
			}
		}
	}

	@Override
	public List<ContourImage> getContourImageList()
	{
		return ImmutableList.copyOf(imageList);
	}

	@Override
	public String getSeriesInstanceUid()
	{
		return uid;
	}

	@Override
	public void setSeriesInstanceUid(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SeriesInstanceUid must not be null or empty");
		}
		this.uid = uid;
	}

	@Override
	public boolean validate()
	{
		String clazz = "RtReferencedSeries";
		boolean instUidOk = Validation.type1(clazz, uid, Tag.SeriesInstanceUID);
		boolean ciOk = Validation.type1(clazz, imageList, 1,
			Tag.ContourImageSequence);

		return instUidOk && ciOk;
	}
}
