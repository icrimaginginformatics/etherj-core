/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.AlgorithmIdentification;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Iods;
import java.io.PrintStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class DefaultAlgorithmIdentification extends AbstractDisplayable
	implements AlgorithmIdentification
{
	private static final Logger logger = LoggerFactory.getLogger(
		DefaultAlgorithmIdentification.class);

	private Code familyCode = Iods.code();
	private String name = "Name";
	private Code nameCode = null;
	private String parameters = null;
	private String source = null;
	private boolean strict = true;
	private String version = "Version";

	public DefaultAlgorithmIdentification()
	{
		this(true);
	}

	public DefaultAlgorithmIdentification(boolean strict)
	{
		this.strict = strict;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"AlgorithmFamilyCode:");
		familyCode.display(ps, indent+"  ", recurse);
		if (nameCode != null)
		{
			ps.println(pad+"AlgorithmNameCode:");
			nameCode.display(ps, indent+"  ", recurse);
		}
		ps.println(pad+"AlgorithmName: "+name);
		ps.println(pad+"AlgorithmVersion: "+version);
		if (!StringUtils.isNullOrEmpty(parameters))
		{
			ps.println(pad+"AlgorithmParameters: "+parameters);
		}
		if (!StringUtils.isNullOrEmpty(source))
		{
			ps.println(pad+"AlgorithmSource: "+source);
		}
	}

	@Override
	public Code getAlgorithmFamilyCode()
	{
		return familyCode;
	}

	@Override
	public String getAlgorithmName()
	{
		return name;
	}

	@Override
	public Code getAlgorithmNameCode()
	{
		return nameCode;
	}

	@Override
	public String getAlgorithmParameters()
	{
		return parameters;
	}

	@Override
	public String getAlgorithmSource()
	{
		return source;
	}

	@Override
	public String getAlgorithmVersion()
	{
		return version;
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setAlgorithmFamilyCode(Code code) throws IllegalArgumentException
	{
		if (code == null)
		{
			String msg = "Algorithm family code must not be null";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		familyCode = code;
	}

	@Override
	public void setAlgorithmName(String name) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(name))
		{
			String msg = "Algorithm name must not be null or empty";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		this.name = name;
	}

	@Override
	public void setAlgorithmNameCode(Code code)
	{
		nameCode = code;
	}

	@Override
	public void setAlgorithmParameters(String parameters)
	{
		this.parameters = parameters;
	}

	@Override
	public void setAlgorithmSource(String source)
	{
		this.source = source;
	}

	@Override
	public void setAlgorithmVersion(String version)
		throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(version))
		{
			String msg = "Algorithm version must not be null or empty";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		this.version = version;
	}
	
}
