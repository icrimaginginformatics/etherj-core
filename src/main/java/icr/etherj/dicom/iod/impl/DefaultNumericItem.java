/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.ContentItem.NumericItem;
import icr.etherj.dicom.iod.ContentRelationship;
import java.io.PrintStream;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class DefaultNumericItem extends AbstractContentItem
	implements NumericItem
{
	private int denominator = 0;
	private double floatValue = Double.NaN;
	private int numerator = 0;
	private String numericValue = null;
	private Code units = null;

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ValueType: "+getValueType());
		Code cnc = getConceptNameCode();
		if (cnc != null)
		{
			ps.println(pad+"ConceptNameCode:");
			cnc.display(ps, indent+"  ", recurse);
		}

		if (numericValue != null)
		{
			ps.println(pad+"NumericValue: "+numericValue);
			if (!Double.isNaN(floatValue))
			{
				ps.println(pad+"FloatingPointValue: "+floatValue);
			}
			if ((numerator != 0) && (denominator != 0))
			{
				ps.println(pad+"RationalNumeratorValue: "+numerator);
				ps.println(pad+"RationalDenominatorValue: "+denominator);
			}
			if (units != null)
			{
				ps.println(pad+"MeasurementUnits:");
				units.display(ps, indent+"  ", recurse);
			}
		}

		List<ContentRelationship> relationships = getContentRelationshipList();
		if (relationships.size() > 0)
		{
			ps.println(pad+"Relationships:");
			for (ContentRelationship r : relationships)
			{
				r.display(ps, indent+"  ", recurse);
			}
		}
	}

	@Override
	public double getFloatingPointValue()
	{
		return floatValue;
	}

	@Override
	public Code getMeasurementUnits()
	{
		return units;
	}

	@Override
	public String getNumericValue()
	{
		return numericValue;
	}

	@Override
	public int getRationalDenominatorValue()
	{
		return denominator;
	}

	@Override
	public int getRationalNumeratorValue()
	{
		return numerator;
	}

	@Override
	public void setFloatingPointValue(double value)
	{
		floatValue = value;
	}

	@Override
	public void setMeasurementUnits(Code units) throws IllegalArgumentException
	{
		if (units == null)
		{
			throw new IllegalArgumentException("MeasuerementUnits must not be null");
		}
		this.units = units;
	}

	@Override
	public void setNumericValue(String value) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(value))
		{
			throw new IllegalArgumentException(
				"Value type must not be null or empty");
		}
		numericValue = value;
	}

	@Override
	public void setRationalDenominatorValue(int value)
	{
		denominator = (value > 0) ? value : 0;
	}

	@Override
	public void setRationalNumeratorValue(int value)
	{
		numerator = value;
	}

	@Override
	public void setValueType(String type) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(type))
		{
			throw new IllegalArgumentException(
				"Value type must not be null or empty");
		}
		if (!Constants.Numeric.equals(type))
		{
			throw new IllegalArgumentException(
				"Value type must be "+Constants.Numeric);
		}
		super.setValueType(type);
	}

}