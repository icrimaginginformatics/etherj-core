/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.SourceImage;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultSourceImage extends DefaultReferencedImage
	implements SourceImage
{
	private String locsPreserved;
	private Code purposeCode;
	private boolean requireCode;

	public DefaultSourceImage()
	{
		this(false);
	}

	public DefaultSourceImage(boolean requireCode)
	{
		this.requireCode = requireCode;
		purposeCode = requireCode ? new DefaultCode() : null;
	}

	@Override
	public void cloneTo(SourceImage target)
	{
		super.cloneTo(target);
		if (target instanceof DefaultSourceImage)
		{
			DefaultSourceImage dsi = (DefaultSourceImage) target;
			dsi.requireCode = requireCode;
		}
		target.setPurposeOfReferenceCode(purposeCode);
		target.setSpatialLocationsPreserved(locsPreserved);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		String sopClassUid = getReferencedSopClassUid();
		ps.println(pad+"ReferencedSopClassUid: "+sopClassUid);
		ps.println(pad+"ReferencedSopInstanceUid: "+getReferencedSopInstanceUid());
		if (DicomUtils.isMultiframeImageSopClass(sopClassUid))
		{
			ps.println(pad+"ReferencedFrameNumber: "+getReferencedFrameNumber());
		}
		if (purposeCode != null)
		{
			purposeCode.display(ps, indent+"  ");
		}
	}

	@Override
	public Code getPurposeOfReferenceCode()
	{
		return purposeCode;
	}

	@Override
	public String getSpatialLocationsPreserved()
	{
		return locsPreserved;
	}

	@Override
	public void setPurposeOfReferenceCode(Code code)
		throws IllegalArgumentException
	{
		if (requireCode && (code == null))
		{
			throw new IllegalArgumentException(
				"PurposeOfReferenceCode must not be null");
		}
		purposeCode = code;
	}

	@Override
	public void setSpatialLocationsPreserved(String preserved)
		throws IllegalArgumentException
	{
		if (preserved == null)
		{
			locsPreserved = null;
			return;
		}
		switch (preserved)
		{
			case "YES":
			case "NO":
			case "REORIENTED_ONLY":
				locsPreserved = preserved;
				break;
			default:
				throw new IllegalArgumentException(
					"Invalid value for spatial locations preserved: "+preserved);
		}
	}

}
