/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.Validation;
import icr.etherj.dicom.iod.RtRoiObservation;
import java.io.PrintStream;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
public class DefaultRtRoiObservation extends AbstractDisplayable
	implements RtRoiObservation
{
	private String desc;
	private String label;
	private int obsNum = 1;
	private int refRoiNum = 1;
	private String roiInterp = "";
	private String roiInterpType = "";

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ObservationNumber: "+obsNum);
		ps.println(pad+"ReferencedRoiNumber: "+refRoiNum);
		if ((desc != null) && !desc.isEmpty())
		{
			ps.println(pad+"RoiObservationDescription: "+desc);
		}
		if ((label != null) && !label.isEmpty())
		{
			ps.println(pad+"RoiObservationLabel: "+label);
		}
		if (!roiInterp.isEmpty())
		{
			ps.println(pad+"RoiInterpreter: "+roiInterp);
		}
		if (!roiInterpType.isEmpty())
		{
			ps.println(pad+"RtRoiInterpretedType: "+roiInterpType);
		}
	}

	@Override
	public String getRoiObservationDescription()
	{
		return desc;
	}

	@Override
	public String getRoiObservationLabel()
	{
		return label;
	}

	@Override
	public int getObservationNumber()
	{
		return obsNum;
	}

	@Override
	public int getReferencedRoiNumber()
	{
		return refRoiNum;
	}

	@Override
	public String getRoiInterpreter()
	{
		return roiInterp;
	}

	@Override
	public String getRtRoiIntepretedType()
	{
		return roiInterpType;
	}

	@Override
	public void setRoiObservationDescription(String description)
	{
		desc = description;
	}

	@Override
	public void setRoiObservationLabel(String label)
	{
		this.label = label;
	}

	@Override
	public void setObservationNumber(int number)
	{
		obsNum = number;
	}

	@Override
	public void setReferencedRoiNumber(int number)
	{
		refRoiNum = number;
	}

	@Override
	public void setRoiInterpreter(String interpreter)
	{
		if (interpreter == null)
		{
			roiInterp = "";
			return;
		}
		roiInterp = interpreter;
	}

	@Override
	public void setRtRoiInterpretedType(String type) throws IllegalArgumentException
	{
		if (type == null)
		{
			roiInterpType = "";
			return;
		}
		switch (type)
		{
			case External:
			case PTV:
			case CTV:
			case GTV:
			case TreatedVolume:
			case IrradVolume:
			case Bolus:
			case Avoidance:
			case Organ:
			case Marker:
			case Registration:
			case Isocentre:
			case ContrastAgent:
			case Cavity:
			case BrachyChannel:
			case BrachyAccessory:
			case BrachySourceApplicator:
			case BrachyChannelShield:
			case Support:
			case Fixation:
			case DoseRegion:
			case Control:
			case "":
				roiInterpType = type;
				break;
			default:
				throw new IllegalArgumentException(
					"Invalid RT ROI interpreted type: "+type);
		}
	}

	@Override
	public boolean validate()
	{
		String clazz = "RtRoiObservation";
		boolean interpOk = Validation.type2(clazz, roiInterp, Tag.ROIInterpreter);
		String[] allowable = new String[] {External, PTV, CTV, GTV, TreatedVolume,
			IrradVolume, Bolus, Avoidance, Organ, Marker, Registration, Isocentre,
			ContrastAgent, Cavity, BrachyChannel, BrachyAccessory,
			BrachySourceApplicator, BrachyChannelShield, Support, Fixation,
			DoseRegion, Control};
		boolean typeOk = Validation.type2(clazz, roiInterpType, allowable,
			Tag.RTROIInterpretedType);

		return interpOk && typeOk;
	}
	
}
