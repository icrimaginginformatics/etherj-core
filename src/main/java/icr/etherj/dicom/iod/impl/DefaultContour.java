/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.Coordinate3D;
import icr.etherj.dicom.Validatable;
import icr.etherj.dicom.Validation;
import icr.etherj.dicom.iod.Contour;
import icr.etherj.dicom.iod.ContourImage;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
public final class DefaultContour extends AbstractDisplayable
	implements Contour
{
	private final List<Coordinate3D> coordList = new ArrayList<>();
	private String geomType = ClosedPlanar;
	private final List<ContourImage> imageList = new ArrayList<>();
	private int number = 1;

	@Override
	public boolean addContourImage(ContourImage image)
	{
		return (image != null) ? imageList.add(image) : false;
	}

	@Override
	public boolean addCoordinate(Coordinate3D coord)
	{
		return (coord != null) ? coordList.add(coord) : false;
	}

	@Override
	public boolean addCoordinate(double x, double y, double z)
	{
		return addCoordinate(new Coordinate3D(x, y, z));
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ContourNumber: "+number);
		int nImages = imageList.size();
		ps.println(pad+"ContourImageList: "+nImages+" item"+
			((nImages != 1) ? "s" : ""));
		if (recurse)
		{
			for (ContourImage ci : imageList)
			{
				ci.display(ps, indent+"  ", recurse);
			}
		}
		ps.println(pad+"ContourGeometryType: "+geomType);
		int nCoords = coordList.size();
		ps.println(pad+"NumberOfContourPoints: "+nCoords);
		if (recurse)
		{
			ps.println(pad+"ContourData: ");
			for (Coordinate3D coord : coordList)
			{
				coord.display(ps, indent+"  ", recurse);
			}
		}
	}

	@Override
	public List<Coordinate3D> getContourData()
	{
		return ImmutableList.copyOf(coordList);
	}

	@Override
	public String getContourGeometricType()
	{
		return geomType;
	}

	@Override
	public List<ContourImage> getContourImageList()
	{
		return ImmutableList.copyOf(imageList);
	}

	@Override
	public int getContourNumber()
	{
		return number;
	}

	@Override
	public int getNumberOfContourPoints()
	{
		return coordList.size();
	}

	@Override
	public void setContourGeometricType(String geomType)
		throws IllegalArgumentException
	{
		if ((geomType == null) || geomType.isEmpty())
		{
			throw new IllegalArgumentException(
				"Contour geometry type must not be null or empty");
		}
		switch (geomType)
		{
			case Point:
			case OpenPlanar:
			case OpenNonPlanar:
			case ClosedPlanar:
				this.geomType = geomType;
				break;
			default:
				throw new IllegalArgumentException(
					"Invalid contour geometry type: "+geomType);	
		}
	}

	@Override
	public void setContourNumber(int number)
	{
		this.number = number;
	}

	@Override
	public boolean validate()
	{
		String clazz = "Contour";
		List<Validatable> list = new ArrayList<>();
		list.addAll(imageList);
		boolean ciOk = Validation.type1(clazz, list, 0, Tag.ContourImageSequence);
		String[] allowable = new String[] {Point, OpenPlanar, OpenNonPlanar,
			ClosedPlanar};
		boolean geomTypeOk = Validation.type1(clazz, geomType, allowable,
			Tag.ContourGeometricType);
		// ToDo: Check point count against geomType

		return ciOk && geomTypeOk;
	}
	
}
