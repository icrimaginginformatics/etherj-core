/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;
import icr.etherj.dicom.Validatable;

/**
 * Structure Set ROI from DICOM IOD.
 * @author jamesd
 */
public interface StructureSetRoi extends Displayable, Validatable
{
	/** ROI generation algorithm defined in DICOM IOD. */
	public final String Automatic = "AUTOMATIC";
	/** ROI generation algorithm defined in DICOM IOD. */
	public final String SemiAutomatic = "SEMIAUTOMATIC";
	/** ROI generation algorithm defined in DICOM IOD. */
	public final String Manual = "MANUAL";

	/**
	 * Returns the number uniquely identifying the ROI in the structure set,
	 * type 1.
	 * @return the number
	 */
	int getRoiNumber();

	/**
	 * Returns the referenced frame of reference UID, type 1.
	 * @return the UID
	 */
	String getReferencedFrameOfReferenceUid();

	/**
	 * Returns the ROI name, type 2.
	 * @return the name
	 */
	String getRoiName();

	/**
	 * Returns the ROI description, type 3.
	 * @return the description
	 */
	String getRoiDescription();

	/**
	 *	Returns the ROI generation algorithm, type 2.
	 * @return the algorithm
	 */
	String getRoiGenerationAlgorithm();

	/**
	 * Sets the number uniquely identifying the ROI in the structure set,
	 * type 1.
	 * @param number the number to set
	 */
	void setRoiNumber(int number);

	/**
	 * Sets the referenced frame of reference UID, type 1.
	 * @param uid the UID to set
	 * @throws IllegalArgumentException if the UID is null or empty
	 */
	void setReferencedFrameOfReferenceUid(String uid)
		throws IllegalArgumentException;

	/**
	 * Sets the ROI name, type 2.
	 * @param name the name to set
	 */
	void setRoiName(String name);

	/**
	 * Sets the ROI description, type 3.
	 * @param description the description to set
	 */
	void setRoiDescription(String description);

	/**
	 *	Sets the ROI generation algorithm, type 2, with defined terms:
	 * {@link StructureSetRoi#Automatic}, {@link StructureSetRoi#SemiAutomatic},
	 * {@link StructureSetRoi#Manual}.
	 * @param algorithm the algorithm to set
	 * @throws IllegalArgumentException if the algorithm isn't one of the defined
	 * terms
	 */
	void setRoiGenerationAlgorithm(String algorithm)
		throws IllegalArgumentException;
}
