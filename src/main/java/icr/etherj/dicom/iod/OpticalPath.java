/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;
import java.util.List;

/**
 *
 * @author jamesd
 */
public interface OpticalPath extends Displayable
{

	boolean addIlluminationTypeCode(Code code);

	void clearIlluminationTypeCodes();

	/**
	 * Returns the ICC profile, type 1C. Required if photometric interpretation
	 * is not MONOCHROME2, or if Palette Color Lookup Table Sequence is present.
	 * @return
	 */
	byte[] getIccProfile();

	/**
	 * Returns the illumination colour code, type 1C.
	 * @return
	 */
	Code getIlluminationColourCode();

	/**
	 * Returns the illumination type codes, type 1.
	 * @return
	 */
	List<Code> getIlluminationTypeCodeList();

	/**
	 * Returns the illumination wave length, type 1C.
	 * @return
	 */
	float getIlluminationWaveLength();

	/**
	 * Returns the illuminator type code, type 3.
	 * @return
	 */
	Code getIlluminatorTypeCode();

	/**
	 * Returns the optical path description, type 1.
	 * @return
	 */
	String getOpticalPathDescription();

	/**
	 * Returns the optical path identifier, type 1.
	 * @return
	 */
	String getOpticalPathIdentifier();

	/**
	 * Sets the ICC profile, type 1C. Required if photometric interpretation
	 * is not MONOCHROME2, or if Palette Color Lookup Table Sequence is present.
	 * @param profile
	 */
	void setIccProfile(byte[] profile);

	/**
	 * Sets the illumination wave length, type 1C.
	 * @param wavelength
	 */
	void setIlluminationWaveLength(float wavelength);

	/**
	 * Sets the illumination colour code, type 1.
	 * @param code
	 * @throws IllegalArgumentException
	 */
	void setIlluminationColourCode(Code code) throws IllegalArgumentException;

	/**
	 * Sets the illuminator type code, type 3.
	 * @param code
	 */
	void setIlluminatorTypeCode(Code code);

	/**
	 * Sets the optical path description, type 3.
	 * @param desc
	 */
	void setOpticalPathDescription(String desc);

	/**
	 * Sets the optical path identifier, type 1.
	 * @param ident
	 * @throws IllegalArgumentException
	 */
	void setOpticalPathIdentifier(String ident) throws IllegalArgumentException;

}
