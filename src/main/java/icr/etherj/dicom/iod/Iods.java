/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.dicom.NewTag;
import icr.etherj.dicom.NewUID;
import icr.etherj.dicom.iod.impl.DefaultAlgorithmIdentification;
import icr.etherj.dicom.iod.impl.DefaultAnnotationGroup;
import icr.etherj.dicom.iod.impl.DefaultCode;
import icr.etherj.dicom.iod.impl.DefaultCodeItem;
import icr.etherj.dicom.iod.impl.DefaultCodingSchemeIdentifier;
import icr.etherj.dicom.iod.impl.DefaultContainerComponent;
import icr.etherj.dicom.iod.impl.DefaultContainerIdentifier;
import icr.etherj.dicom.iod.impl.DefaultContainerItem;
import icr.etherj.dicom.iod.impl.DefaultContentRelationship;
import icr.etherj.dicom.iod.impl.DefaultContentTemplate;
import icr.etherj.dicom.iod.impl.DefaultContour;
import icr.etherj.dicom.iod.impl.DefaultContourImage;
import icr.etherj.dicom.iod.impl.DefaultDerivationImage;
import icr.etherj.dicom.iod.impl.DefaultDimensionIndex;
import icr.etherj.dicom.iod.impl.DefaultEnhancedSr;
import icr.etherj.dicom.iod.impl.DefaultFrameContent;
import icr.etherj.dicom.iod.impl.DefaultFunctionalGroupsReferencedImage;
import icr.etherj.dicom.iod.impl.DefaultHeirarchicalSeriesReference;
import icr.etherj.dicom.iod.impl.DefaultHeirarchicalSopInstanceReference;
import icr.etherj.dicom.iod.impl.DefaultHL7v2HeirarchicDesignator;
import icr.etherj.dicom.iod.impl.DefaultImageItem;
import icr.etherj.dicom.iod.impl.DefaultMeasurement;
import icr.etherj.dicom.iod.impl.DefaultMicroscopyBulkSimpleAnnotations;
import icr.etherj.dicom.iod.impl.DefaultNumericItem;
import icr.etherj.dicom.iod.impl.DefaultOpticalPath;
import icr.etherj.dicom.iod.impl.DefaultPersonNameItem;
import icr.etherj.dicom.iod.impl.DefaultPixelMatrixOrigin;
import icr.etherj.dicom.iod.impl.DefaultPixelMeasures;
import icr.etherj.dicom.iod.impl.DefaultPlanePositionSlide;
import icr.etherj.dicom.iod.impl.DefaultReferencedFrameOfReference;
import icr.etherj.dicom.iod.impl.DefaultReferencedImage;
import icr.etherj.dicom.iod.impl.DefaultReferencedInstance;
import icr.etherj.dicom.iod.impl.DefaultReferencedSeries;
import icr.etherj.dicom.iod.impl.DefaultRoiContour;
import icr.etherj.dicom.iod.impl.DefaultRtReferencedSeries;
import icr.etherj.dicom.iod.impl.DefaultRtReferencedStudy;
import icr.etherj.dicom.iod.impl.DefaultRtRoiObservation;
import icr.etherj.dicom.iod.impl.DefaultRtStruct;
import icr.etherj.dicom.iod.impl.DefaultSegmentation;
import icr.etherj.dicom.iod.impl.DefaultSegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.impl.DefaultSourceImage;
import icr.etherj.dicom.iod.impl.DefaultSpatialCoordItem;
import icr.etherj.dicom.iod.impl.DefaultSpecimenDescription;
import icr.etherj.dicom.iod.impl.DefaultSpecimenPreparationStep;
import icr.etherj.dicom.iod.impl.DefaultStructureSetRoi;
import icr.etherj.dicom.iod.impl.DefaultTextItem;
import icr.etherj.dicom.iod.impl.DefaultUidRefItem;
import icr.etherj.dicom.iod.impl.DefaultWholeSlideMicroscopyImage;
import icr.etherj.dicom.iod.impl.DefaultWsmFunctionalGroupsFrame;
import icr.etherj.dicom.iod.impl.DefaultWsmReferencedImage;
import icr.etherj.dicom.iod.impl.GenericContentItem;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import icr.etherj.dicom.iod.module.MicroscopyBulkSimpleAnnotationsModule;
import icr.etherj.dicom.iod.module.Modules;
import icr.etherj.dicom.iod.module.SlideLabelModule;
import java.util.List;
import java.util.concurrent.ExecutorService;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;

/**
 * Utility and factory class for DICOM IOD concept classes.
 * @author jamesd
 */
public class Iods
{
	/**
	 * Returns a new AlgorithmIdentification.
	 * @return
	 */
	public static AlgorithmIdentification algorithmIdentification()
	{
		return algorithmIdentification(true);
	}

	/**
	 * Returns a new AlgorithmIdentification with the defined strict mode.
	 * @param strict
	 * @return
	 */
	public static AlgorithmIdentification algorithmIdentification(boolean strict)
	{
		return new DefaultAlgorithmIdentification(strict);
	}

	/**
	 * Returns a new AnnotationGroup.
	 * @return
	 */
	public static AnnotationGroup annotationGroup()
	{
		return annotationGroup(true);
	}

	/**
	 * Returns a new AnnotationGroup with the defined strict mode.
	 * @param strict
	 * @return
	 */
	public static AnnotationGroup annotationGroup(boolean strict)
	{
		return new DefaultAnnotationGroup(strict);
	}

	/**
	 * Returns a new Code.
	 * @return
	 */
	public static Code code()
	{
		return new DefaultCode();
	}

	/**
	 * Returns a new ContentItem.CodeItem.
	 * @return
	 */
	public static ContentItem.CodeItem codeItem()
	{
		return new DefaultCodeItem();
	}

	/**
	 * Returns a new CodingSchemeIdentifier.
	 * @return
	 */
	public static CodingSchemeIdentifier codingSchemeIdentifier()
	{
		return new DefaultCodingSchemeIdentifier();
	}

	/**
	 * Returns a new ContainerComponent.
	 * @return
	 */
	public static ContainerComponent containerComponent()
	{
		return new DefaultContainerComponent();
	}

	/**
	 * Returns a new ContentItem.ContainerItem.
	 * @return
	 */
	public static ContentItem.ContainerItem containerItem()
	{
		return new DefaultContainerItem();
	}

	/**
	 * Returns a new ContainerIdentifier.
	 * @return
	 */
	public static ContainerIdentifier containerIdentifier()
	{
		return new DefaultContainerIdentifier();
	}

	/**
	 * Returns a new ContentRelationship.
	 * @return
	 */
	public static ContentRelationship contentRelationship()
	{
		return new DefaultContentRelationship();
	}

	/**
	 * Returns a new ContentTemplate.
	 * @return
	 */
	public static ContentTemplate contentTemplate()
	{
		return new DefaultContentTemplate();
	}

	/**
	 * Returns a new Contour.
	 * @return
	 */
	public static Contour contour()
	{
		return new DefaultContour();
	}

	/**
	 * Returns a new ContourImage.
	 * @return
	 */
	public static ContourImage contourImage()
	{
		return new DefaultContourImage();
	}

	/**
	 * Returns a new DerivationImage.
	 * @return
	 */
	public static DerivationImage derivationImage()
	{
		return new DefaultDerivationImage();
	}

	/**
	 * Returns a new DimensionIndex.
	 * @return
	 */
	public static DimensionIndex dimensionIndex()
	{
		return new DefaultDimensionIndex();
	}

	/**
	 * Returns a new EnhancedSr.
	 * @return
	 */
	public static EnhancedSr enhancedSr()
	{
		return new DefaultEnhancedSr();
	}

	/**
	 * Returns a new EnhancedSr.
	 * @param dcm
	 * @return
	 */
	public static EnhancedSr enhancedSr(DicomObject dcm)
	{
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		String sopClassUid = dcm.getString(Tag.SOPClassUID);
		if ((sopClassUid == null) ||
			 !sopClassUid.equals(UID.EnhancedSRStorage))
		{
			throw new IllegalArgumentException(
				"Requires SOP Class of EnhancedSRStorage, found: "+sopClassUid);
		}
		EnhancedSr sr = new DefaultEnhancedSr();
		Modules.unpack(dcm, sr.getPatientModule());
		Modules.unpack(dcm, sr.getClinicalTrialSubjectModule());
		Modules.unpack(dcm, sr.getGeneralStudyModule());
		Modules.unpack(dcm, sr.getPatientStudyModule());
		Modules.unpack(dcm, sr.getClinicalTrialStudyModule());
		Modules.unpack(dcm, sr.getSrDocumentSeriesModule());
		Modules.unpack(dcm, sr.getClinicalTrialSeriesModule());
		Modules.unpack(dcm, sr.getSynchronisationModule());
		Modules.unpack(dcm, sr.getGeneralEquipmentModule());
		Modules.unpack(dcm, sr.getSrDocumentGeneralModule());
		Modules.unpack(dcm, sr.getSrDocumentContentModule());
		Modules.unpack(dcm, sr.getSopCommonModule());

		return sr;
	}

	/**
	 * Returns a new FrameContent.
	 * @return
	 */
	public static FrameContent frameContent()
	{
		return new DefaultFrameContent();
	}

	/**
	 * Returns a new FunctionalGroupsReferencedImage.
	 * @return
	 */
	public static FunctionalGroupsReferencedImage functionalGroupsReferencedImage()
	{
		return new DefaultFunctionalGroupsReferencedImage();
	}

	/**
	 * Returns a new generic ContentItem.
	 * @return
	 */
	public static ContentItem genericContentItem()
	{
		return new GenericContentItem();
	}

	/**
	 * Returns a new HeirarchicalSeriesReference.
	 * @return
	 */
	public static HeirarchicalSeriesReference heirarchicalSeriesReference()
	{
		return new DefaultHeirarchicalSeriesReference();
	}

	/**
	 * Returns a new HeirarchicalSopInstanceReference.
	 * @return
	 */
	public static HeirarchicalSopInstanceReference
		heirarchicalSopInstanceReference()
	{
		return new DefaultHeirarchicalSopInstanceReference();
	}

	/**
	 * Returns a new HL7v2HierarchicDesignator.
	 * @return
	 */
	public static HL7v2HierarchicDesignator hl7v2HierarchicDesignator()
	{
		return new DefaultHL7v2HeirarchicDesignator();
	}

	/**
	 * Returns a new ContentItem.ImageItem.
	 * @return
	 */
	public static ContentItem.ImageItem imageItem()
	{
		return new DefaultImageItem();
	}

	/**
	 * Returns a unique key for the instance.
	 * @param image
	 * @return
	 */
	public static String makeKey(ReferencedImage image)
	{
		return (image != null)
			? image.getReferencedSopInstanceUid()+"."+image.getReferencedFrameNumber()
			: null;
	}

	/**
	 * Returns a new Measurement.
	 * @return
	 */
	public static Measurement measurement()
	{
		return measurement(true);
	}

	/**
	 * Returns a new Measurement with the defined strictness mode.
	 * @param strict
	 * @return
	 */
	public static Measurement measurement(boolean strict)
	{
		return new DefaultMeasurement(strict);
	}

	/**
	 * Returns a new MicroscopyBulkSimpleAnnotations.
	 * @return the MicroscopyBulkSimpleAnnotations
	 */
	public static MicroscopyBulkSimpleAnnotations microscopyAnnotations()
	{
		return microscopyAnnotations(true);
	}

	/**
	 * Returns a new MicroscopyBulkSimpleAnnotations.
	 * @param strict strict standard compliance
	 * @return the MicroscopyBulkSimpleAnnotations
	 */
	public static MicroscopyBulkSimpleAnnotations microscopyAnnotations(
		boolean strict)
	{
		return new DefaultMicroscopyBulkSimpleAnnotations(strict);
	}

	/**
	 * Returns a new MicroscopyBulkSimpleAnnotations created from the DicomObject.
	 * @param dcm the DicomObject
	 * @return the MicroscopyBulkSimpleAnnotations
	 * @throws IllegalArgumentException if the DicomObject is null or does not
	 * contain a valid MicroscopyBulkSimpleAnnotations
	 */
	public static MicroscopyBulkSimpleAnnotations microscopyAnnotations(
		DicomObject dcm)
		throws IllegalArgumentException
	{
		return microscopyAnnotations(dcm, true);
	}

	/**
	 * Returns a new MicroscopyBulkSimpleAnnotations created from the DicomObject.
	 * @param dcm the DicomObject
	 * @param strict strict standard compliance
	 * @return the MicroscopyBulkSimpleAnnotations
	 * @throws IllegalArgumentException if the DicomObject is null or does not
	 * contain a valid MicroscopyBulkSimpleAnnotations
	 */
	public static MicroscopyBulkSimpleAnnotations microscopyAnnotations(
		DicomObject dcm, boolean strict)
	{
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		String sopClassUid = dcm.getString(Tag.SOPClassUID);
		if ((sopClassUid == null) ||
			 !sopClassUid.equals(NewUID.MicroscopyBulkSimpleAnnotationsStorage))
		{
			throw new IllegalArgumentException(
				"Requires SOP Class of MicroscopyBulkSimpleAnnotationsStorage, found: "+
					sopClassUid);
		}
		MicroscopyBulkSimpleAnnotations mbsa =
			new DefaultMicroscopyBulkSimpleAnnotations(strict);
		Modules.unpack(dcm, mbsa.getPatientModule());
		Modules.unpack(dcm, mbsa.getClinicalTrialSubjectModule());
		Modules.unpack(dcm, mbsa.getGeneralStudyModule());
		Modules.unpack(dcm, mbsa.getPatientStudyModule());
		Modules.unpack(dcm, mbsa.getClinicalTrialStudyModule());
		Modules.unpack(dcm, mbsa.getMicroscopyBulkSimpleAnnotationsSeriesModule());
		Modules.unpack(dcm, mbsa.getClinicalTrialSeriesModule());
		// Check AnnotationCoordinateType
		String coordType = dcm.getString(
			NewTag.AnnotationCoordinateType, VR.CS, null);
		if (Constants.Coord3D.equals(coordType))
		{
			Modules.unpack(dcm, mbsa.getFrameOfReferenceModule());
		}
		Modules.unpack(dcm, mbsa.getEnhancedEquipmentModule());
		Modules.unpack(dcm, mbsa.getMicroscopyBulkSimpleAnnotationsModule());
		Modules.unpack(dcm, mbsa.getCommonInstanceReferenceModule());
		Modules.unpack(dcm, mbsa.getSopCommonModule());

		return mbsa;
	}

	/**
	 * Returns a new ContentItem.NumericItem.
	 * @return
	 */
	public static ContentItem.NumericItem numericItem()
	{
		return new DefaultNumericItem();
	}

	/**
	 * Returns a new OpticalPath.
	 * @return
	 */
	public static OpticalPath opticalPath()
	{
		return new DefaultOpticalPath();
	}

	/**
	 * Packs the EnhancedSr into the DicomObject.
	 * @param enhSr the source EnhancedSr
	 * @param dcm the target DicomObject
	 * @throws IllegalArgumentException if either argument is null
	 */
	public static void pack(EnhancedSr enhSr, DicomObject dcm)
		throws IllegalArgumentException
	{
		if (enhSr == null)
		{
			throw new IllegalArgumentException("EnhancedSR must not be null");
		}
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		dcm.initFileMetaInformation(UID.EnhancedSRStorage,
			enhSr.getSopInstanceUid(), UID.ExplicitVRLittleEndian);
		Modules.pack(enhSr.getPatientModule(), dcm);
		Modules.pack(enhSr.getClinicalTrialSubjectModule(), dcm);
		Modules.pack(enhSr.getGeneralStudyModule(), dcm);
		Modules.pack(enhSr.getPatientStudyModule(), dcm);
		Modules.pack(enhSr.getClinicalTrialStudyModule(), dcm);
		Modules.pack(enhSr.getSrDocumentSeriesModule(), dcm);
		Modules.pack(enhSr.getClinicalTrialSeriesModule(), dcm);
		Modules.pack(enhSr.getGeneralEquipmentModule(), dcm);
		Modules.pack(enhSr.getSynchronisationModule(), dcm);
		Modules.pack(enhSr.getSrDocumentGeneralModule(), dcm);
		Modules.pack(enhSr.getSrDocumentContentModule(), dcm);
		Modules.pack(enhSr.getSopCommonModule(), dcm);
	}

	/**
	 * Packs the MicroscopyBulkSimpleAnnotations into the DicomObject.
	 * @param mbsa
	 * @param dcm the target DicomObject
	 * @throws IllegalArgumentException if either argument is null
	 */
	public static void pack(MicroscopyBulkSimpleAnnotations mbsa,
		DicomObject dcm)
		throws IllegalArgumentException
	{
		if (mbsa == null)
		{
			throw new IllegalArgumentException(
				"MicroscopyBulkSimpleAnnotations must not be null");
		}
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		dcm.initFileMetaInformation(NewUID.MicroscopyBulkSimpleAnnotationsStorage,
			mbsa.getSopInstanceUid(), UID.ImplicitVRLittleEndian);
		String scs = mbsa.getSpecificCharacterSet();
		if (scs != null)
		{
			dcm.putString(Tag.SpecificCharacterSet, VR.CS, scs);
		}
		Modules.pack(mbsa.getPatientModule(), dcm);
		Modules.pack(mbsa.getClinicalTrialSubjectModule(), dcm);
		Modules.pack(mbsa.getGeneralStudyModule(), dcm);
		Modules.pack(mbsa.getPatientStudyModule(), dcm);
		Modules.pack(mbsa.getClinicalTrialStudyModule(), dcm);
		Modules.pack(mbsa.getMicroscopyBulkSimpleAnnotationsSeriesModule(), dcm);
		Modules.pack(mbsa.getClinicalTrialSeriesModule(), dcm);
		// Check AnnotationCoordinateType
		MicroscopyBulkSimpleAnnotationsModule mbsaModule =
			mbsa.getMicroscopyBulkSimpleAnnotationsModule();
		Modules.pack(mbsaModule, dcm);
		String coordType = mbsaModule.getAnnotationCoordinateType();
		if (Constants.Coord3D.equals(coordType))
		{
			Modules.pack(mbsa.getFrameOfReferenceModule(), dcm);
		}
		Modules.pack(mbsa.getEnhancedEquipmentModule(), dcm);
		Modules.pack(mbsa.getCommonInstanceReferenceModule(), dcm);
		Modules.pack(mbsa.getSopCommonModule(), dcm);
	}

	/**
	 * Packs the RtStruct into the DicomObject.
	 * @param rtStruct the source RtStruct
	 * @param dcm the target DicomObject
	 * @throws IllegalArgumentException if either argument is null
	 */
	public static void pack(RtStruct rtStruct, DicomObject dcm)
	{
		if (rtStruct == null)
		{
			throw new IllegalArgumentException("RTSTRUCT must not be null");
		}
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		dcm.initFileMetaInformation(UID.RTStructureSetStorage,
			rtStruct.getSopInstanceUid(), UID.ImplicitVRLittleEndian);
		String scs = rtStruct.getSpecificCharacterSet();
		if (scs != null)
		{
			dcm.putString(Tag.SpecificCharacterSet, VR.CS, scs);
		}
		Modules.pack(rtStruct.getPatientModule(), dcm);
		Modules.pack(rtStruct.getClinicalTrialSubjectModule(), dcm);
		Modules.pack(rtStruct.getGeneralStudyModule(), dcm);
		Modules.pack(rtStruct.getPatientStudyModule(), dcm);
		Modules.pack(rtStruct.getClinicalTrialStudyModule(), dcm);
		Modules.pack(rtStruct.getRtSeriesModule(), dcm);
		Modules.pack(rtStruct.getGeneralEquipmentModule(), dcm);
		Modules.pack(rtStruct.getFrameOfReferenceModule(), dcm);
		Modules.pack(rtStruct.getStructureSetModule(), dcm);
		Modules.pack(rtStruct.getRoiContourModule(), dcm);
		Modules.pack(rtStruct.getRtRoiObservationsModule(), dcm);
		Modules.pack(rtStruct.getGeneralReferenceModule(), dcm);
		Modules.pack(rtStruct.getSopCommonModule(), dcm);
		Modules.pack(rtStruct.getCommonInstanceReferenceModule(), dcm);
	}

	/**
	 * Packs the Segmentation into the DicomObject.
	 * @param segmentation
	 * @param dcm the target DicomObject
	 * @throws IllegalArgumentException if either argument is null
	 */
	public static void pack(Segmentation segmentation, DicomObject dcm)
		throws IllegalArgumentException
	{
		if (segmentation == null)
		{
			throw new IllegalArgumentException("Segmentation must not be null");
		}
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		dcm.initFileMetaInformation(UID.SegmentationStorage,
			segmentation.getSopInstanceUid(), UID.ImplicitVRLittleEndian);
		String scs = segmentation.getSpecificCharacterSet();
		if (scs != null)
		{
			dcm.putString(Tag.SpecificCharacterSet, VR.CS, scs);
		}
		Modules.pack(segmentation.getSopCommonModule(), dcm);
		Modules.pack(segmentation.getPatientModule(), dcm);
		Modules.pack(segmentation.getClinicalTrialSubjectModule(), dcm);
		Modules.pack(segmentation.getGeneralStudyModule(), dcm);
		Modules.pack(segmentation.getPatientStudyModule(), dcm);
		Modules.pack(segmentation.getClinicalTrialStudyModule(), dcm);
		Modules.pack(segmentation.getSegmentationSeriesModule(), dcm);
		FrameOfReferenceModule forModule = segmentation.getFrameOfReferenceModule();
		if (forModule.getFrameOfReferenceUid() != null)
		{
			Modules.pack(forModule, dcm);
		}
		Modules.pack(segmentation.getFrameOfReferenceModule(), dcm);
		Modules.pack(segmentation.getEnhancedEquipmentModule(), dcm);
		Modules.pack(segmentation.getGeneralReferenceModule(), dcm);
		Modules.pack(segmentation.getImagePixelModule(), dcm);
		Modules.pack(segmentation.getSegmentationImageModule(), dcm);
		Modules.pack(segmentation.getMultiframeFunctionalGroupsModule(), dcm);
		Modules.pack(segmentation.getMultiframeDimensionModule(), dcm);
		List<FunctionalGroupsFrame> frames = 
			segmentation.getMultiframeFunctionalGroupsModule()
				.getPerFrameFunctionalGroups().getFrameList();
		if (!frames.isEmpty())
		{
			SegmentationFunctionalGroupsFrame frame =
				(SegmentationFunctionalGroupsFrame) frames.get(0);
			if (!frame.getDerivationImages().isEmpty())
			{
				Modules.pack(segmentation.getCommonInstanceReferenceModule(), dcm);
			}
		}
	}

	/**
	 * Packs the WholeSlideMicroscopyImage into the DicomObject.
	 * @param wsmImage
	 * @param dcm the target DicomObject
	 * @throws IllegalArgumentException if either argument is null
	 */
	public static void pack(WholeSlideMicroscopyImage wsmImage, DicomObject dcm)
		throws IllegalArgumentException
	{
		pack(wsmImage, dcm, UID.ImplicitVRLittleEndian);
	}

	/**
	 * Packs the WholeSlideMicroscopyImage into the DicomObject.
	 * @param wsmImage
	 * @param dcm the target DicomObject
	 * @param xferSyntaxUid
	 * @throws IllegalArgumentException if either argument is null
	 */
	public static void pack(WholeSlideMicroscopyImage wsmImage, DicomObject dcm,
		String xferSyntaxUid)
		throws IllegalArgumentException
	{
		if (wsmImage == null)
		{
			throw new IllegalArgumentException(
				"WholeSlideMicroscopyImage must not be null");
		}
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		if (!UID.ImplicitVRLittleEndian.equals(xferSyntaxUid) &&
			 !UID.ExplicitVRLittleEndian.equals(xferSyntaxUid) &&
			 !UID.JPEGBaseline1.equals(xferSyntaxUid))
		{
			throw new IllegalArgumentException(
				"Unsupported TransferSyntaxUID: "+xferSyntaxUid);
		}
		dcm.initFileMetaInformation(UID.VLWholeSlideMicroscopyImageStorage,
			wsmImage.getSopInstanceUid(), xferSyntaxUid);
		String scs = wsmImage.getSpecificCharacterSet();
		if (scs != null)
		{
			dcm.putString(Tag.SpecificCharacterSet, VR.CS, scs);
		}
		Modules.pack(wsmImage.getSopCommonModule(), dcm);
		Modules.pack(wsmImage.getPatientModule(), dcm);
		Modules.pack(wsmImage.getClinicalTrialSubjectModule(), dcm);
		Modules.pack(wsmImage.getGeneralStudyModule(), dcm);
		Modules.pack(wsmImage.getPatientStudyModule(), dcm);
		Modules.pack(wsmImage.getClinicalTrialStudyModule(), dcm);
		Modules.pack(wsmImage.getWholeSlideMicroscopySeriesModule(), dcm);
		Modules.pack(wsmImage.getClinicalTrialSeriesModule(), dcm);
		FrameOfReferenceModule forModule = wsmImage.getFrameOfReferenceModule();
		if (forModule.getFrameOfReferenceUid() != null)
		{
			Modules.pack(forModule, dcm);
		}
		Modules.pack(wsmImage.getFrameOfReferenceModule(), dcm);
		Modules.pack(wsmImage.getEnhancedEquipmentModule(), dcm);
		Modules.pack(wsmImage.getGeneralImageModule(), dcm);
		Modules.pack(wsmImage.getGeneralReferenceModule(), dcm);
		Modules.pack(wsmImage.getImagePixelModule(), dcm);
		Modules.pack(wsmImage.getAcquisitionContextModule(), dcm);
		Modules.pack(wsmImage.getMultiframeFunctionalGroupsModule(), dcm);
//		List<FunctionalGroupsFrame> frames = 
//			wsmImage.getMultiframeFunctionalGroupsModule()
//				.getPerFrameFunctionalGroups().getFrameList();
//		if (!frames.isEmpty())
//		{
//			SegmentationFunctionalGroupsFrame frame =
//				(SegmentationFunctionalGroupsFrame) frames.get(0);
//			if (!frame.getDerivationImages().isEmpty())
//			{
//				Modules.pack(wsmImage.getCommonInstanceReferenceModule(), dcm);
//			}
//		}
		Modules.pack(wsmImage.getMultiframeDimensionModule(), dcm);
		Modules.pack(wsmImage.getSpecimenModule(), dcm);
		Modules.pack(wsmImage.getWholeSlideMicroscopyImageModule(), dcm);
		Modules.pack(wsmImage.getOpticalPathModule(), dcm);
		String[] imageType = wsmImage.getGeneralImageModule().getImageType();
		if (Constants.Localizer.equals(imageType[2]))
		{
			Modules.pack(wsmImage.getMultiResolutionNavigationModule(), dcm);
		}
		SlideLabelModule slideLabel = wsmImage.getSlideLabelModule();
		if (Constants.Label.equals(imageType[2]) || slideLabel.isEnabled())
		{
			Modules.pack(slideLabel, dcm);
		}
		Modules.pack(wsmImage.getCommonInstanceReferenceModule(), dcm);
	}

	/**
	 * Returns a new ContentItem.PersonNameItem.
	 * @return
	 */
	public static ContentItem.PersonNameItem personNameItem()
	{
		return new DefaultPersonNameItem();
	}

	/**
	 * Returns a new PixelMatrixOrigin.
	 * @return
	 */
	public static PixelMatrixOrigin pixelMatrixOrigin()
	{
		return new DefaultPixelMatrixOrigin();
	}

	/**
	 * Returns a new PixelMeasures.
	 * @return
	 */
	public static PixelMeasures pixelMeasures()
	{
		return new DefaultPixelMeasures();
	}

	/**
	 * Returns a new PlanePositionSlide.
	 * @return
	 */
	public static PlanePositionSlide planePositionSlide()
	{
		return new DefaultPlanePositionSlide();
	}

	/**
	 * Returns a new ReferencedFrameOfReference.
	 * @return
	 */
	public static ReferencedFrameOfReference referencedFrameOfReference()
	{
		return new DefaultReferencedFrameOfReference();
	}

	/**
	 * Returns a new ReferencedImage.
	 * @return
	 */
	public static ReferencedImage referencedImage()
	{
		return new DefaultReferencedImage();
	}

	/**
	 * Returns a new ReferencedInstance.
	 * @return
	 */
	public static ReferencedInstance referencedInstance()
	{
		return new DefaultReferencedInstance();
	}

	/**
	 * Returns a new ReferencedSeries.
	 * @return
	 */
	public static ReferencedSeries referencedSeries()
	{
		return new DefaultReferencedSeries();
	}

	/**
	 * Returns a new RoiContour.
	 * @return
	 */
	public static RoiContour roiContour()
	{
		return new DefaultRoiContour();
	}

	/**
	 * Returns a new RtReferencedSeries.
	 * @return
	 */
	public static RtReferencedSeries rtReferencedSeries()
	{
		return new DefaultRtReferencedSeries();
	}

	/**
	 * Returns a new RtReferencedStudy.
	 * @return
	 */
	public static RtReferencedStudy rtReferencedStudy()
	{
		return new DefaultRtReferencedStudy();
	}

	/**
	 * Returns a new RtRoiObservation.
	 * @return
	 */
	public static RtRoiObservation rtRoiObservation()
	{
		return new DefaultRtRoiObservation();
	}

	/**
	 * Returns a new RtStruct.
	 * @return the RtStruct
	 */
	public static RtStruct rtStruct()
	{
		return new DefaultRtStruct();
	}

	/**
	 * Returns a new RtStruct created from the DicomObject.
	 * @param dcm the DicomObject
	 * @return the RtStruct
	 * @throws IllegalArgumentException if the DicomObject is null or does not
	 * contain a valid RtStruct
	 */
	public static RtStruct rtStruct(DicomObject dcm)
		throws IllegalArgumentException
	{
		return rtStruct(dcm, null);
	}

	/**
	 * Returns a new RtStruct created from the DicomObject.
	 * @param dcm the DicomObject
	 * @param service
	 * @return the RtStruct
	 * @throws IllegalArgumentException if the DicomObject is null or does not
	 * contain a valid RtStruct
	 */
	public static RtStruct rtStruct(DicomObject dcm, ExecutorService service)
		throws IllegalArgumentException
	{
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		String sopClassUid = dcm.getString(Tag.SOPClassUID);
		if ((sopClassUid == null) ||
			 !sopClassUid.equals(UID.RTStructureSetStorage))
		{
			throw new IllegalArgumentException(
				"Requires SOP Class of RTStructureSetStorage, found: "+sopClassUid);
		}
		RtStruct rtStruct = new DefaultRtStruct();
		rtStruct.setSpecificCharacterSet(
			dcm.getString(Tag.SpecificCharacterSet, VR.CS, null));
		Modules.unpack(dcm, rtStruct.getPatientModule());
		Modules.unpack(dcm, rtStruct.getClinicalTrialSubjectModule());
		Modules.unpack(dcm, rtStruct.getGeneralStudyModule());
		Modules.unpack(dcm, rtStruct.getPatientStudyModule());
		Modules.unpack(dcm, rtStruct.getClinicalTrialStudyModule());
		Modules.unpack(dcm, rtStruct.getRtSeriesModule());
		Modules.unpack(dcm, rtStruct.getGeneralEquipmentModule());
		Modules.unpack(dcm, rtStruct.getFrameOfReferenceModule());
		Modules.unpack(dcm, rtStruct.getStructureSetModule());
		Modules.unpack(dcm, rtStruct.getRoiContourModule(), service);
		Modules.unpack(dcm, rtStruct.getRtRoiObservationsModule());
		Modules.unpack(dcm, rtStruct.getGeneralReferenceModule());
		Modules.unpack(dcm, rtStruct.getSopCommonModule());
		Modules.unpack(dcm, rtStruct.getCommonInstanceReferenceModule());

		return rtStruct;
	}

	/**
	 * Returns a new Segmentation.
	 * @return the Segmentation
	 */
	public static Segmentation segmentation()
	{
		return segmentation(true);
	}

	/**
	 * Returns a new Segmentation.
	 * @param strict strict standard compliance
	 * @return the Segmentation
	 */
	public static Segmentation segmentation(boolean strict)
	{
		return new DefaultSegmentation(strict);
	}

	/**
	 * Returns a new Segmentation created from the DicomObject.
	 * @param dcm the DicomObject
	 * @return the Segmentation
	 * @throws IllegalArgumentException if the DicomObject is null or does not
	 * contain a valid Segmentation
	 */
	public static Segmentation segmentation(DicomObject dcm)
		throws IllegalArgumentException
	{
		return segmentation(dcm, true);
	}

	/**
	 * Returns a new Segmentation created from the DicomObject.
	 * @param dcm the DicomObject
	 * @param strict strict standard compliance
	 * @return the Segmentation
	 * @throws IllegalArgumentException if the DicomObject is null or does not
	 * contain a valid Segmentation
	 */
	public static Segmentation segmentation(DicomObject dcm, boolean strict)
		throws IllegalArgumentException
	{
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		String sopClassUid = dcm.getString(Tag.SOPClassUID);
		if ((sopClassUid == null) ||
			 !sopClassUid.equals(UID.SegmentationStorage))
		{
			throw new IllegalArgumentException(
				"Requires SOP Class of SegmentationStorage, found: "+sopClassUid);
		}
		Segmentation segmentation = new DefaultSegmentation(strict);
		segmentation.setSpecificCharacterSet(
			dcm.getString(Tag.SpecificCharacterSet, VR.CS, null));
		Modules.unpack(dcm, segmentation.getPatientModule());
		Modules.unpack(dcm, segmentation.getClinicalTrialSubjectModule());
		Modules.unpack(dcm, segmentation.getGeneralStudyModule());
		Modules.unpack(dcm, segmentation.getPatientStudyModule());
		Modules.unpack(dcm, segmentation.getClinicalTrialStudyModule());
		Modules.unpack(dcm, segmentation.getSegmentationSeriesModule());
		String forUid = dcm.getString(Tag.FrameOfReferenceUID, VR.UI, null);
		if (forUid != null)
		{
			Modules.unpack(dcm, segmentation.getFrameOfReferenceModule());
		}
		Modules.unpack(dcm, segmentation.getEnhancedEquipmentModule());
		Modules.unpack(dcm, segmentation.getGeneralReferenceModule());
		Modules.unpack(dcm, segmentation.getImagePixelModule());
		Modules.unpack(dcm, segmentation.getSegmentationImageModule());
		Modules.unpack(dcm, segmentation.getMultiframeFunctionalGroupsModule());
		Modules.unpack(dcm, segmentation.getMultiframeDimensionModule());
		List<FunctionalGroupsFrame> frames = 
			segmentation.getMultiframeFunctionalGroupsModule()
				.getPerFrameFunctionalGroups().getFrameList();
		if (!frames.isEmpty())
		{
			SegmentationFunctionalGroupsFrame frame =
				(SegmentationFunctionalGroupsFrame) frames.get(0);
			if (!frame.getDerivationImages().isEmpty())
			{
				Modules.unpack(dcm, segmentation.getCommonInstanceReferenceModule());
			}
		}
		Modules.unpack(dcm, segmentation.getSopCommonModule());

		return segmentation;
	}

	/**
	 * Returns a new SegmentationFunctionalGroupsFrame.
	 * @return
	 */
	public static SegmentationFunctionalGroupsFrame segmentationFrame()
	{
		return new DefaultSegmentationFunctionalGroupsFrame();
	}

	/**
	 * Returns a new ContentItem.SpatialCoordItem.
	 * @return
	 */
	public static ContentItem.SpatialCoordItem spatialCoordItem()
	{
		return new DefaultSpatialCoordItem();
	}

	/**
	 * Returns a new SourceImage.
	 * @param required if PurposeOfReferenceCode is required
	 * @return
	 */
	public static SourceImage sourceImage(boolean required)
	{
		return new DefaultSourceImage(required);
	}

	/**
	 * Returns a new SpecimenDescription.
	 * @return
	 */
	public static SpecimenDescription specimenDescription()
	{
		return new DefaultSpecimenDescription();
	}

	/**
	 * Returns a new SpecimenPreparationStep.
	 * @return
	 */
	public static SpecimenPreparationStep specimenPreparationStep()
	{
		return new DefaultSpecimenPreparationStep();
	}

	/**
	 * Returns a new StructureSetRoi.
	 * @return
	 */
	public static StructureSetRoi structureSetRoi()
	{
		return new DefaultStructureSetRoi();
	}

	/**
	 * Returns a new ContentItem.TextItem.
	 * @return
	 */
	public static ContentItem.TextItem textItem()
	{
		return new DefaultTextItem();
	}

	/**
	 * Returns a new ContentItem.UidRefItem.
	 * @return
	 */
	public static ContentItem.UidRefItem uidRefItem()
	{
		return new DefaultUidRefItem();
	}

	/**
	 * Returns a new WsmFunctionalGroupsFrame.
	 * @return
	 */
	public static WsmFunctionalGroupsFrame wholeSlideMicroscopyFrame()
	{
		return new DefaultWsmFunctionalGroupsFrame();
	}

	/**
	 * Returns a new WholeSlideMicroscopyImage.
	 * @return the WholeSlideMicroscopyImage
	 */
	public static WholeSlideMicroscopyImage wholeSlideMicroscopyImage()
	{
		return wholeSlideMicroscopyImage(true);
	}

	/**
	 * Returns a new WholeSlideMicroscopyImage.
	 * @param strict strict standard compliance
	 * @return the WholeSlideMicroscopyImage
	 */
	public static WholeSlideMicroscopyImage wholeSlideMicroscopyImage(
		boolean strict)
	{
		return new DefaultWholeSlideMicroscopyImage(strict);
	}

	/**
	 * Returns a new WholeSlideMicroscopyImage created from the DicomObject.
	 * @param dcm the DicomObject
	 * @return the Segmentation
	 * @throws IllegalArgumentException if the DicomObject is null or does not
	 * contain a valid WholeSlideMicroscopyImage
	 */
	public static WholeSlideMicroscopyImage wholeSlideMicroscopyImage(
		DicomObject dcm) throws IllegalArgumentException
	{
		return wholeSlideMicroscopyImage(dcm, true);
	}

	/**
	 * Returns a new WholeSlideMicroscopyImage created from the DicomObject.
	 * @param dcm the DicomObject
	 * @param strict strict standard compliance
	 * @return the WholeSlideMicroscopyImage
	 * @throws IllegalArgumentException if the DicomObject is null or does not
	 * contain a valid WholeSlideMicroscopyImage
	 */
	public static WholeSlideMicroscopyImage wholeSlideMicroscopyImage(
		DicomObject dcm, boolean strict) throws IllegalArgumentException
	{
		if (dcm == null)
		{
			throw new IllegalArgumentException("DICOM object must not be null");
		}
		String sopClassUid = dcm.getString(Tag.SOPClassUID);
		if ((sopClassUid == null) ||
			 !sopClassUid.equals(UID.VLWholeSlideMicroscopyImageStorage))
		{
			throw new IllegalArgumentException(
				"Requires SOP Class of VLWholeSlideMicroscopyImageStorage, found: "
					+sopClassUid);
		}
		String scs = dcm.getString(Tag.SpecificCharacterSet, VR.CS, null);
		if (scs != null)
		{
			dcm.putString(Tag.SpecificCharacterSet, VR.CS, scs);
		}
		WholeSlideMicroscopyImage wsmImage =
			new DefaultWholeSlideMicroscopyImage(strict);
		wsmImage.setSpecificCharacterSet(
			dcm.getString(Tag.SpecificCharacterSet, VR.CS, null));
		Modules.unpack(dcm, wsmImage.getPatientModule());
		Modules.unpack(dcm, wsmImage.getClinicalTrialSubjectModule());
		Modules.unpack(dcm, wsmImage.getGeneralStudyModule());
		Modules.unpack(dcm, wsmImage.getPatientStudyModule());
		Modules.unpack(dcm, wsmImage.getClinicalTrialStudyModule());
		Modules.unpack(dcm, wsmImage.getGeneralSeriesModule());
		Modules.unpack(dcm, wsmImage.getWholeSlideMicroscopySeriesModule());
		Modules.unpack(dcm, wsmImage.getClinicalTrialSeriesModule());
		String forUid = dcm.getString(Tag.FrameOfReferenceUID, VR.UI, null);
		if (forUid != null)
		{
			Modules.unpack(dcm, wsmImage.getFrameOfReferenceModule());
		}
		Modules.unpack(dcm, wsmImage.getEnhancedEquipmentModule());
		Modules.unpack(dcm, wsmImage.getGeneralImageModule());
		Modules.unpack(dcm, wsmImage.getGeneralReferenceModule());
		Modules.unpack(dcm, wsmImage.getImagePixelModule());
		Modules.unpack(dcm, wsmImage.getAcquisitionContextModule());
		Modules.unpack(dcm, wsmImage.getMultiframeDimensionModule());
		Modules.unpack(dcm, wsmImage.getMultiframeFunctionalGroupsModule());
		Modules.unpack(dcm, wsmImage.getSpecimenModule());
		Modules.unpack(dcm, wsmImage.getWholeSlideMicroscopyImageModule());
		Modules.unpack(dcm, wsmImage.getOpticalPathModule());
		Modules.unpack(dcm, wsmImage.getMultiResolutionNavigationModule());
		Modules.unpack(dcm, wsmImage.getSlideLabelModule());
		Modules.unpack(dcm, wsmImage.getSopCommonModule());
		Modules.unpack(dcm, wsmImage.getCommonInstanceReferenceModule());

		return wsmImage;
	}

	/**
	 * Returns a new WsmReferencedImage.
	 * @return
	 */
	public static WsmReferencedImage wholeSlideMicroscopyReferencedImage()
	{
		return new DefaultWsmReferencedImage();
	}

	//	Prevent instantiation.
	private Iods()
	{}

}
