/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;
import java.util.List;

/**
 *
 * @author jamesd
 */
public interface AnnotationGroup extends Displayable, Iod
{
	boolean addAlgorithmIdentification(AlgorithmIdentification ident);

	boolean addMeasurement(Measurement measurement);

	/**
	 * Returns the annotation group algorithm identifications, type 1C. Required
	 * if generation type is AUTOMATIC or SEMIAUTOMATIC.
	 * @return 
	 */
	List<AlgorithmIdentification> getAlgorithmIdentificationList();

	/**
	 * Returns whether the annotations in the group apply to all optical paths,
	 * type 1.
	 * @return 
	 */
	boolean getAnnotationAppliesToAllOpticalPaths();

	/**
	 * Returns whether the annotations in the group apply to full thickness of 
	 * tissue in the slide, type 1C. Required if annotation coordinate type is
	 * 3D.
	 * @return 
	 */
	boolean getAnnotationAppliesToAllZPlanes();

	/**
	 * Returns the annotation group description, type 3.
	 * @return 
	 */
	String getAnnotationGroupDescription();

	/**
	 * Returns the algorithm used to generate Annotations, type 1. Enumerated
	 * values: AUTOMATIC, SEMIAUTOMATIC, MANUAL.
	 * @return 
	 */
	String getAnnotationGroupGenerationType();

	/**
	 * Returns the annotation group label, type 1. User-defined label identifying
	 * this AnnotationGroup. This may be the same as CodeMeaning of
	 * AnnotationPropertyTypeCodeSequence.
	 * @return 
	 */
	String getAnnotationGroupLabel();

	/**
	 * Returns the annotation group number, type 1. Uniquely identifies an
	 * annotation group within a SOP Instance. Shall start at a value of 1, and
	 * increase monotonically by 1.
	 * @return 
	 */
	int getAnnotationGroupNumber();

	/**
	 * Returns the annotation group UID, type 1.
	 * @return 
	 */
	String getAnnotationGroupUID();

	/**
	 * Returns the annotation property category code, type 1.
	 * @return 
	 */
	Code getAnnotationPropertyCategoryCode();

	/**
	 * Returns the annotation property type code, type 1.
	 * @return 
	 */
	Code getAnnotationPropertyTypeCode();

	/**
	 * Returns the z coordinate(s) common to all annotations in the group, type
	 * 1C. Required if annotation coordinate type is 3D and all annotations are
	 * in the same plane.
	 * @return 
	 */
	double[] getCommonZCoordinateValue();

	/**
	 * Returns the coordinates of one or more points that define the annotations,
	 * encoded in (x,y) or (x,y,z) order, type 1C. Required if
	 * PointCoordinatesData not present.
	 * @return 
	 */
	double[] getDoublePointCoordinatesData();

	/**
	 * Returns the graphic type of annotations within the group, type 1.
	 * Enumerated values: POINT, POLYLINE, POLYGON, ELLIPSE, RECTANGLE.
	 * @return 
	 */
	String getGraphicType();

	/**
	 * Returns the list of point indices, type 1C. Required if graphic type is
	 * POLYLINE or POLYGON.
	 * @return 
	 */
	int[] getLongPrimitivePointIndexList();

	/**
	 * Returns the measurements for annotations in the group, type 3.
	 * @return 
	 */
	List<Measurement> getMeasurementList();

	/**
	 * Returns the number of annotations in the group, type 1.
	 * @return 
	 */
	int getNumberOfAnnotations();

	/**
	 * Returns the coordinates of one or more points that define the annotations,
	 * encoded in (x,y) or (x,y,z) order, type 1C. Required if
	 * DoublePointCoordinatesData not present.
	 * @return 
	 */
	float[] getPointCoordinatesData();

	/**
	 * Returns the optical path identifier(s) to which this group applies, type
	 * 1C. Required if group does not apply to all optical paths.
	 * @return 
	 */
	String[] getReferencedOpticalPathIdentifier();

	boolean removeAlgorithmIdentification(AlgorithmIdentification ident);

	boolean removeMeasurement(Measurement measurement);

	/**
	 * Sets whether the annotations in the group apply to all optical paths,
	 * type 1. 
	 * @param value
	 */
	void setAnnotationAppliesToAllOpticalPaths(boolean value);

	/**
	 * Sets whether the annotations in the group apply to full thickness of 
	 * tissue in the slide, type 1C. Required if annotation coordinate type is
	 * 3D. 
	 * @param value
	 */
	void setAnnotationAppliesToAllZPlanes(boolean value);

	/**
	 * Sets the annotation group description, type 3. 
	 * @param description
	 */
	void setAnnotationGroupDescription(String description);

	/**
	 * Sets the algorithm used to generate Annotations, type 1. Enumerated
	 * values: AUTOMATIC, SEMIAUTOMATIC, MANUAL. 
	 * @param type
	 */
	void setAnnotationGroupGenerationType(String type)
		throws IllegalArgumentException;

	/**
	 * Sets the annotation group label, type 1. User-defined label identifying
	 * this AnnotationGroup. This may be the same as CodeMeaning of
	 * AnnotationPropertyTypeCodeSequence. 
	 * @param label
	 */
	void setAnnotationGroupLabel(String label) throws IllegalArgumentException;

	/**
	 * Sets the annotation group number, type 1. Uniquely identifies an
	 * annotation group within a SOP Instance. Shall start at a value of 1, and
	 * increase monotonically by 1. 
	 * @param number
	 */
	void setAnnotationGroupNumber(int number) throws IllegalArgumentException;

	/**
	 * Returns the annotation group UID, type 1. 
	 * @param uid
	 */
	void setAnnotationGroupUID(String uid) throws IllegalArgumentException;

	/**
	 * Sets the annotation property category code, type 1. 
	 * @param code
	 */
	void setAnnotationPropertyCategoryCode(Code code)
		throws IllegalArgumentException;

	/**
	 * Sets the annotation property type code, type 1. 
	 * @param code
	 */
	void setAnnotationPropertyTypeCode(Code code)
		throws IllegalArgumentException;

	/**
	 * Sets the z coordinate(s) common to all annotations in the group, type
	 * 1C. Required if annotation coordinate type is 3D and all annotations are
	 * in the same plane. 
	 * @param value
	 */
	void setCommonZCoordinateValue(double[] value)
		throws IllegalArgumentException;

	/**
	 * Sets the coordinates of one or more points that define the annotations,
	 * encoded in (x,y) or (x,y,z) order, type 1C. Required if
	 * PointCoordinatesData not present. 
	 * @param data
	 */
	void setDoublePointCoordinatesData(double[] data)
		throws IllegalArgumentException;

	/**
	 * Sets the graphic type of annotations within the group, type 1.
	 * Enumerated values: POINT, POLYLINE, POLYGON, ELLIPSE, RECTANGLE. 
	 * @param type
	 */
	void setGraphicType(String type) throws IllegalArgumentException;

	/**
	 * Sets the list of point indices, type 1C. Required if graphic type is
	 * POLYLINE or POLYGON. 
	 * @param list
	 */
	void setLongPrimitivePointIndexList(int[] list)
		throws IllegalArgumentException;

	/**
	 * Sets the number of annotations in the group, type 1. 
	 * @param number
	 */
	void setNumberOfAnnotations(int number) throws IllegalArgumentException;

	/**
	 * Sets the coordinates of one or more points that define the annotations,
	 * encoded in (x,y) or (x,y,z) order, type 1C. Required if
	 * DoublePointCoordinatesData not present. 
	 * @param data
	 */
	void setPointCoordinatesData(float[] data) throws IllegalArgumentException;

	/**
	 * Returns the optical path identifier(s) to which this group applies, type
	 * 1C. Required if group does not apply to all optical paths. 
	 * @param identifiers
	 */
	void setReferencedOpticalPathIdentifier(String[] identifiers)
		throws IllegalArgumentException;

}
