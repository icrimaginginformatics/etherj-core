/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.module.SegmentationMultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.SegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SegmentationSharedFunctionalGroups;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public final class DefaultSegmentationMultiframeFunctionalGroupsModule
	extends AbstractDisplayable
	implements SegmentationMultiframeFunctionalGroupsModule
{
	private int frameCount = 0;
	private final SegmentationPerFrameFunctionalGroups perFrameGroups;
	private final SegmentationMultiModuleCore segMultiModCore;
	private final SegmentationSharedFunctionalGroups sharedGroups;
	private final boolean strict;

	public DefaultSegmentationMultiframeFunctionalGroupsModule(
		SegmentationMultiModuleCore segMultiModCore,
		SegmentationSharedFunctionalGroups sharedGroups, 
		SegmentationPerFrameFunctionalGroups perFrameGroups)
		throws IllegalArgumentException
	{
		this(segMultiModCore, sharedGroups, perFrameGroups, true);
	}

	public DefaultSegmentationMultiframeFunctionalGroupsModule(
		SegmentationMultiModuleCore segMultiModCore,
		SegmentationSharedFunctionalGroups sharedGroups, 
		SegmentationPerFrameFunctionalGroups perFrameGroups,
		boolean strict)
		throws IllegalArgumentException
	{
		if (segMultiModCore == null)
		{
			throw new IllegalArgumentException(
				"Segmentation multimodule core must not be null");
		}
		if (sharedGroups == null)
		{
			throw new IllegalArgumentException(
				"Shared functional groups must not be null");
		}
		if (perFrameGroups == null)
		{
			throw new IllegalArgumentException(
				"Per-frame functional groups must not be null");
		}
		this.strict = strict;
		this.segMultiModCore = segMultiModCore;
		this.sharedGroups = sharedGroups;
		this.perFrameGroups = perFrameGroups;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"InstanceNumber: "+segMultiModCore.getInstanceNumber());
		ps.println(pad+"ContentDate: "+segMultiModCore.getContentDate());
		ps.println(pad+"ContentTime: "+segMultiModCore.getContentTime());
		ps.println(pad+"NumberOfFrames: "+frameCount);
		ps.println(pad+"SharedFunctionalGroups:");
		sharedGroups.display(ps, indent+"  ");
		ps.println(pad+"PerFrameFunctionalGroups:");
		perFrameGroups.display(ps, indent+"  ");
	}

	@Override
	public String getContentDate()
	{
		return segMultiModCore.getContentDate();
	}

	@Override
	public String getContentTime()
	{
		return segMultiModCore.getContentTime();
	}

	@Override
	public String getFrameOfReferenceUid()
	{
		return segMultiModCore.getFrameOfReferenceUid();
	}

	@Override
	public int getInstanceNumber()
	{
		return segMultiModCore.getInstanceNumber();
	}

	@Override
	public int getNumberOfFrames()
	{
		return frameCount;
	}

	@Override
	public SegmentationPerFrameFunctionalGroups getPerFrameFunctionalGroups()
	{
		return perFrameGroups;
	}

	@Override
	public SegmentationSharedFunctionalGroups getSharedFunctionalGroups()
	{
		return sharedGroups;
	}

	@Override
	public String getSopClassUid()
	{
		return segMultiModCore.getSopClassUid();
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setContentDate(String date) throws IllegalArgumentException
	{
		segMultiModCore.setContentDate(date);
	}

	@Override
	public void setContentTime(String time) throws IllegalArgumentException
	{
		segMultiModCore.setContentTime(time);
	}

	@Override
	public void setInstanceNumber(int number) throws IllegalArgumentException
	{
		segMultiModCore.setInstanceNumber(number);
	}

	@Override
	public void setNumberOfFrames(int frameCount) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, frameCount,
			(int x) -> (x > 0), 
			"NumberOfFrames must be greater than zero");
		this.frameCount = checked;
	}

}
