/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.ContainerComponent;
import icr.etherj.dicom.iod.ContainerIdentifier;
import icr.etherj.dicom.iod.SpecimenDescription;
import icr.etherj.dicom.iod.impl.DefaultContainerIdentifier;
import icr.etherj.dicom.iod.module.SpecimenModule;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class DefaultSpecimenModule extends AbstractDisplayable
	implements SpecimenModule
{
	private final List<ContainerComponent> components = new ArrayList<>();
	private ContainerIdentifier containerIdentifier =
		new DefaultContainerIdentifier();
	private final List<SpecimenDescription> specDescs = new ArrayList<>();
	private final List<Code> typeCodes = new ArrayList<>();
	private final boolean strict;

	public DefaultSpecimenModule()
	{
		this(true);
	}

	public DefaultSpecimenModule(boolean strict)
	{
		this.strict = strict;
	}

	@Override
	public boolean addContainerComponent(ContainerComponent component)
	{
		return (component != null) ? components.add(component) : false;
	}

	@Override
	public boolean addContainerTypeCode(Code code)
	{
		if (code == null)
		{
			return false;
		}
		return typeCodes.add(code);
	}

	@Override
	public boolean addSpecimenDescription(SpecimenDescription desc)
	{
		if (desc == null)
		{
			return false;
		}
		return specDescs.add(desc);
	}

	@Override
	public void clearContainerComponents()
	{
		components.clear();
	}

	@Override
	public void clearContainerTypeCodes()
	{
		typeCodes.clear();
	}

	@Override
	public void clearSpecimenDescriptions()
	{
		specDescs.clear();
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ContainerIdentifier:");
		containerIdentifier.display(ps, indent+"  ");
		int nItems = typeCodes.size();
		ps.println(pad+"ContainerTypeCodes: "+nItems+" item"+
			((nItems != 1) ? "s" : ""));
		typeCodes.forEach((code) -> code.display(ps, indent+"  "));
		nItems = components.size();
		if (nItems > 0)
		{
			ps.println(pad+"ContainerComponents: "+nItems+" item"+
				((nItems != 1) ? "s" : ""));
			components.forEach((comp) -> comp.display(ps, indent+"  "));
		}
		nItems = specDescs.size();
		ps.println(pad+"SpecimenDescriptions: "+nItems+" item"+
			((nItems != 1) ? "s" : ""));
		specDescs.forEach((desc) -> desc.display(ps, indent+"  "));
	}

	@Override
	public List<ContainerComponent> getContainerComponentList()
	{
		return ImmutableList.copyOf(components);
	}

	@Override
	public ContainerIdentifier getContainerIdentifier()
	{
		return containerIdentifier;
	}

	@Override
	public List<Code> getContainerTypeCodeList()
	{
		return ImmutableList.copyOf(typeCodes);
	}

	@Override
	public List<SpecimenDescription> getSpecimenDescriptionList()
	{
		return ImmutableList.copyOf(specDescs);
	}

	@Override
	public void setContainerIdentifier(ContainerIdentifier ident)
	{
		containerIdentifier = ident;
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

}
