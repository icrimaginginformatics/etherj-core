/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.Uids;
import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.CodingSchemeIdentifier;
import icr.etherj.dicom.iod.module.SopCommonModule;
import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public class DefaultSopCommonModule extends AbstractDisplayable
	implements SopCommonModule
{
	private final String classUid;
	private final Map<String,CodingSchemeIdentifier> codeSchemeIds =
		new LinkedHashMap<>();
	private String contentQual = null;
	private int instNumber = -1;
	private String instUid = Uids.generateDicomUid();
	private String longTempInfoMod = null;
	private final boolean strict;

	public DefaultSopCommonModule(String classUid)
		throws IllegalArgumentException
	{
		this(classUid, true);
	}

	public DefaultSopCommonModule(String classUid, boolean strict)
		throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(classUid))
		{
			throw new IllegalArgumentException(
				"SopClassUid must not be null or empty");
		}
		this.classUid = classUid;
		this.strict = strict;
	}

	@Override
	public CodingSchemeIdentifier addCodingSchemeIdentifier(
		CodingSchemeIdentifier csi)
	{
		return (csi != null)
			? codeSchemeIds.put(csi.getCodingSchemeDesignator(), csi)
			: null;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ClassUid: "+classUid);
		ps.println(pad+"InstanceUid: "+instUid);
		if (instNumber > 0)
		{
			ps.println(pad+"InstanceNumber: "+instNumber);
		}
		if ((longTempInfoMod != null) && !longTempInfoMod.isEmpty())
		{
			ps.println(
				pad+"LongitudinalTemporalInformationModified: "+longTempInfoMod);
		}
		if ((contentQual != null) && !contentQual.isEmpty())
		{
			ps.println(
				pad+"ContentQualification: "+contentQual);
		}
		int nCsid = codeSchemeIds.size();
		if (nCsid > 0)
		{
			ps.println(pad+"CodingSchemeIdentifierList: "+nCsid+" item"+
				((nCsid != 1) ? "s" : ""));
			if (recurse)
			{
				for (CodingSchemeIdentifier id : codeSchemeIds.values())
				{
					id.display(ps, indent+"  ", recurse);
				}
			}
		}
	}

	@Override
	public List<CodingSchemeIdentifier> getCodingSchemeIdentifierList()
	{
		return ImmutableList.copyOf(codeSchemeIds.values());
	}

	@Override
	public String getContentQualification()
	{
		return contentQual;
	}

	@Override
	public int getInstanceNumber()
	{
		return instNumber;
	}

	@Override
	public String getLongitudinalTemporalInformationModified()
	{
		return longTempInfoMod;
	}

	@Override
	public String getSopClassUid()
	{
		return classUid;
	}

	@Override
	public String getSopInstanceUid()
	{
		return instUid;
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setContentQualification(String value)
		throws IllegalArgumentException
	{
		if (value == null)
		{
			contentQual = null;
			return;
		}
		switch (value)
		{
			case "PRODUCT":
			case "RESEARCH":
			case "SERVICE":
				contentQual = value;
				break;
			default:
				throw new IllegalArgumentException(
					"Invalid content qualification: "+value);
		}
	}

	@Override
	public void setInstanceNumber(int number)
	{
		instNumber = (number > 0) ? number : -1;
	}

	@Override
	public void setLongitudinalTemporalInformationModified(String value)
		throws IllegalArgumentException
	{
		if (value == null)
		{
			longTempInfoMod = null;
			return;
		}
		switch (value)
		{
			case "UNMODIFIED":
			case "MODIFIED":
			case "REMOVED":
				longTempInfoMod = value;
				break;
			default:
				throw new IllegalArgumentException(
					"Invalid longitudinal temporal information modified: "+value);
		}
	}

	@Override
	public void setSopInstanceUid(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SopInstanceUid must not be null or empty");
		}
		instUid = uid;
	}
	
}
