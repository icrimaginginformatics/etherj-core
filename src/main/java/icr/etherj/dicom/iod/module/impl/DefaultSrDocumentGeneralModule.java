/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.HeirarchicalSopInstanceReference;
import icr.etherj.dicom.iod.impl.DefaultHeirarchicalSopInstanceReference;
import icr.etherj.dicom.iod.module.SrDocumentGeneralModule;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public class DefaultSrDocumentGeneralModule extends AbstractDisplayable
	implements SrDocumentGeneralModule
{
	private String completion;
	private String contentDate = "00000000";
	private String contentTime = "000000.00";
	private Map<String,HeirarchicalSopInstanceReference> currReqProcEvidence =
		new LinkedHashMap<>();
	private int number = -1;
	private final List<Code> perfProcCodes = new ArrayList<>();
	private String verification;

	@Override
	public HeirarchicalSopInstanceReference addCurrentRequestedProcedureEvidence(
		HeirarchicalSopInstanceReference refIn)
	{
		if (refIn == null)
		{
			return null;
		}
		String studyUid = refIn.getStudyInstanceUid();
		HeirarchicalSopInstanceReference ref = currReqProcEvidence.get(studyUid);
		if (ref == null)
		{
			ref = refIn;
			return currReqProcEvidence.put(ref.getStudyInstanceUid(), ref);
		}
		ref.merge(refIn);
		return ref;
	}

	@Override
	public boolean addPerformedProcedureCode(Code code)
	{
		return (code != null) ? perfProcCodes.add(code) : false;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"InstanceNumber: "+number);
		ps.println(pad+"CompletionFlag: "+completion);
		ps.println(pad+"VerificationFlag: "+verification);
		ps.println(pad+"ContentDate: "+contentDate);
		ps.println(pad+"ContentTime: "+contentTime);
		int nPpc = perfProcCodes.size();
		ps.println(pad+"PerformedProcedureCodeList: "+nPpc+" item"+
			((nPpc != 1) ? "s" : ""));
		if (recurse)
		{
			for (Code ppCode : perfProcCodes)
			{
				ppCode.display(ps, indent+"  ", recurse);
			}
		}
		int nCrpe = currReqProcEvidence.size();
		ps.println(pad+"CurrentRequestedProcedureEvidenceList: "+nCrpe+" item"+
			((nCrpe != 1) ? "s" : ""));
		if (recurse)
		{
			for (HeirarchicalSopInstanceReference ref : currReqProcEvidence.values())
			{
				ref.display(ps, indent+"  ", recurse);
			}
		}
	}

	@Override
	public String getCompletionFlag()
	{
		return completion;
	}

	@Override
	public String getContentDate()
	{
		return contentDate;
	}

	@Override
	public String getContentTime()
	{
		return contentTime;
	}

	@Override
	public HeirarchicalSopInstanceReference getCurrentRequestedProcedureEvidence(
		String uid)
	{
		return currReqProcEvidence.get(uid);
	}

	@Override
	public List<HeirarchicalSopInstanceReference> getCurrentRequestedProcedureEvidenceList()
	{
		return ImmutableList.copyOf(currReqProcEvidence.values());
	}

	@Override
	public int getInstanceNumber()
	{
		return number;
	}

	@Override
	public List<Code> getPerformedProcedureCodeList()
	{
		return ImmutableList.copyOf(perfProcCodes);
	}

	@Override
	public String getVerificationFlag()
	{
		return verification;
	}

	@Override
	public HeirarchicalSopInstanceReference removeCurrentRequestedProcedureEvidence(
		HeirarchicalSopInstanceReference ref)
	{
		return (ref != null)
			? currReqProcEvidence.remove(ref.getStudyInstanceUid())
			: null;
	}

	@Override
	public HeirarchicalSopInstanceReference removeCurrentRequestedProcedureEvidence(
		String uid)
	{
		return currReqProcEvidence.remove(uid);
	}

	@Override
	public void setCompletionFlag(String flag) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(flag))
		{
			throw new IllegalArgumentException(
				"Completion flag must not be null or empty");
		}
		switch (flag)
		{
			case Constants.Partial:
			case Constants.Complete:
				break;
			default:
				throw new IllegalArgumentException("Invalid completion flag: "+flag);
		}
		completion = flag;
	}

	@Override
	public void setContentDate(String date) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(date))
		{
			throw new IllegalArgumentException(
				"Content date must not be null or empty");
		}
		Date dt = DicomUtils.parseDate(date);
		if (dt == null)
		{
			throw new IllegalArgumentException(
				"Content time invalid: "+date);
		}
		contentDate = date;
	}

	@Override
	public void setContentTime(String time) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(time))
		{
			throw new IllegalArgumentException(
				"Content time must not be null or empty");
		}
		Date dt = DicomUtils.parseTime(time);
		if (dt == null)
		{
			throw new IllegalArgumentException(
				"Content time invalid: "+time);
		}
		contentTime = time;
	}

	@Override
	public void setInstanceNumber(int number)
	{
		this.number = (number > 0) ? number : -1;
	}

	@Override
	public void setVerificationFlag(String flag) throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(flag))
		{
			throw new IllegalArgumentException(
				"Verification flag must not be null or empty");
		}
		switch (flag)
		{
			case Constants.Unverified:
			case Constants.Verified:
				break;
			default:
				throw new IllegalArgumentException("Invalid verification flag: "+flag);
		}
		verification = flag;
	}
	
}
