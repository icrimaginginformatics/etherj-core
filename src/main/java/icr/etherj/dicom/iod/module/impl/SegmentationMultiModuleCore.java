/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.Uids;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.IodUtils;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Internal class that contains all DICOM fields that are specified in multiple
 * modules. Uses strictest requirements of all modules that specify each field.
 * @author jamesd
 */
public class SegmentationMultiModuleCore
{
	private static final Logger logger = LoggerFactory.getLogger(
		SegmentationMultiModuleCore.class);

	private int bitsAlloc = 8;
	private int bitsStored = 8;
	private int highBit = 7;
	private String contentDate = "00000000";
	private String contentTime = "000000.00";
	private int instNum = 0;
	private String frameOfRefUid = null;
	private String photoInterp = Constants.Monochrome2;
	private int pixelRep = 0;
	private int samplesPerPixel = 1;
	private String sopInstUid = Uids.generateDicomUid();
	private final boolean strict;

	/**
	 *
	 */
	public SegmentationMultiModuleCore()
	{
		this(true);
	}

	/**
	 *
	 * @param strict
	 */
	public SegmentationMultiModuleCore(boolean strict)
	{
		this.strict = strict;
	}

	/**
	 * Returns bits allocated: ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public int getBitsAllocated()
	{
		return bitsAlloc;
	}

	/**
	 * Returns : ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public int getBitsStored()
	{
		return bitsStored;
	}

	/**
	 * Returns , type ?: GeneralImageModule, SegmentationImageModule,
	 * MultiframeFunctionalGroupsModule
	 * @return 
	 */
	public String getContentDate()
	{
		return contentDate;
	}

	/**
	 * Returns , type ?: GeneralImageModule, SegmentationImageModule,
	 * MultiframeFunctionalGroupsModule
	 * @return 
	 */
	public String getContentTime()
	{
		return contentTime;
	}

	/**
	 * 
	 * @return 
	 */
	public String getFrameOfReferenceUid()
	{
		return frameOfRefUid;
	}

	/**
	 * Returns : ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public int getHighBit()
	{
		return highBit;
	}

	/**
	 * Returns instance number, type 1: SopCommonModule, GeneralImageModule,
	 * SegmentationImageModule, MultiFrameFunctionalGroupsModule.
	 * @return 
	 */
	public int getInstanceNumber()
	{
		return instNum;
	}

	/**
	 * Returns : ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public String getPhotometricInterpretation()
	{
		return photoInterp;
	}

	/**
	 * Returns : ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public int getPixelRepresentation()
	{
		return pixelRep;
	}

	/**
	 * Returns : ImagePixelModule, SegmentationImageModule.
	 * @return 
	 */
	public int getSamplesPerPixel()
	{
		return samplesPerPixel;
	}

	/**
	 * Returns SOP class UID, type 1: SopCommonModule
	 * @return 
	 */
	public String getSopClassUid()
	{
		return UID.SegmentationStorage;
	}

	/**
	 * Returns SOP class UID, type 1: SopCommonModule
	 * @return 
	 */
	public String getSopInstanceUid()
	{
		return sopInstUid;
	}

	public boolean isStrict()
	{
		return strict;
	}

	/**
	 * Sets bits allocated, type 1: ImagePixelModule, SegmentationImageModule.
	 * @param bits 
	 */
	public void setBitsAllocated(int bits) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, bits,
			(int x) -> ((x == 1) || (x == 8)),
			"Bits allocated must be 1 or 8");
		bitsAlloc = checked;
	}

	/**
	 * Sets bits stored, type 1: ImagePixelModule, SegmentationImageModule.
	 * @param bits 
	 */
	public void setBitsStored(int bits) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, bits,
			(int x) -> ((x == 1) || (x == 8)),
			"Bits stored must be 1 or 8");
		bitsStored = checked;
	}

	/**
	 * Sets content date, type 1: GeneralImageModule, SegmentationImageModule,
	 * MultiframeFunctionalGroupsModule
	 * @param date
	 * @throws IllegalArgumentException 
	 */
	public void setContentDate(String date) throws IllegalArgumentException
	{
		String checked = IodUtils.checkType1(strict, date, VR.DA,
			"Content Date");
		contentDate = checked;
	}

	/**
	 * Sets content time, type 1: GeneralImageModule, SegmentationImageModule,
	 * MultiframeFunctionalGroupsModule
	 * @param time
	 * @throws IllegalArgumentException 
	 */
	public void setContentTime(String time) throws IllegalArgumentException
	{
		String checked = IodUtils.checkType1(strict, time, VR.TM,
			"Content Time");
		contentTime = checked;
	}

	/**
	 *
	 * @param uid
	 * @throws IllegalArgumentException
	 */
	public void setFrameOfReferenceUid(String uid) throws IllegalArgumentException
	{
		if (strict)
		{
			if ((uid != null) && uid.isEmpty())
			{
				throw new IllegalArgumentException(
					"Frame of reference UID must not be empty");
			}
		}
		if ((uid != null) && uid.isEmpty())
		{
			logger.warn("Frame of reference UID must not be empty");
		}
		frameOfRefUid = uid;
	}

	/**
	 * Sets : ImagePixelModule, SegmentationImageModule.
	 * @param bit 
	 */
	public void setHighBit(int bit) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, bit,
			(int x) -> ((x == 0) || (x == 7)),
			"High bit must be 0 or 7");
		highBit = checked;
	}

	/**
	 * Sets instance number, type 1: SopCommonModule, GeneralImageModule,
	 * SegmentationImageModule, MultiFrameFunctionalGroupsModule.
	 * @param number 
	 */
	public void setInstanceNumber(int number) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, number,
			(int x) -> (x > 0),
			"InstanceNumber must be greater than zero");
		instNum = checked;
	}

	/**
	 * Sets : ImagePixelModule, SegmentationImageModule.
	 * @param interpretation
	 */
	public void setPhotometricInterpretation(String interpretation)
		throws IllegalArgumentException
	{
		String checked = IodUtils.checkCondition(strict, interpretation,
			(x) -> Constants.Monochrome2.equals(x),
			"Photometric interpretation must be "+Constants.Monochrome2);
		photoInterp = checked;
	}

	/**
	 * Sets : ImagePixelModule, SegmentationImageModule.
	 * @param representation
	 */
	public void setPixelRepresentation(int representation)
		throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, representation,
			(int x) -> (x == 0),
			"Pixel representation must be 0");
		pixelRep = checked;
	}

	/**
	 * Sets : ImagePixelModule, SegmentationImageModule.
	 * @param samples
	 */
	public void setSamplesPerPixel(int samples) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, samples,
			(int x) -> (x == 1),
			"Samples per pixel must be 1");
		samplesPerPixel = checked;
	}

	public void setSopInstanceUid(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SopInstanceUid must not be null or empty");
		}
		sopInstUid = uid;
	}
	
}
