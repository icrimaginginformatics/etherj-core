/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.PerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SharedFunctionalGroups;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultMultiframeFunctionalGroupsModule extends AbstractDisplayable
	implements MultiframeFunctionalGroupsModule
{
	private String contentDate = "19000101";
	private String contentTime = "000000.00";
	private int frameCount = 0;
	private int number = 0;
	private final PerFrameFunctionalGroups perFrameGroups;
	private final SharedFunctionalGroups sharedGroups;
	private final String sopClassUid;
	private final boolean strict;

	public DefaultMultiframeFunctionalGroupsModule(String sopClassUid,
		SharedFunctionalGroups sharedGroups, 
		PerFrameFunctionalGroups perFrameGroups)
	{
		this(sopClassUid, sharedGroups, perFrameGroups, true);
	}

	public DefaultMultiframeFunctionalGroupsModule(String sopClassUid,
		SharedFunctionalGroups sharedGroups, 
		PerFrameFunctionalGroups perFrameGroups,
		boolean strict)
	{
		if ((sopClassUid == null) || sopClassUid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SOP class UID must not be null or empty");
		}
		if (sharedGroups == null)
		{
			throw new IllegalArgumentException(
				"SharedFunctionalGroups must not be null");
		}
		if (perFrameGroups == null)
		{
			throw new IllegalArgumentException(
				"PerFrameFunctionalGroups must not be null");
		}
		this.strict = strict;
		this.sopClassUid = sopClassUid;
		this.sharedGroups = sharedGroups;
		this.perFrameGroups = perFrameGroups;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"SharedFunctionalGroups:");
		sharedGroups.display(ps, indent+"  ", recurse);
		ps.println(pad+"PerFrameFunctionalGroups:");
		perFrameGroups.display(ps, indent+"  ", recurse);
		ps.println(pad+"InstanceNumber: "+number);
		ps.println(pad+"ContentDate: "+contentDate);
		ps.println(pad+"ContentTime: "+contentTime);
		ps.println(pad+"NumberOfFrames: "+frameCount);
	}

	@Override
	public String getContentDate()
	{
		return contentDate;
	}

	@Override
	public String getContentTime()
	{
		return contentTime;
	}

	@Override
	public int getInstanceNumber()
	{
		return number;
	}

	@Override
	public int getNumberOfFrames()
	{
		return frameCount;
	}
	
	@Override
	public PerFrameFunctionalGroups getPerFrameFunctionalGroups()
	{
		return perFrameGroups;
	}

	@Override
	public SharedFunctionalGroups getSharedFunctionalGroups()
	{
		return sharedGroups;
	}

	@Override
	public String getSopClassUid()
	{
		return sopClassUid;
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setContentDate(String date) throws IllegalArgumentException
	{
		String checked = IodUtils.checkCondition(strict, date,
			(x) -> (DicomUtils.parseDate(x) != null),
			"Invalid date: "+date);
		contentDate = checked;
	}

	@Override
	public void setContentTime(String time) throws IllegalArgumentException
	{
		String checked = IodUtils.checkCondition(strict, time,
			(x) -> (DicomUtils.parseTime(x) != null),
			"Invalid time: "+time);
		contentTime = checked;
	}

	@Override
	public void setInstanceNumber(int number) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, number,
			(int x) -> (x > 0),
			"InstanceNumber must be greater than zero");
		this.number = checked;
	}

	@Override
	public void setNumberOfFrames(int frameCount) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, frameCount,
			(int x) -> (x > 0),
			"NumberOfFrames must be greater than zero");
		this.frameCount = checked;
	}

}
