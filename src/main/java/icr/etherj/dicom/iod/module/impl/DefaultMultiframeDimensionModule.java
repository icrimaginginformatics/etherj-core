/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.DimensionIndex;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.module.MultiframeDimensionModule;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.dcm4che2.data.VR;

/**
 *
 * @author jamesd
 */
public class DefaultMultiframeDimensionModule extends AbstractDisplayable
	implements MultiframeDimensionModule
{
	private static final Set<String> validTypes = new HashSet<>();

	private String dimOrgType = null;
	private final Set<String> dimOrgUids = new LinkedHashSet<>();
	private final List<DimensionIndex> dimIndices = new ArrayList<>();
	private final boolean strict;

	static
	{
		validTypes.add(Constants.ThreeD);
		validTypes.add(Constants.ThreeDTemporal);
		validTypes.add(Constants.TiledFull);
		validTypes.add(Constants.TiledSparse);
	}

	public DefaultMultiframeDimensionModule()
	{
		this(true);
	}

	public DefaultMultiframeDimensionModule(boolean strict)
	{
		this.strict = strict;
	}

	@Override
	public boolean addDimensionIndex(DimensionIndex index)
	{
		return (index != null) ? dimIndices.add(index) : false;
	}

	@Override
	public boolean addDimensionOrganisationUid(String uid)
		throws IllegalArgumentException
	{
		String checked = IodUtils.checkType1(strict, uid, VR.UI,
			"DimensionOrganisationUid must not be null or empty");
		return dimOrgUids.add(checked);
	}


	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		int nItems = dimOrgUids.size();
		ps.println(pad+"DimensionOrganisationUidList: "+nItems+
			" DimensionOrganisationUid"+((nItems != 1) ? "s" : ""));
		for (String item : dimOrgUids)
		{
			ps.println(pad+"DimensionOrganisationUid: "+item);
		}
		nItems = dimIndices.size();
		ps.println(pad+"DimensionIndexList: "+nItems+
			" DimensionInd"+((nItems != 1) ? "ices" : "ex"));
		for (DimensionIndex item : dimIndices)
		{
			item.display(ps, indent+"  ");
		}
	}

	@Override
	public List<DimensionIndex> getDimensionIndexList()
	{
		return ImmutableList.copyOf(dimIndices);
	}

	@Override
	public String getDimensionOrganisationType()
	{
		return dimOrgType;
	}

	@Override
	public List<String> getDimensionOrganisationUidList()
	{
		return ImmutableList.copyOf(dimOrgUids);
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public boolean removeDimensionIndex(DimensionIndex index)
	{
		return dimIndices.remove(index);
	}

	@Override
	public boolean removeDimensionOrganisationUid(String uid)
	{
		return dimOrgUids.remove(uid);
	}

	@Override
	public void setDimensionOrganisationType(String type)
		throws IllegalArgumentException
	{
		if (type == null)
		{
			dimOrgType = null;
			return;
		}
		String checked = IodUtils.checkCondition(strict, type,
			(x) -> validTypes.contains(x),
			"Invalid dimension organisation type: "+type);
		dimOrgType = checked;
	}
}
