/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;
import icr.etherj.dicom.Validatable;
import icr.etherj.dicom.iod.ReferencedFrameOfReference;
import icr.etherj.dicom.iod.StructureSetRoi;
import java.util.List;

/**
 * StructureSet module in DICOM IOD.
 * @author jamesd
 */
public interface StructureSetModule extends Displayable, Validatable, Module
{

	/**
	 *
	 * @param refFoR
	 * @return
	 */
	boolean addReferencedFrameOfReference(ReferencedFrameOfReference refFoR);

	/**
	 *
	 * @param roi
	 * @return
	 */
	boolean addStructureSetRoi(StructureSetRoi roi);

	/**
	 * Returns the instance number, type 3.
	 * @return
	 */
	int getInstanceNumber();

	/**
	 *	Returns the list of referenced frames of reference, type 3.
	 * @return
	 */
	List<ReferencedFrameOfReference> getReferencedFrameOfReferenceList();

	/**
	 *	Returns the structure set date, type 2.
	 * @return the date
	 */
	String getStructureSetDate();

	/**
	 * Returns the description, type 3.
	 * @return the description
	 */
	String getStructureSetDescription();

	/**
	 * Returns the structure set label, type 1
	 * @return
	 */
	String getStructureSetLabel();

	/**
	 * Returns the structure set name, type 3.
	 * @return
	 */
	String getStructureSetName();

	StructureSetRoi getStructureSetRoi(int roiNumber);

	/**
	 *	Returns the list of structure set ROIs, type 1.
	 * @return
	 */
	List<StructureSetRoi> getStructureSetRoiList();

	/**
	 *	Returns the structure set time, type 2.
	 * @return
	 */
	String getStructureSetTime();

	/**
	 * Removes the structure set ROI that corresponds to the ROI number.
	 * @param roiNumber the ROI number
	 * @return
	 */
	boolean removeStructureSetRoi(int roiNumber);

	/**
	 * Removes the structure set ROI.
	 * @param roi the structure set ROI
	 * @return
	 */
	boolean removeStructureSetRoi(StructureSetRoi roi);

	/**
	 *	Sets the structure set date, type 2.
	 * @param date
	 */
	void setStructureSetDate(String date) throws IllegalArgumentException;

	/**
	 * Sets the description, type 3.
	 * @param description
	 */
	void setStructureSetDescription(String description);

	/**
	 *	Sets the instance number, type 3.
	 * @param number
	 */
	void setInstanceNumber(int number);

	/**
	 * Sets the structure set label, type 1.
	 * @param label
	 */
	void setStructureSetLabel(String label) throws IllegalArgumentException;

	/**
	 * Sets the structure set name, type 3.
	 * @param name
	 */
	void setStructureSetName(String name);

	/**
	 * Sets the structure set time, type 2.
	 * @param time
	 */
	void setStructureSetTime(String time) throws IllegalArgumentException;
}
