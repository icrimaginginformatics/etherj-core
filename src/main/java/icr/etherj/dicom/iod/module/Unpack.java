/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.StringUtils;
import icr.etherj.dicom.Coordinate3D;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.NewTag;
import icr.etherj.dicom.iod.AlgorithmIdentification;
import icr.etherj.dicom.iod.AnnotationGroup;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.ContainerComponent;
import icr.etherj.dicom.iod.ContentItem;
import icr.etherj.dicom.iod.ContentRelationship;
import icr.etherj.dicom.iod.ContentTemplate;
import icr.etherj.dicom.iod.Contour;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.ContourImageContainer;
import icr.etherj.dicom.iod.DerivationImage;
import icr.etherj.dicom.iod.DimensionIndex;
import icr.etherj.dicom.iod.FrameContent;
import icr.etherj.dicom.iod.FunctionalGroupsReferencedImage;
import icr.etherj.dicom.iod.HL7v2HierarchicDesignator;
import icr.etherj.dicom.iod.HeirarchicalSeriesReference;
import icr.etherj.dicom.iod.HeirarchicalSopInstanceReference;
import icr.etherj.dicom.iod.Iods;
import icr.etherj.dicom.iod.Measurement;
import icr.etherj.dicom.iod.OpticalPath;
import icr.etherj.dicom.iod.PixelMatrixOrigin;
import icr.etherj.dicom.iod.PixelMeasures;
import icr.etherj.dicom.iod.PlanePositionSlide;
import icr.etherj.dicom.iod.ReferencedFrameOfReference;
import icr.etherj.dicom.iod.ReferencedImage;
import icr.etherj.dicom.iod.ReferencedInstance;
import icr.etherj.dicom.iod.ReferencedSeries;
import icr.etherj.dicom.iod.RoiContour;
import icr.etherj.dicom.iod.RtReferencedSeries;
import icr.etherj.dicom.iod.RtReferencedStudy;
import icr.etherj.dicom.iod.SegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.SourceImage;
import icr.etherj.dicom.iod.SpecimenDescription;
import icr.etherj.dicom.iod.SpecimenPreparationStep;
import icr.etherj.dicom.iod.WsmFunctionalGroupsFrame;
import icr.etherj.dicom.iod.WsmReferencedImage;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
class Unpack
{
	private static final Logger logger = LoggerFactory.getLogger(Unpack.class);

	static DicomElement getSq(DicomObject dcm, int tag, boolean strict)
		throws IllegalArgumentException
	{
		return getSq(dcm, tag, strict, null);
	}

	static DicomElement getSq(DicomObject dcm, int tag, boolean strict,
		String desc)
		throws IllegalArgumentException
	{
		if (dcm == null)
		{
			throw new IllegalArgumentException("DicomObject must not be null");
		}
		DicomElement sq = dcm.get(tag);
		if (sq == null)
		{
			if (StringUtils.isNullOrEmpty(desc))
			{
				desc = DicomUtils.tagName(tag);
			}
			String msg = desc+" not found";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return null;
		}
		return sq;
	}

	static boolean getYesNo(DicomObject dcm, int tag, boolean strict)
		throws IllegalArgumentException
	{
		return getYesNo(dcm, tag, strict, null);
	}

	static boolean getYesNo(DicomObject dcm, int tag, boolean strict,
		String desc)
		throws IllegalArgumentException
	{
		if (dcm == null)
		{
			throw new IllegalArgumentException("DicomObject must not be null");
		}
		if (StringUtils.isNullOrEmpty(desc))
		{
			desc = DicomUtils.tagName(tag);
		}
		String value = dcm.getString(tag, VR.CS, null);
		if (StringUtils.isNullOrEmpty(value))
		{
			String msg = desc+" not found";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return false;
		}
		switch (value)
		{
			case Constants.Yes:
				return true;
			case Constants.No:
				return false;
			default:
				String msg = desc+" must be YES or NO";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
		}
		return false;
	}

	static AlgorithmIdentification unpackAlgorithmIdentification(DicomObject dcm)
	{
		return unpackAlgorithmIdentification(dcm, true);
	}

	static AlgorithmIdentification unpackAlgorithmIdentification(DicomObject dcm,
		boolean strict)
	{
		AlgorithmIdentification ai = Iods.algorithmIdentification(strict);
		DicomElement familySq = getSq(dcm, Tag.AlgorithmFamilyCodeSequence,
			strict);
		if (familySq != null)
		{
			if (familySq.countItems() == 1)
			{
				ai.setAlgorithmFamilyCode(unpackCode(familySq.getDicomObject()));
			}
			else
			{
				String msg =
					"AlgorithmFamilyCodeSequence must contain exactly one item";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
			}
		}
		ai.setAlgorithmName(dcm.getString(Tag.AlgorithmName, VR.LO, null));
		ai.setAlgorithmVersion(dcm.getString(Tag.AlgorithmVersion, VR.LO, null));
		ai.setAlgorithmParameters(
			dcm.getString(Tag.AlgorithmParameters, VR.LT, null));
		ai.setAlgorithmSource(dcm.getString(Tag.AlgorithmSource, VR.LO, null));

		return ai;
	}

	static void unpackAlgorithmIdentifications(DicomElement aiSq,
		AnnotationGroup group)
	{
		if (aiSq == null)
		{
			return;
		}
		for (int i=0; i < aiSq.countItems(); i++)
		{
			group.addAlgorithmIdentification(
				unpackAlgorithmIdentification(
					aiSq.getDicomObject(i), group.isStrict()));
		}
	}
	
	static AnnotationGroup unpackAnnotationGroup(DicomObject dcm,
		String coordType)
	{
		return unpackAnnotationGroup(dcm, coordType, true);
	}

	static AnnotationGroup unpackAnnotationGroup(DicomObject dcm,
		String coordType, boolean strict)
	{
		AnnotationGroup group = Iods.annotationGroup(strict);
		group.setAnnotationGroupNumber(
			dcm.getInt(Tag.AnnotationGroupNumber, VR.US, Integer.MIN_VALUE));
		group.setAnnotationGroupUID(
			dcm.getString(NewTag.AnnotationGroupUID, VR.UI, null));
		group.setAnnotationGroupLabel(
			dcm.getString(NewTag.AnnotationGroupLabel, VR.LO, null));
		group.setAnnotationGroupDescription(
			dcm.getString(NewTag.AnnotationGroupDescription, VR.UT, null));

		String genType = dcm.getString(NewTag.AnnotationGroupGenerationType,
			VR.CS, null);
		group.setAnnotationGroupGenerationType(genType);
		if (Constants.SemiAutomatic.equals(genType) ||
			 Constants.Automatic.equals(genType))
		{
			DicomElement aiSq = getSq(dcm,
				NewTag.AnnotationGroupAlgorithmIdentificationSequence, strict,
				"AlgorithmIdentificationSequence");
			unpackAlgorithmIdentifications(aiSq, group);
		}

		DicomElement catSq = getSq(dcm,
			NewTag.AnnotationGroupPropertyCategoryCodeSequence, strict,
			"AnnotationGroupPropertyCategoryCodeSequence");
		if (catSq != null)
		{
			if (catSq.countItems() == 1)
			{
				group.setAnnotationPropertyCategoryCode(
					unpackCode(catSq.getDicomObject()));
			}
			else
			{
				String msg =
					"AnnotationGroupPropertyCategoryCodeSequence must contain exactly one item";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
			}
		}
		DicomElement typeSq = getSq(dcm,
			NewTag.AnnotationGroupPropertyTypeCodeSequence, strict,
			"AnnotationGroupPropertyCategoryCodeSequence");
		if (typeSq != null)
		{
			if (typeSq.countItems() == 1)
			{
				group.setAnnotationPropertyTypeCode(
					unpackCode(typeSq.getDicomObject()));
			}
			else
			{
				String msg =
					"AnnotationGroupPropertyTypeCodeSequence must contain exactly one item";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
			}
		}

		group.setNumberOfAnnotations(
			dcm.getInt(NewTag.NumberOfAnnotations, VR.SL, Integer.MIN_VALUE));
		String gfxType = dcm.getString(Tag.GraphicType, VR.CS, null);
		group.setGraphicType(gfxType);
		boolean allPaths = getYesNo(dcm,
			NewTag.AnnotationAppliesToAllOpticalPaths, strict,
			"AnnotationAppliesToAllOpticalPaths");
		if (!allPaths)
		{
			group.setReferencedOpticalPathIdentifier(
				dcm.getStrings(NewTag.ReferencedOpticalPathIdentifier, VR.SH, null));
		}
		if (Constants.Coord3D.equals(coordType))
		{
			group.setAnnotationAppliesToAllZPlanes(
				getYesNo(dcm, NewTag.AnnotationAppliesToAllZPlanes, strict,
					"AnnotationAppliesToAllZPlanes"));
			group.setCommonZCoordinateValue(
				dcm.getDoubles(NewTag.CommonZCoordinateValue, VR.FD, null));
		}
		float[] floatData = dcm.getFloats(Tag.PointCoordinatesData, VR.OF, null);
		if (floatData != null)
		{
			group.setPointCoordinatesData(floatData);
		}
		else
		{
			double[] doubleData = dcm.getDoubles(
				NewTag.DoublePointCoordinatesData, VR.FD, null);
			if (doubleData != null)
			{
				group.setDoublePointCoordinatesData(doubleData);
			}
			else
			{
				String msg =
					"PointCoordinatesData or DoublePointCoordinatesData must be present";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
			}
		}
		if (Constants.Polygon.equals(gfxType) ||
			 Constants.Polyline.equals(gfxType))
		{
			int[] indices = dcm.getInts(NewTag.LongPrimitivePointIndexList, VR.UL,
				null);
			group.setLongPrimitivePointIndexList(indices);
		}

		unpackMeasurements(dcm.get(NewTag.MeasurementsSequence), group);

		return group;
	}

	static Code unpackCode(DicomObject dcm) throws IllegalArgumentException
	{
		return unpackCode(dcm, 0);
	}

	static Code unpackCode(DicomObject dcm, int modifierTag)
		throws IllegalArgumentException
	{
		Code code = Iods.code();
		String value = dcm.getString(Tag.CodeValue, VR.SH, null);
		if (value != null)
		{
			code.setCodeValue(value);
		}
		else
		{
			value = dcm.getString(NewTag.LongCodeValue, VR.UT, null);
			if (value != null)
			{
				code.setLongCodeValue(value);
			}
		}
		if (value != null)
		{
			code.setCodingSchemeDesignator(dcm.getString(
				Tag.CodingSchemeDesignator, VR.SH, null));
		}
		code.setCodingSchemeVersion(dcm.getString(
			Tag.CodingSchemeVersion, VR.SH, null));
		code.setCodeMeaning(dcm.getString(Tag.CodeMeaning, VR.LO, null));
		// Fetch the modifiers
		DicomElement sq = dcm.get(modifierTag);
		if (sq == null)
		{
			return code;
		}
		for (int i = 0; i < sq.countItems(); i++)
		{
			DicomObject modDcm = sq.getDicomObject(i);
			Code modifier = unpackCode(modDcm, 0);
			code.addModifier(modifier);
		}
		return code;
	}

	static ContentItem.CodeItem unpackCodeItem(DicomObject dcm)
	{
		ContentItem.CodeItem item = Iods.codeItem();
		item.setValueType(Constants.Code);
		item.setConceptNameCode(unpackConceptNameCode(dcm));
		DicomElement codeSq = dcm.get(Tag.ConceptCodeSequence);
		if ((codeSq == null) || (codeSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"ConceptCodeSeqeunce must be present and contain a single item");
		}
		item.setCode(unpackCode(codeSq.getDicomObject()));
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}
		return item;
	}

	static Code unpackConceptNameCode(DicomObject dcm)
	{
		return unpackConceptNameCode(dcm, true);
	}

	static Code unpackConceptNameCode(DicomObject dcm, boolean required)
	{
		DicomElement conceptNameSq = dcm.get(Tag.ConceptNameCodeSequence);
		if ((conceptNameSq == null) && !required)
		{
			return null;
		}
		if ((conceptNameSq == null) || (conceptNameSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"ConceptNameSequence must be present and contain a single item");
		}
		return unpackCode(conceptNameSq.getDicomObject(0));
	}

	static ContainerComponent unpackContainerComponent(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContainerComponent cc = Iods.containerComponent();
		DicomElement codeSq = dcm.get(Tag.ContainerComponentTypeCodeSequence);
		if ((codeSq == null) || (codeSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"ContainerComponentTypeCodeSequence must exist and contain a single item");
		}
		cc.setContainerComponentTypeCode(unpackCode(codeSq.getDicomObject()));
		cc.setContainerComponentMaterial(dcm.getString(
			Tag.ContainerComponentMaterial, VR.CS, null));
		return cc;
	}

	static ContentItem.ContainerItem unpackContainerContentItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.ContainerItem container = Iods.containerItem();
		container.setValueType(Constants.Container);
		DicomElement conceptNameSq = dcm.get(Tag.ConceptNameCodeSequence);
		if (conceptNameSq == null)
		{
			return container;
		}
		DicomObject conceptNameDcm = conceptNameSq.getDicomObject(0);
		if (conceptNameDcm == null)
		{
			throw new IllegalArgumentException(
				"ConceptNameCodeSequence must not be empty");
		}
		container.setConceptNameCode(unpackCode(conceptNameDcm));
		container.setContinuityOfContent(dcm.getString(Tag.ContinuityOfContent));
		DicomElement ctSq = dcm.get(Tag.ContentTemplateSequence);
		if (ctSq != null)
		{
			unpackContentTemplate(ctSq, container);
		}
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(container, contentSq);
		}
		return container;
	}

	static ContentItem unpackContentItem(DicomObject dcm)
	{
		ContentItem ci;
		String type = dcm.getString(Tag.ValueType);
		switch (type)
		{
			case Constants.Container:
				ci = unpackContainerContentItem(dcm);
				break;
			case Constants.Code:
				ci = unpackCodeItem(dcm);
				break;
			case Constants.PersonName:
				ci = unpackPersonNameItem(dcm);
				break;
			case Constants.Text:
				ci = unpackTextItem(dcm);
				break;
			case Constants.Image:
				ci = unpackImageItem(dcm);
				break;
			case Constants.UidReference:
				ci = unpackUidRefItem(dcm);
				break;
			case Constants.Numeric:
				ci = unpackNumericItem(dcm);
				break;
			case Constants.SpatialCoord:
				ci = unpackSCoordItem(dcm);
				break;
			default:
				ci = Iods.genericContentItem();
				ci.setValueType(type);
		}
		return ci;
	}

	static void unpackContentRelationships(ContentItem parent,
		DicomElement contentSq) throws IllegalArgumentException
	{
		int nItems = contentSq.countItems();
		if (nItems < 1)
		{
			throw new IllegalArgumentException(
				"ContentSequence must not be empty");
		}
		for (int i = 0; i < nItems; i++)
		{
			ContentRelationship cr = Iods.contentRelationship();
			DicomObject contentDcm = contentSq.getDicomObject(i);
			cr.setRelationshipType(contentDcm.getString(Tag.RelationshipType));
			cr.setReferencedContentItemIdentifier(contentDcm.getInts(
				Tag.ReferencedContentItemIdentifier, VR.UL, null));
			cr.setRelatedContentItem(unpackContentItem(contentDcm));
			parent.addContentRelationship(cr);
		}
	}

	static void unpackContentTemplate(DicomElement ctSq,
		ContentItem.ContainerItem container) throws IllegalArgumentException
	{
		if (ctSq.countItems() != 1)
		{
			throw new IllegalArgumentException(
				"ContentTemplateSequence must contain a single item");
		}
		ContentTemplate template = Iods.contentTemplate();
		DicomObject dcm = ctSq.getDicomObject(0);
		template.setMappingResource(dcm.getString(Tag.MappingResource));
		template.setTemplateIdentifier(dcm.getString(Tag.TemplateIdentifier));
		container.setContentTemplate(template);
	}

	static void unpackContourImages(DicomElement ciSq,
		ContourImageContainer container) throws IllegalArgumentException
	{
		if (ciSq == null)
		{
			return;
		}
		int nItems = ciSq.countItems();
		for (int i = 0; i < nItems; i++)
		{
			DicomObject dcm = ciSq.getDicomObject(i);
			ContourImage ci = Iods.contourImage();
			ci.setReferencedSopClassUid(dcm.getString(Tag.ReferencedSOPClassUID));
			ci.setReferencedSopInstanceUid(dcm.getString(
				Tag.ReferencedSOPInstanceUID));
			container.addContourImage(ci);
			String number = dcm.getString(Tag.ReferencedFrameNumber);
			if (number != null)
			{
				try
				{
					ci.setReferencedFrameNumber(Integer.parseInt(number));
				}
				catch (NumberFormatException ex)
				{
					logger.debug(
						"Invalid instance number (IS) in ContourImage: {}", number);
				}
			}
		}
	}

	static void unpackCoordinates(DicomObject dcm, Contour contour)
		throws IllegalArgumentException
	{
		String[] data = dcm.getStrings(Tag.ContourData);
		if ((data == null) || (data.length < 3))
		{
			throw new IllegalArgumentException(
				"Contour data must be at least one triplet");
		}
		int nCoords = data.length / 3;
		if (3 * nCoords != data.length)
		{
			throw new IllegalArgumentException(
				"Contour data must be in triplets, found " + data.length +
					" individual points");
		}
		for (int i = 0; i < data.length; i += 3)
		{
			try
			{
				Coordinate3D coord = new Coordinate3D(Double.parseDouble(data[i]),
					Double.parseDouble(data[i + 1]),
					Double.parseDouble(data[i + 2]));
				contour.addCoordinate(coord);
			}
			catch (NumberFormatException ex)
			{
				logger.warn("Invalid point (DS) in ContourData", ex);
			}
		}
	}

	static DerivationImage unpackDerivationImage(DicomObject dcm)
	{
		DerivationImage image = Iods.derivationImage();
		DicomElement sourceImageSq = dcm.get(Tag.SourceImageSequence);
		if ((sourceImageSq != null) && (sourceImageSq.countItems() > 0))
		{
			for (int i = 0; i < sourceImageSq.countItems(); i++)
			{
				image.addSourceImage(unpackSourceImage(
					sourceImageSq.getDicomObject(i), true));
			}
		}
		DicomElement derivCodeSq = dcm.get(Tag.DerivationCodeSequence);
		if (derivCodeSq != null)
		{
			for (int i = 0; i < derivCodeSq.countItems(); i++)
			{
				image.addDerivationCode(unpackCode(derivCodeSq.getDicomObject(i)));
			}
		}
		return image;
	}

	static DimensionIndex unpackDimensionIndex(DicomObject dcm)
		throws IllegalArgumentException
	{
		DimensionIndex dimIdx = Iods.dimensionIndex();
		dimIdx.setDimensionOrganisationUid(dcm.getString(
			Tag.DimensionOrganizationUID, VR.UI, null));
		dimIdx.setIndexPointer(dcm.getInt(Tag.DimensionIndexPointer, VR.AT, 0));
		dimIdx.setFunctionalGroupPointer(dcm.getInt(
			Tag.FunctionalGroupPointer, VR.AT, 0));
		dimIdx.setDescriptionLabel(dcm.getString(
			Tag.DimensionDescriptionLabel, VR.LO, null));
		return dimIdx;
	}

	static FrameContent unpackFrameContent(DicomObject dcm)
	{
		FrameContent fc = Iods.frameContent();
		int[] dimIdx = dcm.getInts(Tag.DimensionIndexValues, VR.UL, null);
		if (dimIdx != null)
		{
			fc.setDimensionIndexValues(dimIdx);
		}
		String stackId = dcm.getString(Tag.StackID, VR.SH, null);
		if (stackId != null)
		{
			fc.setStackId(stackId);
			fc.setInStackPositionNumber(dcm.getInt(
				Tag.InStackPositionNumber, VR.UL, -1));
		}
		return fc;
	}

	static FunctionalGroupsReferencedImage unpackFuncGroupRefImage(
		DicomObject dcm) throws IllegalArgumentException
	{
		FunctionalGroupsReferencedImage image =
			Iods.functionalGroupsReferencedImage();
		image.setReferencedSopClassUid(dcm.getString(Tag.ReferencedSOPClassUID));
		image.setReferencedSopInstanceUid(dcm.getString(
			Tag.ReferencedSOPInstanceUID));
		int refFrame = dcm.getInt(Tag.ReferencedFrameNumber, VR.IS, -1);
		if (refFrame > 0)
		{
			image.setReferencedFrameNumber(refFrame);
		}
		DicomElement codeSq = dcm.get(Tag.PurposeOfReferenceCodeSequence);
		if ((codeSq == null) || (codeSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"PurposeOfReferenceCodeSequence must exist and not be empty");
		}
		image.setPurposeOfReferenceCode(unpackCode(codeSq.getDicomObject()));
		return image;
	}

	static HeirarchicalSeriesReference unpackHeirSeriesRef(DicomObject dcm)
	{
		HeirarchicalSeriesReference refSeries =
			Iods.heirarchicalSeriesReference();
		refSeries.setSeriesInstanceUid(dcm.getString(
			Tag.SeriesInstanceUID, VR.UI, null));
		DicomElement refInstSq = dcm.get(Tag.ReferencedSOPSequence);
		if ((refInstSq == null) || (refInstSq.countItems() < 1))
		{
			throw new IllegalArgumentException(
				"Referenced SOP sequence missing or empty");
		}
		for (int i = 0; i < refInstSq.countItems(); i++)
		{
			refSeries.addReferencedInstance(unpackReferencedInstance(
				refInstSq.getDicomObject(i)));
		}
		return refSeries;
	}

	static HeirarchicalSopInstanceReference unpackHeirSopInstRef(DicomObject dcm)
		throws IllegalArgumentException
	{
		HeirarchicalSopInstanceReference hsir =
			Iods.heirarchicalSopInstanceReference();
		hsir.setStudyInstanceUid(dcm.getString(
			Tag.StudyInstanceUID, VR.UI, null));
		DicomElement refSeriesSq = dcm.get(Tag.ReferencedSeriesSequence);
		if ((refSeriesSq == null) || (refSeriesSq.isEmpty()))
		{
			throw new IllegalArgumentException(
				"Referenced series sequence missing or empty");
		}
		for (int i = 0; i < refSeriesSq.countItems(); i++)
		{
			DicomObject serDcm = refSeriesSq.getDicomObject(i);
			HeirarchicalSeriesReference series = unpackHeirSeriesRef(serDcm);
			hsir.addSeriesReference(series);
		}
		return hsir;
	}

	static ContentItem.ImageItem unpackImageItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.ImageItem item = Iods.imageItem();
		item.setValueType(Constants.Image);
		Code cnc = unpackConceptNameCode(dcm, false);
		if (cnc != null)
		{
			item.setConceptNameCode(cnc);
		}
		DicomElement refInstSq = dcm.get(Tag.ReferencedSOPSequence);
		if (refInstSq == null)
		{
			throw new IllegalArgumentException("Referenced SOP sequence missing");
		}
		if (refInstSq.countItems() != 1)
		{
			throw new IllegalArgumentException(
				"Referenced SOP sequence must have a single item");
		}
		item.setReferencedInstance(unpackReferencedInstance(
			refInstSq.getDicomObject()));
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}
		return item;
	}

	static double[] unpackImageOrientationSlide(DicomObject dcm)
	{
		double[] ori = dcm.getDoubles(Tag.ImageOrientationSlide, VR.DS, null);
		if (ori == null)
		{
			throw new IllegalArgumentException("ImageOrientationSlide missing");
		}
		return ori;
	}

	static HL7v2HierarchicDesignator unpackIssuer(DicomObject dcm)
	{
		String localId = dcm.getString(Tag.LocalNamespaceEntityID, VR.UT, null);
		String univId = dcm.getString(Tag.UniversalEntityID, VR.UT, null);
		String univIdType = dcm.getString(Tag.UniversalEntityIDType, VR.CS, null);
		if (localId == null)
		{
			if (univId == null)
			{
				throw new IllegalArgumentException(
					"LocalNamesspaceEntityID and UniversalEntityID must not both be null");
			}
			if (univId.isEmpty())
			{
				throw new IllegalArgumentException(
					"UniversalEntityID must not be empty");
			}
			if (StringUtils.isNullOrEmpty(univIdType))
			{
				throw new IllegalArgumentException(
					"UniversalEntityIDType must not be null or empty");
			}
		}
		else
		{
			if (localId.isEmpty())
			{
				throw new IllegalArgumentException(
					"LocalNamesspaceEntityID must not be empty");
			}
		}
		HL7v2HierarchicDesignator issuer = Iods.hl7v2HierarchicDesignator();
		issuer.setLocalNamespaceEntityId(localId);
		issuer.setUniversalEntityId(univId);
		issuer.setUniversalEntityIdType(univIdType);
		return issuer;
	}

	static Measurement unpackMeasurement(DicomObject dcm, boolean strict)
	{
		Measurement m = Iods.measurement(strict);
		DicomElement nameSq = getSq(dcm, Tag.ConceptNameCodeSequence, strict);
		if (nameSq != null)
		{
			if (nameSq.countItems() == 1)
			{
				m.setConceptNameCode(unpackCode(nameSq.getDicomObject()));
			}
			else
			{
				String msg = "ConceptNameCodeSequence must contain exactly one item";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
			}
		}
		DicomElement unitSq = getSq(dcm, Tag.MeasurementUnitsCodeSequence, strict);
		if (unitSq != null)
		{
			if (unitSq.countItems() == 1)
			{
				m.setMeasurementUnitsCode(unpackCode(unitSq.getDicomObject()));
			}
			else
			{
				String msg = "ConceptNameCodeSequence must contain exactly one item";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
			}
		}
		DicomElement valuesSq = getSq(dcm, NewTag.MeasurementValuesSequence,
			strict, "MeasurementValuesSequence");
		if ((valuesSq != null) && !valuesSq.isEmpty())
		{
			DicomObject valueDcm = valuesSq.getDicomObject();
			float[] values = valueDcm.getFloats(NewTag.FloatingPointValues, VR.OF,
				null);
			int[] indices = valueDcm.getInts(NewTag.AnnotationIndexList, VR.SL,
				null);
			if ((values != null) && (values.length > 0))
			{
				m.setFloatingPointValues(values);
				if ((indices != null) && (indices.length == values.length))
				{
					m.setAnnotationIndexList(indices);
				}
			}
		}

		return m;
	}

	static void unpackMeasurements(DicomElement sq, AnnotationGroup group)
	{
		if (sq == null)
		{
			return;
		}
		for (int i=0; i < sq.countItems(); i++)
		{
			group.addMeasurement(
				unpackMeasurement(sq.getDicomObject(i), group.isStrict()));
		}
	}

	static ContentItem.NumericItem unpackNumericItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.NumericItem item = Iods.numericItem();
		item.setValueType(Constants.Numeric);
		item.setConceptNameCode(unpackConceptNameCode(dcm));
		DicomElement measValSq = dcm.get(Tag.MeasuredValueSequence);
		if (measValSq == null)
		{
			throw new IllegalArgumentException("MeasureValueSequence missing");
		}
		int nObj = measValSq.countItems();
		if (!((nObj == 0) || (nObj == 1)))
		{
			throw new IllegalArgumentException(
				"MeasurementValueSequence must contain 0 or 1 items");
		}
		if (nObj == 1)
		{
			DicomObject valDcm = measValSq.getDicomObject();
			item.setNumericValue(valDcm.getString(Tag.NumericValue, VR.DS, null));
			double floatValue = valDcm.getDouble(
				NewTag.FloatingPointValue, VR.FD, Double.NaN);
			if (!Double.isNaN(floatValue))
			{
				item.setFloatingPointValue(floatValue);
			}
			int numerator = valDcm.getInt(
				NewTag.RationalNumeratorValue, VR.SL, 0);
			int denominator = valDcm.getInt(
				NewTag.RationalDenominatorValue, VR.UL, 0);
			if ((numerator != 0) && (denominator != 0))
			{
				item.setRationalNumeratorValue(numerator);
				item.setRationalDenominatorValue(denominator);
			}
			DicomElement unitsSq = valDcm.get(Tag.MeasurementUnitsCodeSequence);
			if (unitsSq == null)
			{
				throw new IllegalArgumentException(
					"Measurement units code sequence missing");
			}
			if (unitsSq.countItems() != 1)
			{
				throw new IllegalArgumentException(
					"Measurement units code sequence must have a single item");
			}
			item.setMeasurementUnits(unpackCode(unitsSq.getDicomObject()));
		}
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}
		return item;
	}

	static OpticalPath unpackOpticalPath(DicomObject dcm)
	{
		OpticalPath path = Iods.opticalPath();
		path.setOpticalPathIdentifier(dcm.getString(Tag.OpticalPathIdentifier));
		path.setOpticalPathDescription(dcm.getString(Tag.OpticalPathDescription));
		DicomElement illuminationTypeSq = dcm.get(
			Tag.IlluminationTypeCodeSequence);
		if ((illuminationTypeSq == null) || illuminationTypeSq.isEmpty())
		{
			throw new IllegalArgumentException(
				"IlluminationTypeCodeSequence must not be missing or empty");
		}
		for (int i = 0; i < illuminationTypeSq.countItems(); i++)
		{
			path.addIlluminationTypeCode(unpackCode(
				illuminationTypeSq.getDicomObject(i)));
		}
		path.setIccProfile(dcm.getBytes(Tag.ICCProfile));
		float wavelength = dcm.getFloat(Tag.IlluminationWaveLength, VR.FL, Float.NaN);
		DicomElement colourSq = dcm.get(Tag.IlluminationColorCodeSequence);
		if (!Float.isFinite(wavelength) &&
			 ((colourSq == null) ||
			 colourSq.isEmpty()))
		{
			throw new IllegalArgumentException(
				"One of IlluminationWavelength or IlluminationColorCodeSeqence must be present");
		}
		path.setIlluminationWaveLength(wavelength);
		path.setIlluminationColourCode(unpackCode(colourSq.getDicomObject(0)));
		DicomElement illuminatorTypeSq = dcm.get(Tag.IlluminatorTypeCodeSequence);
		if ((illuminatorTypeSq != null) && !illuminatorTypeSq.isEmpty())
		{
			path.setIlluminatorTypeCode(unpackCode(
				illuminatorTypeSq.getDicomObject(0)));
		}
		return path;
	}

	static ContentItem.PersonNameItem unpackPersonNameItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.PersonNameItem item = Iods.personNameItem();
		item.setValueType(Constants.PersonName);
		item.setConceptNameCode(unpackConceptNameCode(dcm));
		item.setPersonName(dcm.getString(Tag.PersonName, VR.PN, null));
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}
		return item;
	}

	static PixelMatrixOrigin unpackPixelMatrixOrigin(DicomObject dcm)
	{
		DicomElement sq = dcm.get(Tag.TotalPixelMatrixOriginSequence);
		DicomObject item = sq.getDicomObject(0);
		PixelMatrixOrigin origin = Iods.pixelMatrixOrigin();
		origin.setXOffsetInSlideCoordinateSystem(item.getDouble(
			Tag.XOffsetInSlideCoordinateSystem));
		origin.setYOffsetInSlideCoordinateSystem(item.getDouble(
			Tag.YOffsetInSlideCoordinateSystem));
		return origin;
	}

	static PixelMeasures unpackPixelMeasures(DicomObject dcm, boolean required)
		throws IllegalArgumentException
	{
		double[] pixelSpacing = dcm.getDoubles(Tag.PixelSpacing, VR.DS, null);
		double sliceThickness = dcm.getDouble(
			Tag.SliceThickness, VR.DS, Double.NaN);
		if (required && ((pixelSpacing == null) ||
			 !Double.isFinite(sliceThickness)))
		{
			throw new IllegalArgumentException(
				"Required pixel measure fields missing");
		}
		PixelMeasures pixelMeasures = Iods.pixelMeasures();
		pixelMeasures.setPixelSpacing(pixelSpacing);
		pixelMeasures.setSliceThickness(sliceThickness);
		pixelMeasures.setSpacingBetweenSlices(dcm.getDouble(
			Tag.SpacingBetweenSlices, VR.DS, Double.NaN));
		return pixelMeasures;
	}

	static double[] unpackPlaneOrientation(DicomObject dcm, boolean required)
		throws IllegalArgumentException
	{
		double[] ori = dcm.getDoubles(Tag.ImageOrientationPatient, VR.DS, null);
		if (required && (ori == null))
		{
			throw new IllegalArgumentException("ImageOrientationPatient missing");
		}
		return ori;
	}

	static double[] unpackPlanePosition(DicomObject dcm, boolean required)
		throws IllegalArgumentException
	{
		double[] pos = dcm.getDoubles(Tag.ImagePositionPatient, VR.DS, null);
		if (required && (pos == null))
		{
			throw new IllegalArgumentException("ImagePositionPatient missing");
		}
		return pos;
	}

	static PlanePositionSlide unpackPlanePositionSlide(DicomObject dcm)
	{
		PlanePositionSlide pos = Iods.planePositionSlide();
		pos.setColumnPositionInTotalImagePixelMatrix(dcm.getInt(
			Tag.ColumnPositionInTotalImagePixelMatrix));
		pos.setRowPositionInTotalImagePixelMatrix(dcm.getInt(
			Tag.RowPositionInTotalImagePixelMatrix));
		pos.setXOffsetInSlideCoordinateSystem(dcm.getDouble(
			Tag.XOffsetInSlideCoordinateSystem));
		pos.setYOffsetInSlideCoordinateSystem(dcm.getDouble(
			Tag.YOffsetInSlideCoordinateSystem));
		pos.setZOffsetInSlideCoordinateSystem(dcm.getDouble(
			Tag.ZOffsetInSlideCoordinateSystem));
		return pos;
	}

	static void unpackReferencedFrameOfReference(DicomElement refFoRSq,
		StructureSetModule ss) throws IllegalArgumentException
	{
		if (refFoRSq == null)
		{
			return;
		}
		int nItems = refFoRSq.countItems();
		for (int i = 0; i < nItems; i++)
		{
			DicomObject dcm = refFoRSq.getDicomObject(i);
			ReferencedFrameOfReference refFoR =
				Iods.referencedFrameOfReference();
			refFoR.setFrameOfReferenceUid(dcm.getString(Tag.FrameOfReferenceUID));
			ss.addReferencedFrameOfReference(refFoR);
			unpackRtReferencedStudy(dcm.get(
				Tag.RTReferencedStudySequence), refFoR);
		}
	}

	static ReferencedImage unpackReferencedImage(DicomObject dcm)
		throws IllegalArgumentException
	{
		ReferencedImage image = Iods.referencedImage();
		image.setReferencedSopClassUid(dcm.getString(
			Tag.ReferencedSOPClassUID, VR.UI, null));
		image.setReferencedSopInstanceUid(dcm.getString(
			Tag.ReferencedSOPInstanceUID, VR.UI, null));
		int number = dcm.getInt(Tag.ReferencedFrameNumber, VR.IS, -1);
		if (number > 0)
		{
			image.setReferencedFrameNumber(number);
		}
		return image;
	}

	static ReferencedInstance unpackReferencedInstance(DicomObject dcm)
		throws IllegalArgumentException
	{
		ReferencedInstance inst = Iods.referencedInstance();
		inst.setReferencedSopClassUid(dcm.getString(
			Tag.ReferencedSOPClassUID, VR.UI, null));
		inst.setReferencedSopInstanceUid(dcm.getString(
			Tag.ReferencedSOPInstanceUID, VR.UI, null));
		return inst;
	}

	static ReferencedSeries unpackReferencedSeries(DicomObject dcm)
		throws IllegalArgumentException
	{
		ReferencedSeries refSeries = Iods.referencedSeries();
		refSeries.setSeriesInstanceUid(dcm.getString(
			Tag.SeriesInstanceUID, VR.UI, null));
		DicomElement refInstSq = dcm.get(Tag.ReferencedInstanceSequence);
		if ((refInstSq == null) || (refInstSq.countItems() < 1))
		{
			throw new IllegalArgumentException(
				"ReferencedInstanceSequence missing or empty");
		}
		for (int i = 0; i < refInstSq.countItems(); i++)
		{
			refSeries.addReferencedInstance(unpackReferencedInstance(
				refInstSq.getDicomObject(i)));
		}
		return refSeries;
	}

	static void unpackRoiContour(DicomElement contourSq, RoiContour rc)
		throws IllegalArgumentException
	{
		if (contourSq == null)
		{
			return;
		}
		int nItems = contourSq.countItems();
		for (int i = 0; i < nItems; i++)
		{
			DicomObject item = contourSq.getDicomObject(i);
			Contour contour = Iods.contour();
			String number = item.getString(Tag.ContourNumber);
			if (number != null)
			{
				try
				{
					contour.setContourNumber(Integer.parseInt(number));
				}
				catch (NumberFormatException ex)
				{
					logger.debug("Invalid instance number (IS) in Contour: {}",
						number);
					// See else branch
					contour.setContourNumber(i + 1);
				}
			}
			else
			{
				// Standard is a pain here. Contour number is optional but required
				// to be unique. RoiContour stores Contours in a map using contour
				// number so a workaround is to enforce a unique number.
				contour.setContourNumber(i + 1);
			}
			contour.setContourGeometricType(item.getString(
				Tag.ContourGeometricType));
			unpackCoordinates(item, contour);
			rc.addContour(contour);
			unpackContourImages(item.get(Tag.ContourImageSequence), contour);
		}
	}

	static void unpackRtReferencedSeries(DicomElement refSeriesSq,
		RtReferencedStudy refStudy) throws IllegalArgumentException
	{
		if (refSeriesSq == null)
		{
			return;
		}
		int nItems = refSeriesSq.countItems();
		for (int i = 0; i < nItems; i++)
		{
			DicomObject dcm = refSeriesSq.getDicomObject(i);
			RtReferencedSeries refSeries = Iods.rtReferencedSeries();
			refSeries.setSeriesInstanceUid(dcm.getString(Tag.SeriesInstanceUID));
			refStudy.addRtReferencedSeries(refSeries);
			unpackContourImages(dcm.get(Tag.ContourImageSequence), refSeries);
		}
	}

	static void unpackRtReferencedStudy(DicomElement refStudySq,
		ReferencedFrameOfReference refFoR) throws IllegalArgumentException
	{
		if (refStudySq == null)
		{
			return;
		}
		int nItems = refStudySq.countItems();
		for (int i = 0; i < nItems; i++)
		{
			DicomObject dcm = refStudySq.getDicomObject(i);
			RtReferencedStudy refStudy = Iods.rtReferencedStudy();
			refStudy.setReferencedSopClassUid(dcm.getString(
				Tag.ReferencedSOPClassUID));
			refStudy.setReferencedSopInstanceUid(dcm.getString(
				Tag.ReferencedSOPInstanceUID));
			refFoR.addRtReferencedStudy(refStudy);
			unpackRtReferencedSeries(dcm.get(
				Tag.RTReferencedSeriesSequence), refStudy);
		}
	}

	static ContentItem.SpatialCoordItem unpackSCoordItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.SpatialCoordItem item = Iods.spatialCoordItem();
		item.setValueType(Constants.SpatialCoord);
		item.setConceptNameCode(unpackConceptNameCode(dcm, false));
		String gfxType = dcm.getString(Tag.GraphicType);
		item.setGraphicType(gfxType);
		float[] gfxData = dcm.getFloats(Tag.GraphicData, VR.FL, new float[]{});
		if (gfxData.length == 0)
		{
			throw new IllegalArgumentException("Graphics data missing or empty");
		}
		switch (gfxType)
		{
			case Constants.Point:
				if (gfxData.length != 2)
				{
					throw new IllegalArgumentException(
						"Graphics type "+gfxType+" requires exactly one column/row pair");
				}
				break;
			case Constants.MultiPoint:
			case Constants.Polyline:
				if ((gfxData.length % 2) != 0)
				{
					throw new IllegalArgumentException(
						"Graphics type "+gfxType+" requires multiple one column/row pairs");
				}
				break;
			case Constants.Circle:
				if (gfxData.length != 4)
				{
					throw new IllegalArgumentException(
						"Graphics type "+gfxType+" requires exactly two column/row pairs");
				}
				break;
			case Constants.Ellipse:
				if (gfxData.length != 8)
				{
					throw new IllegalArgumentException(
						"Graphics type "+gfxType+" requires exactly four column/row pairs");
				}
				break;
			default:
				throw new IllegalArgumentException(
					"Unknown graphic type: "+gfxType);
		}
		for (int i = 0; i < gfxData.length; i += 2)
		{
			item.addCoordinate(gfxData[i], gfxData[i + 1]);
		}
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}
		return item;
	}

	static SegmentationFunctionalGroupsFrame unpackSegmentationFrame(
		DicomObject dcm, String frameOfRefUid) throws IllegalArgumentException
	{
		SegmentationFunctionalGroupsFrame frame = Iods.segmentationFrame();
		DicomElement frameContentSq = dcm.get(Tag.FrameContentSequence);
		if (!((frameContentSq != null) && (frameContentSq.countItems() == 1)))
		{
			throw new IllegalArgumentException("FrameContent missing");
		}
		frame.setFrameContent(unpackFrameContent(
			frameContentSq.getDicomObject()));
		DicomElement derivImageSq = dcm.get(Tag.DerivationImageSequence);
		if ((derivImageSq != null) && (derivImageSq.countItems() > 0))
		{
			for (int i = 0; i < derivImageSq.countItems(); i++)
			{
				frame.addDerivationImage(unpackDerivationImage(
					derivImageSq.getDicomObject(i)));
			}
		}
		DicomElement planePosSq = dcm.get(Tag.PlanePositionSequence);
		if ((planePosSq != null) && (planePosSq.countItems() == 1))
		{
			frame.setImagePositionPatient(unpackPlanePosition(
				planePosSq.getDicomObject(), frameOfRefUid != null));
		}
		DicomElement segIdSq = dcm.get(Tag.SegmentIdentificationSequence);
		if (!((segIdSq != null) && (segIdSq.countItems() == 1)))
		{
			throw new IllegalArgumentException("SegmentIdentification missing");
		}
		int refSeg = segIdSq.getDicomObject().getInt(
			Tag.ReferencedSegmentNumber, VR.US, -1);
		frame.setReferencedSegmentNumber(refSeg);
		return frame;
	}

	static SourceImage unpackSourceImage(DicomObject dcm, boolean required)
		throws IllegalArgumentException
	{
		SourceImage image = Iods.sourceImage(required);
		image.setReferencedSopClassUid(dcm.getString(
			Tag.ReferencedSOPClassUID, VR.UI, null));
		image.setReferencedSopInstanceUid(dcm.getString(
			Tag.ReferencedSOPInstanceUID, VR.UI, null));
		int number = dcm.getInt(Tag.ReferencedFrameNumber, VR.IS, -1);
		if (number > 0)
		{
			image.setReferencedFrameNumber(number);
		}
		image.setSpatialLocationsPreserved(dcm.getString(
			Tag.SpatialLocationsPreserved, VR.CS, null));
		DicomElement codeSq = dcm.get(Tag.PurposeOfReferenceCodeSequence);
		if (required)
		{
			if (!((codeSq != null) && (codeSq.countItems() == 1)))
			{
				throw new IllegalArgumentException(
					"PurposeOfReferenceCodeSequence missing");
			}
		}
		if (codeSq != null)
		{
			image.setPurposeOfReferenceCode(unpackCode(codeSq.getDicomObject()));
		}
		return image;
	}

	static SpecimenDescription unpackSpecimenDescription(DicomObject dcm)
	{
		SpecimenDescription desc = Iods.specimenDescription();
		desc.setIdentifier(dcm.getString(Tag.SpecimenIdentifier));
		desc.setSpecimenUid(dcm.getString(Tag.SpecimenUID));
		DicomElement issuerSq = dcm.get(
			Tag.IssuerOfTheSpecimenIdentifierSequence);
		if (issuerSq == null)
		{
			throw new IllegalArgumentException(
				"IssuerOfTheSpecimenIdentifierSequence is missing");
		}
		for (int i = 0; i < issuerSq.countItems(); i++)
		{
			HL7v2HierarchicDesignator issuer = unpackIssuer(
				issuerSq.getDicomObject(i));
			desc.addIssuer(issuer);
		}
		DicomElement prepSq = dcm.get(Tag.SpecimenPreparationSequence);
		if (prepSq == null)
		{
			throw new IllegalArgumentException(
				"SpecimenPreparationSequence is missing");
		}
		for (int i = 0; i < prepSq.countItems(); i++)
		{
			SpecimenPreparationStep step = unpackSpecimenPreparationStep(
				prepSq.getDicomObject(i));
			desc.addSpecimenPreparationStep(step);
		}
		return desc;
	}

	static SpecimenPreparationStep unpackSpecimenPreparationStep(DicomObject dcm)
		throws IllegalArgumentException
	{
		SpecimenPreparationStep step = Iods.specimenPreparationStep();
		DicomElement itemSq = dcm.get(
			Tag.SpecimenPreparationStepContentItemSequence);
		if ((itemSq == null) || itemSq.isEmpty())
		{
			throw new IllegalArgumentException(
				"SpecimenPreparationStepContentItemSequence is missing or empty");
		}
		for (int i = 0; i < itemSq.countItems(); i++)
		{
			DicomObject itemDcm = itemSq.getDicomObject(i);
			ContentItem item = unpackContentItem(itemDcm);
			step.addSpecimenPreparationStepContentItem(item);
		}
		return step;
	}

	static ContentItem.TextItem unpackTextItem(DicomObject dcm)
	{
		ContentItem.TextItem item = Iods.textItem();
		item.setValueType(Constants.Text);
		item.setConceptNameCode(unpackConceptNameCode(dcm));
		item.setText(dcm.getString(Tag.TextValue, VR.UT, null));
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}
		return item;
	}

	static ContentItem.UidRefItem unpackUidRefItem(DicomObject dcm)
		throws IllegalArgumentException
	{
		ContentItem.UidRefItem item = Iods.uidRefItem();
		item.setValueType(Constants.UidReference);
		item.setConceptNameCode(unpackConceptNameCode(dcm));
		item.setUidReference(dcm.getString(Tag.UID, VR.UI, null));
		DicomElement contentSq = dcm.get(Tag.ContentSequence);
		if (contentSq != null)
		{
			unpackContentRelationships(item, contentSq);
		}
		return item;
	}

	static WsmFunctionalGroupsFrame unpackWsmFrame(DicomObject dcm)
	{
		WsmFunctionalGroupsFrame frame = Iods.wholeSlideMicroscopyFrame();
		DicomElement frameContentSq = dcm.get(Tag.FrameContentSequence);
		if (!((frameContentSq != null) && (frameContentSq.countItems() == 1)))
		{
			throw new IllegalArgumentException("FrameContent missing");
		}
		frame.setFrameContent(unpackFrameContent(
			frameContentSq.getDicomObject()));
		DicomElement derivImageSq = dcm.get(Tag.DerivationImageSequence);
		if ((derivImageSq != null) && (derivImageSq.countItems() > 0))
		{
			for (int i = 0; i < derivImageSq.countItems(); i++)
			{
				frame.addDerivationImage(unpackDerivationImage(
					derivImageSq.getDicomObject(i)));
			}
		}
		DicomElement posSq = dcm.get(Tag.PlanePositionSlideSequence);
		if ((posSq != null) && (posSq.countItems() == 1))
		{
			frame.setPlanePositionSlide(unpackPlanePositionSlide(
				posSq.getDicomObject()));
		}
		return frame;
	}

	static WsmReferencedImage unpackWsmReferencedImage(DicomObject dcm)
	{
		WsmReferencedImage image = Iods.wholeSlideMicroscopyReferencedImage();
		image.setReferencedSopClassUid(dcm.getString(Tag.ReferencedSOPClassUID));
		image.setReferencedSopInstanceUid(dcm.getString(
			Tag.ReferencedSOPInstanceUID));
		image.setReferencedFrameNumber(dcm.getInt(Tag.ReferencedFrameNumber));
		image.setTopLeftHandCornerOfLocaliserArea(dcm.getInts(
			Tag.TopLeftHandCornerOfLocalizerArea));
		image.setBottomRightHandCornerOfLocaliserArea(dcm.getInts(
			Tag.BottomRightHandCornerOfLocalizerArea));
		image.setPixelSpacing(dcm.getDoubles(Tag.PixelSpacing));
		image.setZOffsetInSlideCoordinateSystem(dcm.getDouble(
			Tag.ZOffsetInSlideCoordinateSystem));
		image.setSamplesPerPixel(dcm.getInt(Tag.SamplesPerPixel));
		image.setOpticalPathIdentifier(dcm.getString(Tag.OpticalPathIdentifier));
		return image;
	}

	static boolean yesOrNo(DicomObject dcm, int tag)
	{
		String value = dcm.getString(tag, VR.CS, "!!ERROR!!");
		switch (value)
		{
			case Constants.Yes:
				return true;
			case Constants.No:
				return false;
			default:
				throw new IllegalArgumentException(
					DicomUtils.tagName(tag)+" must be YES or NO");
		}
	}

	private Unpack()
	{}
}
