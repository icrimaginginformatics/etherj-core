/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;
import java.util.List;

/**
 *
 * @author jamesd
 */
public interface ImagePixelModule extends Displayable, Module
{

	/**
	 * Returns the bits allocated, type 1.
	 * @return
	 */
	int getBitsAllocated();

	/**
	 * Returns the bits stored, type 1.
	 * @return
	 */
	int getBitsStored();

	/**
	 * Returns the number of columns, type 1.
	 * @return
	 */
	int getColumnCount();

	/**
	 * Returns the high bit, type 1.
	 * @return
	 */
	int getHighBit();

	/**
	 * Returns largest pixel value, type 3.
	 * @return
	 */
	int getLargestPixelValue();

	/**
	 * Returns the photometric interpretation, type 1.
	 * @return
	 */
	String getPhotometricInterpretation();

	/**
	 * Returns the pixel data, type 1C.
	 * @return
	 */
	byte[] getPixelData();

	/**
	 * Returns the pixel data, type 1C.
	 * @return
	 */
	List<byte[]> getPixelDataFragments();

	/**
	 * Returns the pixel representation, type 1.
	 * @return
	 */
	int getPixelRepresentation();

	/**
	 * Returns the planar configuration, type 1C.
	 * @return
	 */
	int getPlanarConfiguration();

	/**
	 * Returns the number of rows, type 1.
	 * @return
	 */
	int getRowCount();

	/**
	 * Returns the number of samples per pixel, type 1.
	 * @return
	 */
	int getSamplesPerPixel();

	/**
	 * Returns smallest pixel value, type 3.
	 * @return
	 */
	int getSmallestPixelValue();

	/**
	 * Sets the bits allocated, type 1.
	 * @param bits
	 * @throws IllegalArgumentException
	 */
	void setBitsAllocated(int bits) throws IllegalArgumentException;

	/**
	 * Sets the bits stored, type 1.
	 * @param bits
	 * @throws IllegalArgumentException
	 */
	void setBitsStored(int bits) throws IllegalArgumentException;

	/**
	 * Sets the number of columns, type 1.
	 * @param columns
	 * @throws IllegalArgumentException
	 */
	void setColumnCount(int columns) throws IllegalArgumentException;

	/**
	 * Sets the high bit, type 1.
	 * @param bit
	 */
	void setHighBit(int bit) throws IllegalArgumentException;

	/**
	 * Sets the largest pixel value, type 3.
	 * @param value
	 */
	void setLargestPixelValue(int value);

	/**
	 * Sets the photometric interpretation, type 1.
	 * @param interpretation
	 * @throws IllegalArgumentException
	 */
	void setPhotometricInterpretation(String interpretation)
		throws IllegalArgumentException
;

	/**
	 * Sets the pixel data, type 1C.
	 * @param pixels
	 */
	void setPixelData(byte[] pixels);

	/**
	 * Sets the pixel data, type 1C.
	 * @param pixels
	 * @param noCopy
	 */
	void setPixelData(byte[] pixels, boolean noCopy);

	/**
	 *	Sets the pixel data fragments, type 1C.
	 * @param fragments
	 */
	void setPixelDataFragments(List<byte[]> fragments);

	/**
	 *	Sets the pixel data fragments, type 1C.
	 * @param fragments
	 */
	void setPixelDataFragments(List<byte[]> fragments, boolean noCopy);

	/**
	 * Sets the pixel representation, type 1.
	 * @param representation
	 * @throws IllegalArgumentException
	 */
	void setPixelRepresentation(int representation) throws IllegalArgumentException;

	/**
	 * Sets the planar configuration, type 1C.
	 * @param configuration
	 * @throws IllegalArgumentException
	 */
	void setPlanarConfiguration(int configuration) throws IllegalArgumentException;

	/**
	 * Sets the number of rows, type 1.
	 * @param rows
	 * @throws IllegalArgumentException
	 */
	void setRowCount(int rows) throws IllegalArgumentException;

	/**
	 * Sets the number of samples per pixel, type 1.
	 * @param samples
	 * @throws IllegalArgumentException
	 */
	void setSamplesPerPixel(int samples) throws IllegalArgumentException;

	/**
	 * Sets the smallest pixel value, type 3.
	 * @param value
	 */
	void setSmallestPixelValue(int value);

}
