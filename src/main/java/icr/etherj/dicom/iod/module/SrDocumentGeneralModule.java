/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.HeirarchicalSopInstanceReference;
import java.util.List;

/**
 *
 * @author jamesd
 */
public interface SrDocumentGeneralModule extends Displayable
{

	/**
	 *
	 * @param ref
	 * @return
	 */
	HeirarchicalSopInstanceReference addCurrentRequestedProcedureEvidence(
		HeirarchicalSopInstanceReference ref);

	/**
	 * Adds a performed procedure code.
	 * @param code
	 * @return
	 */
	boolean addPerformedProcedureCode(Code code);

	/**
	 * Returns completion flag, type 1. Enumerated values: PARTIAL, COMPLETE.
	 * @return
	 */
	String getCompletionFlag();

	/**
	 * Returns content date, type 1.
	 * @return
	 */
	String getContentDate();

	/**
	 * Returns content time, type 1.
	 * @return
	 */
	String getContentTime();

	/**
	 *
	 * @param uid
	 * @return
	 */
	HeirarchicalSopInstanceReference getCurrentRequestedProcedureEvidence(
		String uid);

	/**
	 *
	 * @return
	 */
	List<HeirarchicalSopInstanceReference> getCurrentRequestedProcedureEvidenceList();

	/**
	 *	Returns instance number, type 1.
	 * @return
	 */
	int getInstanceNumber();

	/**
	 * Returns list of performed procedure codes, type 2.
	 * @return
	 */
	List<Code> getPerformedProcedureCodeList();

	/**
	 * Returns verification flag, type 1. Enumerated values: UNVERIFIED, VERIFIED.
	 * @return
	 */
	String getVerificationFlag();

	/**
	 *
	 * @param ref
	 * @return
	 */
	HeirarchicalSopInstanceReference removeCurrentRequestedProcedureEvidence(
		HeirarchicalSopInstanceReference ref);

	/**
	 *
	 * @param uid
	 * @return
	 */
	HeirarchicalSopInstanceReference removeCurrentRequestedProcedureEvidence(
		String uid);

	/**
	 * Sets completion flag, type 1. Enumerated values: PARTIAL, COMPLETE.
	 * @param flag
	 * @throws IllegalArgumentException
	 */
	void setCompletionFlag(String flag) throws IllegalArgumentException;

	/**
	 * Returns content date, type 1.
	 * @param date
	 * @throws IllegalArgumentException
	 */
	void setContentDate(String date) throws IllegalArgumentException;

	/**
	 * Returns content time, type 1.
	 * @param time
	 * @throws IllegalArgumentException
	 */
	void setContentTime(String time) throws IllegalArgumentException;

	/**
	 *	Returns instance number, type 1.
	 * @param number
	 */
	void setInstanceNumber(int number);

	/**
	 * Returns verification flag, type 1. Enumerated values: UNVERIFIED, VERIFIED.
	 * @param flag
	 * @throws IllegalArgumentException
	 */
	void setVerificationFlag(String flag) throws IllegalArgumentException;

}
