/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.Uids;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.IodUtils;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;

/**
 *
 * @author jamesd
 */
public class WholeSlideMicroscopyMultiModuleCore
{
	private static final Set<String> interps = new HashSet<>();
	private static final Set<String> validAnno = new HashSet<>();

	static
	{
		interps.add(Constants.Monochrome2);
		interps.add(Constants.Rgb);
		interps.add(Constants.YbrFull422);
		interps.add(Constants.YbrIct);
		validAnno.add(Constants.Yes);
		validAnno.add(Constants.No);
	}

	private int bitsAlloc = 8;
	private int bitsStored = 8;
	private String burnedInAnno = null;
	private int highBit = 7;
	private String contentDate = "00000000";
	private String contentTime = "000000.00";
	private String[] imageType = new String[] {};
	private int instNum = 0;
	private boolean lossyComp = false;
	private String photoInterp = Constants.Monochrome2;
	private int pixelRep = 0;
	private int planarConfig = Integer.MIN_VALUE;
	private int samplesPerPixel = 1;
	private String sopInstUid = Uids.generateDicomUid();
	private final boolean strict;

	public WholeSlideMicroscopyMultiModuleCore()
	{
		this(true);
	}

	public WholeSlideMicroscopyMultiModuleCore(boolean strict)
	{
		this.strict = strict;
	}

	/**
	 * Returns bits allocated, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule.
	 * @return 
	 */
	public int getBitsAllocated()
	{
		return bitsAlloc;
	}

	/**
	 * Returns bits stored, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule.
	 * @return 
	 */
	public int getBitsStored()
	{
		return bitsStored;
	}

	/**
	 * Returns burned in image annotation, type 1: GeneralImageModule,
	 * WholeSlideMicroscopyImageModule.
	 * @return 
	 */
	public String getBurnedInAnnotation()
	{
		return burnedInAnno;
	}

	/**
	 * Returns the content date, type 1: GeneralImageModule,
	 * MultiframeFunctionalGroupsModule.
	 * @return 
	 */
	public String getContentDate()
	{
		return contentDate;
	}

	/**
	 * Returns the content time, type 1: GeneralImageModule,
	 * MultiframeFunctionalGroupsModule.
	 * @return 
	 */
	public String getContentTime()
	{
		return contentTime;
	}

	/**
	 * Returns high bit, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule.
	 * @return 
	 */
	public int getHighBit()
	{
		return highBit;
	}

	/**
	 * Returns image type, type 1: GeneralImageModule,
	 * WholeSlideMicroscopyImageModule.
	 * @return 
	 */
	public String[] getImageType()
	{
		return Arrays.copyOf(imageType, imageType.length);
	}

	/**
	 * Returns instance number, type 1: SopCommonModule, GeneralImageModule,
	 * WholeSlideMicroscopyImageModule, MultiFrameFunctionalGroupsModule.
	 * @return 
	 */
	public int getInstanceNumber()
	{
		return instNum;
	}

	/**
	 * Returns lossy compression, type 1: GeneralImageModule,
	 * WholeSlideMicroscopyImageModule.
	 * @return 
	 */
	public boolean getLossyImageCompression()
	{
		return lossyComp;
	}

	/**
	 * Returns photometric interpretation, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule.
	 * @return 
	 */
	public String getPhotometricInterpretation()
	{
		return photoInterp;
	}

	/**
	 * Returns pixel representation, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule.
	 * @return 
	 */
	public int getPixelRepresentation()
	{
		return pixelRep;
	}

	/**
	 * Returns planar configuration, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule.
	 * @return 
	 */
	public int getPlanarConfiguration()
	{
		return planarConfig;
	}
	/**
	 * Returns samples per pixel, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule.
	 * @return 
	 */
	public int getSamplesPerPixel()
	{
		return samplesPerPixel;
	}

	/**
	 * Returns SOP class UID, type 1: SopCommonModule
	 * @return 
	 */
	public String getSopClassUid()
	{
		return UID.VLWholeSlideMicroscopyImageStorage;
	}

	/**
	 * Returns SOP instance UID, type 1: SopCommonModule
	 * @return 
	 */
	public String getSopInstanceUid()
	{
		return sopInstUid;
	}

	public boolean isStrict()
	{
		return strict;
	}

	/**
	 * Sets bits allocated, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule.
	 * @param bits 
	 */
	public void setBitsAllocated(int bits) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, bits,
			(int x) -> ((x == 8) || (x == 16)),
			"Bits allocated must be 8 or 16");
		bitsAlloc = checked;
	}

	/**
	 * Sets bits stored, type 1: ImagePixelModule, WholeSlideMicroscopyImageModule.
	 * @param bits 
	 */
	public void setBitsStored(int bits) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, bits,
			(int x) -> ((x == 8) || (x == 16)),
			"Bits stored must be 8 or 16");
		bitsStored = checked;
	}

	/**
	 * Sets burned in annotation, type 1: GeneralImageModule,
	 * WholeSlideMicroscopyImageModule.
	 * @param value 
	 */
	public void setBurnedInAnnotation(String value)
		throws IllegalArgumentException
	{
		burnedInAnno = IodUtils.checkCondition(strict, value,
			(x) -> validAnno.contains(x),
			"Burned in annotation must be YES, NO or null");
	}

	/**
	 * Sets content date, type 1: GeneralImageModule,
	 * MultiframeFunctionalGroupsModule.
	 * @param date
	 * @throws IllegalArgumentException 
	 */
	public void setContentDate(String date) throws IllegalArgumentException
	{
		String checked = IodUtils.checkType1(strict, date, VR.DA,
			"Content Date");
		contentDate = checked;
	}

	/**
	 * Sets content time, type 1: GeneralImageModule,
	 * MultiframeFunctionalGroupsModule.
	 * @param time
	 * @throws IllegalArgumentException 
	 */
	public void setContentTime(String time) throws IllegalArgumentException
	{
		String checked = IodUtils.checkType1(strict, time, VR.TM,
			"Content Time");
		contentTime = checked;
	}

	/**
	 * Sets high bit, type 1: ImagePixelModule, WholeSlideMicroscopyImageModule.
	 * @param bit 
	 */
	public void setHighBit(int bit) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, bit,
			(int x) -> ((x == 7) || (x == 15)),
			"High bit must be 7 or 15");
		highBit = checked;
	}

	/**
	 * Sets imageType, type 1: GeneralImageModule,
	 * WholeSlideMicroscopyImageModule.
	 * @param imageType
	 */
	public void setImageType(String[] imageType)
	{
		if (imageType == null)
		{
			if (strict)
			{
				throw new IllegalArgumentException("ImageType must not be null");
			}
			this.imageType = new String[] {};
			return;
		}
		if (!strict)
		{
			this.imageType = Arrays.copyOf(imageType, imageType.length);
		}
		if (imageType.length != 4)
		{
			throw new IllegalArgumentException("ImageType must have 4 elements");
		}
		if (!Constants.Original.equals(imageType[0]) &&
			 !Constants.Derived.equals(imageType[0]))
		{
			throw new IllegalArgumentException(
				"ImageType value 1 must be ORIGINAL or DERIVED");
		}
		if (!Constants.Primary.equals(imageType[1]))
		{
			throw new IllegalArgumentException(
				"ImageType value 2 must be PRIMARY");
		}
		if (!Constants.Localizer.equals(imageType[2]) &&
			 !Constants.Volume.equals(imageType[2]) &&
			 !Constants.Label.equals(imageType[2]) &&
			 !Constants.Overview.equals(imageType[2]))
		{
			throw new IllegalArgumentException(
				"ImageType value 3 must be LOCALIZER, VOLUME, LABEL or OVERVIEW");
		}
		if (!Constants.None.equals(imageType[3]) &&
			 !Constants.Resampled.equals(imageType[3]))
		{
			throw new IllegalArgumentException(
				"ImageType value 4 must be NONE or RESMAPLED");
		}
		this.imageType = Arrays.copyOf(imageType, imageType.length);
	}

	/**
	 * Sets instance number, type 1: SopCommonModule, GeneralImageModule,
	 * WholeSlideMicroscopyImageModule, MultiFrameFunctionalGroupsModule.
	 * @param number 
	 */
	public void setInstanceNumber(int number) throws IllegalArgumentException
	{
		instNum = number;
	}

	/**
	 * Sets lossy image compression, type 1: GeneralImageModule,
	 * WholeSlideMicroscopyImageModule.
	 * @param lossyComp
	 */
	public void setLossyImageCompression(boolean lossyComp)
	{
		this.lossyComp = lossyComp;
	}

	/**
	 * Sets photometric interpretation, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule. Allowed terms: IODs 2022a C.8.12.1.1.1
	 * @param interpretation
	 */
	public void setPhotometricInterpretation(String interpretation)
		throws IllegalArgumentException
	{
		String checked = IodUtils.checkCondition(strict, interpretation,
			(x) -> interps.contains(x),
			"Photometric interpretation must be one of: "+Constants.Monochrome2
				+", "+Constants.Rgb+", "+Constants.YbrFull422+", "+Constants.YbrIct
				+", "+Constants.YbrRct);
		photoInterp = checked;
	}

	/**
	 * Sets pixel representation, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule.
	 * @param representation
	 */
	public void setPixelRepresentation(int representation)
		throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, representation,
			(int x) -> (x == 0),
			"Pixel representation must be 0");
		pixelRep = checked;
	}

	/**
	 * Sets planar configuration, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule.
	 * @param config
	 */
	public void setPlanarConfiguration(int config)
	{
		planarConfig = (config == 0) ? 0 : Integer.MIN_VALUE;
	}

	/**
	 * Sets samples per pixel, type 1: ImagePixelModule,
	 * WholeSlideMicroscopyImageModule.
	 * @param samples
	 */
	public void setSamplesPerPixel(int samples) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, samples,
			(int x) -> ((x == 1) || (x == 3)),
			"Samples per pixel must be 1 or 3");
		samplesPerPixel = checked;
	}

	/**
	 * Sets SOP instance UID, type 1: SopCommonModule
	 * @param uid
	 */
	public void setSopInstanceUid(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SopInstanceUid must not be null or empty");
		}
		sopInstUid = uid;
	}
	
}
