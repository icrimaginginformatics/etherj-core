/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;

/**
 *
 * @author jamesd
 */
public interface GeneralImageModule extends Displayable, Module
{

	/**
	 * Returns the acquisition date, type 3.
	 * @return
	 */
	String getAcquisitionDate();

	/**
	 * Returns the acquisition time, type 3.
	 * @return
	 */
	String getAcquisitionTime();

	/**
	 * Returns burned in image annotation, type 3.
	 * @return
	 */
	String getBurnedInAnnotation();

	/**
	 * Returns the content date, type 2C.
	 * @return
	 */
	String getContentDate();

	/**
	 * Returns the content time, type 2C.
	 * @return
	 */
	String getContentTime();

	/**
	 * Returns the image comments, type 3.
	 * @return
	 */
	String getImageComments();
	
	/**
	 * Returns the image type, type 3.
	 * @return
	 */
	String[] getImageType();
	
	/**
	 * Returns the instance number, type 2.
	 * @return
	 */
	int getInstanceNumber();

	/**
	 * Returns the lossy image compression flag, type 3.
	 * @return
	 */
	boolean getLossyImageCompression();

	/**
	 * Returns the content date, type 2C.
	 * @return
	 */
	String[] getPatientOrientation();

	/**
	 * Sets the acquisition date, type 3.
	 * @param date
	 * @throws IllegalArgumentException
	 */
	void setAcquisitionDate(String date) throws IllegalArgumentException;

	/**
	 * Sets the acquisition time, type 3.
	 * @param time
	 * @throws IllegalArgumentException
	 */
	void setAcquisitionTime(String time) throws IllegalArgumentException;

	/**
	 * Sets the burned in annotation flag, type 3.
	 * @param value
	 */
	void setBurnedInAnnotation(String value);

	/**
	 * Sets the content date, type 2C.
	 * @param date
	 * @throws IllegalArgumentException
	 */
	void setContentDate(String date) throws IllegalArgumentException;

	/**
	 * Sets the content time, type 2C.
	 * @param time
	 * @throws IllegalArgumentException
	 */
	void setContentTime(String time) throws IllegalArgumentException;

	/**
	 * Sets the image comments, type 3.
	 * @param comments
	 */
	void setImageComments(String comments);

	/**
	 * Sets the image type, type 3.
	 * @param type
	 */
	void setImageType(String[] type);

	/**
	 * Sets the instance number, type 2.
	 * @param number
	 */
	void setInstanceNumber(int number);

	/**
	 * Sets the lossy image compression flag, type 3.
	 * @param compressed
	 */
	void setLossyImageCompression(boolean compressed);

	/**
	 * Sets the content date, type 2C.
	 * @param orientation
	 * @throws IllegalArgumentException
	 */
	void setPatientOrientation(String[] orientation)
		throws IllegalArgumentException;

}
