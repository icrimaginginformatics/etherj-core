/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.AnnotationGroup;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.ReferencedImage;
import icr.etherj.dicom.iod.module.MicroscopyBulkSimpleAnnotationsModule;
import java.io.PrintStream;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class DefaultMicroscopyBulkSimpleAnnotationsModule
	extends AbstractDisplayable
	implements MicroscopyBulkSimpleAnnotationsModule
{
	private static final Logger logger = LoggerFactory.getLogger(
		MicroscopyBulkSimpleAnnotationsModule.class);
	private final SortedMap<Integer,AnnotationGroup> annoGroups =
		new TreeMap<>();
	private String coordType = Constants.Coord2D;
	private final MicroscopyBulkSimpleAnnotationsMultiModuleCore core;
	private String date = DicomUtils.formatDate(new Date());
	private String desc = "";
	private String label = "Label";
	private String pixelOrigin = Constants.Frame;
	private ReferencedImage refImage = null;
	private final boolean strict;
	private String time = DicomUtils.formatTime(new Date());

	public DefaultMicroscopyBulkSimpleAnnotationsModule(
		MicroscopyBulkSimpleAnnotationsMultiModuleCore core)
	{
		this(core, true);
	}

	public DefaultMicroscopyBulkSimpleAnnotationsModule(
		MicroscopyBulkSimpleAnnotationsMultiModuleCore core, boolean strict)
		throws IllegalArgumentException
	{
		if (core == null)
		{
			throw new IllegalArgumentException(
				"Microscopy bulk simple annotations multimodule core must not be null");
		}
		this.core = core;
		this.strict = strict;
	}

	@Override
	public boolean addAnnotationGroup(AnnotationGroup group)
	{
		if (group == null)
		{
			return false;
		}
		AnnotationGroup previous = annoGroups.put(
			group.getAnnotationGroupNumber(), group);
		// Return true if collection modified
		return (previous == null) || (previous != group);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"InstanceNumber: "+core.getInstanceNumber());
		ps.println(pad+"ContentLabel: "+label);
		ps.println(pad+"ContentDescription: "+desc);
		ps.println(pad+"ContentDate: "+date);
		ps.println(pad+"ContentTime: "+time);
		ps.println(pad+"AnnotationCoordinateType: "+coordType);
		if (Constants.Coord2D.equals(coordType))
		{
			ps.println(pad+"PixelOriginInterpretation: "+pixelOrigin);
			if (refImage != null)
			{
				ps.println(pad+"ReferencedImage:");
				refImage.display(ps, indent+"  ", recurse);
			}
			else
			{
				ps.println(pad+"ReferencedImage: MISSING");
			}
		}
		else
		{
			if (refImage != null)
			{
				ps.println(pad+"ReferencedImage:");
				refImage.display(ps, indent+"  ", recurse);
			}
		}
		int nAnno = annoGroups.size();
		ps.println(pad+"AnnotationGroupsList: "+nAnno+" item"+
			((nAnno != 1) ? "s" : ""));
		if (recurse)
		{
			annoGroups.values().forEach(
				(x) -> x.display(ps, indent+"  ", recurse));
		}
	}

	@Override
	public String getAnnotationCoordinateType()
	{
		return coordType;
	}

	@Override
	public List<AnnotationGroup> getAnnotationGroupList()
	{
		return ImmutableList.copyOf(annoGroups.values());
	}

	@Override
	public String getContentDate()
	{
		return date;
	}

	@Override
	public String getContentDescription()
	{
		return desc;
	}

	@Override
	public String getContentLabel()
	{
		return label;
	}

	@Override
	public String getContentTime()
	{
		return time;
	}

	@Override
	public int getInstanceNumber()
	{
		return core.getInstanceNumber();
	}

	@Override
	public String getPixelOriginInterpretation()
	{
		return pixelOrigin;
	}

	@Override
	public ReferencedImage getReferencedImage()
	{
		return refImage;
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public boolean removeAnnotationGroup(AnnotationGroup group)
	{
		if (group == null)
		{
			return false;
		}
		AnnotationGroup previous = annoGroups.remove(
			group.getAnnotationGroupNumber());
		// Return true if collection modified
		return previous != null;
	}

	@Override
	public void setAnnotationCoordinateType(String type)
		throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(type))
		{
			String msg = "AnnotationCoordinateType must not be null or empty";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			coordType = type;
		}
		switch (type)
		{
			case Constants.Coord2D:
			case Constants.Coord3D:
				coordType = type;
				return;
			default:
				String msg = "AnnotationCoordinateType must be 2D or 3D";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
				coordType = type;
		}
	}

	@Override
	public void setContentDate(String date) throws IllegalArgumentException
	{
		this.date = IodUtils.checkType1(strict, date, VR.DA, "Content Date");
	}

	@Override
	public void setContentDescription(String description)
	{
		desc = IodUtils.checkType2(strict, description);
	}

	@Override
	public void setContentLabel(String label) throws IllegalArgumentException
	{
		this.label = IodUtils.checkType1(strict, label, VR.CS, "Content Label");
	}

	@Override
	public void setContentTime(String time) throws IllegalArgumentException
	{
		this.time = IodUtils.checkType1(strict, time, VR.TM, "Content Time");
	}
	
	@Override
	public void setInstanceNumber(int number)
	{
		core.setInstanceNumber(number);
	}

	@Override
	public void setPixelOriginInterpretation(String interpretation)
		throws IllegalArgumentException
	{
		if (StringUtils.isNullOrEmpty(interpretation))
		{
			String msg = "PixelOriginInterpretation must not be null or empty";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			pixelOrigin = interpretation;
		}
		switch (interpretation)
		{
			case Constants.Frame:
			case Constants.Volume:
				pixelOrigin = interpretation;
				return;
			default:
				String msg = "PixelOriginInterpretation must be FRAME or VOLUME";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
				pixelOrigin = interpretation;
		}
	}

	@Override
	public void setReferencedImage(ReferencedImage image)
	{
		refImage = image;
	}

}
