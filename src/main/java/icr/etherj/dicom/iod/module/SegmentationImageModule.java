/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.dicom.iod.Segment;
import java.util.List;

/**
 *
 * @author jamesd
 */
public interface SegmentationImageModule extends GeneralImageModule
{

	/**
	 *
	 * @param segment
	 * @return
	 */
	boolean addSegment(Segment segment);

	/**
	 * Returns the bits allocated, type 1. Enumerated values: 1 or 8.
	 * @return
	 */
	int getBitsAllocated();

	/**
	 * Returns the bits stored, type 1. Enumerated values: 1 or 8
	 * @return
	 */
	int getBitsStored();

	/**
	 * Returns the content creator's name, type 2.
	 * @return
	 */
	String getContentCreatorsName();

	/**
	 * Returns the content description, type 2.
	 * @return
	 */
	String getContentDescription();

	/**
	 * Returns the content label, type 1.
	 * @return
	 */
	String getContentLabel();

	/**
	 * Returns the high bit, type 1. Enumerated values: 0 or 7
	 * @return
	 */
	int getHighBit();

	/**
	 * Returns the image type, type 1. Value 1 shall be DERIVED. Value 2
	 * shall be PRIMARY. No other values shall be present.
	 * @return
	 */
	@Override
	String[] getImageType();

	/**
	 * Returns the instance number, type 1.
	 * @return
	 */
	@Override
	int getInstanceNumber();

	/**
	 * Returns the lossy image compression flag, type 1.
	 * @return
	 */
	@Override
	boolean getLossyImageCompression();

	/**
	 * Returns the maximum fractional value, type 1C. This value represents a
	 * probability of 1 or complete occupancy.
	 * @return 
	 */
	int getMaximumFractionalValue();

	/**
	 * Returns the photometric interpretation, type 1. Enumerated values:
	 * MONOCHROME2.
	 * @return 
	 */
	String getPhotometricInterpretation();

	/**
	 * Returns the pixel representation, type 1.
	 * @return
	 */
	int getPixelRepresentation();

	/**
	 * Returns the number of samples per pixel, type 1.
	 * @return
	 */
	int getSamplesPerPixel();

	/**
	 * Returns the segmentation type, type 1. Enumerated values: BINARY,
	 * FRACTIONAL.
	 * @return
	 */
	String getSegmentationType();

	/**
	 * Returns the segmentation factional type, type 1C. Enumerated values:
	 * PROBABILITY, OCCUPANCY. Returns null if segmentation type is not
	 * fractional.
	 * @return
	 */
	String getSegmentationFractionalType();

	/**
	 *
	 * @return
	 */
	List<Segment> getSegmentList();

	/**
	 *
	 * @param segment
	 * @return
	 */
	boolean removeSegment(Segment segment);

	/**
	 * Sets the bits allocated, type 1. Enumerated values: 1 or 8.
	 * @param bits
	 * @throws IllegalArgumentException 
	 */
	void setBitsAllocated(int bits) throws IllegalArgumentException;

	/**
	 * Sets the bits stored, type 1. Enumerated values: 1 or 8.
	 * @param bits
	 * @throws IllegalArgumentException 
	 */
	void setBitsStored(int bits) throws IllegalArgumentException;

	/**
	 * Sets the content creator's name, type 2.
	 * @param name
	 */
	void setContentCreatorsName(String name);

	/**
	 * Sets the content description, type 2.
	 * @param description
	 */
	void setContentDescription(String description);

	/**
	 * Sets the content label, type 1.
	 * @param label
	 * @throws IllegalArgumentException 
	 */
	void setContentLabel(String label) throws IllegalArgumentException;

	/**
	 * Sets the high bit, type 1. Enumerated values: 0 or 7.
	 * @param bit
	 * @throws IllegalArgumentException 
	 */
	void setHighBit(int bit) throws IllegalArgumentException;

	/**
	 * Sets the instance number, type 1.
	 * @param number
	 * @throws IllegalArgumentException 
	 */
	@Override
	void setInstanceNumber(int number) throws IllegalArgumentException;

	/**
	 * Sets the lossy image compression flag, type 1.
	 * @param flag 
	 */
	@Override
	void setLossyImageCompression(boolean flag);

	/**
	 * Sets the maximum fractional value, type 1C. This value represents a
	 * probability of 1 or complete occupancy. 
	 * @param value
	 * @throws IllegalArgumentException 
	 */
	void setMaximumFractionalValue(int value) throws IllegalArgumentException;

	/**
	 * Sets the segmentation fractional type, type 1C. Enumerated values:
	 * PROBABILITY, OCCUPANCY.
	 * @param type
	 * @throws IllegalArgumentException 
	 */
	void setSegmentationFractionalType(String type)
		throws IllegalArgumentException;

	/**
	 * Sets the segmentation type, type 1. Enumerated values: BINARY,
	 * FRACTIONAL.
	 * @param type
	 * @throws IllegalArgumentException 
	 */
	void setSegmentationType(String type) throws IllegalArgumentException;

}
