/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.module.SopCommonModule;
import java.io.PrintStream;
import org.dcm4che2.data.UID;

/**
 *
 * @author jamesd
 */
public class WholeSlideMicroscopySopCommonModule
	extends DefaultSopCommonModule implements SopCommonModule
{
	private final WholeSlideMicroscopyMultiModuleCore wsmMultiModCore;

	public WholeSlideMicroscopySopCommonModule(WholeSlideMicroscopyMultiModuleCore wsmMultiModCore)
	{
		this(wsmMultiModCore, true);
	}

	public WholeSlideMicroscopySopCommonModule(
		WholeSlideMicroscopyMultiModuleCore wsmMultiModCore, boolean strict)
		throws IllegalArgumentException
	{
		super(UID.VLWholeSlideMicroscopyImageStorage, strict);
		if (wsmMultiModCore == null)
		{
			throw new IllegalArgumentException(
				"Whole slide microscopy multimodule core must not be null");
		}
		this.wsmMultiModCore = wsmMultiModCore;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ClassUid: "+wsmMultiModCore.getSopClassUid());
		ps.println(pad+"InstanceUid: "+
			wsmMultiModCore.getSopInstanceUid());
		int instNumber = wsmMultiModCore.getInstanceNumber();
		if (instNumber > 0)
		{
			ps.println(pad+"InstanceNumber: "+instNumber);
		}
		String longTempInfoMod = getLongitudinalTemporalInformationModified();
		if (!StringUtils.isNullOrEmpty(longTempInfoMod))
		{
			ps.println(
				pad+"LongitudinalTemporalInformationModified: "+longTempInfoMod);
		}
		String contentQual = getContentQualification();
		if (!StringUtils.isNullOrEmpty(contentQual))
		{
			ps.println(
				pad+"ContentQualification: "+contentQual);
		}
	}

	@Override
	public int getInstanceNumber()
	{
		return wsmMultiModCore.getInstanceNumber();
	}

	@Override
	public String getSopClassUid()
	{
		return wsmMultiModCore.getSopClassUid();
	}

	@Override
	public String getSopInstanceUid()
	{
		return wsmMultiModCore.getSopInstanceUid();
	}

	@Override
	public void setInstanceNumber(int number)
	{
		wsmMultiModCore.setInstanceNumber(number);
	}

	@Override
	public void setSopInstanceUid(String uid) throws IllegalArgumentException
	{
		wsmMultiModCore.setSopInstanceUid(uid);
	}
	
}
