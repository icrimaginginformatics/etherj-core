/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.OpticalPath;
import icr.etherj.dicom.iod.module.OpticalPathModule;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class DefaultOpticalPathModule extends AbstractDisplayable
	implements OpticalPathModule
{
	private final List<OpticalPath> paths = new ArrayList<>();
	private final boolean strict;

	public DefaultOpticalPathModule()
	{
		this(true);
	}

	public DefaultOpticalPathModule(boolean strict)
	{
		this.strict = strict;
	}

	@Override
	public boolean addOpticalPath(OpticalPath path)
	{
		if (path == null)
		{
			return false;
		}
		return paths.add(path);
	}

	@Override
	public void clearOpticalPaths()
	{
		paths.clear();
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		int nItems = paths.size();
		ps.println(pad+"OpticalPaths: "+nItems+" item"+
			((nItems != 1) ? "s" : ""));
		paths.forEach((code) -> code.display(ps, indent+"  "));
	}

	@Override
	public List<OpticalPath> getOpticalPathList()
	{
		return ImmutableList.copyOf(paths);
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

}
