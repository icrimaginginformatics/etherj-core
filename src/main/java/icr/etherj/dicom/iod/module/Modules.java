/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.dicom.NewTag;
import icr.etherj.dicom.iod.AnnotationGroup;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.CodingSchemeIdentifier;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.ContainerComponent;
import icr.etherj.dicom.iod.ContainerIdentifier;
import icr.etherj.dicom.iod.ContentItem;
import icr.etherj.dicom.iod.Contour;
import icr.etherj.dicom.iod.DimensionIndex;
import icr.etherj.dicom.iod.FunctionalGroupsFrame;
import icr.etherj.dicom.iod.FunctionalGroupsReferencedImage;
import icr.etherj.dicom.iod.HL7v2HierarchicDesignator;
import icr.etherj.dicom.iod.HeirarchicalSopInstanceReference;
import icr.etherj.dicom.iod.OpticalPath;
import icr.etherj.dicom.iod.PerFrameFunctionalGroups;
import icr.etherj.dicom.iod.PixelMeasures;
import icr.etherj.dicom.iod.ReferencedFrameOfReference;
import icr.etherj.dicom.iod.ReferencedImage;
import icr.etherj.dicom.iod.ReferencedSeries;
import icr.etherj.dicom.iod.RoiContour;
import icr.etherj.dicom.iod.RtRoiObservation;
import icr.etherj.dicom.iod.Segment;
import icr.etherj.dicom.iod.SegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SegmentationSharedFunctionalGroups;
import icr.etherj.dicom.iod.SharedFunctionalGroups;
import icr.etherj.dicom.iod.SourceImage;
import icr.etherj.dicom.iod.SpecimenDescription;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.WsmFunctionalGroupsFrame;
import icr.etherj.dicom.iod.WsmPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.WsmReferencedImage;
import icr.etherj.dicom.iod.WsmSharedFunctionalGroups;
import icr.etherj.dicom.iod.impl.DefaultCodingSchemeIdentifier;
import icr.etherj.dicom.iod.impl.DefaultContainerIdentifier;
import icr.etherj.dicom.iod.impl.DefaultRoiContour;
import icr.etherj.dicom.iod.impl.DefaultRtRoiObservation;
import icr.etherj.dicom.iod.impl.DefaultSegment;
import icr.etherj.dicom.iod.impl.DefaultStructureSetRoi;
import icr.etherj.dicom.iod.impl.DefaultSegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.impl.DefaultSegmentationSharedFunctionalGroups;
import icr.etherj.dicom.iod.module.impl.DefaultAcquisitionContextModule;
import icr.etherj.dicom.iod.module.impl.DefaultClinicalTrialSeriesModule;
import icr.etherj.dicom.iod.module.impl.DefaultClinicalTrialStudyModule;
import icr.etherj.dicom.iod.module.impl.DefaultClinicalTrialSubjectModule;
import icr.etherj.dicom.iod.module.impl.DefaultCommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.impl.DefaultEnhancedGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.impl.DefaultFrameOfReferenceModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralImageModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralReferenceModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralSeriesModule;
import icr.etherj.dicom.iod.module.impl.DefaultGeneralStudyModule;
import icr.etherj.dicom.iod.module.impl.DefaultImagePixelModule;
import icr.etherj.dicom.iod.module.impl.DefaultMicroscopyBulkSimpleAnnotationsSeriesModule;
import icr.etherj.dicom.iod.module.impl.DefaultMultiResolutionNavigationModule;
import icr.etherj.dicom.iod.module.impl.DefaultMultiframeDimensionModule;
import icr.etherj.dicom.iod.module.impl.DefaultMultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.impl.DefaultOpticalPathModule;
import icr.etherj.dicom.iod.module.impl.DefaultPatientModule;
import icr.etherj.dicom.iod.module.impl.DefaultPatientStudyModule;
import icr.etherj.dicom.iod.module.impl.DefaultRoiContourModule;
import icr.etherj.dicom.iod.module.impl.DefaultRtRoiObservationsModule;
import icr.etherj.dicom.iod.module.impl.DefaultRtSeriesModule;
import icr.etherj.dicom.iod.module.impl.DefaultSegmentationSeriesModule;
import icr.etherj.dicom.iod.module.impl.DefaultSlideLabelModule;
import icr.etherj.dicom.iod.module.impl.DefaultSopCommonModule;
import icr.etherj.dicom.iod.module.impl.DefaultSpecimenModule;
import icr.etherj.dicom.iod.module.impl.DefaultSrDocumentContentModule;
import icr.etherj.dicom.iod.module.impl.DefaultSrDocumentGeneralModule;
import icr.etherj.dicom.iod.module.impl.DefaultSrDocumentSeriesModule;
import icr.etherj.dicom.iod.module.impl.DefaultSynchronisationModule;
import icr.etherj.dicom.iod.module.impl.DefaultWholeSlideMicroscopySeriesModule;
import icr.etherj.dicom.iod.module.impl.SlideMicroscopyFrameOfReferenceModule;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory and utilities class for modules from DICOM IOD.
 * @author jamesd
 */
public class Modules
{
	private static final Logger logger = LoggerFactory.getLogger(Modules.class);

	/**
	 * Returns a new AcquisitionContextModule.
	 * @return the new module
	 */
	public static AcquisitionContextModule acquisitionContextModule()
	{
		return acquisitionContextModule(true);
	}

	/**
	 * Returns a new v.
	 * @param strict
	 * @return the new module
	 */
	public static AcquisitionContextModule acquisitionContextModule(
		boolean strict)
	{
		return new DefaultAcquisitionContextModule(strict);
	}

	/**
	 * Returns a new ClinicalTrialSeriesModule.
	 * @return the new module
	 */
	public static ClinicalTrialSeriesModule clinicalTrialSeriesModule()
	{
		return clinicalTrialSeriesModule(true);
	}

	/**
	 * Returns a new ClinicalTrialSeriesModule.
	 * @param strict
	 * @return the new module
	 */
	public static ClinicalTrialSeriesModule clinicalTrialSeriesModule(
		boolean strict)
	{
		return new DefaultClinicalTrialSeriesModule(strict);
	}

	/**
	 * Returns a new ClinicalTrialStudyModule.
	 * @return the new module
	 */
	public static ClinicalTrialStudyModule clinicalTrialStudyModule()
	{
		return clinicalTrialStudyModule(true);
	}

	/**
	 * Returns a new ClinicalTrialStudyModule.
	 * @param strict
	 * @return the new module
	 */
	public static ClinicalTrialStudyModule clinicalTrialStudyModule(
		boolean strict)
	{
		return new DefaultClinicalTrialStudyModule(strict);
	}

	/**
	 * Returns a new ClinicalTrialSubjectModule.
	 * @return the new module
	 */
	public static ClinicalTrialSubjectModule clinicalTrialSubjectModule()
	{
		return clinicalTrialSubjectModule(true);
	}

	/**
	 * Returns a new ClinicalTrialSubjectModule.
	 * @param strict
	 * @return the new module
	 */
	public static ClinicalTrialSubjectModule clinicalTrialSubjectModule(
		boolean strict)
	{
		return new DefaultClinicalTrialSubjectModule(strict);
	}

	/**
	 * Returns a new CommonInstanceReferenceModule.
	 * @return the new module
	 */
	public static CommonInstanceReferenceModule commonInstanceReferenceModule()
	{
		return commonInstanceReferenceModule(true);
	}

	/**
	 * Returns a new CommonInstanceReferenceModule.
	 * @param strict
	 * @return the new module
	 */
	public static CommonInstanceReferenceModule commonInstanceReferenceModule(
		boolean strict)
	{
		return new DefaultCommonInstanceReferenceModule(strict);
	}

	/**
	 * Returns a new EnhancedGeneralEquipmentModule.
	 * @return the new module
	 */
	public static EnhancedGeneralEquipmentModule enhancedGeneralEquipmentModule()
	{
		return enhancedGeneralEquipmentModule(true);
	}

	/**
	 * Returns a new EnhancedGeneralEquipmentModule.
	 * @param strict strict standard compliance
	 * @return the new module
	 */
	public static EnhancedGeneralEquipmentModule enhancedGeneralEquipmentModule(
		boolean strict)
	{
		return new DefaultEnhancedGeneralEquipmentModule(strict);
	}

	/**
	 * Returns a new FrameOfReferenceModule.
	 * @return the new module
	 */
	public static FrameOfReferenceModule frameOfReferenceModule()
	{
		return frameOfReferenceModule(true);
	}

	/**
	 * Returns a new FrameOfReferenceModule.
	 * @param strict
	 * @return the new module
	 */
	public static FrameOfReferenceModule frameOfReferenceModule(
		boolean strict)
	{
		return new DefaultFrameOfReferenceModule(strict);
	}

	/**
	 * Returns a new GeneralEquipmentModule.
	 * @return the new module
	 */
	public static GeneralEquipmentModule generalEquipmentModule()
	{
		return generalEquipmentModule(true);
	}

	/**
	 * Returns a new GeneralEquipmentModule.
	 * @param strict
	 * @return the new module
	 */
	public static GeneralEquipmentModule generalEquipmentModule(
		boolean strict)
	{
		return new DefaultGeneralEquipmentModule(strict);
	}

	/**
	 * Returns a new GeneralImageModule.
	 * @return the new module
	 */
	public static GeneralImageModule generalImageModule()
	{
		return new DefaultGeneralImageModule();
	}

	/**
	 * Returns a new GeneralReferenceModule.
	 * @return the new module
	 */
	public static GeneralReferenceModule generalReferenceModule()
	{
		return generalReferenceModule(true);
	}

	/**
	 * Returns a new GeneralReferenceModule.
	 * @param strict
	 * @return the new module
	 */
	public static GeneralReferenceModule generalReferenceModule(
		boolean strict)
	{
		return new DefaultGeneralReferenceModule(strict);
	}

	/**
	 * Returns a new GeneralSeriesModule.
	 * @return the new module
	 */
	public static GeneralSeriesModule generalSeriesModule()
	{
		return generalSeriesModule(true);
	}

	/**
	 * Returns a new GeneralSeriesModule.
	 * @param strict
	 * @return the new module
	 */
	public static GeneralSeriesModule generalSeriesModule(
		boolean strict)
	{
		return new DefaultGeneralSeriesModule(strict);
	}

	/**
	 * Returns a new GeneralStudyModule.
	 * @return the new module
	 */
	public static GeneralStudyModule generalStudyModule()
	{
		return generalStudyModule(true);
	}

	/**
	 * Returns a new GeneralStudyModule.
	 * @param strict
	 * @return the new module
	 */
	public static GeneralStudyModule generalStudyModule(
		boolean strict)
	{
		return new DefaultGeneralStudyModule(strict);
	}

	/**
	 * Returns a new ImagePixelModule.
	 * @return the new module
	 */
	public static ImagePixelModule imagePixelModule()
	{
		return new DefaultImagePixelModule();
	}

	/**
	 * Returns a new MicroscopyBulkSimpleAnnotationsSeriesModule.
	 * @return the new module
	 */
	public static MicroscopyBulkSimpleAnnotationsSeriesModule
		microscopyBulkSimpleAnnotationsSeriesModule()
	{
		return microscopyBulkSimpleAnnotationsSeriesModule(true);
	}

	/**
	 * Returns a new MicroscopyBulkSimpleAnnotationsSeriesModule.
	 * @param strict
	 * @return the new module
	 */
	public static MicroscopyBulkSimpleAnnotationsSeriesModule
		microscopyBulkSimpleAnnotationsSeriesModule(boolean strict)
	{
		return new DefaultMicroscopyBulkSimpleAnnotationsSeriesModule(strict);
	}

	/**
	 * Returns a new MultiframeDimensionModule.
	 * @return the new module
	 */
	public static MultiframeDimensionModule multiframeDimensionModule()
	{
		return multiframeDimensionModule(true);
	}

	/**
	 * Returns a new MultiframeDimensionModule.
	 * @param strict
	 * @return the new module
	 */
	public static MultiframeDimensionModule multiframeDimensionModule(
		boolean strict)
	{
		return new DefaultMultiframeDimensionModule(strict);
	}

	/**
	 * Returns a new MultiframeFunctionalGroupsModule.
	 * @param sopClassUid
	 * @return the new module
	 * @throws IllegalArgumentException
	 */
	public static MultiframeFunctionalGroupsModule multiframeFunctionalGroupsModule(
		String sopClassUid) throws IllegalArgumentException
	{
		SharedFunctionalGroups sharedGroups = null;
		PerFrameFunctionalGroups perFrameGroups = null;
		switch (sopClassUid)
		{
			case UID.SegmentationStorage:
				sharedGroups = new DefaultSegmentationSharedFunctionalGroups();
				perFrameGroups = new DefaultSegmentationPerFrameFunctionalGroups();
				break;
			default:
				throw new IllegalArgumentException(
					"Unknown or unsupported SOP class UID: "+sopClassUid);
		}
		return new DefaultMultiframeFunctionalGroupsModule(sopClassUid,
			sharedGroups, perFrameGroups);
	}

	/**
	 * Returns a new MultiResolutionNavigationModule.
	 * @return the new module
	 */
	public static MultiResolutionNavigationModule multiResolutionNavigationModule()
	{
		return multiResolutionNavigationModule(true);
	}

	/**
	 * Returns a new MultiResolutionNavigationModule.
	 * @param strict
	 * @return the new module
	 */
	public static MultiResolutionNavigationModule multiResolutionNavigationModule(
		boolean strict)
	{
		return new DefaultMultiResolutionNavigationModule(strict);
	}

	/**
	 * Returns a new OpticalPathModule.
	 * @return the new module
	 */
	public static OpticalPathModule opticalPathModule()
	{
		return opticalPathModule(true);
	}

	/**
	 * Returns a new OpticalPathModule.
	 * @param strict
	 * @return the new module
	 */
	public static OpticalPathModule opticalPathModule(
		boolean strict)
	{
		return new DefaultOpticalPathModule(strict);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(AcquisitionContextModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		DicomElement acqCtxSq = Pack.getOrCreateSq(dcm,
			Tag.AcquisitionContextSequence);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(ClinicalTrialSeriesModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(ClinicalTrialStudyModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		String id = module.getClinicalTrialTimePointId();
		if (id == null)
		{
			return;
		}
		dcm.putString(Tag.ClinicalTrialTimePointID, VR.LO, id);
		String desc = module.getClinicalTrialTimePointDescription();
		if (desc != null)
		{
			dcm.putString(Tag.ClinicalTrialTimePointDescription, VR.ST, desc);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(ClinicalTrialSubjectModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		String sponsor = module.getClinicalTrialSponsorName();
		if (sponsor == null)
		{
			return;
		}
		dcm.putString(Tag.ClinicalTrialSponsorName, VR.LO,
			module.getClinicalTrialSponsorName());
		dcm.putString(Tag.ClinicalTrialProtocolID, VR.LO,
			module.getClinicalTrialProtocolId());
		dcm.putString(Tag.ClinicalTrialProtocolName, VR.LO,
			module.getClinicalTrialProtocolName());
		dcm.putString(Tag.ClinicalTrialSiteID, VR.LO,
			module.getClinicalTrialSiteId());
		dcm.putString(Tag.ClinicalTrialSiteName, VR.LO,
			module.getClinicalTrialSiteName());
		String subjId = module.getClinicalTrialSubjectId();
		if (subjId != null)
		{
			dcm.putString(Tag.ClinicalTrialSubjectID, VR.LO, subjId);
		}
		else
		{
			String readId = module.getClinicalTrialSubjectReadingId();
			if (readId == null)
			{
				throw new IllegalArgumentException(
					"Subject ID and subject reading ID cannot both be null");
			}
			dcm.putString(Tag.ClinicalTrialSubjectReadingID, VR.LO, readId);
		}	
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(CommonInstanceReferenceModule module,
		DicomObject dcm)
	{
		List<ReferencedSeries> refSeriesList = module.getReferencedSeriesList();
		if (refSeriesList.isEmpty())
		{
			return;
		}
		dcm.putSequence(Tag.ReferencedSeriesSequence);
		DicomElement refSerSq = dcm.get(Tag.ReferencedSeriesSequence);
		for (ReferencedSeries refSeries : refSeriesList)
		{
			DicomObject item = new BasicDicomObject();
			Pack.packReferencedSeries(refSeries, item);
			refSerSq.addDicomObject(item);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(EnhancedGeneralEquipmentModule module,
		DicomObject dcm)
	{
		pack((GeneralEquipmentModule) module, dcm);
		dcm.putString(Tag.Manufacturer, VR.LO, module.getManufacturer());
		dcm.putString(Tag.ManufacturerModelName, VR.LO, 
			module.getManufacturersModelName());
		dcm.putString(Tag.DeviceSerialNumber, VR.LO, module.getDeviceSerialNumber());
		dcm.putString(Tag.SoftwareVersions, VR.LO, module.getSoftwareVersion());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(FrameOfReferenceModule module, DicomObject dcm)
	{
		String forUid = module.getFrameOfReferenceUid();
		if (forUid == null)
		{
			return;
		}
		dcm.putString(Tag.FrameOfReferenceUID, VR.UI, forUid);
		dcm.putString(Tag.PositionReferenceIndicator, VR.LO,
			module.getPositionReferenceIndicator());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(GeneralEquipmentModule module, DicomObject dcm)
	{
		dcm.putString(Tag.Manufacturer, VR.LO, module.getManufacturer());
		String modelName = module.getManufacturersModelName();
		if ((modelName != null) && !modelName.isEmpty())
		{
			dcm.putString(Tag.ManufacturerModelName, VR.LO, modelName);
		}
		String serial = module.getDeviceSerialNumber();
		if ((serial != null) && !serial.isEmpty())
		{
			dcm.putString(Tag.DeviceSerialNumber, VR.LO, serial);
		}
		String software = module.getSoftwareVersion();
		if ((software != null) && !software.isEmpty())
		{
			dcm.putString(Tag.SoftwareVersions, VR.LO, software);
		}
		double spatialRes = module.getSpatialResolution();
		if (Double.isFinite(spatialRes))
		{
			dcm.putDouble(Tag.SpatialResolution, VR.DS, spatialRes);
		}
		int pixPadding = module.getPixelPaddingValue();
		if (pixPadding != Integer.MIN_VALUE)
		{
			dcm.putInt(Tag.PixelPaddingValue, VR.US, pixPadding);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(GeneralImageModule module, DicomObject dcm)
	{
		dcm.putInt(Tag.InstanceNumber, VR.IS, module.getInstanceNumber());
		Pack.putIfExists(module.getPatientOrientation(), dcm, Tag.PatientOrientation,
			VR.CS);
		Pack.putIfExists(module.getContentDate(), dcm, Tag.ContentDate, VR.DA);
		Pack.putIfExists(module.getContentTime(), dcm, Tag.ContentTime, VR.TM);
		Pack.putIfExists(module.getImageType(), dcm, Tag.ImageType, VR.CS);
		Pack.putIfExists(module.getAcquisitionDate(), dcm, Tag.AcquisitionDate, VR.DA);
		Pack.putIfExists(module.getAcquisitionTime(), dcm, Tag.AcquisitionTime, VR.TM);
		Pack.putIfExists(module.getImageComments(), dcm, Tag.ImageComments, VR.LT);
		Pack.putIfExists(module.getBurnedInAnnotation(), dcm, Tag.BurnedInAnnotation,
			VR.CS);
		if (module.getLossyImageCompression())
		{
			dcm.putString(Tag.LossyImageCompression, VR.CS, "01");
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(GeneralReferenceModule module, DicomObject dcm)
	{
		List<ReferencedImage> refImages = module.getReferencedImageList();
		if (!refImages.isEmpty())
		{
			dcm.putSequence(Tag.ReferencedImageSequence);
			DicomElement refSq = dcm.get(Tag.ReferencedImageSequence);
			refImages.forEach((image) -> Pack.packReferencedImage(image, refSq));
		}
		Pack.putIfExists(module.getDerivationDescription(), dcm,
			Tag.DerivationDescription, VR.ST);
		List<Code> derivCodes = module.getDerivationCodeList();
		if (!derivCodes.isEmpty())
		{
			dcm.putSequence(Tag.DerivationCodeSequence);
			DicomElement codeSq = dcm.get(Tag.DerivationCodeSequence);
			derivCodes.forEach((code) -> Pack.packCode(code, codeSq));
		}
		List<SourceImage> sourceImages = module.getSourceImageList();
		if (!sourceImages.isEmpty())
		{
			dcm.putSequence(Tag.SourceImageSequence);
			DicomElement srcSq = dcm.get(Tag.SourceImageSequence);
			sourceImages.forEach((image) -> Pack.packSourceImage(image, srcSq));
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(GeneralSeriesModule module, DicomObject dcm)
	{
		dcm.putString(Tag.Modality, VR.CS, module.getModality());
		dcm.putString(Tag.SeriesInstanceUID, VR.UI, module.getSeriesInstanceUid());
		int number = module.getSeriesNumber();
		dcm.putString(Tag.SeriesNumber, VR.IS,
			(number > 0) ? Integer.toString(number) : "");
		Pack.putIfExists(module.getLaterality(), dcm, Tag.Laterality, VR.CS);
		Pack.putIfExists(module.getSeriesDate(), dcm, Tag.SeriesDate, VR.DA);
		Pack.putIfExists(module.getSeriesTime(), dcm, Tag.SeriesTime, VR.TM);
		Pack.putIfExists(module.getSeriesDescription(), dcm, Tag.SeriesDescription,
			VR.LO);
		Pack.putIfExists(module.getOperatorsName(), dcm, Tag.OperatorsName, VR.PN);
		Pack.putIfExists(module.getProtocolName(), dcm, Tag.ProtocolName, VR.LO);
		Pack.putIfExists(module.getPatientPosition(), dcm, Tag.PatientPosition, VR.CS);
		int minPix = module.getSmallestPixelValueInSeries();
		if (minPix >= 0)
		{
			dcm.putInt(Tag.SmallestPixelValueInSeries, VR.US, minPix);
		}
		int maxPix = module.getLargestPixelValueInSeries();
		if (maxPix >= 0)
		{
			dcm.putInt(Tag.LargestPixelValueInSeries, VR.US, maxPix);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(GeneralStudyModule module, DicomObject dcm)
	{
		dcm.putString(Tag.StudyInstanceUID, VR.UI, module.getStudyInstanceUid());
		dcm.putString(Tag.StudyDate, VR.DA, module.getStudyDate());
		dcm.putString(Tag.StudyTime, VR.TM, module.getStudyTime());
		dcm.putString(Tag.ReferringPhysicianName, VR.PN,
			module.getReferringPhysicianName());
		dcm.putString(Tag.StudyID, VR.SH, module.getStudyId());
		dcm.putString(Tag.AccessionNumber, VR.SH, module.getAccessionNumber());
		Pack.putIfExists(module.getStudyDescription(), dcm, Tag.StudyDescription, VR.LO);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(ImagePixelModule module, DicomObject dcm)
	{
		byte[] pixelData = module.getPixelData();
		if (pixelData.length > 0)
		{
			dcm.putBytes(Tag.PixelData, VR.OB, pixelData);
		}
		else
		{
			DicomElement pixels = dcm.putFragments(Tag.PixelData, VR.OB, true);
			List<byte[]> frags = module.getPixelDataFragments();
			frags.forEach((x) -> pixels.addFragment(x));
		}
		dcm.putInt(Tag.SamplesPerPixel, VR.US, module.getSamplesPerPixel());
		dcm.putString(Tag.PhotometricInterpretation, VR.CS,
			module.getPhotometricInterpretation());
		dcm.putInt(Tag.Rows, VR.US, module.getRowCount());
		dcm.putInt(Tag.Columns, VR.US, module.getColumnCount());
		dcm.putInt(Tag.BitsAllocated, VR.US, module.getBitsAllocated());
		dcm.putInt(Tag.BitsStored, VR.US, module.getBitsStored());
		dcm.putInt(Tag.HighBit, VR.US, module.getHighBit());
		dcm.putInt(Tag.PixelRepresentation, VR.US, module.getPixelRepresentation());
		if (module.getSamplesPerPixel() > 1)
		{
			dcm.putInt(Tag.PlanarConfiguration, VR.US,
				module.getPlanarConfiguration());
		}
		int value = module.getSmallestPixelValue();
		if (value >= 0)
		{
			dcm.putInt(Tag.SmallestImagePixelValue, VR.US, value);
		}
		value = module.getLargestPixelValue();
		if (value >= 0)
		{
			dcm.putInt(Tag.LargestImagePixelValue, VR.US, value);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(MicroscopyBulkSimpleAnnotationsModule module,
		DicomObject dcm)
	{
		dcm.putInt(Tag.InstanceNumber, VR.IS, module.getInstanceNumber());
		dcm.putString(Tag.ContentLabel, VR.CS, module.getContentLabel());
		dcm.putString(Tag.ContentDescription, VR.LO,
			module.getContentDescription());
		dcm.putString(Tag.ContentDate, VR.DA, module.getContentDate());
		dcm.putString(Tag.ContentTime, VR.TM, module.getContentTime());

		String coordType = module.getAnnotationCoordinateType();
		dcm.putString(NewTag.AnnotationCoordinateType, VR.CS, coordType);
		if (Constants.Coord2D.equals(coordType))
		{
			dcm.putString(Tag.PixelOriginInterpretation, VR.CS,
				module.getPixelOriginInterpretation());
			DicomElement refImageSq = Pack.getOrCreateSq(
				dcm, Tag.ReferencedImageSequence);
			Pack.packReferencedImage(module.getReferencedImage(), refImageSq);
		}
		else if (Constants.Coord3D.equals(coordType))
		{
			ReferencedImage image = module.getReferencedImage();
			if (image != null)
			{
				DicomElement refImageSq = Pack.getOrCreateSq(
					dcm, Tag.ReferencedImageSequence);
				Pack.packReferencedImage(module.getReferencedImage(), refImageSq);
			}
		}

		DicomElement annoGrpSq = Pack.getOrCreateSq(dcm, NewTag.AnnotationGroupSequence);
		List<AnnotationGroup> groups = module.getAnnotationGroupList();
		for (int i=0; i<groups.size(); i++)
		{
			AnnotationGroup group = groups.get(i);
			// Force numbering incrementing from 1
			group.setAnnotationGroupNumber(i+1);
			Pack.packAnnotationGroup(group, annoGrpSq, coordType);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(MicroscopyBulkSimpleAnnotationsSeriesModule module,
		DicomObject dcm)
	{
		pack((GeneralSeriesModule) module, dcm);
		dcm.putString(Tag.Modality, VR.CS, module.getModality());
		dcm.putInt(Tag.SeriesNumber, VR.IS, module.getSeriesNumber());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param parentDcm the target
	 */
	public static void pack(MultiframeDimensionModule module,
		DicomObject parentDcm)
	{
		parentDcm.putSequence(Tag.DimensionOrganizationSequence);
		DicomElement dimOrgSq = parentDcm.get(Tag.DimensionOrganizationSequence);
		for (String uid : module.getDimensionOrganisationUidList())
		{
			DicomObject dcm = new BasicDicomObject();
			dcm.putString(Tag.DimensionOrganizationUID, VR.UI, uid);
			dimOrgSq.addDicomObject(dcm);
		}
		String dimOrgType = module.getDimensionOrganisationType();
		Pack.putIfExists(dimOrgType, parentDcm, Tag.DimensionOrganizationType, VR.CS);
		boolean dimIdxReq = (dimOrgType == null) ||
			!Constants.TiledFull.equals(dimOrgType);
		List<DimensionIndex> dimIdxList = module.getDimensionIndexList();
		if (dimIdxReq || !dimIdxList.isEmpty())
		{
			parentDcm.putSequence(Tag.DimensionIndexSequence);
			DicomElement dimIdxSq = parentDcm.get(Tag.DimensionIndexSequence);
			dimIdxList.forEach(idx -> Pack.packDimensionIndex(idx, dimIdxSq));
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(MultiframeFunctionalGroupsModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		String sopClassUid = module.getSopClassUid();
		switch (sopClassUid)
		{
			case UID.SegmentationStorage:
				Pack.packSegmentationStorage(
					(SegmentationMultiframeFunctionalGroupsModule) module, dcm);
				break;
			case UID.VLWholeSlideMicroscopyImageStorage:
				packWsmImageStorage(
					(WsmMultiframeFunctionalGroupsModule) module, dcm);
				break;
			default:
				throw new IllegalArgumentException(
					"Unknown or unsupported SOP class UID");
		}
		dcm.putInt(Tag.InstanceNumber, VR.IS, module.getInstanceNumber());
		dcm.putString(Tag.ContentDate, VR.DA, module.getContentDate());
		dcm.putString(Tag.ContentTime, VR.TM, module.getContentTime());
		dcm.putInt(Tag.NumberOfFrames, VR.IS, module.getNumberOfFrames());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(MultiResolutionNavigationModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		if (!module.isEnabled())
		{
			return;
		}
		DicomElement sq = Pack.getOrCreateSq(dcm,
			Tag.ReferencedImageNavigationSequence);
		module.getReferencedImageList().forEach((image) -> Pack.packWsmReferencedImage(image, sq));
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(OpticalPathModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		List<OpticalPath> pathList = module.getOpticalPathList();
		dcm.putInt(NewTag.NumberOfOpticalPaths, VR.UL, pathList.size());
		DicomElement pathSq = Pack.getOrCreateSq(dcm, Tag.OpticalPathSequence);
		pathList.forEach((path) -> Pack.packOpticalPath(path, pathSq));
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(PatientModule module, DicomObject dcm)
	{
		dcm.putString(Tag.PatientName, VR.PN, module.getPatientName());
		dcm.putString(Tag.PatientID, VR.LO, module.getPatientId());
		dcm.putString(Tag.PatientBirthDate, VR.DA, module.getPatientBirthDate());
		dcm.putString(Tag.PatientSex, VR.CS, module.getPatientSex());
		Pack.putIfExists(module.getPatientIdentityRemoved(), dcm,
			Tag.PatientIdentityRemoved, VR.CS);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(PatientStudyModule module, DicomObject dcm)
	{
		Pack.putIfExists(module.getPatientAge(), dcm, Tag.PatientAge, VR.AS);
		Pack.putIfFinite(module.getPatientSize(), dcm, Tag.PatientSize, VR.DS);
		Pack.putIfFinite(module.getPatientWeight(), dcm, Tag.PatientWeight, VR.DS);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException if no contours found
	 */
	public static void pack(RoiContourModule module, DicomObject dcm)
		throws IllegalArgumentException
	{
		List<RoiContour> list = module.getRoiContourList();
		if (list.size() < 1)
		{
			throw new IllegalArgumentException("No RoiContours present");
		}
		dcm.putSequence(Tag.ROIContourSequence);
		DicomElement rcSq = dcm.get(Tag.ROIContourSequence);
		for (RoiContour rc : list)
		{
			DicomObject rcItem = new BasicDicomObject();
			rcItem.putInt(Tag.ReferencedROINumber, VR.IS,
				rc.getReferencedRoiNumber());
			int[] rgb = rc.getRoiDisplayColour();
			if ((rgb != null) && (rgb.length == 3))
			{
				rcItem.putInts(Tag.ROIDisplayColor, VR.IS, rgb);
			}
			rcSq.addDicomObject(rcItem);
			List<Contour> conList = rc.getContourList();
			if (conList.isEmpty())
			{
				continue;
			}
			Pack.packContours(conList, rcItem);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException if no observations found
	 */
	public static void pack(RtRoiObservationsModule module, DicomObject dcm)
		throws IllegalArgumentException
	{
		List<RtRoiObservation> list = module.getRtRoiObservationList();
		if (list.isEmpty())
		{
			throw new IllegalArgumentException("No RtRoiObservations present");
		}
		dcm.putSequence(Tag.RTROIObservationsSequence);
		DicomElement obsSq = dcm.get(Tag.RTROIObservationsSequence);
		for (RtRoiObservation obs : list)
		{
			DicomObject obsItem = new BasicDicomObject();
			obsItem.putInt(Tag.ObservationNumber, VR.IS,
				obs.getObservationNumber());
			obsItem.putInt(Tag.ReferencedROINumber, VR.IS,
				obs.getReferencedRoiNumber());
			String label = obs.getRoiObservationLabel();
			if ((label != null) && !label.isEmpty())
			{
				obsItem.putString(Tag.ROIObservationLabel, VR.SH, label);
			}
			String desc = obs.getRoiObservationDescription();
			if ((desc != null) && !desc.isEmpty())
			{
				obsItem.putString(Tag.ROIObservationDescription, VR.ST, desc);
			}
			obsItem.putString(Tag.RTROIInterpretedType, VR.CS,
				obs.getRtRoiIntepretedType());
			obsItem.putString(Tag.ROIInterpreter, VR.PN, obs.getRoiInterpreter());
			obsSq.addDicomObject(obsItem);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(RtSeriesModule module, DicomObject dcm)
	{
		dcm.putString(Tag.Modality, VR.CS, module.getModality());
		dcm.putString(Tag.SeriesInstanceUID, VR.UI, module.getSeriesInstanceUid());
		int number = module.getSeriesNumber();
		dcm.putString(Tag.SeriesNumber, VR.IS,
			(number > 0) ? Integer.toString(number) : "");
		String date = module.getSeriesDate();
		if ((date != null) && !date.isEmpty())
		{
			dcm.putString(Tag.SeriesDate, VR.DA, date);
		}
		String time = module.getSeriesTime();
		if ((time != null) && !time.isEmpty())
		{
			dcm.putString(Tag.SeriesTime, VR.TM, time);
		}
		String desc = module.getSeriesDescription();
		if ((desc != null) && !desc.isEmpty())
		{
			dcm.putString(Tag.SeriesDescription, VR.LO, desc);
		}
		dcm.putString(Tag.OperatorsName, VR.PN, module.getOperatorsName());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(SegmentationImageModule module, DicomObject dcm)
	{
		pack((GeneralImageModule) module, dcm);
		dcm.putStrings(Tag.ImageType, VR.CS, new String[] {"DERIVED","PRIMARY"});
		dcm.putInt(Tag.InstanceNumber, VR.IS, module.getInstanceNumber());
		dcm.putString(Tag.ContentLabel, VR.CS, module.getContentLabel());
		dcm.putString(Tag.ContentDescription, VR.LO, module.getContentDescription());
		dcm.putString(Tag.ContentCreatorName, VR.PN, module.getContentCreatorsName());
		dcm.putInt(Tag.SamplesPerPixel, VR.US, module.getSamplesPerPixel());
		dcm.putString(Tag.PhotometricInterpretation, VR.CS,
			module.getPhotometricInterpretation());
		dcm.putInt(Tag.PixelRepresentation, VR.US, module.getPixelRepresentation());
		dcm.putInt(Tag.BitsAllocated, VR.US, module.getBitsAllocated());
		dcm.putInt(Tag.BitsStored, VR.US, module.getBitsStored());
		dcm.putInt(Tag.HighBit, VR.US, module.getHighBit());
		dcm.putString(Tag.LossyImageCompression, VR.CS,
			module.getLossyImageCompression() ? "01" : "00");
		String segType = module.getSegmentationType();
		dcm.putString(Tag.SegmentationType, VR.CS, segType);
		if (Constants.Fractional.equals(segType))
		{
			dcm.putString(Tag.SegmentationFractionalType, VR.CS,
				module.getSegmentationFractionalType());
			dcm.putInt(Tag.MaximumFractionalValue, VR.US,
				module.getMaximumFractionalValue());
		}
		dcm.putSequence(Tag.SegmentSequence);
		DicomElement segSq = dcm.get(Tag.SegmentSequence);
		for (Segment segment : module.getSegmentList())
		{
			Pack.packSegment(segment, segSq);
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(SegmentationSeriesModule module, DicomObject dcm)
	{
		pack((GeneralSeriesModule) module, dcm);
		dcm.putString(Tag.Modality, VR.CS, module.getModality());
		dcm.putInt(Tag.SeriesNumber, VR.IS, module.getSeriesNumber());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(SlideLabelModule module, DicomObject dcm)
		throws IllegalArgumentException
	{
		if (!module.isEnabled())
		{
			return;
		}
		dcm.putString(Tag.BarcodeValue, VR.LT, module.getBarcodeValue());
		dcm.putString(Tag.LabelText, VR.UT, module.getLabelText());
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 */
	public static void pack(SopCommonModule module, DicomObject dcm)
	{
		dcm.putString(Tag.SOPClassUID, VR.UI, module.getSopClassUid());
		dcm.putString(Tag.SOPInstanceUID, VR.UI, module.getSopInstanceUid());
		int number = module.getInstanceNumber();
		if (number > 0)
		{
			dcm.putInt(Tag.InstanceNumber, VR.IS, number);
		}
		Pack.putIfExists(module.getLongitudinalTemporalInformationModified(),
			dcm, Tag.LongitudinalTemporalInformationModified, VR.CS);
		Pack.putIfExists(module.getContentQualification(), dcm,
			Tag.ContentQualification, VR.CS);
		List<CodingSchemeIdentifier> csiList =
			module.getCodingSchemeIdentifierList();
		if (!csiList.isEmpty())
		{
			dcm.putSequence(Tag.CodingSchemeIdentificationSequence);
			DicomElement csiSq = dcm.get(Tag.CodingSchemeIdentificationSequence);
			for (CodingSchemeIdentifier csi : csiList)
			{
				DicomObject csiDcm = new BasicDicomObject();
				csiDcm.putString(Tag.CodingSchemeDesignator, VR.SH,
					csi.getCodingSchemeDesignator());
				Pack.putIfExists(csi.getCodingSchemeName(), csiDcm, Tag.CodingSchemeName,
					VR.ST);
				Pack.putIfExists(csi.getCodingSchemeVersion(), csiDcm,
					Tag.CodingSchemeVersion, VR.SH);
				Pack.putIfExists(csi.getCodingSchemeResponsibleOrganisation(), csiDcm, 
					Tag.CodingSchemeResponsibleOrganization, VR.ST);
				csiSq.addDicomObject(csiDcm);
			}
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(SpecimenModule module, DicomObject dcm)
		throws IllegalArgumentException
	{
		ContainerIdentifier ident = module.getContainerIdentifier();
		dcm.putString(Tag.ContainerIdentifier, VR.LO, ident.getIdentifier());
		DicomElement issuerSq = Pack.getOrCreateSq(dcm,
			Tag.IssuerOfTheContainerIdentifierSequence);
		ident.getIssuerList().forEach((issuer) -> Pack.packIssuer(issuer, issuerSq));
		DicomElement typeCodeSq = Pack.getOrCreateSq(dcm,
			Tag.ContainerTypeCodeSequence);
		module.getContainerTypeCodeList().forEach((code) -> Pack.packCode(code, typeCodeSq));
		List<ContainerComponent> compList = module.getContainerComponentList();
		if (!compList.isEmpty())
		{
			DicomElement compSq = Pack.getOrCreateSq(dcm, Tag.ContainerComponentSequence);
			compList.forEach((cc) -> Pack.packContainerComponent(cc, compSq));
		}
		DicomElement specDescSq = Pack.getOrCreateSq(dcm,
			Tag.SpecimenDescriptionSequence);
		module.getSpecimenDescriptionList().forEach((desc) -> Pack.packSpecimenDescription(desc, specDescSq));
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(SrDocumentContentModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		ContentItem.ContainerItem rootItem =  module.getRootContentItem();
		Pack.packContentItem((ContentItem) rootItem, dcm);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(SrDocumentGeneralModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		dcm.putString(Tag.InstanceNumber, VR.IS,
			Integer.toString(module.getInstanceNumber()));
		dcm.putString(Tag.CompletionFlag, VR.CS, module.getCompletionFlag());
		dcm.putString(Tag.VerificationFlag, VR.CS, module.getVerificationFlag());
		dcm.putString(Tag.ContentDate, VR.DA, module.getContentDate());
		dcm.putString(Tag.ContentTime, VR.TM, module.getContentTime());
		dcm.putSequence(Tag.PerformedProcedureCodeSequence);
		DicomElement perfProcSq = dcm.get(Tag.PerformedProcedureCodeSequence);
		for (Code ppCode : module.getPerformedProcedureCodeList())
		{
			Pack.packCode(ppCode, perfProcSq);
		}
		List<HeirarchicalSopInstanceReference> crpeList = 
			module.getCurrentRequestedProcedureEvidenceList();
		if (!crpeList.isEmpty())
		{
			dcm.putSequence(Tag.CurrentRequestedProcedureEvidenceSequence);
			DicomElement crpeSq = dcm.get(
				Tag.CurrentRequestedProcedureEvidenceSequence);
			for (HeirarchicalSopInstanceReference ref : crpeList)
			{
				DicomObject refDcm = new BasicDicomObject();
				Pack.packHeirSopInstRef(ref, refDcm);
				crpeSq.addDicomObject(refDcm);
			}
		}
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(SrDocumentSeriesModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		dcm.putString(Tag.Modality, VR.CS, module.getModality());
		dcm.putString(Tag.SeriesInstanceUID, VR.UI, module.getSeriesInstanceUid());
		dcm.putString(Tag.SeriesNumber, VR.IS,
			Integer.toString(module.getSeriesNumber()));
		String date = module.getSeriesDate();
		if ((date != null) && !date.isEmpty())
		{
			dcm.putString(Tag.SeriesDate, VR.DA, date);
		}
		String time = module.getSeriesTime();
		if ((time != null) && !time.isEmpty())
		{
			dcm.putString(Tag.SeriesTime, VR.TM, time);
		}
		String desc = module.getSeriesDescription();
		if ((desc != null) && !desc.isEmpty())
		{
			dcm.putString(Tag.SeriesDescription, VR.LO, desc);
		}
		dcm.putSequence(Tag.ReferencedPerformedProcedureStepSequence);
		DicomElement rppsSq = dcm.get(Tag.ReferencedPerformedProcedureStepSequence);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException if structure set is invalid
	 */
	public static void pack(StructureSetModule module, DicomObject dcm)
		throws IllegalArgumentException
	{
		dcm.putString(Tag.StructureSetLabel, VR.SH, module.getStructureSetLabel());
		String name = module.getStructureSetName();
		if ((name != null) && !name.isEmpty())
		{
			dcm.putString(Tag.StructureSetName, VR.LO, name);
		}
		String desc = module.getStructureSetDescription();
		if ((desc != null) && !desc.isEmpty())
		{
			dcm.putString(Tag.StructureSetDescription, VR.ST, desc);
		}
		int number = module.getInstanceNumber();
		if (number > 0)
		{
			dcm.putInt(Tag.InstanceNumber, VR.IS, number);
		}
		dcm.putString(Tag.StructureSetDate, VR.DA, module.getStructureSetDate());
		dcm.putString(Tag.StructureSetTime, VR.TM, module.getStructureSetTime());
		List<ReferencedFrameOfReference> refFoRList =
			module.getReferencedFrameOfReferenceList();
		if (!refFoRList.isEmpty())
		{
			Pack.packReferencedFramesOfReference(refFoRList, dcm);
		}
		List<StructureSetRoi> ssRoiList = module.getStructureSetRoiList();
		if (ssRoiList.isEmpty())
		{
			throw new IllegalArgumentException("No structure set ROIs present");
		}
		Pack.packStructureSetRois(ssRoiList, dcm);
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(SynchronisationModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
	}

	/**
	 * Pack the module's fields into the DicomObject.
	 * @param module the source module
	 * @param dcm the target
	 * @throws IllegalArgumentException
	 */
	public static void pack(WholeSlideMicroscopyImageModule module,
		DicomObject dcm) throws IllegalArgumentException
	{
		dcm.putStrings(Tag.ImageType, VR.CS, module.getImageType());
		dcm.putFloat(Tag.ImagedVolumeWidth, VR.FL, module.getImagedVolumeWidth());
		dcm.putFloat(Tag.ImagedVolumeHeight, VR.FL,
			module.getImagedVolumeHeight());
		dcm.putFloat(Tag.ImagedVolumeDepth, VR.FL,
			module.getImagedVolumeDepth());
		dcm.putInt(Tag.TotalPixelMatrixColumns, VR.UL,
			module.getTotalPixelMatrixColumns());
		dcm.putInt(Tag.TotalPixelMatrixRows, VR.UL,
			module.getTotalPixelMatrixRows());
		int tpmFocalPlanes = module.getTotalPixelMatrixFocalPlanes();
		if (tpmFocalPlanes > 0)
		{
			dcm.putInt(NewTag.TotalPixelMatrixFocalPlanes, VR.UL, tpmFocalPlanes);
		}
		dcm.putSequence(Tag.TotalPixelMatrixOriginSequence);
		Pack.packPixelMatrixOrigin(module.getTotalPixelMatrixOrigin(),
			dcm.get(Tag.TotalPixelMatrixOriginSequence));
		dcm.putDoubles(Tag.ImageOrientationSlide, VR.DS,
			module.getImageOrientationSlide());
		Pack.putIfFinite(module.getImageOrientationSlide(), dcm,
			Tag.ImageOrientationSlide, VR.DS);
		int nSamples = module.getSamplesPerPixel();
		dcm.putInt(Tag.SamplesPerPixel, VR.US, nSamples);
		dcm.putString(Tag.PhotometricInterpretation, VR.CS,
			module.getPhotometricInterpretation());
		if (nSamples > 1)
		{
			dcm.putInt(Tag.PlanarConfiguration, VR.US,
				module.getPlanarConfiguration());
		}
		dcm.putInt(Tag.NumberOfFrames, VR.IS, module.getNumberOfFrames());
		dcm.putInt(Tag.BitsAllocated, VR.US, module.getBitsAllocated());
		dcm.putInt(Tag.BitsStored, VR.US, module.getBitsStored());
		dcm.putInt(Tag.HighBit, VR.US, module.getHighBit());
		dcm.putInt(Tag.PixelRepresentation, VR.US,
			module.getPixelRepresentation());
		dcm.putString(Tag.AcquisitionDateTime, VR.DT,
			module.getAcquisitionDateTime());
		Pack.putIfFinite(module.getAcquisitionDuration(), dcm, Tag.AcquisitionDuration,
			VR.FD);
		boolean lossyComp = module.getLossyImageCompression();
		dcm.putString(Tag.LossyImageCompression, VR.CS,
			(lossyComp ? "01" : "00"));
		if (lossyComp)
		{
			Pack.putIfFinite(module.getLossyImageCompressionRatio(), dcm,
				Tag.LossyImageCompressionRatio, VR.DS);
			Pack.putIfExists(module.getLossyImageCompressionMethod(), dcm,
				Tag.LossyImageCompressionMethod, VR.CS);
		}
		if (Constants.Monochrome2.equals(module.getPhotometricInterpretation()))
		{
			dcm.putString(Tag.PresentationLUTShape, VR.CS,
				module.getPresentationLutShape());
			dcm.putDouble(Tag.RescaleIntercept, VR.DS,
				module.getRescaleIntercept());
			dcm.putDouble(Tag.RescaleSlope, VR.DS, module.getRescaleSlope());
		}
		dcm.putString(Tag.VolumetricProperties, VR.CS,
			module.getVolumetricProperties());
		Pack.putYesNo(module.getSpecimenLabelInImage(), dcm, Tag.SpecimenLabelInImage);
		dcm.putString(Tag.BurnedInAnnotation, VR.CS,
			module.getBurnedInAnnotation());
		dcm.putString(Tag.FocusMethod, VR.CS, module.getFocusMethod());
		boolean extDoF = module.getExtendedDepthOfField();
		Pack.putYesNo(extDoF, dcm, Tag.ExtendedDepthOfField);
		if (extDoF)
		{
			dcm.putInt(Tag.NumberOfFocalPlanes, VR.US,
				module.getNumberOfFocalPlanes());
			dcm.putFloat(Tag.DistanceBetweenFocalPlanes, VR.FL,
				module.getDistanceBetweenFocalPlanes());
		}
	}

	/**
	 * Returns a new PatientModule.
	 * @return the new module
	 */
	public static PatientModule patientModule()
	{
		return patientModule(true);
	}

	/**
	 * Returns a new PatientModule.
	 * @param strict
	 * @return the new module
	 */
	public static PatientModule patientModule(
		boolean strict)
	{
		return new DefaultPatientModule(strict);
	}

	/**
	 * Returns a new PatientStudyModule.
	 * @return the new module
	 */
	public static PatientStudyModule patientStudyModule()
	{
		return new DefaultPatientStudyModule();
	}

	/**
	 * Returns a new PatientStudyModule.
	 * @param strict
	 * @return the new module
	 */
	public static PatientStudyModule patientStudyModule(
		boolean strict)
	{
		return new DefaultPatientStudyModule(strict);
	}

	/**
	 * Returns a new RoiContourModule.
	 * @return the new module
	 */
	public static RoiContourModule roiContourModule()
	{
		return new DefaultRoiContourModule();
	}

	/**
	 * Returns a new RoiContourModule.
	 * @param strict
	 * @return the new module
	 */
	public static RoiContourModule roiContourModule(
		boolean strict)
	{
		return new DefaultRoiContourModule(strict);
	}

	/**
	 * Returns a new RtRoiObservationsModule.
	 * @return the new module
	 */
	public static RtRoiObservationsModule rtRoiObservationsModule()
	{
		return new DefaultRtRoiObservationsModule();
	}

	/**
	 * Returns a new RtRoiObservationsModule.
	 * @param strict
	 * @return the new module
	 */
	public static RtRoiObservationsModule rtRoiObservationsModule(
		boolean strict)
	{
		return new DefaultRtRoiObservationsModule(strict);
	}

	/**
	 * Returns a new RtSeriesModule.
	 * @return the new module
	 */
	public static RtSeriesModule rtSeriesModule()
	{
		return new DefaultRtSeriesModule();
	}

	/**
	 * Returns a new RtSeriesModule.
	 * @param strict
	 * @return the new module
	 */
	public static RtSeriesModule rtSeriesModule(
		boolean strict)
	{
		return new DefaultRtSeriesModule(strict);
	}

	/**
	 * Returns a new SegmentationSeriesModule.
	 * @return the new module
	 */
	public static SegmentationSeriesModule segmentationSeriesModule()
	{
		return segmentationSeriesModule(true);
	}

	/**
	 * Returns a new SegmentationSeriesModule.
	 * @param strict
	 * @return the new module
	 */
	public static SegmentationSeriesModule segmentationSeriesModule(
		boolean strict)
	{
		return new DefaultSegmentationSeriesModule(strict);
	}

	/**
	 * Returns a new SlideLabelModule.
	 * @return the new module
	 */
	public static SlideLabelModule slideLabelModule()
	{
		return slideLabelModule(true);
	}

	/**
	 * Returns a new SlideLabelModule.
	 * @param strict
	 * @return the new module
	 */
	public static SlideLabelModule slideLabelModule(
		boolean strict)
	{
		return new DefaultSlideLabelModule(strict);
	}

	/**
	 * Returns a new MicroscopyBulkSimpleAnnotationsSeriesModule.
	 * @return the new module
	 */
	public static FrameOfReferenceModule
		slideMicroscopyFrameOfReferenceModule()
	{
		return slideMicroscopyFrameOfReferenceModule(true);
	}

	/**
	 * Returns a new MicroscopyBulkSimpleAnnotationsSeriesModule.
	 * @param strict
	 * @return the new module
	 */
	public static FrameOfReferenceModule
		slideMicroscopyFrameOfReferenceModule(boolean strict)
	{
		return new SlideMicroscopyFrameOfReferenceModule(strict);
	}

	/**
	 *
	 * @param classUid
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static SopCommonModule sopCommonModule(String classUid)
		throws IllegalArgumentException
	{
		if ((classUid == null) || classUid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SopClassUid must not be null or empty");
		}
		return new DefaultSopCommonModule(classUid);
	}

	/**
	 * Returns a new SpecimenModule.
	 * @return the new module
	 */
	public static SpecimenModule specimenModule()
	{
		return specimenModule(true);
	}

	/**
	 * Returns a new SpecimenModule.
	 * @param strict
	 * @return the new module
	 */
	public static SpecimenModule specimenModule(
		boolean strict)
	{
		return new DefaultSpecimenModule(strict);
	}

	/**
	 * Returns a new SrDocumentContentModule.
	 * @return the new module
	 */
	public static SrDocumentContentModule srDocumentContentModule()
	{
		return new DefaultSrDocumentContentModule();
	}

	/**
	 * Returns a new SrDocumentGeneralModule.
	 * @return the new module
	 */
	public static SrDocumentGeneralModule srDocumentGeneralModule()
	{
		return new DefaultSrDocumentGeneralModule();
	}

	/**
	 * Returns a new SrDocumentSeriesModule.
	 * @return the new module
	 */
	public static SrDocumentSeriesModule srDocumentSeriesModule()
	{
		return new DefaultSrDocumentSeriesModule();
	}

	/**
	 * Returns a new SynchronisationModule.
	 * @return the new module
	 */
	public static SynchronisationModule synchronisationModule()
	{
		return new DefaultSynchronisationModule();
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, AcquisitionContextModule module)
		throws IllegalArgumentException
	{
		DicomElement acqCtxSq = dcm.get(Tag.AcquisitionContextSequence);
		if (acqCtxSq == null)
		{
			throw new IllegalArgumentException(
				"AcquisitionContextSequence is missing");
		}
		if (!acqCtxSq.isEmpty())
		{
			throw new UnsupportedOperationException(
				"AcquisitionContext item not yet supported");
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		ClinicalTrialSeriesModule module) throws IllegalArgumentException
	{
		String name = dcm.getString(Tag.ClinicalTrialCoordinatingCenterName,
			VR.LO, null);
		if (name == null)
		{
			module.setClinicalTrialCoordinatingCenterName(null);
			module.setClinicalTrialSeriesId(null);
			module.setClinicalTrialSeriesDescription(null);
			return;
		}
		module.setClinicalTrialCoordinatingCenterName(name);
		module.setClinicalTrialSeriesId(
			dcm.getString(Tag.ClinicalTrialSeriesID, VR.LO, null));
		module.setClinicalTrialSeriesDescription(
			dcm.getString(Tag.ClinicalTrialSeriesDescription, VR.LO, null));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		ClinicalTrialStudyModule module) throws IllegalArgumentException
	{
		String timePointId = dcm.getString(Tag.ClinicalTrialTimePointID, VR.LO, null);
		if (timePointId == null)
		{
			module.setClinicalTrialTimePointId(null);
			module.setClinicalTrialTimePointDescription(null);
			return;
		}
		module.setClinicalTrialTimePointId(timePointId);
		module.setClinicalTrialTimePointDescription(
			dcm.getString(Tag.ClinicalTrialTimePointDescription, VR.ST, null));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		ClinicalTrialSubjectModule module) throws IllegalArgumentException
	{
		String sponsor = dcm.getString(Tag.ClinicalTrialSponsorName, VR.LO, null);
		String protoId = dcm.getString(Tag.ClinicalTrialProtocolID, VR.LO, null);
		if ((sponsor == null) || sponsor.isEmpty() ||
			 (protoId == null) || protoId.isEmpty())
		{
			module.setClinicalTrialSponsorName(null);
			module.setClinicalTrialProtocolId(null);
			module.setClinicalTrialProtocolName(null);
			module.setClinicalTrialSiteId(null);
			module.setClinicalTrialSiteName(null);
			return;
		}
		String subjId = dcm.getString(Tag.ClinicalTrialSubjectID, VR.LO, null);
		if ((subjId != null) && !subjId.isEmpty())
		{
			module.setClinicalTrialSubjectId(subjId);
		}
		else
		{
			String readId = dcm.getString(Tag.ClinicalTrialSubjectReadingID, VR.LO,
				null);
			if ((readId == null) || readId.isEmpty())
			{
				throw new IllegalArgumentException(
					"Subject ID and subject reading ID cannot both be null");
			}
			module.setClinicalTrialSubjectReadingId(readId);
		}
		module.setClinicalTrialSponsorName(sponsor);
		module.setClinicalTrialProtocolId(protoId);
		module.setClinicalTrialProtocolName(
			dcm.getString(Tag.ClinicalTrialProtocolName, VR.LO, null));
		module.setClinicalTrialSiteId(
			dcm.getString(Tag.ClinicalTrialSiteID, VR.LO, null));
		module.setClinicalTrialSiteName(
			dcm.getString(Tag.ClinicalTrialSiteName, VR.LO, null));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		CommonInstanceReferenceModule module) throws IllegalArgumentException
	{
		DicomElement refSerSq = dcm.get(Tag.ReferencedSeriesSequence);
		// Sequence is 1C so may be null.
		if (refSerSq == null)
		{
			return;
		}
		if (refSerSq.countItems() < 1)
		{
			throw new IllegalArgumentException("ReferencedSeriesSequence empty");
		}
		for (int i=0; i<refSerSq.countItems(); i++)
		{
			module.addReferencedSeries(Unpack.unpackReferencedSeries(refSerSq.getDicomObject(i)));
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		EnhancedGeneralEquipmentModule module) throws IllegalArgumentException
	{
		unpack(dcm, (GeneralEquipmentModule) module);
		module.setManufacturer(dcm.getString(Tag.Manufacturer));
		module.setManufacturersModelName(dcm.getString(Tag.ManufacturerModelName));
		module.setDeviceSerialNumber(dcm.getString(Tag.DeviceSerialNumber));
		module.setSoftwareVersion(dcm.getString(Tag.SoftwareVersions));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, FrameOfReferenceModule module)
		throws IllegalArgumentException
	{
		String forUid = dcm.getString(Tag.FrameOfReferenceUID, VR.UI, null);
		module.setFrameOfReferenceUid(forUid);
		if (forUid == null)
		{
			return;
		}
		module.setPositionReferenceIndicator(
			dcm.getString(Tag.PositionReferenceIndicator, VR.LO, null));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 */
	public static void unpack(DicomObject dcm, GeneralEquipmentModule module)
	{
		module.setManufacturer(dcm.getString(Tag.Manufacturer));
		String modelName = dcm.getString(Tag.ManufacturerModelName);
		if (modelName != null)
		{
			module.setManufacturersModelName(modelName);
		}
		String serial = dcm.getString(Tag.DeviceSerialNumber);
		if (serial != null)
		{
			module.setDeviceSerialNumber(serial);
		}
		module.setSoftwareVersions(dcm.getStrings(Tag.SoftwareVersions));
		module.setSpatialResolution(
			dcm.getDouble(Tag.SpatialResolution, VR.DS, Double.NaN));
		module.setPixelPaddingValue(
			dcm.getInt(Tag.PixelPaddingValue, VR.US, Integer.MIN_VALUE));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, GeneralImageModule module)
		throws IllegalArgumentException
	{
		int instNum = dcm.getInt(Tag.InstanceNumber, -1);
		if (instNum == -1)
		{
			throw new IllegalArgumentException("InstanceNumber must exist");
		}
		module.setInstanceNumber(instNum);
		module.setPatientOrientation(
			dcm.getStrings(Tag.PatientOrientation, VR.CS, null));
		module.setContentDate(dcm.getString(Tag.ContentDate, VR.DA, ""));
		module.setContentTime(dcm.getString(Tag.ContentTime, VR.TM, ""));
		module.setImageType(dcm.getStrings(Tag.ImageType, VR.CS, null));
		module.setAcquisitionDate(dcm.getString(Tag.AcquisitionDate, VR.DA, null));
		module.setAcquisitionTime(dcm.getString(Tag.AcquisitionTime, VR.TM, null));
		module.setImageComments(dcm.getString(Tag.ImageComments, VR.LT, null));
		module.setBurnedInAnnotation(
			dcm.getString(Tag.BurnedInAnnotation, VR.CS, null));
		String lossyCompression = dcm.getString(Tag.LossyImageCompression, VR.CS,
			"");
		switch (lossyCompression)
		{
			case "00":
				module.setLossyImageCompression(false);
				break;
			case "01":
				module.setLossyImageCompression(true);
				break;
			case "":
				break;
			default:
				throw new IllegalArgumentException(
					"LossyImageCompression invalid: "+lossyCompression);
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, GeneralReferenceModule module)
		throws IllegalArgumentException
	{
		DicomElement refImageSq = dcm.get(Tag.ReferencedImageSequence);
		if ((refImageSq != null) && !refImageSq.isEmpty())
		{
			int nItems = refImageSq.countItems();
			for (int i=0; i<nItems; i++)
			{
				DicomObject item = refImageSq.getDicomObject(i);
				module.addReferencedImage(Unpack.unpackReferencedImage(item));
			}
		}
		module.setDerivationDescription(
			dcm.getString(Tag.DerivationDescription, VR.ST, null));
		DicomElement derivCodeSq = dcm.get(Tag.DerivationCodeSequence);
		if ((derivCodeSq != null) && !derivCodeSq.isEmpty())
		{
			int nItems = derivCodeSq.countItems();
			for (int i=0; i<nItems; i++)
			{
				DicomObject item = derivCodeSq.getDicomObject(i);
				module.addDerivationCode(Unpack.unpackCode(item));
			}
		}	
		DicomElement srcImageSq = dcm.get(Tag.SourceImageSequence);
		if ((srcImageSq != null) && !srcImageSq.isEmpty())
		{
			int nItems = srcImageSq.countItems();
			for (int i=0; i<nItems; i++)
			{
				DicomObject item = srcImageSq.getDicomObject(i);
				module.addSourceImage(Unpack.unpackSourceImage(item, false));
			}
		}	
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, GeneralSeriesModule module)
		throws IllegalArgumentException
	{
		module.setModality(dcm.getString(Tag.Modality));
		module.setSeriesInstanceUid(dcm.getString(Tag.SeriesInstanceUID));
		module.setSeriesNumber(dcm.getInt(Tag.SeriesNumber));
		module.setLaterality(dcm.getString(Tag.Laterality, VR.CS, null));
		String date = dcm.getString(Tag.SeriesDate);
		if (date != null)
		{
			module.setSeriesDate(date);
		}
		String time = dcm.getString(Tag.SeriesTime);
		if (time != null)
		{
			module.setSeriesTime(time);
		}
		String desc = dcm.getString(Tag.SeriesDescription);
		if (desc != null)
		{
			module.setSeriesDescription(desc);
		}
		module.setOperatorsName(dcm.getString(Tag.OperatorsName));
		module.setProtocolName(dcm.getString(Tag.ProtocolName));
		module.setPatientPosition(dcm.getString(Tag.PatientPosition));
		module.setSmallestPixelValueInSeries(
			dcm.getInt(Tag.SmallestPixelValueInSeries, VR.US, -1));
		module.setLargestPixelValueInSeries(
			dcm.getInt(Tag.LargestPixelValueInSeries, VR.US, -1));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, GeneralStudyModule module)
		throws IllegalArgumentException
	{
		module.setStudyInstanceUid(dcm.getString(Tag.StudyInstanceUID));
		module.setStudyDate(dcm.getString(Tag.StudyDate));
		module.setStudyTime(dcm.getString(Tag.StudyTime));
		module.setReferringPhysician(dcm.getString(Tag.ReferringPhysicianName));
		module.setStudyId(dcm.getString(Tag.StudyID));
		module.setAccessionNumber(dcm.getString(Tag.AccessionNumber));
		module.setStudyDescription(dcm.getString(Tag.StudyDescription, VR.LO, null));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, ImagePixelModule module)
		throws IllegalArgumentException
	{
		DicomElement pixels = dcm.get(Tag.PixelData);
		if (!pixels.hasFragments())
		{
			module.setPixelData(dcm.getBytes(Tag.PixelData));
			module.setPixelDataFragments(null);
		}
		else
		{
			List<byte[]> frags = new ArrayList<>();
			for (int i=0; i<pixels.countItems(); i++)
			{
				byte[] frag = pixels.getFragment(i);
				frags.add(Arrays.copyOf(frag, frag.length));
			}
			module.setPixelData(null);
			module.setPixelDataFragments(frags, true);
		}
		module.setSamplesPerPixel(dcm.getInt(Tag.SamplesPerPixel, VR.US));
		module.setPhotometricInterpretation(
			dcm.getString(Tag.PhotometricInterpretation, VR.CS, null));
		module.setRowCount(dcm.getInt(Tag.Rows, VR.US, -1));
		module.setColumnCount(dcm.getInt(Tag.Columns, VR.US, -1));
		module.setBitsAllocated(dcm.getInt(Tag.BitsAllocated, VR.US, -1));
		module.setBitsStored(dcm.getInt(Tag.BitsStored, VR.US, -1));
		int highBit = dcm.getInt(Tag.HighBit, VR.US, -1);
		if (highBit != module.getBitsStored()-1)
		{
			throw new IllegalArgumentException("HighBit must be (BitsStored-1)");
		}
		module.setHighBit(highBit);
		module.setPixelRepresentation(
			dcm.getInt(Tag.PixelRepresentation, VR.US, -1));
		if (module.getSamplesPerPixel() > 1)
		{
			module.setPlanarConfiguration(
				dcm.getInt(Tag.PlanarConfiguration, VR.US, -1));
		}
		module.setSmallestPixelValue(
			dcm.getInt(Tag.SmallestImagePixelValue, VR.US, -1));
		module.setLargestPixelValue(
			dcm.getInt(Tag.LargestImagePixelValue, VR.US, -1));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		MicroscopyBulkSimpleAnnotationsModule module)
		throws IllegalArgumentException
	{
		boolean strict = module.isStrict();
		module.setInstanceNumber(
			dcm.getInt(Tag.InstanceNumber, Integer.MIN_VALUE));
		module.setContentLabel(dcm.getString(Tag.ContentLabel, VR.CS, null));
		module.setContentDescription(dcm.getString(
			Tag.ContentDescription, VR.LO, null));
		module.setContentDate(dcm.getString(Tag.ContentDate, VR.DA, null));
		module.setContentTime(dcm.getString(Tag.ContentTime, VR.TM, null));
		String coordType = dcm.getString(
			NewTag.AnnotationCoordinateType, VR.CS, null);
		module.setAnnotationCoordinateType(coordType);
		if (Constants.Coord2D.equals(coordType))
		{
			module.setPixelOriginInterpretation(dcm.getString(
				Tag.PixelOriginInterpretation, VR.CS, null));
			DicomElement riSq = Unpack.getSq(dcm, Tag.ReferencedImageSequence, 
				strict);
			if (riSq != null)
			{
				if (riSq.countItems() == 1)
				{
					ReferencedImage ri = Unpack.unpackReferencedImage(
						riSq.getDicomObject());
					module.setReferencedImage(ri);
				}
				else
				{
					String msg =
						"ReferenceImageSequence must contain exactly one item";
					if (strict)
					{
						throw new IllegalArgumentException(msg);
					}
					logger.warn(msg);
				}
			}
			else
			{
				String msg = "ReferencedImageSequence missing";
				if (strict)
				{
					throw new IllegalArgumentException(msg);
				}
				logger.warn(msg);
			}
		}
		else if (Constants.Coord3D.equals(coordType))
		{
			String msg = "3D Coordinates not yet supported";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
		}
		else
		{
			throw new IllegalArgumentException(
				"Unknown coordinate type: "+coordType);
		}

		DicomElement annoGrpSq = dcm.get(NewTag.AnnotationGroupSequence);
		if ((annoGrpSq == null) || (annoGrpSq.isEmpty()))
		{
			String msg = "AnnotationGroupSequence missing or empty";
			if (strict)
			{
				throw new IllegalArgumentException(msg);
			}
			logger.warn(msg);
			return;
		}
		for (int i=0; i<annoGrpSq.countItems(); i++)
		{
			module.addAnnotationGroup(
				Unpack.unpackAnnotationGroup(annoGrpSq.getDicomObject(i),
					coordType, module.isStrict()));
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		MicroscopyBulkSimpleAnnotationsSeriesModule module)
		throws IllegalArgumentException
	{
		unpack(dcm, (GeneralSeriesModule) module);
		module.setSeriesNumber(dcm.getInt(Tag.SeriesNumber));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 */
	public static void unpack(DicomObject dcm, MultiframeDimensionModule module)
		throws IllegalArgumentException
	{
		DicomElement dimOrgSq = dcm.get(Tag.DimensionOrganizationSequence);
		if ((dimOrgSq == null) || (dimOrgSq.countItems() < 1))
		{
			throw new IllegalArgumentException(
				"DimensionOrganizationSequence missing or empty");
		}
		for (int i=0; i<dimOrgSq.countItems(); i++)
		{
			DicomObject dimOrgDcm = dimOrgSq.getDicomObject(i);
			module.addDimensionOrganisationUid(
				dimOrgDcm.getString(Tag.DimensionOrganizationUID, VR.UI, null));
		}

		String dimOrgType = dcm.getString(Tag.DimensionOrganizationType, VR.CS,
			null);
		module.setDimensionOrganisationType(dimOrgType);

		DicomElement dimIdxSq = dcm.get(Tag.DimensionIndexSequence);
		boolean dimIdxReq = (dimOrgType == null) ||
			!Constants.TiledFull.equals(dimOrgType);
		if (dimIdxReq &&
			 ((dimIdxSq == null) || (dimIdxSq.countItems() < 1)))
		{
			throw new IllegalArgumentException(
				"DimensionIndexSequence missing or empty");
		}
		if (dimIdxSq != null)
		{
			for (int i=0; i<dimIdxSq.countItems(); i++)
			{
				module.addDimensionIndex(Unpack.unpackDimensionIndex(dimIdxSq.getDicomObject(i)));
			}
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module
	 * @throws IllegalArgumentException
	 */
	public static void unpack(DicomObject dcm,
		MultiframeFunctionalGroupsModule module) throws IllegalArgumentException
	{
		String sopClassUid = dcm.getString(Tag.SOPClassUID, VR.UI, "");
		switch (sopClassUid)
		{
			case UID.SegmentationStorage:
				unpackSegmentationStorage(dcm,
					(SegmentationMultiframeFunctionalGroupsModule) module);
				break;
			case UID.VLWholeSlideMicroscopyImageStorage:
				unpackWsmStorage(dcm, (WsmMultiframeFunctionalGroupsModule) module);
				break;
			default:
				throw new IllegalArgumentException(
					"Unknown or unsupported SOP class UID");
		}
		module.setInstanceNumber(dcm.getInt(Tag.InstanceNumber, VR.IS, -1));
		module.setContentDate(dcm.getString(Tag.ContentDate, VR.DA, null));
		module.setContentTime(dcm.getString(Tag.ContentTime, VR.TM, null));
		module.setNumberOfFrames(dcm.getInt(Tag.NumberOfFrames, VR.IS, -1));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		MultiResolutionNavigationModule module) throws IllegalArgumentException
	{
		String[] imageType = dcm.getStrings(Tag.ImageType, VR.CS, null);
		if ((imageType == null) || (imageType.length < 4))
		{
			throw new IllegalArgumentException("Image type not properly defined");
		}
		if (!Constants.Localizer.equals(imageType[2]))
		{
			// Only required if value 3 of image type is LOCALIZER
			return;
		}
		DicomElement sq = dcm.get(Tag.ReferencedImageNavigationSequence);
		if ((sq == null) || sq.isEmpty())
		{
			throw new IllegalArgumentException(
				"ReferencedImageNavigationSequence must be present");
		}
		module.setEnabled(true);
		for (int i=0; i<sq.countItems(); i++)
		{
			WsmReferencedImage image = Unpack.unpackWsmReferencedImage(
				sq.getDicomObject(i));
			module.addReferencedImage(image);
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		OpticalPathModule module) throws IllegalArgumentException
	{
		String dimOrgType = dcm.getString(Tag.DimensionOrganizationType, VR.CS,
			null);
		if (Constants.TiledFull.equals(dimOrgType) &&
			!dcm.contains(NewTag.NumberOfOpticalPaths))
		{
			throw new IllegalArgumentException(
				"NumberOfOpticalPaths required for TILED_FULL images");
		}
		int numOptPaths = dcm.getInt(NewTag.NumberOfOpticalPaths, VR.UL, -1);
		DicomElement pathSq = dcm.get(Tag.OpticalPathSequence);
		if (pathSq == null)
		{
			throw new IllegalArgumentException("OpticalPathSequence must exist");
		}
		int pathItems = pathSq.countItems();
		if (pathItems != numOptPaths)
		{
			throw new IllegalArgumentException("Mismatch in optical paths. "+
				"Found: "+pathItems+", Expected: "+numOptPaths);
		}
		for (int i=0; i<pathItems; i++)
		{
			OpticalPath path = Unpack.unpackOpticalPath(pathSq.getDicomObject(i));
			module.addOpticalPath(path);
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 */
	public static void unpack(DicomObject dcm, PatientModule module)
	{
		module.setPatientName(dcm.getString(Tag.PatientName));
		module.setPatientId(dcm.getString(Tag.PatientID));
		module.setPatientBirthDate(dcm.getString(Tag.PatientBirthDate));
		String sex = dcm.getString(Tag.PatientSex);
		if (sex != null)
		{
			module.setPatientSex(sex);
		}
		module.setPatientIdentityRemoved(
			dcm.getString(Tag.PatientIdentityRemoved, VR.CS, null));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 */
	public static void unpack(DicomObject dcm, PatientStudyModule module)
	{
		module.setPatientAge(dcm.getString(Tag.PatientAge, VR.AS, null));
		module.setPatientSize(dcm.getDouble(Tag.PatientSize, VR.DS, Double.NaN));
		module.setPatientWeight(
			dcm.getDouble(Tag.PatientWeight, VR.DS, Double.NaN));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, RoiContourModule module)
		throws IllegalArgumentException
	{
		unpack(dcm, module, null);
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @param service optional executor service
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, RoiContourModule module,
		ExecutorService service)
		throws IllegalArgumentException
	{
		DicomElement roiContourSq = dcm.get(Tag.ROIContourSequence);
		if ((roiContourSq == null) || roiContourSq.isEmpty())
		{
			throw new IllegalArgumentException(
				"RoiContourSequence must not be null or empty");
		}
		if (service == null)
		{
			unpackRoiContoursST(roiContourSq, module);
			return;
		}
		unpackRoiContoursMT(roiContourSq, module, service);
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		RtRoiObservationsModule module) throws IllegalArgumentException
	{
		DicomElement rtRoiObsSq = dcm.get(Tag.RTROIObservationsSequence);
		if ((rtRoiObsSq == null) || rtRoiObsSq.isEmpty())
		{
			throw new IllegalArgumentException(
				"RtRoiObservationsSequence must not be null or empty");
		}
		int nItems = rtRoiObsSq.countItems();
		for (int i=0; i<nItems; i++)
		{
			DicomObject item = rtRoiObsSq.getDicomObject(i);
			RtRoiObservation rrObs = new DefaultRtRoiObservation();
			try
			{
				rrObs.setObservationNumber(item.getInt(Tag.ObservationNumber));
			}
			catch (NumberFormatException ex)
			{
				throw new IllegalArgumentException(
					"Invalid observation number (IS) in RT ROI observation", ex);
			}
			try
			{
				rrObs.setReferencedRoiNumber(item.getInt(Tag.ReferencedROINumber));
			}
			catch (NumberFormatException ex)
			{
				throw new IllegalArgumentException(
					"Invalid referenced ROI number (IS) in RT ROI observation", ex);
			}
			rrObs.setRoiInterpreter(item.getString(Tag.ROIInterpreter));
			rrObs.setRtRoiInterpretedType(item.getString(Tag.RTROIInterpretedType));
			module.addRtRoiObservation(rrObs);
			rrObs.setRoiObservationDescription(
				item.getString(Tag.ROIObservationDescription));
			rrObs.setRoiObservationLabel(item.getString(Tag.ROIObservationLabel));
		}		
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, RtSeriesModule module)
		throws IllegalArgumentException
	{
		module.setModality(dcm.getString(Tag.Modality));
		module.setSeriesInstanceUid(dcm.getString(Tag.SeriesInstanceUID));
		module.setSeriesNumber(dcm.getInt(Tag.SeriesNumber));
		String date = dcm.getString(Tag.SeriesDate);
		if (date != null)
		{
			module.setSeriesDate(date);
		}
		String time = dcm.getString(Tag.SeriesTime);
		if (time != null)
		{
			module.setSeriesTime(time);
		}
		String desc = dcm.getString(Tag.SeriesDescription);
		if (desc != null)
		{
			module.setSeriesDescription(desc);
		}
		module.setOperatorsName(dcm.getString(Tag.OperatorsName));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, SegmentationImageModule module)
		throws IllegalArgumentException
	{
		Modules.unpack(dcm, (GeneralImageModule) module);
		module.setContentLabel(dcm.getString(Tag.ContentLabel, VR.CS, null));
		module.setContentDescription(
			dcm.getString(Tag.ContentDescription, VR.LO, null));
		module.setContentCreatorsName(
			dcm.getString(Tag.ContentCreatorName, VR.PN, null));
		module.setBitsAllocated(dcm.getInt(Tag.BitsAllocated, VR.US, -1));
		module.setBitsStored(dcm.getInt(Tag.BitsStored, VR.US, -1));
		module.setHighBit(dcm.getInt(Tag.HighBit, VR.US, -1));
		String lic = dcm.getString(Tag.LossyImageCompression, VR.CS, null);
		if ("00".equals(lic) || "01".equals(lic))
		{
			module.setLossyImageCompression("01".equals(lic));
		}
		else
		{
			throw new IllegalArgumentException("Invalid LossyImageCompression: "+
				lic);
		}
		module.setSegmentationType(
			dcm.getString(Tag.SegmentationType, VR.CS, null));
		if (module.getSegmentationType().equals(Constants.Fractional))
		{
			module.setSegmentationFractionalType(
				dcm.getString(Tag.SegmentationFractionalType, VR.CS, null));
			module.setMaximumFractionalValue(
				dcm.getInt(Tag.MaximumFractionalValue, VR.US, -1));
		}
		DicomElement segSq = dcm.get(Tag.SegmentSequence);
		if ((segSq == null) || (segSq.countItems() < 1))
		{
			throw new IllegalArgumentException(
				"SegmentSequence missing or invalid size");
		}
		int nSeg = segSq.countItems();
		for (int i=0; i<nSeg; i++)
		{
			DicomObject dcmSeg = segSq.getDicomObject(i);
			unpackSegment(dcmSeg, module);
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, SegmentationSeriesModule module)
		throws IllegalArgumentException
	{
		unpack(dcm, (GeneralSeriesModule) module);
		module.setSeriesNumber(dcm.getInt(Tag.SeriesNumber));
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		SlideLabelModule module) throws IllegalArgumentException
	{
		String[] imageType = dcm.getStrings(Tag.ImageType, VR.CS, null);
		if ((imageType == null) || (imageType.length < 4))
		{
			throw new IllegalArgumentException("Image type not properly defined");
		}
		boolean required = Constants.Label.equals(imageType[2]);
		if (required)
		{
			module.setEnabled(true);
		}
		String barcode = dcm.getString(Tag.BarcodeValue, VR.LT, null);
		module.setBarcodeValue(barcode);
		String label = dcm.getString(Tag.LabelText, VR.UT, null);
		module.setLabelText(label);
		if ((barcode != null) && (label != null))
		{
			module.setEnabled(true);
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, SopCommonModule module)
		throws IllegalArgumentException
	{
		// SOP class UID will already be set in module constructor
		module.setSopInstanceUid(dcm.getString(Tag.SOPInstanceUID));
		String number = dcm.getString(Tag.InstanceNumber);
		if (number != null)
		{
			try
			{
				module.setInstanceNumber(Integer.parseInt(number));
			}
			catch (NumberFormatException ex)
			{
				logger.debug(
					"Invalid instance number (IS) in SopCommonModule: {}", number);
			}
		}
		module.setLongitudinalTemporalInformationModified(
			dcm.getString(Tag.LongitudinalTemporalInformationModified, VR.CS, null));
		module.setContentQualification(dcm.getString(Tag.ContentQualification,
			VR.CS, null));
		unpackCodingSchemeIds(dcm.get(Tag.CodingSchemeIdentificationSequence),
			module);
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		SpecimenModule module) throws IllegalArgumentException
	{
		ContainerIdentifier ident = new DefaultContainerIdentifier();
		ident.setIdentifier(dcm.getString(Tag.ContainerIdentifier));
		module.setContainerIdentifier(ident);
		DicomElement issuerSq = dcm.get(Tag.IssuerOfTheContainerIdentifierSequence);
		if (issuerSq == null)
		{
			throw new IllegalArgumentException(
				"IssuerOfTheContainerIdentifierSequence missing");
		}
		for (int i=0; i<issuerSq.countItems(); i++)
		{
			HL7v2HierarchicDesignator issuer = Unpack.unpackIssuer(
				issuerSq.getDicomObject(i));
			ident.addIssuer(issuer);
		}
		DicomElement containerTypeSq = dcm.get(Tag.ContainerTypeCodeSequence);
		if (containerTypeSq == null)
		{
			throw new IllegalArgumentException(
				"ContainerTypeCodeSequence missing");
		}
		for (int i=0; i<containerTypeSq.countItems(); i++)
		{
			Code code = Unpack.unpackCode(containerTypeSq.getDicomObject(i));
			module.addContainerTypeCode(code);
		}
		DicomElement containerCompSq = dcm.get(Tag.ContainerComponentSequence);
		if (containerCompSq != null)
		{
			if (containerCompSq.isEmpty())
			{
				throw new IllegalArgumentException(
					"ContainerComponentSequence must not be empty");
			}
			for (int i=0; i<containerCompSq.countItems(); i++)
			{
				ContainerComponent cc = Unpack.unpackContainerComponent(
					containerCompSq.getDicomObject(i));
				module.addContainerComponent(cc);
			}
		}
		DicomElement specDescSq = dcm.get(Tag.SpecimenDescriptionSequence);
		if ((specDescSq == null) || specDescSq.isEmpty())
		{
			throw new IllegalArgumentException(
				"SpecimenDescriptionSequence missing or empty");
		}
		for (int i=0; i<specDescSq.countItems(); i++)
		{
			SpecimenDescription desc = Unpack.unpackSpecimenDescription(
				specDescSq.getDicomObject(i));
			module.addSpecimenDescription(desc);
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		SrDocumentContentModule module) throws IllegalArgumentException
	{
		ContentItem ci = Unpack.unpackContentItem(dcm);
		if (!(ci instanceof ContentItem.ContainerItem))
		{
			throw new IllegalArgumentException("Root content item must be a container");
		}
		module.setRootContentItem((ContentItem.ContainerItem) ci);
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		SrDocumentGeneralModule module) throws IllegalArgumentException
	{
		module.setInstanceNumber(dcm.getInt(Tag.InstanceNumber));
		module.setCompletionFlag(dcm.getString(Tag.CompletionFlag));
		module.setVerificationFlag(dcm.getString(Tag.VerificationFlag));
		module.setContentDate(dcm.getString(Tag.ContentDate));
		module.setContentTime(dcm.getString(Tag.ContentTime));
		DicomElement perfProcSq = dcm.get(Tag.PerformedProcedureCodeSequence);
		if (perfProcSq == null)
		{
			throw new IllegalArgumentException(
				"Performed procedure code sequence missing");
		}
		int nPerfProc = perfProcSq.countItems();
		for (int i=0; i<nPerfProc; i++)
		{
			DicomObject ppDcm = perfProcSq.getDicomObject(i);
			module.addPerformedProcedureCode(Unpack.unpackCode(ppDcm));
		}
		unpackCurrReqProcEvidence(dcm, module);
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		SrDocumentSeriesModule module) throws IllegalArgumentException
	{
		module.setSeriesInstanceUid(dcm.getString(Tag.SeriesInstanceUID));
		module.setSeriesNumber(dcm.getInt(Tag.SeriesNumber));
		String date = dcm.getString(Tag.SeriesDate);
		if (date != null)
		{
			module.setSeriesDate(date);
		}
		String time = dcm.getString(Tag.SeriesTime);
		if (time != null)
		{
			module.setSeriesTime(time);
		}
		String desc = dcm.getString(Tag.SeriesDescription);
		if (desc != null)
		{
			module.setSeriesDescription(desc);
		}
		DicomElement rppsSq = dcm.get(Tag.ReferencedPerformedProcedureStepSequence);
		if (rppsSq == null)
		{
			throw new IllegalArgumentException(
				"ReferencedPerformedProcedureStepSequence missing");
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm, StructureSetModule module)
		throws IllegalArgumentException
	{
		module.setStructureSetLabel(dcm.getString(Tag.StructureSetLabel));
		String name = dcm.getString(Tag.StructureSetName);
		if (name != null)
		{
			module.setStructureSetName(name);
		}
		String desc = dcm.getString(Tag.StructureSetDescription);
		if (desc != null)
		{
			module.setStructureSetDescription(desc);
		}
		String number = dcm.getString(Tag.InstanceNumber);
		if (number != null)
		{
			try
			{
				module.setInstanceNumber(Integer.parseInt(number));
			}
			catch (NumberFormatException ex)
			{
				logger.debug(
					"Invalid instance number (IS) in StructureSetModule: {}", number);
			}
		}
		module.setStructureSetDate(dcm.getString(Tag.StructureSetDate));
		module.setStructureSetTime(dcm.getString(Tag.StructureSetTime));
		Unpack.unpackReferencedFrameOfReference(
			dcm.get(Tag.ReferencedFrameOfReferenceSequence), module);
		unpackStructureSetRoi(dcm.get(Tag.StructureSetROISequence), module);
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		SynchronisationModule module) throws IllegalArgumentException
	{
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		WholeSlideMicroscopyImageModule module) throws IllegalArgumentException
	{
		module.setImageType(dcm.getStrings(Tag.ImageType));
		module.setImageVolumeWidth(dcm.getFloat(Tag.ImagedVolumeWidth));
		module.setImageVolumeHeight(dcm.getFloat(Tag.ImagedVolumeHeight));
		module.setImageVolumeDepth(dcm.getFloat(Tag.ImagedVolumeDepth));
		module.setTotalPixelMatrixColumns(dcm.getInt(Tag.TotalPixelMatrixColumns));
		module.setTotalPixelMatrixRows(dcm.getInt(Tag.TotalPixelMatrixRows));
		String dimOrgType = dcm.getString(Tag.DimensionOrganizationType);
		int tpmFocalPlanes = dcm.getInt(NewTag.TotalPixelMatrixFocalPlanes, VR.UL, 0);
		if (Constants.TiledFull.equals(dimOrgType) || (tpmFocalPlanes > 0))
		{
			module.setTotalPixelMatrixFocalPlanes(tpmFocalPlanes);
		}
		module.setTotalPixelMatrixOrigin(Unpack.unpackPixelMatrixOrigin(dcm));
		module.setImageOrientationSlide(Unpack.unpackImageOrientationSlide(dcm));
		module.setSamplesPerPixel(dcm.getInt(Tag.SamplesPerPixel));
		module.setPhotometricInterpretation(
			dcm.getString(Tag.PhotometricInterpretation));
		if (module.getSamplesPerPixel() > 1)
		{
			module.setPlanarConfiguration(dcm.getInt(Tag.PlanarConfiguration));
		}
		module.setNumberOfFrames(dcm.getInt(Tag.NumberOfFrames));
		module.setBitsAllocated(dcm.getInt(Tag.BitsAllocated, VR.US, -1));
		module.setBitsStored(dcm.getInt(Tag.BitsStored, VR.US, -1));
		int highBit = dcm.getInt(Tag.HighBit, VR.US, -1);
		if (highBit != module.getBitsStored()-1)
		{
			throw new IllegalArgumentException("HighBit must be (BitsStored-1)");
		}
		module.setHighBit(highBit);
		module.setPixelRepresentation(dcm.getInt(Tag.PixelRepresentation));
		module.setAcquisitionDateTime(dcm.getString(Tag.AcquisitionDateTime));
		module.setAcquisitionDuration(dcm.getDouble(Tag.AcquisitionDuration,
			VR.FD, Double.NaN));
		boolean lossy = "01".equals(
			dcm.getString(Tag.LossyImageCompression, VR.CS, "00"));
		module.setLossyImageCompression(lossy);
		if (lossy)
		{
			module.setLossyImageCompressionRatio(
				dcm.getDoubles(Tag.LossyImageCompressionRatio));
			module.setLossyImageCompressionMethod(
				dcm.getStrings(Tag.LossyImageCompressionMethod));
		}
		if (Constants.Monochrome2.equals(module.getPhotometricInterpretation()))
		{
			module.setPresentationLutShape(dcm.getString(Tag.PresentationLUTShape));
			module.setRescaleIntercept(dcm.getDouble(Tag.RescaleIntercept));
			module.setRescaleSlope(dcm.getDouble(Tag.RescaleSlope));
		}
		module.setSpecimenLabelInImage(Unpack.yesOrNo(dcm, Tag.SpecimenLabelInImage));
		module.setBurnedInAnnotation(dcm.getString(Tag.BurnedInAnnotation));
		module.setFocusMethod(dcm.getString(Tag.FocusMethod));
		module.setExtendedDepthOfField(Unpack.yesOrNo(dcm, Tag.ExtendedDepthOfField));
		if (module.getExtendedDepthOfField())
		{
			module.setNumberOfFocalPlanes(dcm.getInt(Tag.NumberOfFocalPlanes));
			module.setDistanceBetweenFocalPlanes(
				dcm.getFloat(Tag.DistanceBetweenFocalPlanes));
		}
	}

	/**
	 *	Unpack the DicomObject into the module's fields.
	 * @param dcm the source to unpack
	 * @param module the module 
	 * @throws IllegalArgumentException if required fields are missing or invalid
	 */
	public static void unpack(DicomObject dcm,
		WholeSlideMicroscopySeriesModule module) throws IllegalArgumentException
	{
	}

	/**
	 * Returns a new WholeSlideMicroscopySeriesModule.
	 * @return the new module
	 */
	public static WholeSlideMicroscopySeriesModule wholeSlideMicroscopySeriesModule()
	{
		return wholeSlideMicroscopySeriesModule(true);
	}

	/**
	 * Returns a new WholeSlideMicroscopySeriesModule.
	 * @param strict
	 * @return the new module
	 */
	public static WholeSlideMicroscopySeriesModule wholeSlideMicroscopySeriesModule(
		boolean strict)
	{
		return new DefaultWholeSlideMicroscopySeriesModule(strict);
	}

	private static void packWsmImageStorage(
		WsmMultiframeFunctionalGroupsModule module, DicomObject dcm)
	{
		DicomElement sharedSq = Pack.getOrCreateSq(dcm,
			Tag.SharedFunctionalGroupsSequence);
		DicomObject sharedDcm = new BasicDicomObject();
		sharedSq.addDicomObject(sharedDcm);
		WsmSharedFunctionalGroups sharedGroups =
			module.getSharedFunctionalGroups();
		PixelMeasures pixMeasures = sharedGroups.getPixelMeasures();
		Pack.packPixelMeasures(pixMeasures, sharedDcm, true);
		String[] frameType = sharedGroups.getWholeSlideMicroscopyImageFrameType();
		DicomObject wsmiFrameTypeDcm = Pack.getOrCreateSqItem(sharedDcm, NewTag.WSMImageFrameTypeSequence);
		wsmiFrameTypeDcm.putStrings(Tag.FrameType, VR.CS, frameType);
		String optPathIdent = sharedGroups.getOpticalPathIdentifier();
		if (optPathIdent != null)
		{
			DicomObject optIdentDcm = Pack.getOrCreateSqItem(sharedDcm,
				Tag.OpticalPathIdentificationSequence);
			optIdentDcm.putString(Tag.OpticalPathIdentifier, VR.SH, optPathIdent);
		}
		List<FunctionalGroupsReferencedImage> refImages =
			sharedGroups.getReferencedImageList();
		if (!refImages.isEmpty())
		{
			DicomElement refImageSq = Pack.getOrCreateSq(sharedDcm,
				Tag.ReferencedImageSequence);
			refImages.forEach((image) -> Pack.packFuncGroupsRefImage(image, refImageSq));
		}

		WsmPerFrameFunctionalGroups perFrameGroups =
			module.getPerFrameFunctionalGroups();
		List<FunctionalGroupsFrame> frames = perFrameGroups.getFrameList();
		if (!frames.isEmpty())
		{
			DicomElement perFrameSq = Pack.getOrCreateSq(dcm,
				Tag.PerFrameFunctionalGroupsSequence);
			frames.forEach((frame) ->
				Pack.packWsmFrame((WsmFunctionalGroupsFrame) frame, perFrameSq)
			);
		}
	}

	private static void unpackCodingSchemeIds(DicomElement csiSq,
		SopCommonModule module)
	{
		if ((csiSq == null) || csiSq.isEmpty())
		{
			return;
		}
		for (int i=0; i<csiSq.countItems(); i++)
		{
			DicomObject dcm = csiSq.getDicomObject(i);
			CodingSchemeIdentifier csi = new DefaultCodingSchemeIdentifier();
			csi.setCodingSchemeDesignator(dcm.getString(Tag.CodingSchemeDesignator,
				VR.SH, null));
			csi.setCodingSchemeName(dcm.getString(Tag.CodingSchemeName,
				VR.ST, null));
			csi.setCodingSchemeVersion(dcm.getString(Tag.CodingSchemeVersion,
				VR.SH, null));
			csi.setCodingSchemeResposibleOrganisation(
				dcm.getString(Tag.CodingSchemeResponsibleOrganization, VR.ST, null));
			module.addCodingSchemeIdentifier(csi);
		}
	}


	private static void unpackCurrReqProcEvidence(DicomObject dcm,
		SrDocumentGeneralModule module)
	{
		DicomElement currReqSq = dcm.get(
			Tag.CurrentRequestedProcedureEvidenceSequence);
		if (currReqSq == null)
		{
			return;
		}
		int nCurrReq = currReqSq.countItems();
		for (int i=0; i<nCurrReq; i++)
		{
			DicomObject crDcm = currReqSq.getDicomObject(i);
			module.addCurrentRequestedProcedureEvidence(Unpack.unpackHeirSopInstRef(crDcm));
		}
	}


	private static void unpackRoiContour(DicomObject item, RoiContourModule module)
	{
		RoiContour rc = new DefaultRoiContour();
		try
		{
			rc.setReferencedRoiNumber(item.getInt(Tag.ReferencedROINumber));
			int[] rgb = item.getInts(Tag.ROIDisplayColor);
			if ((rgb != null) && (rgb.length == 3))
			{
				rc.setRoiDisplayColour(rgb);
			}
		}
		catch (NumberFormatException ex)
		{
			logger.debug(
				"Invalid integer (IS) in RoiContour", ex);
		}
		module.addRoiContour(rc);
		Unpack.unpackRoiContour(item.get(Tag.ContourSequence), rc);
	}


	private static void unpackRoiContoursMT(DicomElement roiContourSq,
		RoiContourModule module, ExecutorService service)
	{
		int nItems = roiContourSq.countItems();
		List<Callable<Void>> tasks = new ArrayList(nItems);
		for (int i=0; i<nItems; i++)
		{
			DicomObject item = roiContourSq.getDicomObject(i);
			Callable<Void> task = new Callable<Void>()
			{
				@Override
				public Void call()
				{
					unpackRoiContour(item, module);
					return null;
				}
			};
			tasks.add(task);
		}
		try
		{
			service.invokeAll(tasks);
		}
		catch (InterruptedException ex)
		{
			logger.error("RoiContour unpacking failed:"+ex.getMessage(), ex);
		}
	}

	private static void unpackRoiContoursST(DicomElement roiContourSq,
		RoiContourModule module)
	{
		int nItems = roiContourSq.countItems();
		for (int i=0; i<nItems; i++)
		{
			unpackRoiContour(roiContourSq.getDicomObject(i), module);
		}
	}


	private static void unpackSegment(DicomObject dcm,
		SegmentationImageModule module) throws IllegalArgumentException
	{
		Segment segment = new DefaultSegment();
		segment.setSegmentNumber(dcm.getInt(Tag.SegmentNumber, VR.US, -1));
		segment.setSegmentLabel(dcm.getString(Tag.SegmentLabel, VR.LO, null));
		segment.setSegmentDescription(
			dcm.getString(Tag.SegmentDescription, VR.ST, null));
		segment.setSegmentAlgorithmType(
			dcm.getString(Tag.SegmentAlgorithmType, VR.CS, null));
		if (!segment.getSegmentAlgorithmType().equals(Constants.Manual))
		{
			segment.setSegmentAlgorithmName(
				dcm.getString(Tag.SegmentAlgorithmName, VR.LO, null));
		}
		int[] cieValue = dcm.getInts(Tag.RecommendedDisplayCIELabValue, VR.US, null);
		if ((cieValue != null) && (cieValue.length == 3))
		{
			segment.setRecommendedDisplayCIELabValue(cieValue);
		}

		DicomElement anatomicRegSq = dcm.get(Tag.AnatomicRegionSequence);
		if (anatomicRegSq != null)
		{
			for (int i=0; i<anatomicRegSq.countItems(); i++)
			{
				Code anatomicRegCode = Unpack.unpackCode(anatomicRegSq.getDicomObject(i),
					Tag.AnatomicRegionModifierSequence);
				segment.addAnatomicRegionCode(anatomicRegCode);
			}
		}
		DicomElement propCategorySq =
			dcm.get(Tag.SegmentedPropertyCategoryCodeSequence);
		if ((propCategorySq == null) || (propCategorySq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"SegmentedPropertyCategoryCodeSequence missing or invalid size");
		}
		segment.setSegmentedPropertyCategoryCode(Unpack.unpackCode(propCategorySq.getDicomObject(0)));
		DicomElement propTypeSq = dcm.get(Tag.SegmentedPropertyTypeCodeSequence);
		if ((propTypeSq == null) || (propTypeSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"SegmentedPropertyTypeCodeSequence missing or invalid size");
		}
		segment.setSegmentedPropertyTypeCode(Unpack.unpackCode(propTypeSq.getDicomObject(0), NewTag.SegmentedPropertyTypeModifierCodeSequence));

		module.addSegment(segment);
	}


	private static void unpackSegmentationStorage(DicomObject dcm,
		SegmentationMultiframeFunctionalGroupsModule module)
		throws IllegalArgumentException
	{
		DicomElement sharedSq = dcm.get(Tag.SharedFunctionalGroupsSequence);
		if ((sharedSq == null) || (sharedSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"SharedFunctionalGroups missing or empty");
		}
		DicomElement perFrameSq = dcm.get(Tag.PerFrameFunctionalGroupsSequence);
		int nFrames = dcm.getInt(Tag.NumberOfFrames, -1);
		if ((perFrameSq == null) || (perFrameSq.countItems() != nFrames))
		{
			throw new IllegalArgumentException(
				"PerFrameFunctionalGroups missing or invalid length");
		}

		SegmentationSharedFunctionalGroups sharedGroups =
			module.getSharedFunctionalGroups();
		String frameOfRefUid = dcm.getString(Tag.FrameOfReferenceUID, VR.UI, null);
		DicomElement pixMeasSq =
			sharedSq.getDicomObject().get(Tag.PixelMeasuresSequence);
		if ((pixMeasSq != null) && (pixMeasSq.countItems() == 1))
		{
			sharedGroups.setPixelMeasures(Unpack.unpackPixelMeasures(pixMeasSq.getDicomObject(),
					(frameOfRefUid != null)));
		}
		DicomElement planeOriSq =
			sharedSq.getDicomObject().get(Tag.PlaneOrientationSequence);
		if ((planeOriSq != null) && (planeOriSq.countItems() == 1))
		{
			sharedGroups.setImageOrientationPatient(Unpack.unpackPlaneOrientation(planeOriSq.getDicomObject(),
					(frameOfRefUid != null)));
		}

		SegmentationPerFrameFunctionalGroups perFrameGroups =
			module.getPerFrameFunctionalGroups();
		for (int i=0; i<nFrames; i++)
		{
			FunctionalGroupsFrame frame = Unpack.unpackSegmentationFrame(
				perFrameSq.getDicomObject(i), frameOfRefUid);
			perFrameGroups.addFrame(frame);
		}
	}


	private static void unpackStructureSetRoi(DicomElement roiSq,
		StructureSetModule ss) throws IllegalArgumentException
	{
		if (roiSq == null)
		{
			throw new IllegalArgumentException(
				"StructureSetROISequence must not be null");
		}
		int nItems = roiSq.countItems();
		if (nItems < 1)
		{
			throw new IllegalArgumentException(
				"StructureSetROISequence must not be empty");
		}
		for (int i=0; i<nItems; i++)
		{
			DicomObject dcm = roiSq.getDicomObject(i);
			StructureSetRoi ssRoi = new DefaultStructureSetRoi();
			String number = dcm.getString(Tag.ROINumber);
			if (number != null)
			{
				try
				{
					ssRoi.setRoiNumber(Integer.parseInt(number));
				}
				catch (NumberFormatException ex)
				{
					logger.debug(
						"Invalid instance number (IS) in StructureSetRoi: {}", number);
				}
			}
			ssRoi.setReferencedFrameOfReferenceUid(
				dcm.getString(Tag.ReferencedFrameOfReferenceUID));
			ss.addStructureSetRoi(ssRoi);
			ssRoi.setRoiName(dcm.getString(Tag.ROIName));
			ssRoi.setRoiDescription(dcm.getString(Tag.ROIDescription));
			ssRoi.setRoiGenerationAlgorithm(
				dcm.getString(Tag.ROIGenerationAlgorithm));
		}
	}


	private static void unpackWsmStorage(DicomObject dcm,
		WsmMultiframeFunctionalGroupsModule module)
	{
		DicomElement sharedSq = dcm.get(Tag.SharedFunctionalGroupsSequence);
		if ((sharedSq == null) || (sharedSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"SharedFunctionalGroups missing or empty");
		}

		String dimOrgType = dcm.getString(Tag.DimensionOrganizationType, VR.CS,
			null);
		// TILED_FULL images don't need per-frame groups
		boolean perFrameReq = !Constants.TiledFull.equals(dimOrgType);
		DicomElement perFrameSq = dcm.get(Tag.PerFrameFunctionalGroupsSequence);
		int nFrames = dcm.getInt(Tag.NumberOfFrames, -1);
		if (perFrameReq &&
			 ((perFrameSq == null) || (perFrameSq.countItems() != nFrames)))
		{
			throw new IllegalArgumentException(
				"PerFrameFunctionalGroups missing or invalid length");
		}

		WsmSharedFunctionalGroups sharedGroups =
			(WsmSharedFunctionalGroups) module.getSharedFunctionalGroups();
		DicomObject sharedDcm = sharedSq.getDicomObject();
		// PixelMeasures
		DicomElement pixMeasSq = sharedDcm.get(Tag.PixelMeasuresSequence);
		if ((pixMeasSq == null) || (pixMeasSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"PixelMeasuresSequence missing or invalid length");
		}
		sharedGroups.setPixelMeasures(Unpack.unpackPixelMeasures(pixMeasSq.getDicomObject(), false));
		DicomElement wsmiFrameTypeSq = sharedDcm.get(NewTag.WSMImageFrameTypeSequence);
		if ((wsmiFrameTypeSq == null) || (wsmiFrameTypeSq.countItems() != 1))
		{
			throw new IllegalArgumentException(
				"WholeSlideMicroscopyImageFrameTypeSequence missing or invalid length");
		}
		DicomObject wsmiFrameTypeDcm = wsmiFrameTypeSq.getDicomObject();
		sharedGroups.setWholeSlideMicroscopyImageFrameType(
			wsmiFrameTypeDcm.getStrings(Tag.FrameType, VR.CS, null));
		// OpticalPathID
		DicomElement optIdentSq = sharedDcm.get(
			Tag.OpticalPathIdentificationSequence);
		if (optIdentSq != null)
		{
			if (optIdentSq.isEmpty())
			{
				throw new IllegalArgumentException(
					"OpticalPathIndentificationSequence must not be empty");
			}
			DicomObject optIdentDcm = optIdentSq.getDicomObject();
			sharedGroups.setOpticalPathIdentifier(
				optIdentDcm.getString(Tag.OpticalPathIdentifier, VR.SH, null));
		}
		// ReferencedImage
		DicomElement refImageSq = sharedDcm.get(Tag.ReferencedImageSequence);
		if (refImageSq != null)
		{
			for (int i=0; i<refImageSq.countItems(); i++)
			{
				sharedGroups.addReferencedImage(Unpack.unpackFuncGroupRefImage(refImageSq.getDicomObject(i)));
			}
		}

		if ((perFrameSq != null))
		{
			WsmPerFrameFunctionalGroups perFrameGroups =
				module.getPerFrameFunctionalGroups();
			for (int i=0; i<nFrames; i++)
			{
				FunctionalGroupsFrame frame = Unpack.unpackWsmFrame(
					perFrameSq.getDicomObject(i));
				perFrameGroups.addFrame(frame);
			}
		}
	}


	private Modules()
	{}

}
