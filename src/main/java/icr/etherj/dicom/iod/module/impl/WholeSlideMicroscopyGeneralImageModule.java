/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.dicom.iod.module.GeneralImageModule;

/**
 *
 * @author jamesd
 */
public class WholeSlideMicroscopyGeneralImageModule
	extends DefaultGeneralImageModule implements GeneralImageModule
{
	private final WholeSlideMicroscopyMultiModuleCore core;

	public WholeSlideMicroscopyGeneralImageModule(
		WholeSlideMicroscopyMultiModuleCore wsmMultiModCore)
	{
		this(wsmMultiModCore, true);
	}

	public WholeSlideMicroscopyGeneralImageModule(
		WholeSlideMicroscopyMultiModuleCore core, boolean strict)
	{
		super(strict);
		if (core == null)
		{
			throw new IllegalArgumentException(
				"Whole slide microscopy multimodule core must not be null");
		}
		this.core = core;
	}

	@Override
	public String getBurnedInAnnotation()
	{
		return core.getBurnedInAnnotation();
	}

	@Override
	public String getContentDate()
	{
		return core.getContentDate();
	}

	@Override
	public String getContentTime()
	{
		return core.getContentTime();
	}

	@Override
	public String[] getImageType()
	{
		return core.getImageType();
	}

	@Override
	public boolean getLossyImageCompression()
	{
		return core.getLossyImageCompression();
	}

	/**
	 * Returns the patient orientation, type 2C. Not required as WSI requires
	 * Image Position (Slide).
	 * @return 
	 */
	@Override
	public String[] getPatientOrientation()
	{
		return null;
	}

	@Override
	public void setBurnedInAnnotation(String value)
		throws IllegalArgumentException
	{
		core.setBurnedInAnnotation(value);
	}

	@Override
	public void setImageType(String[] type)
	{
		core.setImageType(type);
	}
	
	@Override
	public void setLossyImageCompression(boolean compressed)
	{
		core.setLossyImageCompression(compressed);
	}

}
