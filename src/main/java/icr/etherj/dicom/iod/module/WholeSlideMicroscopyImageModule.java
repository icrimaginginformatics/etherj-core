/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;
import icr.etherj.dicom.iod.PixelMatrixOrigin;

/**
 *
 * @author jamesd
 */
public interface WholeSlideMicroscopyImageModule extends Displayable
{
	
	/**
	 * Returns the acquisition datetime, type 1.
	 * @return
	 */
	String getAcquisitionDateTime();

	/**
	 * Returns the acquisition duration in seconds, type 3.
	 * @return
	 */
	double getAcquisitionDuration();

	/**
	 * Returns the bits allocated, type 1. Enumerated values: 8 or 16.
	 * @return
	 */
	int getBitsAllocated();

	/**
	 * Returns the bits stored, type 1. Must equal bits allocated.
	 * @return
	 */
	int getBitsStored();

	/**
	 * Returns burned in image annotation, type 1. Enumerated values: YES, NO.
	 * @return
	 */
	String getBurnedInAnnotation();

	/**
	 * Returns the distance between focal planes, type 1C. Required if extended
	 * depth of field is YES.
	 * @return
	 */
	float getDistanceBetweenFocalPlanes();

	/**
	 * Returns extended depth of field, type 1. Enumerated values: YES, NO.
	 * @return
	 */
	boolean getExtendedDepthOfField();

	/**
	 * Returns focus method, type 1. Enumerated values: AUTO, MANUAL.
	 * @return
	 */
	String getFocusMethod();

	/**
	 * Returns the high bit, type 1. Must be one less than bits stored.
	 * @return
	 */
	int getHighBit();

	/**
	 * Returns the image orientation (slide), type 1 (VM 6).
	 * @return
	 */
	double[] getImageOrientationSlide();

	/**
	 * Returns the image type, type 1.
	 * @return
	 */
	String[] getImageType();

	/**
	 * Returns the image volume depth, type 1.
	 * @return
	 */
	float getImagedVolumeDepth();

	/**
	 * Returns the image volume height, type 1.
	 * @return
	 */
	float getImagedVolumeHeight();

	/**
	 * Returns the image volume width, type 1.
	 * @return
	 */
	float getImagedVolumeWidth();

	/**
	 * Returns the lossy image compression flag, type 1. Enumerated values: 00,
	 * 01
	 * @return
	 */
	boolean getLossyImageCompression();

	/**
	 * Returns the lossy image compression ratio, type 1C. Required if lossy
	 * image compression is true.
	 * @return
	 */
	double[] getLossyImageCompressionRatio();

	/**
	 * Returns the lossy image compression method, type 1C. Required if lossy
	 * image compression is true.
	 * @return
	 */
	String[] getLossyImageCompressionMethod();

	/**
	 * Returns the number of focal planes, type 1C. Required if extended depth of
	 * field is YES.
	 * @return
	 */
	int getNumberOfFocalPlanes();

	/**
	 * Returns the number of frames, type 1.
	 * @return
	 */
	int getNumberOfFrames();

	/**
	 * Returns the photometric interpretation, type 1. Enumerated values:
	 * MONOCHROME2, RGB, YBR_FULL_422, YBR_ICT.
	 * @return 
	 */
	String getPhotometricInterpretation();

	/**
	 * Returns the pixel representation, type 1. Enumerated values: 0.
	 * @return
	 */
	int getPixelRepresentation();

	/**
	 * Returns the planar configuration, type 1C.
	 * @return
	 */
	int getPlanarConfiguration();

	/**
	 * Returns the lossy image compression method, type 1C. Enumerated values:
	 * IDENTITY. Required if photometric interpretation is MONOCHROME2.
	 * @return
	 */
	String getPresentationLutShape();

	/**
	 * Returns the rescale intercept, type 1C. Enumerated values: 0. Required if
	 * photometric interpretation is MONOCHROME2.
	 * @return
	 */
	double getRescaleIntercept();

	/**
	 * Returns the rescale slope, type 1C. Enumerated values: 1. Required if
	 * photometric interpretation is MONOCHROME2.
	 * @return
	 */
	double getRescaleSlope();

	/**
	 * Returns the number of samples per pixel, type 1. Enumerated values: 1 or 3
	 * @return
	 */
	int getSamplesPerPixel();

	/**
	 * Returns whether specimen label is in image, type 1. Enumerated values:
	 * YES, NO.
	 * @return
	 */
	boolean getSpecimenLabelInImage();

	/**
	 * Returns the total pixel matrix columns, type 1.
	 * @return
	 */
	int getTotalPixelMatrixColumns();

	/**
	 * Returns the total pixel matrix planes, type 1C. Required if TILED_FULL
	 * @return
	 */
	int getTotalPixelMatrixFocalPlanes();

	/**
	 * Returns the total pixel matrix origin, type 1.
	 * @return
	 */
	PixelMatrixOrigin getTotalPixelMatrixOrigin();

	/**
	 * Returns the total pixel matrix rows, type 1.
	 * @return
	 */
	int getTotalPixelMatrixRows();

	/**
	 * Returns the photometric interpretation, type 1. Enumerated values:
	 * VOLUME.
	 * @return 
	 */
	String getVolumetricProperties();

	/**
	 * Sets the acquisition datetime, type 1.
	 * @param datetime
	 * @throws IllegalArgumentException
	 */
	void setAcquisitionDateTime(String datetime) throws IllegalArgumentException;

	/**
	 * Sets the acquisition duration in seconds, type 3.
	 * @param duration
	 * @throws IllegalArgumentException
	 */
	void setAcquisitionDuration(double duration) throws IllegalArgumentException;

	/**
	 * Sets the bits allocated, type 1. Enumerated values: 8 or 16.
	 * @param bits
	 * @throws IllegalArgumentException
	 */
	void setBitsAllocated(int bits) throws IllegalArgumentException;

	/**
	 * Sets the bits stored, type 1. Must equal bits allocated.
	 * @param bits
	 * @throws IllegalArgumentException
	 */
	void setBitsStored(int bits) throws IllegalArgumentException;

	/**
	 * Sets the burned in annotation flag, type 1. Enumerated values: YES, NO.
	 * @param value
	 */
	void setBurnedInAnnotation(String value) throws IllegalArgumentException;

	/**
	 * Sets the distance between focal planes, type 1C. Required if extended
	 * depth of field is YES.
	 * @param distance
	 */
	void setDistanceBetweenFocalPlanes(float distance);

	/**
	 * Sets extended depth of field, type 1. Enumerated values: YES, NO.
	 * @param flag
	 */
	void setExtendedDepthOfField(boolean flag);

	/**
	 * Sets focus method, type 1. Enumerated values: AUTO, MANUAL.
	 * @param value
	 */
	void setFocusMethod(String value);

	/**
	 * Sets the high bit, type 1. Must be one less than bits stored.
	 * @param bit
	 */
	void setHighBit(int bit) throws IllegalArgumentException;

	/**
	 * Sets the image orientation (slide), type 1 (VM 6).
	 * @param orientation
	 */
	 void setImageOrientationSlide(double[] orientation);

	/**
	 * Sets the image type, type 1.
	 * @param type
	 */
	void setImageType(String[] type);
	
	/**
	 * Sets the image volume depth, type 1.
	 * @param depth
	 */
	void setImageVolumeDepth(float depth);

	/**
	 * Sets the image volume height, type 1.
	 * @param height
	 */
	void setImageVolumeHeight(float height);

	/**
	 * Sets the image volume width, type 1.
	 * @param width
	 */
	void setImageVolumeWidth(float width);

	/**
	 * Sets the lossy image compression flag, type 1.
	 * @param compressed
	 */
	void setLossyImageCompression(boolean compressed);

	/**
	 * Sets the lossy image compression ratio, type 1C. Required if lossy
	 * image compression is true.
	 * @param ratio
	 */
	 void setLossyImageCompressionRatio(double[] ratio);

	/**
	 * Sets the lossy image compression method, type 1C. Required if lossy
	 * image compression is true.
	 * @param method
	 */
	 void setLossyImageCompressionMethod(String[] method);

	/**
	 * Returns the number of focal planes, type 1C. Required if extended depth of
	 * field is YES.
	 * @param planes
	 */
	void setNumberOfFocalPlanes(int planes);

	/**
	 * Returns the number of frames, type 1.
	 * @param frames
	 */
	void setNumberOfFrames(int frames);

	/**
	 * Returns the photometric interpretation, type 1. Enumerated values:
	 * MONOCHROME2, RGB, YBR_FULL_422, YBR_ICT.
	 * @param interpretation 
	 */
	void setPhotometricInterpretation(String interpretation);

	/**
	 * Returns the pixel representation, type 1. Enumerated values: 0.
	 * @param representation
	 */
	void setPixelRepresentation(int representation);

	/**
	 * Returns the planar configuration, type 1C. Required if samples per pixel
	 * has a value greater than 1.  Enumerated values: 0.
	 * @param config
	 */
	void setPlanarConfiguration(int config);

	/**
	 * Returns the lossy image compression method, type 1C. Enumerated values:
	 * IDENTITY. Required if photometric interpretation is MONOCHROME2.
	 * @param shape
	 */
	void setPresentationLutShape(String shape);

	/**
	 * Returns the rescale intercept, type 1C. Enumerated values: 0. Required if
	 * photometric interpretation is MONOCHROME2.
	 * @param intercept
	 */
	void setRescaleIntercept(double intercept);

	/**
	 * Returns the rescale slope, type 1C. Enumerated values: 1. Required if
	 * photometric interpretation is MONOCHROME2.
	 * @param slope
	 */
	void setRescaleSlope(double slope);

	/**
	 * Returns the number of samples per pixel, type 1. Enumerated values: 1 or 3
	 * @param samples
	 */
	void setSamplesPerPixel(int samples);

	/**
	 * Returns whether specimen label is in image, type 1. Enumerated values:
	 * YES, NO.
	 * @param flag
	 */
	void setSpecimenLabelInImage(boolean flag);

	/**
	 * Returns the total pixel matrix columns, type 1.
	 * @param columns
	 */
	void setTotalPixelMatrixColumns(int columns);

	/**
	 * Returns the number of total pixel matrix focal planes, type 1C.
	 * Required if TILED_FULL.
	 * @param planes
	 */
	void setTotalPixelMatrixFocalPlanes(int planes);

	/**
	 * Returns the total pixel matrix origin, type 1.
	 * @param origin
	 */
	void setTotalPixelMatrixOrigin(PixelMatrixOrigin origin);

	/**
	 * Returns the total pixel matrix rows, type 1.
	 * @param rows
	 */
	void setTotalPixelMatrixRows(int rows);

}
