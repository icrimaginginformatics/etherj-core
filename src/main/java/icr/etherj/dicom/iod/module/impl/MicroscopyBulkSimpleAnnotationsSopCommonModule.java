/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.dicom.iod.module.SopCommonModule;

/**
 *
 * @author jamesd
 */
public class MicroscopyBulkSimpleAnnotationsSopCommonModule
	extends DefaultSopCommonModule implements SopCommonModule
{
	private final MicroscopyBulkSimpleAnnotationsMultiModuleCore core;

	public MicroscopyBulkSimpleAnnotationsSopCommonModule(
		MicroscopyBulkSimpleAnnotationsMultiModuleCore core)
	{
		this(core, true);
	}

	public MicroscopyBulkSimpleAnnotationsSopCommonModule(
		MicroscopyBulkSimpleAnnotationsMultiModuleCore core, boolean strict)
		throws IllegalArgumentException
	{
		super("1.2.840.10008.5.1.4.1.1.91.1", strict);
		if (core == null)
		{
			throw new IllegalArgumentException(
				"Microscopy bulk simple annotations multimodule core must not be null");
		}
		this.core = core;
	}

	@Override
	public int getInstanceNumber()
	{
		return core.getInstanceNumber();
	}

	@Override
	public String getSopClassUid()
	{
		return core.getSopClassUid();
	}

	@Override
	public String getSopInstanceUid()
	{
		return core.getSopInstanceUid();
	}

	@Override
	public void setInstanceNumber(int number)
	{
		core.setInstanceNumber(number);
	}

	@Override
	public void setSopInstanceUid(String uid) throws IllegalArgumentException
	{
		core.setSopInstanceUid(uid);
	}
	
}
