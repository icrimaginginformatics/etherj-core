/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.WsmPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.WsmSharedFunctionalGroups;
import icr.etherj.dicom.iod.module.WsmMultiframeFunctionalGroupsModule;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class DefaultWsmMultiframeFunctionalGroupsModule
	extends AbstractDisplayable
	implements WsmMultiframeFunctionalGroupsModule
{
	private int frameCount = 0;
	private final WsmPerFrameFunctionalGroups perFrameGroups;
	private final WholeSlideMicroscopyMultiModuleCore core;
	private final WsmSharedFunctionalGroups sharedGroups;
	private final boolean strict;

	public DefaultWsmMultiframeFunctionalGroupsModule(
		WholeSlideMicroscopyMultiModuleCore core,
		WsmSharedFunctionalGroups sharedGroups, 
		WsmPerFrameFunctionalGroups perFrameGroups)
		throws IllegalArgumentException
	{
		this(core, sharedGroups, perFrameGroups, true);
	}

	public DefaultWsmMultiframeFunctionalGroupsModule(
		WholeSlideMicroscopyMultiModuleCore core,
		WsmSharedFunctionalGroups sharedGroups, 
		WsmPerFrameFunctionalGroups perFrameGroups,
		boolean strict)
		throws IllegalArgumentException
	{
		if (core == null)
		{
			throw new IllegalArgumentException(
				"WholeSlideMicroscopy multimodule core must not be null");
		}
		if (sharedGroups == null)
		{
			throw new IllegalArgumentException(
				"Shared functional groups must not be null");
		}
		if (perFrameGroups == null)
		{
			throw new IllegalArgumentException(
				"Per-frame functional groups must not be null");
		}
		this.strict = strict;
		this.core = core;
		this.sharedGroups = sharedGroups;
		this.perFrameGroups = perFrameGroups;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"InstanceNumber: "+core.getInstanceNumber());
		ps.println(pad+"ContentDate: "+core.getContentDate());
		ps.println(pad+"ContentTime: "+core.getContentTime());
		ps.println(pad+"NumberOfFrames: "+frameCount);
		ps.println(pad+"SharedFunctionalGroups:");
		sharedGroups.display(ps, indent+"  ");
		ps.println(pad+"PerFrameFunctionalGroups:");
		perFrameGroups.display(ps, indent+"  ");
	}

	@Override
	public String getContentDate()
	{
		return core.getContentDate();
	}

	@Override
	public String getContentTime()
	{
		return core.getContentTime();
	}

	@Override
	public String getFrameOfReferenceUid()
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public int getInstanceNumber()
	{
		return core.getInstanceNumber();
	}

	@Override
	public int getNumberOfFrames()
	{
		return frameCount;
	}

	@Override
	public WsmPerFrameFunctionalGroups getPerFrameFunctionalGroups()
	{
		return perFrameGroups;
	}

	@Override
	public WsmSharedFunctionalGroups getSharedFunctionalGroups()
	{
		return sharedGroups;
	}

	@Override
	public String getSopClassUid()
	{
		return core.getSopClassUid();
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setContentDate(String date) throws IllegalArgumentException
	{
		core.setContentDate(date);
	}

	@Override
	public void setContentTime(String time) throws IllegalArgumentException
	{
		core.setContentTime(time);
	}

	@Override
	public void setInstanceNumber(int number) throws IllegalArgumentException
	{
		core.setInstanceNumber(number);
	}

	@Override
	public void setNumberOfFrames(int frameCount) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, frameCount,
			(int x) -> (x > 0), 
			"NumberOfFrames must be greater than zero");
		this.frameCount = checked;
	}

}
