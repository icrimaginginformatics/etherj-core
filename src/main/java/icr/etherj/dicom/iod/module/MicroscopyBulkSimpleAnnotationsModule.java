/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.Displayable;
import icr.etherj.dicom.iod.AnnotationGroup;
import icr.etherj.dicom.iod.ReferencedImage;
import java.util.List;

/**
 *
 * @author jamesd
 */
public interface MicroscopyBulkSimpleAnnotationsModule
	extends Displayable, Module
{
	boolean addAnnotationGroup(AnnotationGroup group);

	/**
	 * Returns the annotation coordinate type, type 1. Enumerated values: 2D, 3D.
	 * @return 
	 */
	String getAnnotationCoordinateType();

	/**
	 * Returns the annotation groups, type 1.
	 * @return 
	 */
	List<AnnotationGroup> getAnnotationGroupList();

	/**
	 * Returns the content date, type 1.
	 * @return 
	 */
	String getContentDate();

	/**
	 * Returns the content description, type 2.
	 * @return 
	 */
	String getContentDescription();

	/**
	 * Returns the content label, type 1.
	 * @return 
	 */
	String getContentLabel();

	/**
	 * Returns the content time, type 1.
	 * @return 
	 */
	String getContentTime();

	/**
	 * Returns instance number, type 1.
	 * @return 
	 */
	int getInstanceNumber();
	
	/**
	 * Returns the pixel origin interpretation, type 1C. Required if
	 * annotation coordinate type is 2D.
	 * @return 
	 */
	String getPixelOriginInterpretation();

	/**
	 * Returns the referenced image, type 1C. Required if annotation coordinate
	 * type is 2D.
	 * @return 
	 */
	ReferencedImage getReferencedImage();

	boolean removeAnnotationGroup(AnnotationGroup group);

	/**
	 * Sets the annotation coordinate type, type 1. Enumerated values: 2D, 3D. 
	 * @param type
	 */
	void setAnnotationCoordinateType(String type)
		throws IllegalArgumentException;

	/**
	 * Sets the content date, type 1. 
	 * @param date
	 */
	void setContentDate(String date)
		throws IllegalArgumentException;

	/**
	 * Sets the content description, type 2.
	 * @param description 
	 */
	void setContentDescription(String description);

	/**
	 * Sets the content label, type 1.
	 * @param label 
	 */
	void setContentLabel(String label)
		throws IllegalArgumentException;

	/**
	 * Sets the content time, type 1.
	 * @param time 
	 */
	void setContentTime(String time)
		throws IllegalArgumentException;

	/**
	 * Sets instance number, type 1.
	 * @param number 
	 */
	void setInstanceNumber(int number);

	/**
	 * Sets the pixel origin interpretation, type 1C. Required if
	 * annotation coordinate type is 2D.
	 * @param interpretation 
	 */
	void setPixelOriginInterpretation(String interpretation)
		throws IllegalArgumentException;

	/**
	 * Sets the referenced image, type 1C. Required if annotation coordinate type
	 * is 2D.
	 * @param image 
	 */
	void setReferencedImage(ReferencedImage image);

}
