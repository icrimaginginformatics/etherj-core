/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.dicom.iod.ReferencedFrameOfReference;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.Validatable;
import icr.etherj.dicom.Validation;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.module.StructureSetModule;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;

/**
 *
 * @author jamesd
 */
public final class DefaultStructureSetModule extends AbstractDisplayable
	implements StructureSetModule
{
	private String date = "";
	private String desc;
	private String label = "StructureSetLabel";
	private String name;
	private final List<ReferencedFrameOfReference> refFoRList = new ArrayList<>();
	private final RtStructMultiModuleCore rtMultiModCore;
	private final Map<Integer,StructureSetRoi> ssRoiMap = new TreeMap<>();
	private final boolean strict;
	private String time = "";

	public DefaultStructureSetModule(RtStructMultiModuleCore rtMultiModCore)
	{
		this(rtMultiModCore, true);
	}

	public DefaultStructureSetModule(RtStructMultiModuleCore rtMultiModCore,
		boolean strict)
	{
		if (rtMultiModCore == null)
		{
			throw new IllegalArgumentException(
				"RtStruct multi module core must not be null");
		}
		this.rtMultiModCore = rtMultiModCore;
		this.strict = strict;
	}

	@Override
	public boolean addReferencedFrameOfReference(
		ReferencedFrameOfReference refFoR)
	{
		return (refFoR != null) ? refFoRList.add(refFoR) : false;
	}

	@Override
	public boolean addStructureSetRoi(StructureSetRoi roi)
	{
		if (roi == null)
		{
			return false;
		}
		ssRoiMap.put(roi.getRoiNumber(), roi);
		return true;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"Label: "+label);
		if ((name != null) && !name.isEmpty())
		{
			ps.println(pad+"Name: "+name);
		}
		if ((desc != null) && !desc.isEmpty())
		{
			ps.println(pad+"Description: "+desc);
		}
		int instNumber = rtMultiModCore.getInstanceNumber();
		if (instNumber > 0)
		{
			ps.println(pad+"InstanceNumber: "+instNumber);
		}
		if (!date.isEmpty())
		{
			ps.println(pad+"Date: "+date);
		}
		if (!time.isEmpty())
		{
			ps.println(pad+"Time: "+time);
		}
		int nRefFoR = refFoRList.size();
		ps.println(pad+"ReferencedFrameOfReference: "+nRefFoR+" item"+
			((nRefFoR != 1) ? "s" : ""));
		if (recurse)
		{
			for (ReferencedFrameOfReference refFoR : refFoRList)
			{
				refFoR.display(ps, indent+"  ", recurse);
			}
		}
		int nSsRoi = ssRoiMap.size();
		ps.println(pad+"StructureSetRoi: "+nSsRoi+" item"+
			((nSsRoi != 1) ? "s" : ""));
		if (recurse)
		{
			for (StructureSetRoi ssRoi : ssRoiMap.values())
			{
				ssRoi.display(ps, indent+"  ", recurse);
			}
		}
	}

	@Override
	public List<ReferencedFrameOfReference> getReferencedFrameOfReferenceList()
	{
		return ImmutableList.copyOf(refFoRList);
	}

	@Override
	public String getStructureSetDate()
	{
		return date;
	}

	@Override
	public String getStructureSetDescription()
	{
		return desc;
	}

	@Override
	public int getInstanceNumber()
	{
		return rtMultiModCore.getInstanceNumber();
	}

	@Override
	public String getStructureSetLabel()
	{
		return label;
	}

	@Override
	public String getStructureSetName()
	{
		return name;
	}

	@Override
	public StructureSetRoi getStructureSetRoi(int roiNumber)
	{
		return ssRoiMap.get(roiNumber);
	}

	@Override
	public List<StructureSetRoi> getStructureSetRoiList()
	{
		return ImmutableList.copyOf(ssRoiMap.values());
	}

	@Override
	public String getStructureSetTime()
	{
		return time;
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public boolean removeStructureSetRoi(int roiNumber)
	{
		StructureSetRoi removed = ssRoiMap.remove(roiNumber);
		return (removed != null);
	}

	@Override
	public boolean removeStructureSetRoi(StructureSetRoi roi)
	{
		if (roi == null)
		{
			return false;
		}
		return removeStructureSetRoi(roi.getRoiNumber());
	}

	@Override
	public void setStructureSetDate(String date) throws IllegalArgumentException
	{
		this.date = IodUtils.checkType2Date(strict, date, "Structure Set Date");
	}

	@Override
	public void setStructureSetDescription(String description)
	{
		desc = description;
	}

	@Override
	public void setInstanceNumber(int number)
	{
		rtMultiModCore.setInstanceNumber(number);
	}

	@Override
	public void setStructureSetLabel(String label) throws IllegalArgumentException
	{
		this.label = IodUtils.checkType1(strict, label, VR.SH,
			"StructureSetLabel must not be null or empty");
	}

	@Override
	public void setStructureSetName(String name)
	{
		this.name = name;
	}

	@Override
	public void setStructureSetTime(String time) throws IllegalArgumentException
	{
		this.time = IodUtils.checkType2Time(strict, time, "Structure Set Time");
	}

	@Override
	public boolean validate()
	{
		String clazz = "StructureSetModule";
		boolean labelOk = Validation.type1(clazz, label, Tag.StructureSetLabel);
		boolean dateOk = Validation.type2Date(clazz, date, Tag.StructureSetDate);
		boolean timeOk = Validation.type2Time(clazz, time, Tag.StructureSetTime);
		boolean refFoROk = validateRefFoR(clazz);
		boolean ssRoiOk = validateSsRoi(clazz);

		return labelOk && dateOk && timeOk && refFoROk && ssRoiOk;
	}

	private boolean validateRefFoR(String clazz)
	{
		List<Validatable> list = new ArrayList<>();
		list.addAll(refFoRList);
		return Validation.type1(clazz, list, 0,
			Tag.ReferencedFrameOfReferenceSequence);
	}

	private boolean validateSsRoi(String clazz)
	{
		List<Validatable> list = new ArrayList<>();
		list.addAll(ssRoiMap.values());
		// ToDo: Add unique numbering requirement
		return Validation.type1(clazz, list, 1, Tag.StructureSetROISequence);
	}
	
}
