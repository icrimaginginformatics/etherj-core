/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class SegmentationImagePixelModule extends AbstractDisplayable
	implements ImagePixelModule
{
	private int maxPix = -1;
	private int minPix = -1;
	private int nCols = 0;
	private int nRows = 0;
	private byte[] pixelData = new byte[] {};
	private final List<byte[]> pixelFrags = new ArrayList<>();
	private final SegmentationMultiModuleCore core;
	private final boolean strict;

	public SegmentationImagePixelModule(SegmentationMultiModuleCore core)
		throws IllegalArgumentException
	{
		this(core, true);
	}

	public SegmentationImagePixelModule(SegmentationMultiModuleCore core,
		boolean strict) throws IllegalArgumentException
	{
		if (core == null)
		{
			throw new IllegalArgumentException(
				"Segmentation multimodule core must not be null");
		}
		this.core = core;
		this.strict = strict;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		if (pixelData.length > 0)
		{
			ps.println(pad+"PixelData: "+pixelData.length+" bytes");
		}
		else
		{
			ps.println(pad+"PixelData: "+pixelFrags.size()+" fragments");
			if (recurse)
			{
				pixelFrags.forEach(
					(x) -> ps.println("  "+pad+"Fragment: "+x.length+" bytes"));
			}
		}
		ps.println(pad+"SamplesPerPixel: "+getSamplesPerPixel());
		ps.println(pad+"PhotometricInterpretation: "+
			getPhotometricInterpretation());
		ps.println(pad+"Rows: "+nRows);
		ps.println(pad+"Columns: "+nCols);
		ps.println(pad+"BitsAllocated: "+getBitsAllocated());
		ps.println(pad+"BitsStored: "+getBitsStored());
		ps.println(pad+"HighBit: "+getHighBit());
		ps.println(pad+"PixelRepresentation: "+getPixelRepresentation());
		if (minPix >= 0)
		{
			ps.println(pad+"SmallestPixelValue: "+minPix);
		}
		if (maxPix >= 0)
		{
			ps.println(pad+"LargestPixelValue: "+maxPix);
		}
	}

	@Override
	public int getBitsAllocated()
	{
		return core.getBitsAllocated();
	}

	@Override
	public int getBitsStored()
	{
		return core.getBitsStored();
	}

	@Override
	public int getColumnCount()
	{
		return nCols;
	}

	@Override
	public int getHighBit()
	{
		return core.getHighBit();
	}

	@Override
	public int getLargestPixelValue()
	{
		return maxPix;
	}

	@Override
	public String getPhotometricInterpretation()
	{
		return core.getPhotometricInterpretation();
	}

	@Override
	public byte[] getPixelData()
	{
		return pixelData;
	}

	@Override
	public List<byte[]> getPixelDataFragments()
	{
		return ImmutableList.copyOf(pixelFrags);
	}

	@Override
	public int getPixelRepresentation()
	{
		return core.getPixelRepresentation();
	}

	@Override
	public int getPlanarConfiguration()
	{
		throw new UnsupportedOperationException("Not valid for segmentations");
	}

	@Override
	public int getRowCount()
	{
		return nRows;
	}

	@Override
	public int getSamplesPerPixel()
	{
		return core.getSamplesPerPixel();
	}

	@Override
	public int getSmallestPixelValue()
	{
		return minPix;
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setBitsAllocated(int bits) throws IllegalArgumentException
	{
		core.setBitsAllocated(bits);
	}

	@Override
	public void setBitsStored(int bits) throws IllegalArgumentException
	{
		core.setBitsStored(bits);
	}

	@Override
	public void setColumnCount(int columns) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, columns,
			(int x) -> (x > 0),
			"Invalid column count: "+columns);
		nCols = checked;
	}

	@Override
	public void setHighBit(int bit) throws IllegalArgumentException
	{
		core.setHighBit(bit);
	}

	@Override
	public void setLargestPixelValue(int value)
	{
		maxPix = (value >= 0) ? value : -1;
	}

	@Override
	public void setPhotometricInterpretation(String interpretation)
		throws IllegalArgumentException
	{
		core.setPhotometricInterpretation(interpretation);
	}

	@Override
	public void setPixelData(byte[] pixels) throws IllegalArgumentException
	{
		setPixelData(pixels, false);
	}

	@Override
	public void setPixelData(byte[] pixels, boolean noCopy)
	{
		if (pixels == null)
		{
			this.pixelData = new byte[] {};
			return;
		}
		this.pixelFrags.clear();
		this.pixelData = (!noCopy)
			? Arrays.copyOf(pixels, pixels.length)
			: pixels;
	}

	@Override
	public void setPixelDataFragments(List<byte[]> fragments)
	{
		setPixelDataFragments(fragments, false);
	}

	@Override
	public void setPixelDataFragments(List<byte[]> fragments, boolean noCopy)
	{
		pixelFrags.clear();
		if (fragments == null)
		{
			return;
		}
		this.pixelData = new byte[] {};		
		if (noCopy)
		{
			pixelFrags.addAll(fragments);
			return;
		}
		fragments.forEach((x) -> pixelFrags.add(Arrays.copyOf(x, x.length)));
	}

	@Override
	public void setPixelRepresentation(int representation)
		throws IllegalArgumentException
	{
		core.setPixelRepresentation(representation);
	}

	@Override
	public void setPlanarConfiguration(int configuration)
		throws IllegalArgumentException
	{
		throw new UnsupportedOperationException("Not valid for segmentations");
	}

	@Override
	public void setRowCount(int rows) throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(strict, rows,
			(int x) -> (x > 0),
			"Invalid row count: "+rows);
		nRows = checked;
	}

	@Override
	public void setSamplesPerPixel(int samples)
		throws IllegalArgumentException
	{
		core.setSamplesPerPixel(samples);
	}

	@Override
	public void setSmallestPixelValue(int value)
	{
		minPix = (value >= 0) ? value : -1;
	}
	
}
