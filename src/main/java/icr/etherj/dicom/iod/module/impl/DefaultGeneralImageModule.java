/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.module.GeneralImageModule;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author jamesd
 */
public class DefaultGeneralImageModule extends AbstractDisplayable
	implements GeneralImageModule
{
	private static final Set<String> validAnno = new HashSet<>();

	private String acqDate;
	private String acqTime;
	private String burnedInAnno = null;
	private String comments;
	private String contentDate = "";
	private String contentTime = "";
	private boolean lossyComp = false;
	private int number = 0;
	private String[] patOrient = new String[] {};
	private final boolean strict;
	private String[] type = new String[] {};

	static
	{
		validAnno.add(Constants.Yes);
		validAnno.add(Constants.No);
	}

	public DefaultGeneralImageModule()
	{
		this(true);
	}

	public DefaultGeneralImageModule(boolean strict)
	{
		this.strict = strict;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		if (number > 0)
		{
			ps.println(pad+"InstanceNumber: "+number);
		}
		if (patOrient.length == 2)
		{
			ps.println(pad+"PatientOrient: "+patOrient[0]+"\\"+patOrient[1]);
		}
		if ((contentDate != null) && !contentDate.isEmpty())
		{
			ps.println(pad+"ContentDate: "+contentDate);
		}
		if ((contentTime != null) && !contentTime.isEmpty())
		{
			ps.println(pad+"ContentTime: "+contentTime);
		}
		if (type.length > 0)
		{
			ps.println(pad+"ImageType: "+String.join("\\", type));
		}
		if ((acqDate != null) && !acqDate.isEmpty())
		{
			ps.println(pad+"AcquisitionDate: "+acqDate);
		}
		if ((acqTime != null) && !acqTime.isEmpty())
		{
			ps.println(pad+"AcquisitionTime: "+acqTime);
		}
		if ((comments != null) && !comments.isEmpty())
		{
			ps.println(pad+"ImageComments: "+comments);
		}
		if ((burnedInAnno != null) && !burnedInAnno.isEmpty())
		{
			ps.println(pad+"BurnedInAnnotation: "+burnedInAnno);
		}
		if (lossyComp)
		{
			ps.println(pad+"LossyImageCompression: true");
		}
	}

	@Override
	public String getAcquisitionDate()
	{
		return acqDate;
	}

	@Override
	public String getAcquisitionTime()
	{
		return acqTime;
	}

	@Override
	public String getBurnedInAnnotation()
	{
		return burnedInAnno;
	}

	@Override
	public String getContentDate()
	{
		return contentDate;
	}

	@Override
	public String getContentTime()
	{
		return contentTime;
	}

	@Override
	public String getImageComments()
	{
		return comments;
	}

	@Override
	public String[] getImageType()
	{
		return Arrays.copyOf(type, type.length);
	}

	@Override
	public int getInstanceNumber()
	{
		return number;
	}

	@Override
	public boolean getLossyImageCompression()
	{
		return lossyComp;
	}

	@Override
	public String[] getPatientOrientation()
	{
		return Arrays.copyOf(patOrient, patOrient.length);
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setAcquisitionDate(String date) throws IllegalArgumentException
	{
		acqDate = IodUtils.checkType3Date(strict, date, "Acquisition Date");
	}

	@Override
	public void setAcquisitionTime(String time) throws IllegalArgumentException
	{
		acqTime = IodUtils.checkType3Time(strict, time, "Acquisition Time");
	}

	@Override
	public void setBurnedInAnnotation(String value)
		throws IllegalArgumentException
	{
		if (value == null)
		{
			burnedInAnno = null;
			return;
		}
		burnedInAnno = IodUtils.checkCondition(strict, value,
			(x) -> validAnno.contains(x),
			"Burned in annotation must be YES, NO or null");
	}

	@Override
	public void setContentDate(String date) throws IllegalArgumentException
	{
		contentDate = IodUtils.checkType3Date(strict, date, "Content Date");
	}

	@Override
	public void setContentTime(String time) throws IllegalArgumentException
	{
		contentTime = IodUtils.checkType3Time(strict, time, "Content Time");
	}

	@Override
	public void setImageComments(String comments)
	{
		this.comments = comments;
	}

	@Override
	public void setImageType(String[] type)
	{
		if (type == null)
		{
			this.type = new String[] {};
			return;
		}
		this.type = Arrays.copyOf(type, type.length);
	}

	@Override
	public void setInstanceNumber(int number)
	{
		this.number = (number < 1) ? 0 : number;
	}

	@Override
	public void setLossyImageCompression(boolean compressed)
	{
		lossyComp = compressed;
	}

	@Override
	public void setPatientOrientation(String[] orientation)
		throws IllegalArgumentException
	{
		if ((orientation == null) || (orientation.length != 2))
		{
			patOrient = new String[] {};
			return;
		}
		patOrient = Arrays.copyOf(orientation, 2);
	}
	
}
