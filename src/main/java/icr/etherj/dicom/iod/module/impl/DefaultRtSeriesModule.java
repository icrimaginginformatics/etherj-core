/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.Uids;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.Validation;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.Modality;
import icr.etherj.dicom.iod.module.RtSeriesModule;
import java.io.PrintStream;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
public final class DefaultRtSeriesModule extends AbstractDisplayable
	implements RtSeriesModule
{
	private String date;
	private String desc;
	private String instUid = Uids.generateDicomUid();
	private String modality = Modality.RTSTRUCT;
	private int number = -1;
	private String opName = "";
	private final boolean strict;
	private String time;

	public DefaultRtSeriesModule()
	{
		this(true);
	}

	public DefaultRtSeriesModule(boolean strict)
	{
		this.strict = strict;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"Modality: "+modality);
		ps.println(pad+"SeriesInstanceUid: "+instUid);
		if (number > 0)
		{
			ps.println(pad+"SeriesNumber: "+number);
		}
		if ((date != null) && !date.isEmpty())
		{
			ps.println(pad+"SeriesDate: "+date);
		}
		if ((time != null) && !time.isEmpty())
		{
			ps.println(pad+"SeriesTime: "+time);
		}
		if ((desc != null) && !desc.isEmpty())
		{
			ps.println(pad+"SeriesDescription: "+desc);
		}
	}

	@Override
	public String getModality()
	{
		return modality;
	}

	@Override
	public String getOperatorsName()
	{
		return opName;
	}

	@Override
	public String getSeriesDate()
	{
		return date;
	}

	@Override
	public String getSeriesDescription()
	{
		return desc;
	}

	@Override
	public String getSeriesInstanceUid()
	{
		return instUid;
	}

	@Override
	public int getSeriesNumber()
	{
		return number;
	}

	@Override
	public String getSeriesTime()
	{
		return time;
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setModality(String modality) throws IllegalArgumentException
	{
		if ((modality == null) || modality.isEmpty())
		{
			throw new IllegalArgumentException(
				"Modality must not be null or empty");
		}
		switch (modality)
		{
			case Modality.RTDOSE:
			case Modality.RTIMAGE:
			case Modality.RTPLAN:
			case Modality.RTRECORD:
			case Modality.RTSTRUCT:
				this.modality = modality;
				break;
			default:
			throw new IllegalArgumentException(
				"Invalid modality for RtSeries: "+modality);
		}
	}

	@Override
	public void setOperatorsName(String name)
	{
		opName = (name != null) ? name : "";
	}

	@Override
	public void setSeriesDate(String date) throws IllegalArgumentException
	{
		this.date = IodUtils.checkType3Date(strict, date, "Series Date");
	}

	@Override
	public void setSeriesDescription(String description)
	{
		desc = description;
	}

	@Override
	public void setSeriesInstanceUid(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SopInstanceUid must not be null or empty");
		}
		instUid = uid;
	}

	@Override
	public void setSeriesNumber(int number)
	{
		this.number = (number > 0) ? number : -1;
	}

	@Override
	public void setSeriesTime(String time) throws IllegalArgumentException
	{
		this.time = IodUtils.checkType3Time(strict, time, "Series Time");
	}

	@Override
	public boolean validate()
	{
		String clazz = "RtSeriesModule";
		String[] allowable = new String[] { Modality.RTDOSE, Modality.RTIMAGE,
		Modality.RTPLAN, Modality.RTRECORD, Modality.RTSTRUCT };
		boolean modalityOk = Validation.type1(clazz, modality, allowable,
			Tag.Modality);
		boolean uidOk = Validation.type1(clazz, instUid, Tag.SeriesInstanceUID);
		boolean dateOk = Validation.type3Date(clazz, date, Tag.SeriesDate);
		boolean timeOk = Validation.type3Time(clazz, time, Tag.SeriesTime);

		return modalityOk && uidOk && dateOk && timeOk;
	}
	
}
