/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.module.ClinicalTrialSubjectModule;
import java.io.PrintStream;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author jamesd
 */
public class DefaultClinicalTrialSubjectModule extends AbstractDisplayable
	implements ClinicalTrialSubjectModule
{
	// Lock required as some methods set multiple fields and this must be atomic.
	private final Lock lock = new ReentrantLock();
	private String protocolId = null;
	private String protocolName = "";
	private String siteId = null;
	private String siteName = "";
	private String sponsorName = null;
	private final boolean strict;
	private String subjectId = null;
	private String subjectReadingId = null;

	public DefaultClinicalTrialSubjectModule()
	{
		this(true);
	}

	public DefaultClinicalTrialSubjectModule(boolean strict)
	{
		this.strict = strict;
	}

	@Override
	public void cloneTo(ClinicalTrialSubjectModule target)
	{
		try
		{
			lock.lock();
			cloneToImpl(target);
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		if ((sponsorName == null) || (protocolId == null))
		{
			return;
		}
		String pad = indent+"  * ";
		ps.println(pad+"ClinicalTrialSponsorName: "+sponsorName);
		ps.println(pad+"ClinicalTrialProtocolId: "+protocolId);
		ps.println(pad+"ClinicalTrialProtocolName: "+protocolName);
		ps.println(pad+"ClinicalTrialSiteId: "+siteId);
		ps.println(pad+"ClinicalTrialSiteName: "+siteName);
		if (subjectReadingId == null)
		{
			ps.println(pad+"ClinicalTrialSubjectId: "+subjectId);
		}
		else
		{
			ps.println(
				pad+"ClinicalTrialSubjectReadingId: "+subjectReadingId);
		}
	}

	@Override
	public String getClinicalTrialProtocolId()
	{
		return protocolId;
	}

	@Override
	public String getClinicalTrialProtocolName()
	{
		return protocolName;
	}

	@Override
	public String getClinicalTrialSiteId()
	{
		return siteId;
	}

	@Override
	public String getClinicalTrialSiteName()
	{
		return siteName;
	}

	@Override
	public String getClinicalTrialSponsorName()
	{
		return sponsorName;
	}

	@Override
	public String getClinicalTrialSubjectId()
	{
		return subjectId;
	}

	@Override
	public String getClinicalTrialSubjectReadingId()
	{
		return subjectReadingId;
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public void setClinicalTrialProtocolId(String id)
	{
		try
		{
			lock.lock();
			if ((id == null) || id.isEmpty())
			{
				reset();
				return;
			}
			protocolId = id;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public void setClinicalTrialProtocolName(String name)
	{
		try
		{
			lock.lock();
			protocolName = (name != null) ? name : "";
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public void setClinicalTrialSiteId(String id)
	{
		try
		{
			lock.lock();
			siteId = (id != null) ? id : "";
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public void setClinicalTrialSiteName(String name)
	{
		try
		{
			lock.lock();
			siteName = (name != null) ? name : "";
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public void setClinicalTrialSponsorName(String name)
	{
		try
		{
			lock.lock();
			if ((name == null) || name.isEmpty())
			{
				reset();
				return;
			}
			sponsorName = name;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public void setClinicalTrialSubjectId(String id)
		throws IllegalArgumentException
	{
		try
		{
			lock.lock();
			if ((id == null) || id.isEmpty())
			{
				if (subjectReadingId == null)
				{
					throw new IllegalArgumentException(
						"Subject ID and subject reading ID cannot both be null");
				}
				return;
			}
			subjectId = id;
			subjectReadingId = null;
		}
		finally
		{
			lock.unlock();
		}
	}

	@Override
	public void setClinicalTrialSubjectReadingId(String id) throws IllegalArgumentException
	{
		try
		{
			lock.lock();
			if ((id == null) || id.isEmpty())
			{
				if (subjectId == null)
				{
					throw new IllegalArgumentException(
						"Subject reading ID and subject reading ID cannot both be null");
				}
				return;
			}
			subjectId = null;
			subjectReadingId = id;
		}
		finally
		{
			lock.unlock();
		}
	}

	private void cloneToImpl(ClinicalTrialSubjectModule target)
	{
		target.setClinicalTrialSponsorName(sponsorName);
		if (sponsorName == null)
		{
			// Skip rest if module disabled
			return;
		}
		target.setClinicalTrialProtocolId(protocolId);
		target.setClinicalTrialProtocolName(protocolName);
		target.setClinicalTrialSiteId(siteId);
		target.setClinicalTrialSiteName(siteName);
		target.setClinicalTrialSubjectId(subjectId);
		target.setClinicalTrialSubjectReadingId(subjectReadingId);
	}

	private void reset()
	{
		protocolId = null;
		protocolName = "";
		siteId = null;
		siteName = "";
		sponsorName = null;
		subjectId = null;
		subjectReadingId = null;
	}
	
}
