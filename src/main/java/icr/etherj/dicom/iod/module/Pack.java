/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module;

import icr.etherj.StringUtils;
import icr.etherj.dicom.Coordinate2D;
import icr.etherj.dicom.Coordinate3D;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.NewTag;
import icr.etherj.dicom.iod.AlgorithmIdentification;
import icr.etherj.dicom.iod.AnnotationGroup;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.ContainerComponent;
import icr.etherj.dicom.iod.ContentItem;
import icr.etherj.dicom.iod.ContentRelationship;
import icr.etherj.dicom.iod.ContentTemplate;
import icr.etherj.dicom.iod.Contour;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.DerivationImage;
import icr.etherj.dicom.iod.DimensionIndex;
import icr.etherj.dicom.iod.FrameContent;
import icr.etherj.dicom.iod.FunctionalGroupsFrame;
import icr.etherj.dicom.iod.FunctionalGroupsReferencedImage;
import icr.etherj.dicom.iod.HL7v2HierarchicDesignator;
import icr.etherj.dicom.iod.HeirarchicalSeriesReference;
import icr.etherj.dicom.iod.HeirarchicalSopInstanceReference;
import icr.etherj.dicom.iod.Measurement;
import icr.etherj.dicom.iod.OpticalPath;
import icr.etherj.dicom.iod.PixelMatrixOrigin;
import icr.etherj.dicom.iod.PixelMeasures;
import icr.etherj.dicom.iod.PlanePositionSlide;
import icr.etherj.dicom.iod.ReferencedFrameOfReference;
import icr.etherj.dicom.iod.ReferencedImage;
import icr.etherj.dicom.iod.ReferencedInstance;
import icr.etherj.dicom.iod.ReferencedSeries;
import icr.etherj.dicom.iod.RtReferencedSeries;
import icr.etherj.dicom.iod.RtReferencedStudy;
import icr.etherj.dicom.iod.Segment;
import icr.etherj.dicom.iod.SegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.SegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SegmentationSharedFunctionalGroups;
import icr.etherj.dicom.iod.SourceImage;
import icr.etherj.dicom.iod.SpecimenDescription;
import icr.etherj.dicom.iod.SpecimenPreparationStep;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.WsmFunctionalGroupsFrame;
import icr.etherj.dicom.iod.WsmReferencedImage;
import java.util.List;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
class Pack
{
	private static final Logger logger = LoggerFactory.getLogger(Pack.class);

	static DicomObject getOrCreateItem(DicomElement sq)
	{
		return getOrCreateItem(sq, 0);
	}

	static DicomObject getOrCreateItem(DicomElement sq, int idx)
	{
		if (sq.countItems() > idx)
		{
			return sq.getDicomObject(idx);
		}
		sq.addDicomObject(idx, new BasicDicomObject());
		return sq.getDicomObject(idx);
	}

	static DicomElement getOrCreateSq(DicomObject dcm, int tag)
	{
		DicomElement sq = dcm.get(tag);
		if (sq == null)
		{
			dcm.putSequence(tag);
			sq = dcm.get(tag);
		}
		return sq;
	}

	static DicomObject getOrCreateSqItem(DicomObject dcm, int tag)
	{
		return getOrCreateSqItem(dcm, tag, 0);
	}

	static DicomObject getOrCreateSqItem(DicomObject dcm, int tag, int idx)
	{
		DicomElement sq = getOrCreateSq(dcm, tag);
		return getOrCreateItem(sq, idx);
	}

	static void packAlgorithmIdentification(AlgorithmIdentification ident,
		DicomElement sq)
	{
		DicomObject dcm = new BasicDicomObject();
		DicomElement familySq = getOrCreateSq(dcm,
			Tag.AlgorithmFamilyCodeSequence);
		packCode(ident.getAlgorithmFamilyCode(), familySq);
		dcm.putString(Tag.AlgorithmName, VR.LO, ident.getAlgorithmName());
		dcm.putString(Tag.AlgorithmVersion, VR.LO, ident.getAlgorithmVersion());
		String params = ident.getAlgorithmParameters();
		if (!StringUtils.isNullOrEmpty(params))
		{
			dcm.putString(Tag.AlgorithmParameters, VR.LT, params);
		}
		String source = ident.getAlgorithmSource();
		if (!StringUtils.isNullOrEmpty(params))
		{
			dcm.putString(Tag.AlgorithmSource, VR.LO, source);
		}

		sq.addDicomObject(dcm);
	}

	static void packAnnotationGroup(AnnotationGroup group, DicomElement sq,
		String coordType)
	{
		DicomObject dcm = new BasicDicomObject();
		dcm.putInt(Tag.AnnotationGroupNumber, VR.US,
			group.getAnnotationGroupNumber());
		dcm.putString(NewTag.AnnotationGroupUID, VR.UI,
			group.getAnnotationGroupUID());
		dcm.putString(NewTag.AnnotationGroupLabel, VR.LO,
			group.getAnnotationGroupLabel());
		String desc = group.getAnnotationGroupDescription();
		if (!StringUtils.isNullOrEmpty(desc))
		{
			dcm.putString(NewTag.AnnotationGroupDescription, VR.UT, desc);
		}

		String genType = group.getAnnotationGroupGenerationType();
		dcm.putString(NewTag.AnnotationGroupGenerationType, VR.CS, genType);
		if (Constants.SemiAutomatic.equals(genType) ||
			 Constants.Automatic.equals(genType))
		{
			DicomElement algoSq = getOrCreateSq(dcm, 
				NewTag.AnnotationGroupAlgorithmIdentificationSequence);
			group.getAlgorithmIdentificationList().forEach(
				(x) -> packAlgorithmIdentification(x, algoSq));
		}

		DicomElement catSq = getOrCreateSq(dcm,
			NewTag.AnnotationGroupPropertyCategoryCodeSequence);
		packCode(group.getAnnotationPropertyCategoryCode(), catSq);
		DicomElement typeSq = getOrCreateSq(dcm,
			NewTag.AnnotationGroupPropertyTypeCodeSequence);
		packCode(group.getAnnotationPropertyTypeCode(), typeSq);

		dcm.putInt(NewTag.NumberOfAnnotations, VR.SL,
			group.getNumberOfAnnotations());
		String gfxType = group.getGraphicType();
		dcm.putString(Tag.GraphicType, VR.CS, gfxType);
		boolean allPaths = group.getAnnotationAppliesToAllOpticalPaths();
		putYesNo(allPaths, dcm, NewTag.AnnotationAppliesToAllOpticalPaths);
		if (!allPaths)
		{
			dcm.putStrings(NewTag.ReferencedOpticalPathIdentifier, VR.SH,
				group.getReferencedOpticalPathIdentifier());
		}
		if (Constants.Coord3D.equals(coordType))
		{
			putYesNo(group.getAnnotationAppliesToAllZPlanes(), dcm,
				NewTag.AnnotationAppliesToAllZPlanes);
			dcm.putDoubles(NewTag.CommonZCoordinateValue, VR.FD,
				group.getCommonZCoordinateValue());
		}
		float[] floatData = group.getPointCoordinatesData();
		if ((floatData != null) && (floatData.length > 0))
		{
			dcm.putFloats(Tag.PointCoordinatesData, VR.OF, floatData);
		}
		else
		{
			double[] doubleData = group.getDoublePointCoordinatesData();
			if ((doubleData != null) && (doubleData.length > 0))
			{
				dcm.putDoubles(NewTag.DoublePointCoordinatesData, VR.FD,
					doubleData);
			}
		}
		if (Constants.Polygon.equals(gfxType) ||
			 Constants.Polyline.equals(gfxType))
		{
			int[] indices = group.getLongPrimitivePointIndexList();
			if ((indices != null) && (indices.length > 0))
			{
				dcm.putInts(NewTag.LongPrimitivePointIndexList, VR.UL, indices);
			}
		}

		packMeasurements(group.getMeasurementList(), dcm);

		sq.addDicomObject(dcm);
	}

	static void packCode(Code code, DicomElement codeSq)
	{
		packCode(code, codeSq, 0);
	}

	static void packCode(Code code, DicomElement codeSq, int modifierTag)
	{
		DicomObject dcm = new BasicDicomObject();
		String value = code.getCodeValue();
		if (value == null)
		{
			value = code.getLongCodeValue();
			if (value != null)
			{
				dcm.putString(NewTag.LongCodeValue, VR.UT, value);
			}
		}
		else
		{
			dcm.putString(Tag.CodeValue, VR.SH, value);
		}
		if (value != null)
		{
			dcm.putString(Tag.CodingSchemeDesignator, VR.SH,
				code.getCodingSchemeDesignator());
		}
		putIfExists(code.getCodingSchemeVersion(), dcm, Tag.CodingSchemeVersion,
			VR.SH);
		dcm.putString(Tag.CodeMeaning, VR.LO, code.getCodeMeaning());
		codeSq.addDicomObject(dcm);
		List<Code> mods = code.getModifiers();
		if ((modifierTag == 0) || mods.isEmpty())
		{
			return;
		}
		dcm.putSequence(modifierTag);
		DicomElement modSq = dcm.get(modifierTag);
		mods.forEach((Code x) -> packCode(x, modSq));
	}

	static void packCodeItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.CodeItem))
		{
			throw new IllegalArgumentException("Content item not a code");
		}
		ContentItem.CodeItem item = (ContentItem.CodeItem) ci;
		Code code = item.getCode();
		if (code != null)
		{
			dcm.putSequence(Tag.ConceptCodeSequence);
			DicomElement codeSq = dcm.get(Tag.ConceptCodeSequence);
			packCode(code, codeSq);
		}
		packContentItemUniversal(ci, dcm);
	}

	static void packContainerComponent(ContainerComponent cc, DicomElement sq)
	{
		DicomObject dcm = new BasicDicomObject();
		DicomElement codeSq = getOrCreateSq(dcm,
			Tag.ContainerComponentTypeCodeSequence);
		packCode(cc.getContainerComponentTypeCode(), codeSq);
		putIfExists(cc.getContainerComponentMaterial(), dcm,
			Tag.ContainerComponentMaterial, VR.CS);
		sq.addDicomObject(dcm);
	}

	static void packContainerContentItem(ContentItem ci, DicomObject dcm)
		throws IllegalArgumentException
	{
		if (!(ci instanceof ContentItem.ContainerItem))
		{
			throw new IllegalArgumentException("Content item not a container");
		}
		ContentItem.ContainerItem container = (ContentItem.ContainerItem) ci;
		dcm.putString(Tag.ContinuityOfContent, VR.CS,
			container.getContinuityOfContent());
		ContentTemplate template = container.getContentTemplate();
		if (template != null)
		{
			dcm.putSequence(Tag.ContentTemplateSequence);
			DicomElement ctSq = dcm.get(Tag.ContentTemplateSequence);
			DicomObject ctDcm = new BasicDicomObject();
			ctSq.addDicomObject(ctDcm);
			packContentTemplate(template, ctDcm);
		}
		packContentItemUniversal(ci, dcm);
	}

	static void packContentItem(ContentItem ci, DicomObject dcm)
	{
		String type = ci.getValueType();
		if (StringUtils.isNullOrEmpty(type))
		{
			throw new IllegalArgumentException(
				"Content item has null or empty value type");
		}
		switch (type)
		{
			case Constants.Container:
				packContainerContentItem(ci, dcm);
				break;
			case Constants.Code:
				packCodeItem(ci, dcm);
				break;
			case Constants.PersonName:
				packPersonNameItem(ci, dcm);
				break;
			case Constants.Text:
				packTextItem(ci, dcm);
				break;
			case Constants.Image:
				packImageItem(ci, dcm);
				break;
			case Constants.UidReference:
				packUidRefItem(ci, dcm);
				break;
			case Constants.Numeric:
				packNumericItem(ci, dcm);
				break;
			case Constants.SpatialCoord:
				packSCoordItem(ci, dcm);
				break;
			case Constants.Date:
			case Constants.Time:
			case Constants.DateTime:
			case Constants.Composite:
			case Constants.Waveform:
			case Constants.SpatialCoord3D:
			case Constants.TemporalCoord:
				packContentItemUniversal(ci, dcm);
				break;
			default:
				throw new IllegalArgumentException("Invalid value type: " + type);
		}
	}

	static void packContentItemUniversal(ContentItem ci, DicomObject dcm)
	{
		dcm.putString(Tag.ValueType, VR.CS, ci.getValueType());
		Code conceptNameCode = ci.getConceptNameCode();
		if (conceptNameCode != null)
		{
			dcm.putSequence(Tag.ConceptNameCodeSequence);
			DicomElement conceptNameSq = dcm.get(Tag.ConceptNameCodeSequence);
			packCode(conceptNameCode, conceptNameSq);
		}
		List<ContentRelationship> crList = ci.getContentRelationshipList();
		if (!crList.isEmpty())
		{
			dcm.putSequence(Tag.ContentSequence);
			DicomElement contentSq = dcm.get(Tag.ContentSequence);
			packContentRelationships(crList, contentSq);
		}
	}

	static void packContentRelationship(ContentRelationship cr,
		DicomElement contentSq)
	{
		DicomObject dcm = new BasicDicomObject();
		dcm.putString(Tag.RelationshipType, VR.CS, cr.getRelationshipType());
		ContentItem item = cr.getRelatedContentItem();
		if (item != null)
		{
			packContentItem(item, dcm);
		}
		else
		{
			int[] refIds = cr.getReferencedContentItemIdentifier();
			if ((refIds != null) && (refIds.length > 0))
			{
				dcm.putInts(Tag.ReferencedContentItemIdentifier, VR.UL, refIds);
			}
		}
		contentSq.addDicomObject(dcm);
	}

	static void packContentRelationships(List<ContentRelationship> crList,
		DicomElement contentSq)
	{
		crList.forEach((x) -> packContentRelationship(x, contentSq));
	}

	static void packContentTemplate(ContentTemplate template, DicomObject dcm)
	{
		dcm.putString(Tag.MappingResource, VR.CS, template.getMappingResource());
		dcm.putString(Tag.TemplateIdentifier, VR.CS,
			template.getTemplateIdentifier());
	}

	static void packContourImage(ContourImage ci, DicomElement ciSq)
	{
		DicomObject ciItem = new BasicDicomObject();
		String sopClassUid = ci.getReferencedSopClassUid();
		ciItem.putString(Tag.ReferencedSOPClassUID, VR.UI, sopClassUid);
		ciItem.putString(Tag.ReferencedSOPInstanceUID, VR.UI,
			ci.getReferencedSopInstanceUid());
		int frame = ci.getReferencedFrameNumber();
		if ((frame > 0) && DicomUtils.isMultiframeImageSopClass(sopClassUid))
		{
			ciItem.putInt(Tag.ReferencedFrameNumber, VR.IS, frame);
		}
		ciSq.addDicomObject(ciItem);
	}

	static void packContourImages(List<ContourImage> ciList, DicomObject conItem)
	{
		conItem.putSequence(Tag.ContourImageSequence);
		DicomElement ciSq = conItem.get(Tag.ContourImageSequence);
		ciList.forEach((x) -> packContourImage(x, ciSq));
	}

	static void packContour(Contour contour, DicomElement cSq)
		throws IllegalArgumentException
	{
		DicomObject conItem = new BasicDicomObject();
		conItem.putInt(Tag.ContourNumber, VR.IS, contour.getContourNumber());
		conItem.putString(Tag.ContourGeometricType, VR.CS,
			contour.getContourGeometricType());
		List<Coordinate3D> coordList = contour.getContourData();
		int nCoords = coordList.size();
		if (nCoords < 1)
		{
			throw new IllegalArgumentException("No contour coordinates found");
		}
		conItem.putInt(Tag.NumberOfContourPoints, VR.IS, nCoords);
		double[] coords = new double[3 * nCoords];
		int idx = 0;
		for (Coordinate3D coord : coordList)
		{
			coords[idx++] = coord.x;
			coords[idx++] = coord.y;
			coords[idx++] = coord.z;
		}
		conItem.putDoubles(Tag.ContourData, VR.DS, coords);
		cSq.addDicomObject(conItem);
		List<ContourImage> ciList = contour.getContourImageList();
		if (!ciList.isEmpty())
		{
			packContourImages(ciList, conItem);
		}
	}

	static void packContours(List<Contour> conList, DicomObject rcItem)
		throws IllegalArgumentException
	{
		rcItem.putSequence(Tag.ContourSequence);
		DicomElement cSq = rcItem.get(Tag.ContourSequence);
		conList.forEach((x) -> packContour(x, cSq));
	}

	static void packDerivationImage(DerivationImage image, DicomElement derivSq)
	{
		DicomObject dcm = new BasicDicomObject();
		List<SourceImage> sources = image.getSourceImageList();
		if (!sources.isEmpty())
		{
			dcm.putSequence(Tag.SourceImageSequence);
			DicomElement sourceImageSq = dcm.get(Tag.SourceImageSequence);
			sources.forEach((SourceImage x) -> packSourceImage(x, sourceImageSq));
		}
		List<Code> codes = image.getDerivationCodeList();
		if (!codes.isEmpty())
		{
			dcm.putSequence(Tag.DerivationCodeSequence);
			DicomElement codeSq = dcm.get(Tag.DerivationCodeSequence);
			codes.forEach((Code x) -> packCode(x, codeSq));
		}
		if (!dcm.isEmpty())
		{
			derivSq.addDicomObject(dcm);
		}
	}

	static void packDimensionIndex(DimensionIndex idx, DicomElement dimIdxSq)
	{
		DicomObject dcm = new BasicDicomObject();
		dcm.putString(Tag.DimensionOrganizationUID, VR.UI,
			idx.getDimensionOrganisationUid());
		dcm.putInt(Tag.DimensionIndexPointer, VR.AT, idx.getIndexPointer());
		int funcPtr = idx.getFunctionalGroupPointer();
		if (funcPtr != 0)
		{
			dcm.putInt(Tag.FunctionalGroupPointer, VR.AT, funcPtr);
		}
		putIfExists(idx.getDescriptionLabel(), dcm, Tag.DimensionDescriptionLabel,
			VR.LO);
		dimIdxSq.addDicomObject(dcm);
	}

	static void packFrameContent(FrameContent fc, DicomObject parentDcm)
	{
		DicomObject dcm = new BasicDicomObject();
		int[] dimIdx = fc.getDimensionIndexValues();
		if (dimIdx.length != 0)
		{
			dcm.putInts(Tag.DimensionIndexValues, VR.UL, dimIdx);
		}
		String stackId = fc.getStackId();
		if ((stackId != null) && !stackId.isEmpty())
		{
			dcm.putString(Tag.StackID, VR.SH, stackId);
			dcm.putInt(Tag.InStackPositionNumber, VR.UL,
				fc.getInStackPositionNumber());
		}
		if (!dcm.isEmpty())
		{
			parentDcm.putSequence(Tag.FrameContentSequence);
			DicomElement sq = parentDcm.get(Tag.FrameContentSequence);
			sq.addDicomObject(dcm);
		}
	}

	static void packFuncGroupsRefImage(FunctionalGroupsReferencedImage image,
		DicomElement sq)
	{
		DicomObject dcm = new BasicDicomObject();
		sq.addDicomObject(dcm);
		dcm.putString(Tag.ReferencedSOPClassUID, VR.UI,
			image.getReferencedSopClassUid());
		dcm.putString(Tag.ReferencedSOPInstanceUID, VR.UI,
			image.getReferencedSopInstanceUid());
		int refFrame = image.getReferencedFrameNumber();
		if (refFrame > 0)
		{
			dcm.putInt(refFrame, VR.IS, refFrame);
		}
		Code code = image.getPurposeOfReferenceCode();
		if (code != null)
		{
			DicomElement codeSq = getOrCreateSq(dcm,
				Tag.PurposeOfReferenceCodeSequence);
			packCode(code, codeSq);
		}
	}

	static void packHeirSeriesRef(HeirarchicalSeriesReference hsr,
		DicomObject dcm)
	{
		dcm.putString(Tag.SeriesInstanceUID, VR.UI, hsr.getSeriesInstanceUid());
		dcm.putSequence(Tag.ReferencedSOPSequence);
		DicomElement refInstSq = dcm.get(Tag.ReferencedSOPSequence);
		hsr.getReferencedInstanceList().forEach(
			(ReferencedInstance x) -> packReferencedInstance(x, refInstSq));
	}

	static void packHeirSopInstRef(HeirarchicalSopInstanceReference hsir,
		DicomObject dcm)
	{
		dcm.putString(Tag.StudyInstanceUID, VR.UI, hsir.getStudyInstanceUid());
		dcm.putSequence(Tag.ReferencedSeriesSequence);
		DicomElement serSq = dcm.get(Tag.ReferencedSeriesSequence);
		for (HeirarchicalSeriesReference ref : hsir.getSeriesReferenceList())
		{
			DicomObject refDcm = new BasicDicomObject();
			packHeirSeriesRef(ref, refDcm);
			serSq.addDicomObject(refDcm);
		}
	}

	static void packImageItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.ImageItem))
		{
			throw new IllegalArgumentException("Content item not an image");
		}
		ContentItem.ImageItem item = (ContentItem.ImageItem) ci;
		ReferencedInstance inst = item.getReferencedInstance();
		if (inst != null)
		{
			dcm.putSequence(Tag.ReferencedSOPSequence);
			DicomElement refInstSq = dcm.get(Tag.ReferencedSOPSequence);
			packReferencedInstance(inst, refInstSq);
		}
		packContentItemUniversal(ci, dcm);
	}

	static void packIssuer(HL7v2HierarchicDesignator issuer, DicomElement sq)
	{
		DicomObject dcm = new BasicDicomObject();
		String localId = issuer.getLocalNamespaceEntityId();
		if (localId != null)
		{
			dcm.putString(Tag.LocalNamespaceEntityID, VR.UT, localId);
		}
		String univId = issuer.getUniversalEntityId();
		if (univId == null)
		{
			dcm.putString(Tag.UniversalEntityID, VR.UT, univId);
			dcm.putString(Tag.UniversalEntityIDType, VR.CS,
				issuer.getUniversalEntityIdType());
		}
		sq.addDicomObject(dcm);
	}

	static void packMeasurement(Measurement m, DicomElement sq)
	{
		DicomObject dcm = new BasicDicomObject();
		packCode(m.getConceptNameCode(),
			getOrCreateSq(dcm, Tag.ConceptNameCodeSequence));
		packCode(m.getMeasurementUnitsCode(),
			getOrCreateSq(dcm, Tag.MeasurementUnitsCodeSequence));
		DicomElement valueSq = getOrCreateSq(dcm,
			NewTag.MeasurementValuesSequence);
		float[] values = m.getFloatingPointValues();
		int[] indices = m.getAnnotationIndexList();
		DicomObject valueDcm = new BasicDicomObject();
		valueDcm.putFloats(NewTag.FloatingPointValues, VR.OF, values);
		if ((indices != null) && (values.length == indices.length))
		{
			valueDcm.putInts(NewTag.AnnotationIndexList, VR.UL, indices);
		}
		valueSq.addDicomObject(valueDcm);
		sq.addDicomObject(dcm);
	}

	static void packMeasurements(List<Measurement> measurementList,
		DicomObject dcm)
	{
		DicomElement sq = getOrCreateSq(dcm, NewTag.MeasurementsSequence);
		measurementList.forEach((x) -> packMeasurement(x, sq));
	}

	static void packNumericItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.NumericItem))
		{
			throw new IllegalArgumentException("Content item not a numeric value");
		}
		ContentItem.NumericItem item = (ContentItem.NumericItem) ci;
		dcm.putSequence(Tag.MeasuredValueSequence);
		String numVal = item.getNumericValue();
		if (!StringUtils.isNullOrEmpty(numVal))
		{
			DicomElement measValSq = dcm.get(Tag.MeasuredValueSequence);
			DicomObject valDcm = new BasicDicomObject();
			valDcm.putString(Tag.NumericValue, VR.DS, numVal);
			double floatValue = item.getFloatingPointValue();
			if (!Double.isNaN(floatValue))
			{
				valDcm.putDouble(NewTag.FloatingPointValue, VR.FD, floatValue);
			}
			int numerator = item.getRationalNumeratorValue();
			int denominator = item.getRationalDenominatorValue();
			if ((numerator != 0) && (denominator != 0))
			{
				valDcm.putInt(NewTag.RationalNumeratorValue, VR.SL, numerator);
				valDcm.putInt(NewTag.RationalDenominatorValue, VR.UL, denominator);
			}
			valDcm.putSequence(Tag.MeasurementUnitsCodeSequence);
			DicomElement unitsSq = valDcm.get(Tag.MeasurementUnitsCodeSequence);
			packCode(item.getMeasurementUnits(), unitsSq);
			measValSq.addDicomObject(valDcm);
		}
		packContentItemUniversal(ci, dcm);
	}

	static void packOpticalPath(OpticalPath path, DicomElement sq)
	{
		DicomObject dcm = new BasicDicomObject();
		dcm.putString(Tag.OpticalPathIdentifier, VR.SH,
			path.getOpticalPathIdentifier());
		String desc = path.getOpticalPathDescription();
		if (desc != null)
		{
			dcm.putString(Tag.OpticalPathDescription, VR.ST, desc);
		}
		byte[] iccProfile = path.getIccProfile();
		if ((iccProfile != null) && (iccProfile.length > 0))
		{
			dcm.putBytes(Tag.ICCProfile, VR.OB, iccProfile);
		}
		DicomElement typeSq = getOrCreateSq(dcm,
			Tag.IlluminationTypeCodeSequence);
		path.getIlluminationTypeCodeList().forEach(
			(Code code) -> packCode(code, typeSq));
		float wavelength = path.getIlluminationWaveLength();
		if (Float.isFinite(wavelength))
		{
			dcm.putFloat(Tag.IlluminationWaveLength, VR.FL, wavelength);
		}
		Code colourCode = path.getIlluminationColourCode();
		if (colourCode != null)
		{
			DicomElement codeSq = getOrCreateSq(dcm,
				Tag.IlluminationColorCodeSequence);
			packCode(colourCode, codeSq);
		}
		Code illuminatorType = path.getIlluminatorTypeCode();
		if (illuminatorType != null)
		{
			DicomElement codeSq = getOrCreateSq(dcm,
				Tag.IlluminatorTypeCodeSequence);
			packCode(illuminatorType, codeSq);
		}
		sq.addDicomObject(dcm);
	}

	static void packPersonNameItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.PersonNameItem))
		{
			throw new IllegalArgumentException("Content item not a person name");
		}
		ContentItem.PersonNameItem item = (ContentItem.PersonNameItem) ci;
		dcm.putString(Tag.PersonName, VR.PN, item.getPersonName());
		packContentItemUniversal(ci, dcm);
	}

	static void packPixelMatrixOrigin(PixelMatrixOrigin origin, DicomElement sq)
	{
		DicomObject dcm = new BasicDicomObject();
		dcm.putDouble(Tag.XOffsetInSlideCoordinateSystem, VR.DS,
			origin.getXOffsetInSlideCoordinateSystem());
		dcm.putDouble(Tag.YOffsetInSlideCoordinateSystem, VR.DS,
			origin.getYOffsetInSlideCoordinateSystem());
		sq.addDicomObject(dcm);
	}

	static void packPixelMeasures(PixelMeasures pixelMeasures,
		DicomObject parentDcm, boolean hasFrameOfRefUid)
	{
		if (pixelMeasures == null)
		{
			return;
		}
		DicomObject dcm = null;
		double[] pixSpacing = pixelMeasures.getPixelSpacing();
		double thickness = pixelMeasures.getSliceThickness();
		double sliceSpacing = pixelMeasures.getSpacingBetweenSlices();
		if (hasFrameOfRefUid)
		{
			// Fields are required
			dcm = new BasicDicomObject();
			dcm.putDoubles(Tag.PixelSpacing, VR.DS, pixSpacing);
			dcm.putDouble(Tag.SliceThickness, VR.DS, thickness);
			if (Double.isFinite(sliceSpacing))
			{
				dcm.putDouble(Tag.SpacingBetweenSlices, VR.DS, sliceSpacing);
			}
		}
		else
		{
			if ((pixSpacing.length == 2) && Double.isFinite(thickness))
			{
				// Fields are optional but have been set.
				dcm = new BasicDicomObject();
				dcm.putDoubles(Tag.PixelSpacing, VR.DS, pixSpacing);
				dcm.putDouble(Tag.SliceThickness, VR.DS, thickness);
				if (Double.isFinite(sliceSpacing))
				{
					dcm.putDouble(Tag.SpacingBetweenSlices, VR.DS, sliceSpacing);
				}
			}
		}
		if (dcm != null)
		{
			parentDcm.putSequence(Tag.PixelMeasuresSequence);
			DicomElement sq = parentDcm.get(Tag.PixelMeasuresSequence);
			sq.addDicomObject(dcm);
		}
	}

	static void packPlaneOrientation(double[] ori, DicomObject parentDcm,
		boolean hasFrameOfRefUid)
	{
		DicomObject dcm = null;
		if (hasFrameOfRefUid)
		{
			// Field is required
			dcm = new BasicDicomObject();
			dcm.putDoubles(Tag.ImageOrientationPatient, VR.DS, ori);
		}
		else
		{
			if (ori.length == 6)
			{
				// Field is optional but have been set.
				dcm = new BasicDicomObject();
				dcm.putDoubles(Tag.ImageOrientationPatient, VR.DS, ori);
			}
		}
		if (dcm != null)
		{
			parentDcm.putSequence(Tag.PlaneOrientationSequence);
			DicomElement sq = parentDcm.get(Tag.PlaneOrientationSequence);
			sq.addDicomObject(dcm);
		}
	}

	static void packPlanePosition(double[] pos, DicomObject parentDcm,
		boolean hasFrameOfRefUid)
	{
		DicomObject dcm = null;
		if (hasFrameOfRefUid)
		{
			// Field is required
			dcm = new BasicDicomObject();
			dcm.putDoubles(Tag.ImagePositionPatient, VR.DS, pos);
		}
		else
		{
			if (pos.length == 3)
			{
				// Field is optional but have been set.
				dcm = new BasicDicomObject();
				dcm.putDoubles(Tag.ImagePositionPatient, VR.DS, pos);
			}
		}
		if (dcm != null)
		{
			parentDcm.putSequence(Tag.PlanePositionSequence);
			DicomElement sq = parentDcm.get(Tag.PlanePositionSequence);
			sq.addDicomObject(dcm);
		}
	}

	static void packPlanePositionSlide(PlanePositionSlide pos,
		DicomObject parentDcm)
	{
		DicomElement sq = getOrCreateSq(parentDcm, Tag.PlanePositionSlideSequence);
		DicomObject dcm = new BasicDicomObject();
		sq.addDicomObject(dcm);
		dcm.putInt(Tag.ColumnPositionInTotalImagePixelMatrix, VR.SL,
			pos.getColumnPositionInTotalImagePixelMatrix());
		dcm.putInt(Tag.RowPositionInTotalImagePixelMatrix, VR.SL,
			pos.getRowPositionInTotalImagePixelMatrix());
		dcm.putDouble(Tag.XOffsetInSlideCoordinateSystem, VR.DS,
			pos.getXOffsetInSlideCoordinateSystem());
		dcm.putDouble(Tag.YOffsetInSlideCoordinateSystem, VR.DS,
			pos.getYOffsetInSlideCoordinateSystem());
		dcm.putDouble(Tag.ZOffsetInSlideCoordinateSystem, VR.DS,
			pos.getZOffsetInSlideCoordinateSystem());
	}

	static void packReferencedFrameOfReference(
		ReferencedFrameOfReference refFoR, DicomElement refFoRSq)
		throws IllegalArgumentException
	{
		DicomObject refFoRItem = new BasicDicomObject();
		refFoRItem.putString(Tag.FrameOfReferenceUID, VR.UI,
			refFoR.getFrameOfReferenceUid());
		refFoRSq.addDicomObject(refFoRItem);
		List<RtReferencedStudy> studyList = refFoR.getRtReferencedStudyList();
		if (!studyList.isEmpty())
		{
			packRtReferencedStudies(studyList, refFoRItem);
		}
	}

	static void packReferencedFramesOfReference(
		List<ReferencedFrameOfReference> refFoRList, DicomObject dcm)
		throws IllegalArgumentException
	{
		dcm.putSequence(Tag.ReferencedFrameOfReferenceSequence);
		DicomElement refFoRSq = dcm.get(Tag.ReferencedFrameOfReferenceSequence);
		refFoRList.forEach((x) -> packReferencedFrameOfReference(x, refFoRSq));
	}

	static void packReferencedImage(ReferencedImage image, DicomElement riSq)
	{
		if (image == null)
		{
			logger.warn("Ignoring null ReferencedImage");
			return;
		}
		DicomObject dcm = new BasicDicomObject();
		String sopClassUid = image.getReferencedSopClassUid();
		dcm.putString(Tag.ReferencedSOPClassUID, VR.UI, sopClassUid);
		dcm.putString(Tag.ReferencedSOPInstanceUID, VR.UI,
			image.getReferencedSopInstanceUid());
		if (DicomUtils.isMultiframeImageSopClass(sopClassUid))
		{
			dcm.putInt(Tag.ReferencedFrameNumber, VR.IS,
				image.getReferencedFrameNumber());
		}
		riSq.addDicomObject(dcm);
	}

	static void packReferencedInstance(ReferencedInstance inst,
		DicomElement riSq)
	{
		if (inst == null)
		{
			logger.warn("Ignoring null ReferencedInstance");
			return;
		}
		DicomObject dcm = new BasicDicomObject();
		String sopClassUid = inst.getReferencedSopClassUid();
		dcm.putString(Tag.ReferencedSOPClassUID, VR.UI, sopClassUid);
		dcm.putString(Tag.ReferencedSOPInstanceUID, VR.UI,
			inst.getReferencedSopInstanceUid());
		riSq.addDicomObject(dcm);
	}

	static void packReferencedSeries(ReferencedSeries refSeries,
		DicomObject rsItem)
	{
		rsItem.putString(Tag.SeriesInstanceUID, VR.UI,
			refSeries.getSeriesInstanceUid());
		rsItem.putSequence(Tag.ReferencedInstanceSequence);
		DicomElement refInstSq = rsItem.get(Tag.ReferencedInstanceSequence);
		refSeries.getReferencedInstanceList().forEach(
			(ReferencedInstance x) -> packReferencedInstance(x, refInstSq));
	}

	static void packRtReferencedSeries(RtReferencedSeries series,
		DicomElement rtRefSeriesSq)
	{
		DicomObject seriesItem = new BasicDicomObject();
		seriesItem.putString(Tag.SeriesInstanceUID, VR.UI,
			series.getSeriesInstanceUid());
		rtRefSeriesSq.addDicomObject(seriesItem);
		List<ContourImage> ciList = series.getContourImageList();
		if (ciList.isEmpty())
		{
			throw new IllegalArgumentException("No referenced images found");
		}
		packContourImages(ciList, seriesItem);
	}

	static void packRtReferencedSeries(List<RtReferencedSeries> seriesList,
		DicomObject studyItem)
	{
		studyItem.putSequence(Tag.RTReferencedSeriesSequence);
		DicomElement rtRefSeriesSq = studyItem.get(
			Tag.RTReferencedSeriesSequence);
		seriesList.forEach((x) -> packRtReferencedSeries(x, rtRefSeriesSq));
	}

	static void packRtReferencedStudies(List<RtReferencedStudy> studyList,
		DicomObject refFoRItem) throws IllegalArgumentException
	{
		refFoRItem.putSequence(Tag.RTReferencedStudySequence);
		DicomElement rtRefStudySq = refFoRItem.get(Tag.RTReferencedStudySequence);
		studyList.forEach((x) -> packRtReferencedStudy(x, rtRefStudySq));
	}

	static void packRtReferencedStudy(RtReferencedStudy study,
		DicomElement rtRefStudySq) throws IllegalArgumentException
	{
		DicomObject studyItem = new BasicDicomObject();
		studyItem.putString(Tag.ReferencedSOPClassUID, VR.UI,
			study.getReferencedSopClassUid());
		studyItem.putString(Tag.ReferencedSOPInstanceUID, VR.UI,
			study.getReferencedSopInstanceUid());
		rtRefStudySq.addDicomObject(studyItem);
		List<RtReferencedSeries> seriesList = study.getRtReferencedSeriesList();
		if (seriesList.isEmpty())
		{
			throw new IllegalArgumentException("No referenced series found");
		}
		packRtReferencedSeries(seriesList, studyItem);
}

	static void packSCoordItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.SpatialCoordItem))
		{
			throw new IllegalArgumentException(
				"Content item not a spatial coordinate");
		}
		ContentItem.SpatialCoordItem item = (ContentItem.SpatialCoordItem) ci;
		String gfxType = item.getGraphicType();
		if (!StringUtils.isNullOrEmpty(gfxType))
		{
			dcm.putString(Tag.GraphicType, VR.CS, gfxType);
			switch (gfxType)
			{
				case Constants.Point:
				case Constants.MultiPoint:
				case Constants.Polyline:
				case Constants.Circle:
				case Constants.Ellipse:
					List<Coordinate2D> coordList = item.getGraphicData();
					int nCoords = coordList.size();
					float[] gfxData = new float[2 * nCoords];
					for (int i = 0; i < nCoords; i++)
					{
						Coordinate2D coord = coordList.get(i);
						gfxData[2 * i] = (float) coord.column;
						gfxData[2 * i + 1] = (float) coord.row;
					}
					dcm.putFloats(Tag.GraphicData, VR.FL, gfxData);
					break;
				default:
			}
		}
		packContentItemUniversal(ci, dcm);
	}

	static void packSegment(Segment segment, DicomElement segSq)
	{
		DicomObject dcm = new BasicDicomObject();
		dcm.putInt(Tag.SegmentNumber, VR.US, segment.getSegmentNumber());
		dcm.putString(Tag.SegmentLabel, VR.LO, segment.getSegmentLabel());
		putIfExists(segment.getSegmentDescription(), dcm, Tag.SegmentDescription,
			VR.ST);
		dcm.putString(Tag.SegmentAlgorithmType, VR.CS,
			segment.getSegmentAlgorithmType());
		if (!Constants.Manual.equals(segment.getSegmentAlgorithmType()))
		{
			dcm.putString(Tag.SegmentAlgorithmName, VR.LO,
				segment.getSegmentAlgorithmName());
		}
		int[] cieValue = segment.getRecommendedDisplayCIELabValue();
		if ((cieValue != null) && (cieValue.length == 3))
		{
			dcm.putInts(Tag.RecommendedDisplayCIELabValue, VR.US, cieValue);
		}
		List<Code> anatomicRegCodes = segment.getAnatomicRegionCodeList();
		if (!anatomicRegCodes.isEmpty())
		{
			dcm.putSequence(Tag.AnatomicRegionSequence);
			DicomElement anatomicRegSq = dcm.get(Tag.AnatomicRegionSequence);
			anatomicRegCodes.forEach(
				(Code x) -> packCode(x, anatomicRegSq, Tag.AnatomicRegionModifierSequence));
		}
		dcm.putSequence(Tag.SegmentedPropertyCategoryCodeSequence);
		DicomElement catSq = dcm.get(Tag.SegmentedPropertyCategoryCodeSequence);
		packCode(segment.getSegmentedPropertyCategoryCode(), catSq);
		dcm.putSequence(Tag.SegmentedPropertyTypeCodeSequence);
		DicomElement typeSq = dcm.get(Tag.SegmentedPropertyTypeCodeSequence);
		packCode(segment.getSegmentedPropertyTypeCode(), typeSq);
		segSq.addDicomObject(dcm);
	}

	static void packSegmentIdentification(int number, DicomObject parentDcm)
	{
		parentDcm.putSequence(Tag.SegmentIdentificationSequence);
		DicomElement sq = parentDcm.get(Tag.SegmentIdentificationSequence);
		DicomObject dcm = new BasicDicomObject();
		dcm.putInt(Tag.ReferencedSegmentNumber, VR.US, number);
		sq.addDicomObject(dcm);
	}

	static void packSegmentationFrame(SegmentationFunctionalGroupsFrame frame,
		DicomElement perFrameSq, boolean hasFrameOfRefUid)
	{
		DicomObject dcm = new BasicDicomObject();
		packFrameContent(frame.getFrameContent(), dcm);
		List<DerivationImage> derivImages = frame.getDerivationImages();
		if (!derivImages.isEmpty())
		{
			dcm.putSequence(Tag.DerivationImageSequence);
			DicomElement derivSq = dcm.get(Tag.DerivationImageSequence);
			derivImages.forEach(
				(DerivationImage x) -> packDerivationImage(x, derivSq));
		}
		packPlanePosition(frame.getImagePositionPatient(), dcm, hasFrameOfRefUid);
		packSegmentIdentification(frame.getReferencedSegmentNumber(), dcm);
		perFrameSq.addDicomObject(dcm);
	}

	static void packSegmentationStorage(
		SegmentationMultiframeFunctionalGroupsModule module, DicomObject dcm)
		throws IllegalArgumentException
	{
		boolean hasFrameOfRefUid = module.getFrameOfReferenceUid() == null;
		dcm.putSequence(Tag.SharedFunctionalGroupsSequence);
		DicomElement sharedSq = dcm.get(Tag.SharedFunctionalGroupsSequence);
		DicomObject sharedDcm = new BasicDicomObject();
		sharedSq.addDicomObject(sharedDcm);
		SegmentationSharedFunctionalGroups sharedGroups =
			module.getSharedFunctionalGroups();
		PixelMeasures pixMeasures = sharedGroups.getPixelMeasures();
		packPixelMeasures(pixMeasures, sharedDcm, hasFrameOfRefUid);
		packPlaneOrientation(sharedGroups.getImageOrientationPatient(), sharedDcm,
			hasFrameOfRefUid);
		dcm.putSequence(Tag.PerFrameFunctionalGroupsSequence);
		DicomElement perFrameSq = dcm.get(Tag.PerFrameFunctionalGroupsSequence);
		SegmentationPerFrameFunctionalGroups perFrameGroups =
			module.getPerFrameFunctionalGroups();
		for (FunctionalGroupsFrame frame : perFrameGroups.getFrameList())
		{
			packSegmentationFrame((SegmentationFunctionalGroupsFrame) frame,
				perFrameSq, hasFrameOfRefUid);
		}
	}

	static void packSourceImage(SourceImage image, DicomElement imageSq)
	{
		DicomObject dcm = new BasicDicomObject();
		String sopClassUid = image.getReferencedSopClassUid();
		dcm.putString(Tag.ReferencedSOPClassUID, VR.UI, sopClassUid);
		dcm.putString(Tag.ReferencedSOPInstanceUID, VR.UI,
			image.getReferencedSopInstanceUid());
		int number = image.getReferencedFrameNumber();
		if (DicomUtils.isMultiframeImageSopClass(sopClassUid))
		{
			dcm.putInt(Tag.ReferencedFrameNumber, VR.IS, number);
		}
		putIfExists(image.getSpatialLocationsPreserved(), dcm,
			Tag.SpatialLocationsPreserved, VR.CS);
		Code code = image.getPurposeOfReferenceCode();
		if (code != null)
		{
			dcm.putSequence(Tag.PurposeOfReferenceCodeSequence);
			packCode(code, dcm.get(Tag.PurposeOfReferenceCodeSequence));
		}
		if (!dcm.isEmpty())
		{
			imageSq.addDicomObject(dcm);
		}
	}

	static void packSpecimenDescription(SpecimenDescription desc,
		DicomElement sq)
	{
		DicomObject dcm = new BasicDicomObject();
		sq.addDicomObject(dcm);
		dcm.putString(Tag.SpecimenIdentifier, VR.LO, desc.getIdentifier());
		dcm.putString(Tag.SpecimenUID, VR.UI, desc.getSpecimenUid());
		DicomElement issuerSq = getOrCreateSq(dcm,
			Tag.IssuerOfTheSpecimenIdentifierSequence);
		desc.getIssuerList().forEach(
			(HL7v2HierarchicDesignator issuer) -> packIssuer(issuer, issuerSq));
		DicomObject specPrepDcm = getOrCreateSqItem(dcm,
			Tag.SpecimenPreparationSequence);
		DicomElement prepItemSq = getOrCreateSq(specPrepDcm,
			Tag.SpecimenPreparationStepContentItemSequence);
		desc.getSpecimenPreparationStepList().forEach(
			(SpecimenPreparationStep step) -> packSpecimenPreparationStep(step, prepItemSq));
	}

	static void packSpecimenPreparationStep(SpecimenPreparationStep step,
		DicomElement sq)
	{
		DicomObject dcm = new BasicDicomObject();
		sq.addDicomObject(dcm);
		List<? extends ContentItem> items =
			step.getSpecimenPreparationStepContentItemList();
		for (int i = 0; i < items.size(); i++)
		{
			ContentItem item = items.get(i);
			DicomObject itemDcm = getOrCreateItem(sq, i);
			packContentItem(item, itemDcm);
		}
	}

	static void packStructureSetRois(List<StructureSetRoi> ssRoiList,
		DicomObject dcm)
	{
		dcm.putSequence(Tag.StructureSetROISequence);
		DicomElement ssRoiSq = dcm.get(Tag.StructureSetROISequence);
		for (StructureSetRoi ssRoi : ssRoiList)
		{
			DicomObject roiItem = new BasicDicomObject();
			ssRoiSq.addDicomObject(roiItem);
			roiItem.putInt(Tag.ROINumber, VR.IS, ssRoi.getRoiNumber());
			roiItem.putString(Tag.ReferencedFrameOfReferenceUID, VR.UI,
				ssRoi.getReferencedFrameOfReferenceUid());
			roiItem.putString(Tag.ROIName, VR.LO, ssRoi.getRoiName());
			String desc = ssRoi.getRoiDescription();
			if ((desc != null) && !desc.isEmpty())
			{
				roiItem.putString(Tag.ROIDescription, VR.ST, desc);
			}
			roiItem.putString(Tag.ROIGenerationAlgorithm, VR.CS,
				ssRoi.getRoiGenerationAlgorithm());
		}
	}

	static void packTextItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.TextItem))
		{
			throw new IllegalArgumentException("Content item not text");
		}
		ContentItem.TextItem item = (ContentItem.TextItem) ci;
		dcm.putString(Tag.TextValue, VR.UT, item.getText());
		packContentItemUniversal(ci, dcm);
	}

	static void packUidRefItem(ContentItem ci, DicomObject dcm)
	{
		if (!(ci instanceof ContentItem.UidRefItem))
		{
			throw new IllegalArgumentException("Content item not a UID reference");
		}
		ContentItem.UidRefItem item = (ContentItem.UidRefItem) ci;
		dcm.putString(Tag.UID, VR.UI, item.getUidReference());
		packContentItemUniversal(ci, dcm);
	}

	static void packWsmFrame(WsmFunctionalGroupsFrame frame,
		DicomElement perFrameSq)
	{
		DicomObject dcm = new BasicDicomObject();
		packFrameContent(frame.getFrameContent(), dcm);
		List<DerivationImage> derivImages = frame.getDerivationImages();
		if (!derivImages.isEmpty())
		{
			dcm.putSequence(Tag.DerivationImageSequence);
			DicomElement derivSq = dcm.get(Tag.DerivationImageSequence);
			derivImages.forEach(
				(DerivationImage x) -> packDerivationImage(x, derivSq));
		}
		PlanePositionSlide pos = frame.getPlanePositionSlide();
		if (pos != null)
		{
			packPlanePositionSlide(pos, dcm);
		}
		perFrameSq.addDicomObject(dcm);
	}

	static void packWsmReferencedImage(WsmReferencedImage image, DicomElement sq)
	{
		DicomObject dcm = new BasicDicomObject();
		dcm.putString(Tag.ReferencedSOPClassUID, VR.UI,
			image.getReferencedSopClassUid());
		dcm.putString(Tag.ReferencedSOPInstanceUID, VR.UI,
			image.getReferencedSopInstanceUid());
		dcm.putInt(Tag.ReferencedFrameNumber, VR.IS,
			image.getReferencedFrameNumber());
		dcm.putInts(Tag.TopLeftHandCornerOfLocalizerArea, VR.US,
			image.getTopLeftHandCornerOfLocaliserArea());
		dcm.putInts(Tag.BottomRightHandCornerOfLocalizerArea, VR.US,
			image.getBottomRightHandCornerOfLocaliserArea());
		dcm.putDoubles(Tag.PixelSpacing, VR.DS, image.getPixelSpacing());
		dcm.putDouble(Tag.ZOffsetInSlideCoordinateSystem, VR.DS,
			image.getZOffsetInSlideCoordinateSystem());
		dcm.putInt(Tag.SamplesPerPixel, VR.US, image.getSamplesPerPixel());
		dcm.putString(Tag.OpticalPathIdentifier, VR.SH,
			image.getOpticalPathIdentifier());
		sq.addDicomObject(dcm);
	}

	static void putIfExists(String value, DicomObject dcm, int tag, VR vr)
	{
		if ((value != null) && !value.isEmpty())
		{
			dcm.putString(tag, vr, value);
		}
	}

	static void putIfExists(String[] value, DicomObject dcm, int tag, VR vr)
	{
		if ((value != null) && (value.length > 0))
		{
			dcm.putStrings(tag, vr, value);
		}
	}

	static void putIfFinite(double value, DicomObject dcm, int tag, VR vr)
	{
		if (Double.isFinite(value))
		{
			dcm.putDouble(tag, vr, value);
		}
	}

	static void putIfFinite(double[] array, DicomObject dcm, int tag, VR vr)
	{
		if ((array == null) || (array.length == 0))
		{
			return;
		}
		boolean finite = true;
		for (double value : array)
		{
			if (!Double.isFinite(value))
			{
				finite = false;
				break;
			}
		}
		if (finite)
		{
			dcm.putDoubles(tag, vr, array);
		}
	}

	static void putYesNo(boolean flag, DicomObject dcm, int tag)
	{
		dcm.putString(tag, VR.CS, flag ? Constants.Yes : Constants.No);
	}

	private Pack()
	{}
}
