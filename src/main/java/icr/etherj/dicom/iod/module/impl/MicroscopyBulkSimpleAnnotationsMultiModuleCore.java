/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.Uids;

/**
 *
 * @author jamesd
 */
public class MicroscopyBulkSimpleAnnotationsMultiModuleCore
{
	private int instNum = 0;
	private String sopInstUid = Uids.generateDicomUid();
	private final boolean strict;

	public MicroscopyBulkSimpleAnnotationsMultiModuleCore()
	{
		this(true);
	}

	public MicroscopyBulkSimpleAnnotationsMultiModuleCore(boolean strict)
	{
		this.strict = strict;
	}

	/**
	 * Returns instance number, type 1: SopCommonModule,
	 * MicroscopyBulkSimpleAnnotationsModule.
	 * @return 
	 */
	public int getInstanceNumber()
	{
		return instNum;
	}

	/**
	 * Returns SOP class UID, type 1: SopCommonModule
	 * @return 
	 */
	public String getSopClassUid()
	{
		return "1.2.840.10008.5.1.4.1.1.91.1";
	}

	/**
	 * Returns SOP instance UID, type 1: SopCommonModule
	 * @return 
	 */
	public String getSopInstanceUid()
	{
		return sopInstUid;
	}

	public boolean isStrict()
	{
		return strict;
	}

	/**
	 * Sets instance number, type 1: SopCommonModule,
	 * MicroscopyBulkSimpleAnnotationsModule.
	 * @param number 
	 */
	public void setInstanceNumber(int number) throws IllegalArgumentException
	{
		instNum = number;
	}

	/**
	 * Sets SOP instance UID, type 1: SopCommonModule
	 * @param uid
	 */
	public void setSopInstanceUid(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException(
				"SopInstanceUid must not be null or empty");
		}
		sopInstUid = uid;
	}

}
