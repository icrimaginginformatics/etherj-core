/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.StringUtils;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.Segment;
import icr.etherj.dicom.iod.module.SegmentationImageModule;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author jamesd
 */
public class DefaultSegmentationImageModule extends DefaultGeneralImageModule
	implements SegmentationImageModule
{
	private static final String[] imageTypeDef =
		new String[] {"DERIVED", "PRIMARY"};

	private String contentDesc = "";
	private String contentLabel = "ContentLabel";
	private String creatorName = "";
	private final String[] imageType =
		Arrays.copyOf(imageTypeDef, imageTypeDef.length);
	private int maxFrac = 255;
	private String segFracType = Constants.Probability;
	private final SegmentationMultiModuleCore segMultiModCore;
	private final List<Segment> segments = new ArrayList<>();
	private String segType = Constants.Fractional;

	public DefaultSegmentationImageModule(
		SegmentationMultiModuleCore segMultiModCore)
	{
		this(segMultiModCore, true);
	}

	public DefaultSegmentationImageModule(
		SegmentationMultiModuleCore segMultiModCore, boolean strict)
	{
		super(true);
		if (segMultiModCore == null)
		{
			throw new IllegalArgumentException(
				"Segmentation multimodule core must not be null");
		}
		this.segMultiModCore = segMultiModCore;
	}

	@Override
	public boolean addSegment(Segment segment)
	{
		return (segment != null) ? segments.add(segment) : false;
	}
	
	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ImageType: DERIVED\\PRIMARY");
		ps.println(pad+"InstanceNumber: "+getInstanceNumber());
		String[] patOrient = getPatientOrientation();
		if (patOrient.length == 2)
		{
			ps.println(pad+"PatientOrient: "+patOrient[0]+"\\"+patOrient[1]);
		}
		String contentDate = getContentDate();
		if (!StringUtils.isNullOrEmpty(contentDate))
		{
			ps.println(pad+"ContentDate: "+contentDate);
		}
		String contentTime = getContentTime();
		if (!StringUtils.isNullOrEmpty(contentTime))
		{
			ps.println(pad+"ContentTime: "+contentTime);
		}
		ps.println(pad+"ContentLabel: "+contentLabel);
		if (!contentDesc.isEmpty())
		{
			ps.println(pad+"ContentDescription: "+contentDesc);
		}
		if (!creatorName.isEmpty())
		{
			ps.println(pad+"ContentCreatorsName: "+creatorName);
		}
		String acqDate = getAcquisitionDate();
		if (!StringUtils.isNullOrEmpty(acqDate))
		{
			ps.println(pad+"AcquisitionDate: "+acqDate);
		}
		String acqTime = getAcquisitionTime();
		if (!StringUtils.isNullOrEmpty(acqTime))
		{
			ps.println(pad+"AcquisitionTime: "+acqTime);
		}
		String comments = getImageComments();
		if (!StringUtils.isNullOrEmpty(comments))
		{
			ps.println(pad+"ImageComments: "+comments);
		}
		String burnedInAnno = getBurnedInAnnotation();
			if (!StringUtils.isNullOrEmpty(burnedInAnno))
		{
			ps.println(pad+"BurnedInAnnotation: "+burnedInAnno);
		}
		ps.println(pad+"SamplesPerPixel: 1");
		ps.println(pad+"PhotometricInterpretation: "+Constants.Monochrome2);
		ps.println(pad+"PixelRepresentation: 0");
		ps.println(pad+"BitsAllocated: "+segMultiModCore.getBitsAllocated());
		ps.println(pad+"BitsStored: "+segMultiModCore.getBitsStored());
		ps.println(pad+"HighBit: "+segMultiModCore.getHighBit());
		ps.println(pad+"LossyImageCompression: "+getLossyImageCompression());
		ps.println(pad+"SegmentationType: "+segType);
		if (segType.equals(Constants.Fractional))
		{
			ps.println(pad+"SegmentationFractionalType: "+segFracType);
			ps.println(pad+"MaximumFractionalValue: "+maxFrac);
		}
		int nSegments = segments.size();
		ps.println(pad+"SegmentList: "+nSegments+
			" Segment"+((nSegments != 1) ? "s" : ""));
		for (Segment segment : segments)
		{
			segment.display(ps, indent+"  ");
		}
	}

	@Override
	public int getBitsAllocated()
	{
		return segMultiModCore.getBitsAllocated();
	}

	@Override
	public int getBitsStored()
	{
		return segMultiModCore.getBitsStored();
	}

	@Override
	public String getContentCreatorsName()
	{
		return creatorName;
	}

	@Override
	public String getContentDate()
	{
		return segMultiModCore.getContentDate();
	}

	@Override
	public String getContentDescription()
	{
		return contentDesc;
	}

	@Override
	public String getContentLabel()
	{
		return contentLabel;
	}

	@Override
	public String getContentTime()
	{
		return segMultiModCore.getContentTime();
	}

	@Override
	public int getHighBit()
	{
		return segMultiModCore.getHighBit();
	}

	@Override
	public String[] getImageType()
	{
		return Arrays.copyOf(imageType, imageType.length);
	}

	@Override
	public int getInstanceNumber()
	{
		return segMultiModCore.getInstanceNumber();
	}

	@Override
	public int getMaximumFractionalValue()
	{
		return segType.equals(Constants.Fractional) ? maxFrac : -1;
	}

	@Override
	public String getPhotometricInterpretation()
	{
		return segMultiModCore.getPhotometricInterpretation();
	}

	@Override
	public int getPixelRepresentation()
	{
		return segMultiModCore.getPixelRepresentation();
	}

	@Override
	public int getSamplesPerPixel()
	{
		return segMultiModCore.getSamplesPerPixel();
	}

	@Override
	public List<Segment> getSegmentList()
	{
		return ImmutableList.copyOf(segments);
	}

	@Override
	public String getSegmentationFractionalType()
	{
		return segType.equals(Constants.Fractional) ? segFracType : null;
	}

	@Override
	public String getSegmentationType()
	{
		return segType;
	}

	@Override
	public boolean removeSegment(Segment segment)
	{
		return segments.remove(segment);
	}
	
	@Override
	public void setBitsAllocated(int bits) throws IllegalArgumentException
	{
		segMultiModCore.setBitsAllocated(bits);
	}

	@Override
	public void setBitsStored(int bits) throws IllegalArgumentException
	{
		segMultiModCore.setBitsStored(bits);
	}

	@Override
	public void setContentCreatorsName(String name)
	{
		creatorName = (name != null) ? name : "";
	}

	@Override
	public void setContentDate(String date) throws IllegalArgumentException
	{
		segMultiModCore.setContentDate(date);
	}

	@Override
	public void setContentDescription(String description)
	{
		contentDesc = (description != null) ? description : "";
	}

	@Override
	public void setContentLabel(String label) throws IllegalArgumentException
	{
		String checked = IodUtils.checkCondition(isStrict(), label,
			(x) -> !StringUtils.isNullOrEmpty(x),
			"ContentLabel must not be null or empty");
		contentLabel = checked;
	}

	@Override
	public void setContentTime(String time) throws IllegalArgumentException
	{
		segMultiModCore.setContentTime(time);
	}

	@Override
	public void setHighBit(int bit)
	{
		segMultiModCore.setHighBit(bit);
	}

	@Override
	public void setInstanceNumber(int number) throws IllegalArgumentException
	{
		segMultiModCore.setInstanceNumber(number);
	}

	@Override
	public void setMaximumFractionalValue(int value)
		throws IllegalArgumentException
	{
		int checked = IodUtils.checkCondition(isStrict(), value,
			(int x) -> ((x >= 0) && (x < 256)),
			"Invalid maximum fractional value: "+value);
		maxFrac = checked;
	}

	@Override
	public void setSegmentationFractionalType(String type)
		throws IllegalArgumentException
	{
		String checked = IodUtils.checkCondition(isStrict(), type,
			(x) -> (Constants.Probability.equals(x) || Constants.Occupancy.equals(x)),
			"Segmentation fractional type must be PROBABILITY or OCCUPANCY");
		segFracType = checked;
	}

	@Override
	public void setSegmentationType(String type) throws IllegalArgumentException
	{
		String checked = IodUtils.checkCondition(isStrict(), type,
			(x) -> (Constants.Binary.equals(x) || Constants.Fractional.equals(x)),
			"Segmentation type must be BINARY or FRACTIONAL");
		segType = checked;
	}

}