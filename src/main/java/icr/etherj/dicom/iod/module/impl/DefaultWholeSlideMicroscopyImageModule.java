/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.PixelMatrixOrigin;
import icr.etherj.dicom.iod.impl.DefaultPixelMatrixOrigin;
import icr.etherj.dicom.iod.module.WholeSlideMicroscopyImageModule;
import java.io.PrintStream;
import java.util.Arrays;
import org.dcm4che2.data.VR;

/**
 *
 * @author jamesd
 */
public class DefaultWholeSlideMicroscopyImageModule
	extends AbstractDisplayable implements WholeSlideMicroscopyImageModule
{
	private String acqDateTime = "00000000000000.00";
	private double acqDuration = Double.NaN;
	private final WholeSlideMicroscopyMultiModuleCore core;
	private boolean extDepthOfField = false;
	private int focalPlanes = Integer.MIN_VALUE;
	private int frames = Integer.MIN_VALUE;
	private String focusMethod = Constants.Manual;
	private final double[] imageOriSlide = {Double.NaN, Double.NaN, Double.NaN,
		Double.NaN, Double.NaN, Double.NaN};
	private float imagedVolDepth = Float.NaN;
	private float imagedVolHeight = Float.NaN;
	private float imagedVolWidth = Float.NaN;
	private float interPlaneDist = Float.NaN;
	private boolean labelInImage = false;
	private String[] lossyMethod = new String[] {};
	private double[] lossyRatio = new double[] {};
	private PixelMatrixOrigin origin = new DefaultPixelMatrixOrigin();
	private String presLutShape = null;
	private double rescaleIntercept = Double.NaN;
	private double rescaleSlope = Double.NaN;
	private final boolean strict;
	private int totalMatrixCols = Integer.MIN_VALUE;
	private int totalMatrixFocalPlanes = Integer.MIN_VALUE;
	private int totalMatrixRows = Integer.MIN_VALUE;

	public DefaultWholeSlideMicroscopyImageModule(
		WholeSlideMicroscopyMultiModuleCore wsmMultiModCore)
	{
		this(wsmMultiModCore, true);
	}

	public DefaultWholeSlideMicroscopyImageModule(
		WholeSlideMicroscopyMultiModuleCore core, boolean strict)
	{
		if (core == null)
		{
			throw new IllegalArgumentException(
				"Whole slide microscopy multimodule core must not be null");
		}
		this.core = core;
		this.strict = strict;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"ImageType: "+String.join("\\", core.getImageType()));
		ps.println(pad+"ImagedVolumeWidth: "+imagedVolWidth);
		ps.println(pad+"ImagedVolumeHeight: "+imagedVolHeight);
		ps.println(pad+"ImagedVolumeDepth: "+imagedVolDepth);
		ps.println(pad+"TotalPixelMatrixColumns: "+totalMatrixCols);
		ps.println(pad+"TotalPixelMatrixRows: "+totalMatrixRows);
		ps.println(pad+"TotalPixelMatrixOrigin:");
		origin.display(ps, indent+"  ");
		ps.println(pad+"ImageOrientationSlide: "
			+imageOriSlide[0]+"\\"+imageOriSlide[1]+"\\"+imageOriSlide[2]+"\\"
			+imageOriSlide[3]+"\\"+imageOriSlide[4]+"\\"+imageOriSlide[5]);
		int nSamples = getSamplesPerPixel();
		ps.println(pad+"SamplesPerPixel: "+nSamples);
		ps.println(pad+"PhotometricInterpretation: "+
			getPhotometricInterpretation());
		if (nSamples > 1)
		{
			ps.println(pad+"PlanarConfiguration: "+getPlanarConfiguration());
		}
		ps.println(pad+"NumberOfFrames: "+frames);
		ps.println(pad+"BitsAllocated: "+core.getBitsAllocated());
		ps.println(pad+"BitsStored: "+core.getBitsStored());
		ps.println(pad+"HighBit: "+core.getHighBit());
		ps.println(pad+"PixelRepresentation: "+core.getPixelRepresentation());
		ps.println(pad+"AcquisitionDateTime: "+acqDateTime);
		if (Double.isFinite(acqDuration))
		{
			ps.println(pad+"AcquisitionDuration: "+acqDuration);
		}
		ps.println(pad+"LossyImageCompression: "+core.getLossyImageCompression());
		if (core.getLossyImageCompression())
		{
			String[] ratio = new String[lossyRatio.length];
			for (int i=0; i<lossyRatio.length; i++)
			{
				ratio[i] = String.valueOf(lossyRatio[i]);
			}
			ps.println(pad+"LossyImageCompressionRatio: "+
				String.join("\\", ratio));
			ps.println(pad+"LossyImageCompressionMethod: "+
				String.join("\\", lossyMethod));
		}
		if (Constants.Monochrome2.equals(core.getPixelRepresentation()))
		{
			ps.println(pad+"PresentationLutShape: "+presLutShape);
			ps.println(pad+"RescaleIntercept: "+rescaleIntercept);
			ps.println(pad+"RescaleSlope: "+rescaleSlope);
		}
		ps.println(pad+"VolumetricProperties: "+getVolumetricProperties());
		ps.println(pad+"SpecimenLabelInImage: "+labelInImage);
		ps.println(pad+"BurnedInAnnotation: "+core.getBurnedInAnnotation());
		ps.println(pad+"FocusMethod: "+focusMethod);
		ps.println(pad+"ExtendedDepthOfField: "+extDepthOfField);
		if (extDepthOfField)
		{
			ps.println(pad+"DistanceBetweenFocalPlanes: "+interPlaneDist);
		}
	}

	@Override
	public String getAcquisitionDateTime()
	{
		return acqDateTime;
	}

	@Override
	public double getAcquisitionDuration()
	{
		return acqDuration;
	}

	@Override
	public int getBitsAllocated()
	{
		return core.getBitsAllocated();
	}

	@Override
	public int getBitsStored()
	{
		return core.getBitsStored();
	}

	@Override
	public String getBurnedInAnnotation()
	{
		return core.getBurnedInAnnotation();
	}

	@Override
	public float getDistanceBetweenFocalPlanes()
	{
		return interPlaneDist;
	}

	@Override
	public boolean getExtendedDepthOfField()
	{
		return extDepthOfField;
	}

	@Override
	public String getFocusMethod()
	{
		return focusMethod;
	}

	@Override
	public int getHighBit()
	{
		return core.getHighBit();
	}

	@Override
	public double[] getImageOrientationSlide()
	{
		return Arrays.copyOf(imageOriSlide, imageOriSlide.length);
	}

	@Override
	public String[] getImageType()
	{
		return core.getImageType();
	}

	@Override
	public float getImagedVolumeDepth()
	{
		return imagedVolDepth;
	}

	@Override
	public float getImagedVolumeHeight()
	{
		return imagedVolHeight;
	}

	@Override
	public float getImagedVolumeWidth()
	{
		return imagedVolWidth;
	}

	@Override
	public boolean getLossyImageCompression()
	{
		return core.getLossyImageCompression();
	}

	@Override
	public String[] getLossyImageCompressionMethod()
	{
		return Arrays.copyOf(lossyMethod, lossyMethod.length);
	}

	@Override
	public double[] getLossyImageCompressionRatio()
	{
		return Arrays.copyOf(lossyRatio, lossyRatio.length);
	}

	@Override
	public int getNumberOfFocalPlanes()
	{
		return focalPlanes;
	}

	@Override
	public int getNumberOfFrames()
	{
		return frames;
	}

	@Override
	public String getPhotometricInterpretation()
	{
		return core.getPhotometricInterpretation();
	}

	@Override
	public int getPixelRepresentation()
	{
		return core.getPixelRepresentation();
	}

	@Override
	public int getPlanarConfiguration()
	{
		return core.getPlanarConfiguration();
	}

	@Override
	public String getPresentationLutShape()
	{
		return presLutShape;
	}

	@Override
	public double getRescaleIntercept()
	{
		return rescaleIntercept;
	}

	@Override
	public double getRescaleSlope()
	{
		return rescaleSlope;
	}

	@Override
	public int getSamplesPerPixel()
	{
		return core.getSamplesPerPixel();
	}

	@Override
	public boolean getSpecimenLabelInImage()
	{
		return labelInImage;
	}

	@Override
	public int getTotalPixelMatrixColumns()
	{
		return totalMatrixCols;
	}

	@Override
	public int getTotalPixelMatrixFocalPlanes()
	{
		return totalMatrixFocalPlanes;
	}

	@Override
	public PixelMatrixOrigin getTotalPixelMatrixOrigin()
	{
		return origin;
	}

	@Override
	public int getTotalPixelMatrixRows()
	{
		return totalMatrixRows;
	}

	@Override
	public String getVolumetricProperties()
	{
		return Constants.Volume;
	}

	@Override
	public void setAcquisitionDateTime(String datetime)
		throws IllegalArgumentException
	{
		String checked = IodUtils.checkType1(strict, datetime, VR.DT,
			"Acquisition DateTime");
		acqDateTime = checked;
	}

	@Override
	public void setAcquisitionDuration(double duration)
		throws IllegalArgumentException
	{
		acqDuration = (Double.isFinite(duration) && (duration >= 0.0))
			? duration : Double.NaN;
	}

	@Override
	public void setBitsAllocated(int bits) throws IllegalArgumentException
	{
		core.setBitsAllocated(bits);
	}

	@Override
	public void setBitsStored(int bits) throws IllegalArgumentException
	{
		core.setBitsStored(bits);
	}

	@Override
	public void setBurnedInAnnotation(String value) throws IllegalArgumentException
	{
		core.setBurnedInAnnotation(value);
	}

	@Override
	public void setDistanceBetweenFocalPlanes(float distance)
	{
		interPlaneDist = (Double.isFinite(distance) && (distance >= 0.0))
			? distance : Float.NaN;
	}

	@Override
	public void setExtendedDepthOfField(boolean flag)
	{
		extDepthOfField = flag;
	}

	@Override
	public void setFocusMethod(String value)
	{
		String checked = IodUtils.checkCondition(strict, value,
			(String x) -> (x.equals(Constants.Auto) || x.equals(Constants.Manual)),
			"Focus method must be AUTO or MANUAL");
		focusMethod = checked;
	}

	@Override
	public void setHighBit(int bit) throws IllegalArgumentException
	{
		core.setHighBit(bit);
	}

	@Override
	public void setImageOrientationSlide(double[] orientation)
	{
		if ((orientation == null) || (orientation.length != 6))
		{
			throw new IllegalArgumentException(
				"Image orientation slide must be six elements");
		}
		System.arraycopy(orientation, 0, imageOriSlide, 0, 6);
	}

	@Override
	public void setImageType(String[] type)
	{
		core.setImageType(type);
	}

	@Override
	public void setImageVolumeDepth(float depth)
	{
		float checked = IodUtils.checkCondition(strict, depth,
			(Float x) -> (Float.isFinite(x) && (x > 0.0f)),
			"Image volume depth must be finite and greater than zero");
		imagedVolDepth = checked;
	}

	@Override
	public void setImageVolumeHeight(float height)
	{
		float checked = IodUtils.checkCondition(strict, height,
			(Float x) -> (Float.isFinite(x) && (x > 0.0f)),
			"Image volume height must be finite and greater than zero");
		imagedVolHeight = checked;
	}

	@Override
	public void setImageVolumeWidth(float width)
	{
		float checked = IodUtils.checkCondition(strict, width,
			(Float x) -> (Float.isFinite(x) && (x > 0.0f)),
			"Image volume width must be finite and greater than zero");
		imagedVolWidth = checked;
	}

	@Override
	public void setLossyImageCompression(boolean compressed)
	{
		core.setLossyImageCompression(compressed);
	}

	@Override
	public void setLossyImageCompressionMethod(String[] method)
	{
		if (method == null)
		{
			lossyMethod = new String[] {};
			return;
		}
		lossyMethod = Arrays.copyOf(method, method.length);
	}

	@Override
	public void setLossyImageCompressionRatio(double[] ratio)
	{
		if (ratio == null)
		{
			lossyRatio = new double[] {};
			return;
		}
		lossyRatio = Arrays.copyOf(ratio, ratio.length);
	}

	@Override
	public void setNumberOfFocalPlanes(int planes)
	{
		focalPlanes = (planes > 0) ? planes : Integer.MIN_VALUE;
	}

	@Override
	public void setNumberOfFrames(int frames)
	{
		int checked = IodUtils.checkCondition(strict, frames,
			(int x) -> (x > 0),
			"Number of frames must be greater than zero");
		this.frames = checked;
	}

	@Override
	public void setPhotometricInterpretation(String interp)
	{
		core.setPhotometricInterpretation(interp);
	}

	@Override
	public void setPixelRepresentation(int pixelRep)
	{
		core.setPixelRepresentation(pixelRep);
	}

	@Override
	public void setPlanarConfiguration(int config)
	{
		core.setPlanarConfiguration(config);
	}

	@Override
	public void setPresentationLutShape(String shape)
	{
		if (shape == null)
		{
			presLutShape = null;
			return;
		}
		String checked = IodUtils.checkCondition(strict, shape,
			(String x) -> x.equals(Constants.Identity),
			"Presentation LUT shape must be "+Constants.Identity+" or null");
		presLutShape = checked;
	}

	@Override
	public void setRescaleIntercept(double intercept)
	{
		if (!Double.isFinite(intercept))
		{
			rescaleIntercept = Double.NaN;
			return;
		}
		double checked = IodUtils.checkCondition(strict, intercept,
			(double x) -> (x == 0), "Rescale intercept must be 0");
		rescaleIntercept = checked;
	}

	@Override
	public void setRescaleSlope(double slope)
	{
		if (!Double.isFinite(slope))
		{
			rescaleSlope = Double.NaN;
			return;
		}
		double checked = IodUtils.checkCondition(strict, slope,
			(double x) -> (x == 1), "Rescale slope must be 1");
		rescaleSlope = checked;
	}

	@Override
	public void setSamplesPerPixel(int samples)
	{
		core.setSamplesPerPixel(samples);
	}

	@Override
	public void setSpecimenLabelInImage(boolean flag)
	{
		labelInImage = flag;
	}

	@Override
	public void setTotalPixelMatrixColumns(int columns)
	{
		int checked = IodUtils.checkCondition(strict, columns,
			(int x) -> (x > 0),
			"Total pixel matrix columns must be greater than zero");
		totalMatrixCols = checked;
	}

	@Override
	public void setTotalPixelMatrixFocalPlanes(int planes)
	{
		int checked = IodUtils.checkCondition(strict, planes,
			(int x) -> (x > 0),
			"Total pixel matrix focal planes must be greater than zero");
		totalMatrixFocalPlanes = checked;
	}

	@Override
	public void setTotalPixelMatrixOrigin(PixelMatrixOrigin origin)
	{
		if (origin == null)
		{
			throw new IllegalArgumentException(
				"Total pixel matrix origin must not be null");
		}
		this.origin = origin;
	}

	@Override
	public void setTotalPixelMatrixRows(int rows)
	{
		int checked = IodUtils.checkCondition(strict, rows,
			(int x) -> (x > 0),
			"Total pixel matrix rows must be greater than zero");
		totalMatrixRows = checked;
	}
	
}
