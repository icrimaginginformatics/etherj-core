/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod.module.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Iods;
import icr.etherj.dicom.iod.ReferencedImage;
import icr.etherj.dicom.iod.ReferencedInstance;
import icr.etherj.dicom.iod.SourceImage;
import icr.etherj.dicom.iod.impl.DefaultCode;
import icr.etherj.dicom.iod.impl.DefaultReferencedImage;
import icr.etherj.dicom.iod.impl.DefaultSourceImage;
import icr.etherj.dicom.iod.module.GeneralReferenceModule;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jamesd
 */
public class DefaultGeneralReferenceModule extends AbstractDisplayable
	implements GeneralReferenceModule
{
	private final List<Code> derivCodes = new ArrayList<>();
	private String derivDesc = null;
	private final Map<String,ReferencedImage> refImages = new LinkedHashMap<>();
	private final Map<String,SourceImage> sourceImages = new LinkedHashMap<>();
	private final boolean strict;

	public DefaultGeneralReferenceModule()
	{
		this(true);
	}

	public DefaultGeneralReferenceModule(boolean strict)
	{
		this.strict = strict;
	}

	@Override
	public boolean addDerivationCode(Code code)
	{
		return (code != null) ? derivCodes.add(code) : false;
	}

	@Override
	public boolean addReferencedImage(ReferencedImage image)
	{
		if (image == null)
		{
			return false;
		}
		String key = Iods.makeKey(image);
		if (!refImages.containsKey(key))
		{
			refImages.put(key, image);
			return true;
		}
		return false;
	}

	@Override
	public boolean addSourceImage(SourceImage image)
	{
		if (image == null)
		{
			return false;
		}
		String key = Iods.makeKey(image);
		if (!sourceImages.containsKey(key))
		{
			sourceImages.put(key, image);
			return true;
		}
		return false;
	}

	@Override
	public void cloneTo(GeneralReferenceModule target)
	{
		for (Code derivCode : derivCodes)
		{
			Code code = new DefaultCode();
			derivCode.cloneTo(code);
			target.addDerivationCode(code);
		}
		target.setDerivationDescription(derivDesc);
		for (ReferencedImage ri : refImages.values())
		{
			ReferencedImage clone = new DefaultReferencedImage();
			ri.cloneTo(clone);
			target.addReferencedImage(clone);
		}
		for (SourceImage si : sourceImages.values())
		{
			SourceImage clone = new DefaultSourceImage();
			si.cloneTo(clone);
			target.addSourceImage(clone);
		}
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		int nRefImages = refImages.size();
		ps.println(pad+"ReferencedImage: "+nRefImages+" referenced image"+
			((nRefImages != 1) ? "s" : ""));
		if (recurse)
		{
			for (ReferencedInstance inst : refImages.values())
			{
				inst.display(ps, indent+"  ", recurse);
			}
		}
		if ((derivDesc != null) && !derivDesc.isEmpty())
		{
			ps.println(pad+"DerivationDescription: "+derivDesc);
		}
		int nDerivCodes = derivCodes.size();
		if (nDerivCodes > 0)
		{
			ps.println(pad+"DerivationCode: "+nDerivCodes+" derivation code"+
				((nDerivCodes != 1) ? "s" : ""));
			if (recurse)
			{
				for (Code code : derivCodes)
				{
					code.display(ps, indent+"  ", recurse);
				}
			}
		}
		int nSourceImages = sourceImages.size();
		ps.println(pad+"SourceImage: "+nSourceImages+" source image"+
			((nSourceImages != 1) ? "s" : ""));
		if (recurse)
		{
			for (SourceImage image : sourceImages.values())
			{
				image.display(ps, indent+"  ", recurse);
			}
		}
	}

	@Override
	public List<Code> getDerivationCodeList()
	{
		return ImmutableList.copyOf(derivCodes);
	}

	@Override
	public String getDerivationDescription()
	{
		return derivDesc;
	}

	@Override
	public List<ReferencedImage> getReferencedImageList()
	{
		return ImmutableList.copyOf(refImages.values());
	}

	@Override
	public List<SourceImage> getSourceImageList()
	{
		return ImmutableList.copyOf(sourceImages.values());
	}

	@Override
	public boolean isStrict()
	{
		return strict;
	}

	@Override
	public boolean removeDerivationCode(Code code)
	{
		return derivCodes.remove(code);
	}

	@Override
	public boolean removeReferencedImage(ReferencedImage image)
	{
		String key = Iods.makeKey(image);
		if (refImages.containsKey(key))
		{
			refImages.remove(key);
			return true;
		}
		return false;
	}

	@Override
	public boolean removeSourceImage(SourceImage image)
	{
		String key = Iods.makeKey(image);
		if (sourceImages.containsKey(key))
		{
			sourceImages.remove(key);
			return true;
		}
		return false;
	}

	@Override
	public void setDerivationDescription(String description)
	{
		derivDesc = description;
	}
	
}
