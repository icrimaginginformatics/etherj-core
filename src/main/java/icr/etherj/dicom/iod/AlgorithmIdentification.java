/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;

/**
 *
 * @author jamesd
 */
public interface AlgorithmIdentification extends Displayable, Iod
{
	/**
	 * Returns the algorithm family code, type 1.
	 * @return 
	 */
	Code getAlgorithmFamilyCode();

	/**
	 * Returns the algorithm name, type 1.
	 * @return 
	 */
	String getAlgorithmName();

	/**
	 * Returns the algorithm name code, type 3.
	 * @return 
	 */
	Code getAlgorithmNameCode();

	/**
	 * Returns the algorithm parameters, type 3.
	 * @return 
	 */
	String getAlgorithmParameters();

	/**
	 * Returns the algorithm source, type 3.
	 * @return 
	 */
	String getAlgorithmSource();

	/**
	 * Returns the algorithm version, type 1.
	 * @return 
	 */
	String getAlgorithmVersion();

	/**
	 * Sets the algorithm family code, type 1.
	 * @param code 
	 */
	void setAlgorithmFamilyCode(Code code) throws IllegalArgumentException;

	/**
	 * Sets the algorithm name, type 1.
	 * @param name 
	 */
	void setAlgorithmName(String name) throws IllegalArgumentException;

	/**
	 * Sets the algorithm name code, type 3.
	 * @param code 
	 */
	void setAlgorithmNameCode(Code code);

	/**
	 * Sets the algorithm parameters, type 3.
	 * @param parameters 
	 */
	void setAlgorithmParameters(String parameters);

	/**
	 * Sets the algorithm source, type 3.
	 * @param source 
	 */
	void setAlgorithmSource(String source);

	/**
	 * Sets the algorithm version, type 1.
	 * @param version 
	 */
	void setAlgorithmVersion(String version) throws IllegalArgumentException;

}
