/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

/**
 *
 * @author jamesd
 */
public interface WsmReferencedImage extends ReferencedImage
{

	/**
	 * Returns the bottom right hand corner of localiser area, type 1.
	 * @return
	 */
	int[] getBottomRightHandCornerOfLocaliserArea();

	/**
	 * Returns the optical path identifier, type 1.
	 * @return
	 */
	String getOpticalPathIdentifier();

	/**
	 * Returns the pixel spacing, type 1.
	 * @return
	 */
	double[] getPixelSpacing();

	/**
	 * Returns the samples per pixel, type 1.
	 * @return
	 */
	int getSamplesPerPixel();

	/**
	 * Returns the top left hand corner of localiser area for the image, type 1.
	 * @return
	 */
	int[] getTopLeftHandCornerOfLocaliserArea();

	/**
	 * Returns the Z offset in slide coordinate system, type 1.
	 * @return
	 */
	double getZOffsetInSlideCoordinateSystem();

	/**
	 * Sets the bottom right hand corner of localiser area, type 1.
	 * @param brhc
	 * @throws IllegalArgumentException
	 */
	void setBottomRightHandCornerOfLocaliserArea(int[] brhc)
		throws IllegalArgumentException;

	/**
	 * Sets the optical path identifier, type 1.
	 * @param ident
	 * @throws IllegalArgumentException
	 */
	void setOpticalPathIdentifier(String ident) throws IllegalArgumentException;

	/**
	 * Sets the pixel spacing, type 1.
	 * @param spacing
	 * @throws IllegalArgumentException
	 */
	void setPixelSpacing(double[] spacing) throws IllegalArgumentException;

	/**
	 * Sets the samples per pixel, type 1.
	 * @param samples
	 * @throws IllegalArgumentException
	 */
	void setSamplesPerPixel(int samples) throws IllegalArgumentException;

	/**
	 * Sets the top left hand corner of localiser area for the image, type 1.
	 * @param tlhc
	 * @throws IllegalArgumentException
	 */
	void setTopLeftHandCornerOfLocaliserArea(int[] tlhc)
		throws IllegalArgumentException;

	/**
	 * Sets the Z offset in slide coordinate system, type 1.
	 * @param zOffset
	 * @throws IllegalArgumentException
	 */
	void setZOffsetInSlideCoordinateSystem(double zOffset)
		throws IllegalArgumentException;

}
