/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;
import icr.etherj.dicom.Coordinate3D;
import icr.etherj.dicom.Validatable;
import java.util.List;

/**
 * Contour from RoiContour module in DICOM IOD.
 * @author jamesd
 */
public interface Contour extends ContourImageContainer, Displayable, Validatable
{
	/** Contour geometry type defined in DICOM IOD. */
	public static final String Point = "POINT";
	/** Contour geometry type defined in DICOM IOD. */
	public static final String OpenPlanar = "OPEN_PLANAR";
	/** Contour geometry type defined in DICOM IOD. */
	public static final String OpenNonPlanar = "OPEN_NONPLANAR";
	/** Contour geometry type defined in DICOM IOD. */
	public static final String ClosedPlanar = "CLOSED_PLANAR";

	/**
	 * Adds a coordinate to the contour.
	 * @param coord the coordinate to add
	 * @return true if a non-null coordinate is added
	 */
	boolean addCoordinate(Coordinate3D coord);

	/**
	 * Adds a coordinate to the contour using its X, Y and Z components.
	 * @param x the x component
	 * @param y the y component
	 * @param z the z component
	 * @return true if the coordinate is added
	 */
	boolean addCoordinate(double x, double y, double z);

	/**
	 * Returns the list of coordinates defining the contour, type 1.
	 * @return the coordinate list
	 */
	List<Coordinate3D> getContourData();

	/**
	 * Returns the geometric type of the contour, type 1. Defined values:
	 * {@link Contour#ClosedPlanar},
	 * {@link Contour#OpenNonPlanar},
	 * {@link Contour#OpenPlanar},
	 * {@link Contour#Point}.
	 * @return the geometric type
	 */
	String getContourGeometricType();

	/**
	 * Returns the contour number, type 3.
	 * @return the contour number
	 */
	int getContourNumber();

	/**
	 * Returns the number of contour points.
	 * @return the number of points
	 */
	int getNumberOfContourPoints();

	/**
	 * Sets the geometry type of the contour, type 1, defined terms:
	 * {@link Contour#Point},
	 * {@link Contour#OpenPlanar},
	 * {@link Contour#OpenNonPlanar},
	 * {@link Contour#ClosedPlanar}.
	 * @param geomType the geometric type to set
	 * @throws IllegalArgumentException if the geometric types is unrecognised
	 */
	void setContourGeometricType(String geomType) throws IllegalArgumentException;

	/**
	 * Sets the contour number, type 3.
	 * @param number the contour number to set
	 */
	void setContourNumber(int number);

}
