/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

/**
 * Constants from the DICOM IOD.
 * @author jamesd
 */
public class Constants
{

	/** Patient position from DICOM IOD: Head First-Prone. */ 
	public static final String HFP = "HFP";
	/** Patient position from DICOM IOD: Head First-Supine. */ 
	public static final String HFS = "HFS";
	/** Patient position from DICOM IOD: Head First-Decubitus Right. */ 
	public static final String HFDR = "HFDR";
	/** Patient position from DICOM IOD: Head First-Decubitus Left. */ 
	public static final String HFDL = "HFDL";
	/** Patient position from DICOM IOD: Feet First-Decubitus Right. */ 
	public static final String FFDR = "FFDR";
	/** Patient position from DICOM IOD: Feet First-Decubitus Left. */ 
	public static final String FFDL = "FFDL";
	/** Patient position from DICOM IOD: Feet First-Prone. */ 
	public static final String FFP = "FFP";
	/** Patient position from DICOM IOD: Feet First-Supine. */ 
	public static final String FFS = "FFS";
	/** Patient position from DICOM IOD: Left First-Prone. */ 
	public static final String LFP = "LFP";
	/** Patient position from DICOM IOD: Left First-Supine. */ 
	public static final String LFS = "LFS";
	/** Patient position from DICOM IOD: Right First-Prone. */ 
	public static final String RFP = "RFP";
	/** Patient position from DICOM IOD: Right First-Supine. */ 
	public static final String RFS = "RFS";
	/** Patient position from DICOM IOD: Anterior First-Decubitus Right. */ 
	public static final String AFDR = "AFDR";
	/** Patient position from DICOM IOD: Anterior First-Decubitus Left. */ 
	public static final String AFDL = "AFDL";
	/** Patient position from DICOM IOD: Posterior First-Decubitus Right. */ 
	public static final String PFDR = "PFDR";
	/** Patient position from DICOM IOD: Posterior First-Decubitus Left. */ 
	public static final String PFDL = "PFDL";

	/**
	 * Pixel data represent a single monochrome image plane, minimum value
	 * displayed as white.
	 */ 
	public static final String Monochrome1 = "MONOCHROME1";
	/**
	 * Pixel data represent a single monochrome image plane, minimum value
	 * displayed as black.
	 */ 
	public static final String Monochrome2 = "MONOCHROME2";
	/**
	 * Pixel data describe a colour image with a single sample per pixel (single
	 * image plane). The pixel value is used as an index into each of the Red, 
	 * Blue and Green Palette Colour Lookup Tables.
	 */ 
	public static final String PaletteColour = "PALETTE COLOR";
	/**
	 * Pixel data represent a colour image described by red, green, and blue
	 * image planes.
	 */ 
	public static final String Rgb = "RGB";
	/**
	 * Pixel data represent a colour image described by one luminance (Y) and two
	 * chrominance planes (CB and CR).
	 */ 
	public static final String YbrFull = "YBR_FULL";
	/**
	 * The same as YBR_FULL except that the CB and CR values are sampled
	 * horizontally at half the Y rate.
	 */ 
	public static final String YbrFull422 = "YBR_FULL_422";
	/**
	 * Pixel data represent a colour image described by one luminance (Y) and two
	 * chrominance planes (CB and CR).
	 */ 
	public static final String YbrPartial420 = "YBR_PARTIAL_420";
	/** Irreversible Colour Transformation. */ 
	public static final String YbrIct = "YBR_ICT";
	/** Reversible Colour Transformation. */ 
	public static final String YbrRct = "YBR_RCT";

	/** Data representation of the pixel samples: unsigned integer. */ 
	public static final int UnsignedInteger = 0;
	/** Data representation of the pixel samples: 2's complement. */ 
	public static final int TwosComplement = 1;

	/** 
	 * The sample values for the first pixel are followed by the sample values
	 * for the second pixel, etc.
	 */ 
	public static final int Samplewise = 0;
	/** Each colour plane shall be encoded contiguously. */ 
	public static final int Planewise = 1;

	/**
	 * Binary segmentation.
	 */
	public static final String Binary = "BINARY";
	/**
	 * Fractional segmentation.
	 */
	public static final String Fractional = "FRACTIONAL";
	/**
	 * Fractional segmentation probability type.
	 */
	public static final String Probability = "PROBABILITY";
	/**
	 * Fractional segmentation occupancy type.
	 */
	public static final String Occupancy = "OCCUPANCY";
	/**
	 * Segment algorithm type: AUTOMATIC.
	 */
	public static final String Automatic = "AUTOMATIC";
	/**
	 * Segment algorithm type, WSM focus method: MANUAL.
	 */
	public static final String Manual = "MANUAL";
	/**
	 * Segment algorithm type: SEMIAUTOMATIC.
	 */
	public static final String SemiAutomatic = "SEMIAUTOMATIC";

	/**
	 * Partial SR completion.
	 */
	public static final String Partial = "PARTIAL";
	/**
	 * Full SR completion.
	 */
	public static final String Complete = "COMPLETE";
	/**
	 * Unverified SR.
	 */
	public static final String Unverified = "UNVERIFIED";
	/**
	 * Verified SR.
	 */
	public static final String Verified = "VERIFIED";
	/**
	 * SR container content item type.
	 */
	public static final String Container = "CONTAINER";
	/**
	 * SR text content item type.
	 */
	public static final String Text = "TEXT";
	/**
	 * SR number content item type.
	 */
	public static final String Numeric = "NUM";
	/**
	 * SR code content item type.
	 */
	public static final String Code = "CODE";
	/**
	 * SR date content item type.
	 */
	public static final String Date = "DATE";
	/**
	 * SR time content item type.
	 */
	public static final String Time = "TIME";
	/**
	 * SR datetime content item type.
	 */
	public static final String DateTime = "DATETIME";
	/**
	 * SR UID reference content item type.
	 */
	public static final String UidReference = "UIDREF";
	/**
	 * SR person name.
	 */
	public static final String PersonName = "PNAME";
	/**
	 * SR composite.
	 */
	public static final String Composite = "COMPOSITE";
	/**
	 * SR image content item type.
	 */
	public static final String Image = "IMAGE";
	/**
	 * SR waveform content item type.
	 */
	public static final String Waveform = "WAVEFORM";
	/**
	 * SR spatial coordinate content item type.
	 */
	public static final String SpatialCoord = "SCOORD";
	/**
	 * SR 3D spatial coordinate content item type.
	 */
	public static final String SpatialCoord3D = "SCOORD3D";
	/**
	 * SR temporal coordinate content item type.
	 */
	public static final String TemporalCoord = "TCOORD";
	/**
	 * SR container continuity type.
	 */
	public static final String Separate = "SEPARATE";
	/**
	 * SR container continuity type.
	 */
	public static final String Continuous = "CONTINUOUS";
	/**
	 * SR spatial coordinate, microscopy annotation point graphic type.
	 */
	public static final String Point = "POINT";
	/**
	 * SR spatial coordinate multipoint graphic type.
	 */
	public static final String MultiPoint = "MULTIPOINT";
	/**
	 * SR spatial coordinate, microscopy annotation polyline graphic type.
	 */
	public static final String Polyline = "POLYLINE";
	/**
	 * SR spatial coordinate circle graphic type.
	 */
	public static final String Circle = "CIRCLE";
	/**
	 * SR spatial coordinate, microscopy annotation ellipse graphic type.
	 */
	public static final String Ellipse = "ELLIPSE";

	/**
	 * Multiframe dimension organisation type: 3D.
	 */
	public static final String ThreeD = "3D";
	/**
	 * Multiframe dimension organisation type: 3D_TEMPORAL.
	 */
	public static final String ThreeDTemporal = "3D_TEMPORAL";
	/**
	 * Multiframe dimension organisation type: TILED_FULL.
	 */
	public static final String TiledFull = "TILED_FULL";
	/**
	 * Multiframe dimension organisation type: TILED_SPARSE.
	 */
	public static final String TiledSparse = "TILED_SPARSE";

	/**
	 * Slide-based coordinate system position reference indicator: SLIDE_CORNER
	 */
	public static final String SlideCorner = "SLIDE_CORNER";

	/**
	 * Container component material: GLASS
	 */
	public static final String Glass = "GLASS";
	/**
	 * Container component material: METAL
	 */
	public static final String Metal = "METAL";
	/**
	 * Container component material: PLASTIC
	 */
	public static final String Plastic = "Plastic";

	/**
	 * Defined term: YES.
	 */
	public static final String Yes = "YES";
	/**
	 * Defined term: NO.
	 */
	public static final String No = "NO";

	/**
	 * Defined sex: M.
	 */
	public static final String Male = "M";
	/**
	 * Defined sex: F.
	 */
	public static final String Female = "F";
	/**
	 * Defined sex: O.
	 */
	public static final String Other = "O";

	/**
	 * WSM focus method: AUTO.
	 */
	public static final String Auto = "AUTO";

	/**
	 * Presentation LUT shape: IDENTITY.
	 */
	public static final String Identity = "IDENTITY";

	/**
	 * Image type element, WSM volumetric properties, microscopy pixel origin
	 * interpretation: VOLUME.
	 */
	public static final String Volume = "VOLUME";

	/**
	 * Image type element: DERIVED.
	 */
	public static final String Derived = "DERIVED";
	/**
	 * Image type element: LABEL.
	 */
	public static final String Label = "LABEL";
	/**
	 * Image type element: LOCALIZER.
	 */
	public static final String Localizer = "LOCALIZER";
	/**
	 * Image type element: NONE.
	 */
	public static final String None = "NONE";
	/**
	 * Image type element: ORIGINAL.
	 */
	public static final String Original = "ORIGINAL";
	/**
	 * Image type element: OVERVIEW.
	 */
	public static final String Overview = "OVERVIEW";
	/**
	 * Image type element: PRIMARY.
	 */
	public static final String Primary = "PRIMARY";
	/**
	 * Image type element: RESAMPLED.
	 */
	public static final String Resampled = "RESAMPLED";

	/**
	 * HL7v2 heirarchic designator ID format: DNS.
	 */
	public static final String DNS = "DNS";
	/**
	 * HL7v2 heirarchic designator ID format: EUI64.
	 */
	public static final String EUI64 = "EUI64";
	/**
	 * HL7v2 heirarchic designator ID format: ISO.
	 */
	public static final String ISO = "ISO";
	/**
	 * HL7v2 heirarchic designator ID format: URI.
	 */
	public static final String URI = "URI";
	/**
	 * HL7v2 heirarchic designator ID format: UUID.
	 */
	public static final String UUID = "UUID";
	/**
	 * HL7v2 heirarchic designator ID format: X400.
	 */
	public static final String X400 = "X400";
	/**
	 * HL7v2 heirarchic designator ID format: X500.
	 */
	public static final String X500 = "X500";

	/**
	 * Microscopy coordinate type: 2D.
	 */
	public static final String Coord2D = "2D";
	/**
	 * Microscopy coordinate type: 3D.
	 */
	public static final String Coord3D = "3D";
	/**
	 * Microscopy pixel origin interpretation: FRAME.
	 */
	public static final String Frame = "FRAME";
	/**
	 * microscopy annotation polygon graphic type.
	 */
	public static final String Polygon = "POLYGON";
	/**
	 * microscopy annotation rectangle graphic type.
	 */
	public static final String Rectangle = "RECTANGLE";

	//	Prevent instantiation.
	private Constants()
	{}
}
