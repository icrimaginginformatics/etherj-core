/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.iod;

import icr.etherj.Displayable;

/**
 *
 * @author jamesd
 */
public interface Measurement extends Displayable, Iod
{
	/**
	 * Returns indices referencing annotations identified in
	 * LongPrimitivePointIndexList or successive points stored in
	 * PointCoordinatesData or DoublePointCoordinatesData for which measurement
	 * values shall be stored, type 1C. Required if measurements do not apply to 
	 * all annotations in the group.
	 * @return 
	 */
	int[] getAnnotationIndexList();

	/**
	 * Returns the concept name code for the measurement, type 1. This is the
	 * type of measurement: area? signal average? See DCID 8136
	 * "Microscopy Measurement Types".
	 * @return 
	 */
	Code getConceptNameCode();

	/**
	 * Returns the measurement units code for the measurement, type 1. See CID 82
	 * "Units of Measurement".
	 * @return 
	 */
	Code getMeasurementUnitsCode();

	/**
	 * Returns measurement values for annotations stored in this group, type 1.
	 * @return 
	 */
	float[] getFloatingPointValues();

	/**
	 * Sets indices referencing annotations identified in
	 * LongPrimitivePointIndexList or successive points stored in
	 * PointCoordinatesData or DoublePointCoordinatesData for which measurement
	 * values shall be stored, type 1C. Required if measurements do not apply to 
	 * all annotations in the group.
	 * @param indices 
	 */
	 void setAnnotationIndexList(int[] indices);

	/**
	 * Sets the concept name code for the measurement, type 1. This is the
	 * type of measurement: area? signal average? See DCID 8136
	 * "Microscopy Measurement Types".
	 * @param code 
	 */
	 void setConceptNameCode(Code code) throws IllegalArgumentException;

	/**
	 * Sets measurement values for annotations stored in this group, type 1.
	 * @param values 
	 */
	 void setFloatingPointValues(float[] values) throws IllegalArgumentException;

	/**
	 * Sets the measurement units code for the measurement, type 1. See CID 82
	 * "Units of Measurement".
	 * @param code 
	 */
	void setMeasurementUnitsCode(Code code) throws IllegalArgumentException;

}
