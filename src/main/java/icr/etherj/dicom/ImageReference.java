/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom;

import icr.etherj.AbstractDisplayable;
import java.io.PrintStream;

/**
 * An immutable reference to identify a single DICOM image frame.
 * @author jamesd
 */
public class ImageReference extends AbstractDisplayable
{
	public final String sopClassUid;
	public final String sopInstanceUid;
	public final int referencedFrameNumber;

	/**
	 * Constructs a new instance with the supplied SOP class UID, SOP instance
	 * UID and referenced frame number of 1.
	 * @param sopClassUid the SOP class UID
	 * @param sopInstanceUid the SOP instance UID
	 */
	public ImageReference(String sopClassUid, String sopInstanceUid)
	{
		this(sopClassUid, sopInstanceUid, 0);
	}

	/**
	 * Constructs a new instance with the supplied SOP class UID, SOP instance
	 * UID and referenced frame number.
	 * @param sopClassUid the SOP class UID
	 * @param sopInstanceUid the SOP instance UID
	 * @param referencedFrameNumber the referenced frame number
	 */
	public ImageReference(String sopClassUid, String sopInstanceUid,
		int referencedFrameNumber)
	{
		this.sopClassUid = sopClassUid;
		this.sopInstanceUid = sopInstanceUid;
		this.referencedFrameNumber = referencedFrameNumber;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"SopClassUid: "+sopClassUid);
		ps.println(pad+"SopInstanceUid: "+sopInstanceUid);
		if (referencedFrameNumber != 0)
		{
			ps.println(pad+"ReferencedFrameNumber: "+referencedFrameNumber);
		}
	}
}
