/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.Iods;
import icr.etherj.dicom.iod.PixelMeasures;
import icr.etherj.dicom.iod.ReferencedInstance;
import icr.etherj.dicom.iod.ReferencedSeries;
import icr.etherj.dicom.iod.Segment;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.dicom.iod.SegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.SegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SegmentationSharedFunctionalGroups;
import icr.etherj.dicom.iod.impl.DefaultContourImage;
import icr.etherj.dicom.iod.impl.DefaultPixelMeasures;
import icr.etherj.dicom.iod.impl.DefaultReferencedInstance;
import icr.etherj.dicom.iod.impl.DefaultReferencedSeries;
import icr.etherj.dicom.iod.impl.DefaultSegment;
import icr.etherj.dicom.iod.impl.DefaultSegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.module.CommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.EnhancedGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import icr.etherj.dicom.iod.module.GeneralSeriesModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.SegmentationImageModule;
import icr.etherj.nifti.Nifti;
import icr.etherj.nifti.NiftiHeader;
import icr.etherj.nifti.NiftiUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
final class NiftiToSegmentationConverter extends BaseToSegmentationConverter
{
	private static final Logger logger =
		LoggerFactory.getLogger(NiftiToSegmentationConverter.class);

	private boolean dimensionMismatch = false;
	private final List<Image3DInfo> i3dList = new ArrayList<>();
	private final Lock lock = new ReentrantLock();
	private Nifti nifti;
	private double[] pixDims;
	private final Map<Integer,SegmentInfo> segInfoMap = new TreeMap<>();
	private boolean threeDMismatch = false;

	// Lock the single public method to allow use of instance variables
	public Segmentation convert(Nifti nifti, Map<String,DicomObject> dcmMap)
		throws ConversionException
	{
		try
		{
			lock.lock();
			return convertImpl(nifti, dcmMap);
		}
		finally
		{
			clearInternal();
			lock.unlock();
		}
	}

	@Override
	protected void clearInternal()
	{
		super.clearInternal();
		segInfoMap.clear();
		nifti = null;
	}

	@Override
	protected int computeTotalFrames()
	{
		int nFrames = 0;
		for (SegmentInfo segment : segInfoMap.values())
		{
			nFrames += segment.getFrameSpan();
		}
		return nFrames;
	}

	@Override
	protected void configPerFrameFuncGroups(SegmentationPerFrameFunctionalGroups pfFuncGroups)
	{
		for (SegmentInfo segment : segInfoMap.values())
		{
			int refRoiNumber = segment.getSegment().getSegmentNumber();
			for (BaseToSegmentationConverter.TargetFrame tf : segment.getTargetFrameList())
			{
				SegmentationFunctionalGroupsFrame frame =
					new DefaultSegmentationFunctionalGroupsFrame();
				frame.addDerivationImage(buildDerivationImage(tf));
				frame.setFrameContent(buildFrameContent(tf));
				frame.setReferencedSegmentNumber(refRoiNumber);
				frame.setImagePositionPatient(tf.getImagePositionPatient());
				pfFuncGroups.addFrame(frame);
			}
		}
	}

	@Override
	protected void configSharedFuncGroups(SegmentationSharedFunctionalGroups sharedFuncGroups)
		throws ConversionException
	{
		ContourImage ci = contourImages.values().iterator().next();
		int refFrameNumber = ci.getReferencedFrameNumber();
		DicomObject dcm = dcmMap.get(ci.getReferencedSopInstanceUid());

		sharedFuncGroups.setImageOrientationPatient(
			getImageOrientationPatient(dcm, isMultiFrame, refFrameNumber));
		// Store pixDims as will need for coordinate conversion later
		pixDims = getPixelSpacing(dcm, isMultiFrame, refFrameNumber);
		PixelMeasures pm = new DefaultPixelMeasures();
		pm.setPixelSpacing(pixDims);
		pm.setSliceThickness(getSliceThickness(dcm, isMultiFrame, refFrameNumber));
		pm.setSpacingBetweenSlices(
			getSpacingBetweenSlices(dcm, isMultiFrame, refFrameNumber));
		sharedFuncGroups.setPixelMeasures(pm);
	}

	@Override
	protected String createContentLabel()
	{
		return "Nifti";
	}

	private void buildContourImages()
	{
		for (DicomObject dcm : dcmMap.values())
		{
			String sopClassUid = dcm.getString(Tag.SOPClassUID);
			String sopInstUid = dcm.getString(Tag.SOPInstanceUID);
			ContourImage ci = new DefaultContourImage();
			ci.setReferencedSopClassUid(sopClassUid);
			ci.setReferencedSopInstanceUid(sopInstUid);
			contourImages.put(sopInstUid, ci);
		}
	}

	private void buildMaps() throws ConversionException
	{
		// Assume 1 segment for now
		int number = 1;
		Segment segment = buildSegment(number);
		segmentMap.put(number, segment);

		buildOrderedImages();
		buildContourImages();
		segInfoMap.put(number, buildSegmentInfo(number));
	}

	private void buildOrderedImages() throws ConversionException
	{
		i3dList.clear();
		for (DicomObject dcm : dcmMap.values())
		{
			int nFrames = dcm.getInt(Tag.NumberOfFrames, 1);
			for (int frame=1; frame<=nFrames; frame++)
			{
				i3dList.add(new Image3DInfo(dcm, frame));
			}
		}
		i3dList.sort(new Image3DInfoComparator());
	}

	private Segment buildSegment(int number)
	{
		Segment segment = new DefaultSegment();
		segment.setSegmentNumber(number);
		segment.setSegmentLabel(String.format("Segment_%05d", number));
		segment.setSegmentAlgorithmType(Constants.Manual);
		segment.setSegmentAlgorithmName(null);
		Code catCode = segment.getSegmentedPropertyCategoryCode();
		setSegPropDefaults(catCode);
		Code typeCode = segment.getSegmentedPropertyTypeCode();
		setSegPropDefaults(typeCode);

		return segment;
	}

	private SegmentInfo buildSegmentInfo(int segNumber)
		throws ConversionException
	{
		Segment segment = segmentMap.get(segNumber);
		SegmentInfo segInfo = new SegmentInfo(segment);

		Table<String,Integer,BaseToSegmentationConverter.SourceFrame> ciFrameTable = TreeBasedTable.create();
		for (ContourImage ci : contourImages.values())
		{
			String uid = ci.getReferencedSopInstanceUid();
			int refFrameIdx = ci.getReferencedFrameNumber();
			if (!ciFrameTable.contains(uid, refFrameIdx))
			{
				DicomObject dcm = dcmMap.get(uid);
				BaseToSegmentationConverter.SourceFrame sf = new BaseToSegmentationConverter.SourceFrame(
					getImageOrientationPatient(dcm, isMultiFrame, refFrameIdx),
					getImagePositionPatient(dcm, isMultiFrame, refFrameIdx),
					ci, dcm.getInt(Tag.Rows), dcm.getInt(Tag.Columns));
				ciFrameTable.put(uid, refFrameIdx, sf);
			}
		}

		List<BaseToSegmentationConverter.SourceFrame> sourceList = new ArrayList<>();
		sourceList.addAll(ciFrameTable.values());
		Collections.sort(sourceList);
		// idx used as frameInRoiIndex here, could read frameInSegmentIndex and be
		// equivalent.
		int idx = 1;
		for (BaseToSegmentationConverter.SourceFrame sf : sourceList)
		{
			BaseToSegmentationConverter.TargetFrame tf = new BaseToSegmentationConverter.TargetFrame(segNumber, idx, sf);
			segInfo.addTargetFrame(tf);
			idx++;
		}

		return segInfo;
	}

	private void checkDimensions() throws ConversionException
	{
		String ciUid = dcmMap.keySet().iterator().next();
		DicomObject protoDcm = dcmMap.get(ciUid);
		int refFrameNumber = protoDcm.getInt(Tag.NumberOfFrames, 1);
		NiftiHeader header = nifti.getHeader();

		// Overall matrix dimensions
		long[] niftiDims = header.getDimensions();
		int niftiRows = (int) niftiDims[1];
		int niftiCols = (int) niftiDims[2];
		int nRows = protoDcm.getInt(Tag.Rows);
		int nCols = protoDcm.getInt(Tag.Columns);
		if ((niftiRows != nRows) || (niftiRows != nRows))
		{
			logger.warn("NIfTI image dimensions ("+niftiRows+","+niftiCols+") do "+
				"not match DICOM image dimensions ("+nRows+","+nCols+")");
			dimensionMismatch = true;
		}
		int niftiFrames = (int) niftiDims[3];
		int nFrames = dcmMap.size();
		if (niftiFrames != nFrames)
		{
			logger.warn("NIfTI frame count ("+niftiFrames+") does "+
				"not match DICOM frame count ("+nFrames+")");
			dimensionMismatch = true;
		}

		// Pixel dimensions
		double[] niftiPixDims = header.getPixelSpacing();
		double[] dcmPixDims = getPixelSpacing(protoDcm, isMultiFrame,
			refFrameNumber);
		double dcmSliceThickness = getSliceThickness(protoDcm, isMultiFrame,
			refFrameNumber);
		if (!doubleEqual(dcmPixDims[0], niftiPixDims[1]) ||
			 !doubleEqual(dcmPixDims[1], niftiPixDims[2]))
		{
			logger.warn(
				"NIfTI pixel dims ("+niftiPixDims[1]+","+niftiPixDims[2]+")"+
				" do not match "+
				"DICOM pixel dims ("+dcmPixDims[0]+","+dcmPixDims[1]+")");
			dimensionMismatch = true;
		}
		if (!doubleEqual(niftiPixDims[3], dcmSliceThickness))
		{
			logger.warn(
				"NIfTI slice thickness ("+niftiPixDims[3]+")"+
				" does not match "+
				"DICOM slice thickness ("+dcmSliceThickness+")");
			dimensionMismatch = true;
		}

		// Is this even a 3D volume?
		double dcmSliceSpacing = getSpacingBetweenSlices(protoDcm, isMultiFrame,
			refFrameNumber);
		if (dcmSliceSpacing > 0.0)
		{
			logger.warn("Slice spacing > 0. Volume is not 3D");
		}

		// 3D info
		double[] niftiPos = getNiftiPosition();
		double[] ipp = getImagePositionPatient(protoDcm, isMultiFrame,
			refFrameNumber);
		if (!doubleEqual(niftiPos[0], ipp[0]) ||
			 !doubleEqual(niftiPos[1], ipp[1]) ||
			 !doubleEqual(niftiPos[2], ipp[2]))
		{
			logger.warn(
				"NIfTI position ("+niftiPos[0]+","+niftiPos[1]+","+niftiPos[2]+")"+
				" does not match "+
				"DICOM IPP ("+ipp[0]+","+ipp[1]+","+ipp[2]+")");
			threeDMismatch = true;
		}
		double[] niftiOri = getNiftiOrientation();
		double[] iop = getImageOrientationPatient(protoDcm, isMultiFrame,
			refFrameNumber);
		if (!doubleEqual(niftiOri[0], iop[0]) ||
			 !doubleEqual(niftiOri[1], iop[1]) ||
			 !doubleEqual(niftiOri[2], iop[2]))
		{
			logger.warn(
				"NIfTI row ("+niftiOri[0]+","+niftiOri[1]+","+niftiOri[2]+")"+
				" does not match "+
				"DICOM row ("+iop[0]+","+iop[1]+","+iop[2]+")");
			threeDMismatch = true;
		}
		if (!doubleEqual(niftiOri[3], iop[3]) ||
			 !doubleEqual(niftiOri[4], iop[4]) ||
			 !doubleEqual(niftiOri[5], iop[5]))
		{
			logger.warn(
				"NIfTI column ("+niftiOri[3]+","+niftiOri[4]+","+niftiOri[5]+")"+
				" does not match "+
				"DICOM column ("+iop[3]+","+iop[4]+","+iop[5]+")");
			threeDMismatch = true;
		}
	}

//	private void checkDimensionsOld(Segmentation seg)
//	{
//		String ciUid = dcmMap.keySet().iterator().next();
//		DicomObject protoDcm = dcmMap.get(ciUid);
//
//		// Nifti
//		long[] niftiDims = nifti.getHeader().getDimensions();
//		StringBuilder sb = new StringBuilder("Nifti Dims:");
//		sb.append("\n* nDims= ").append(niftiDims[0])
//			.append("\n* nRows= ").append(niftiDims[1])
//			.append("\n* nCols= ").append(niftiDims[2])
//			.append("\n* nFrames= ").append(niftiDims[3]);
//		System.out.println(sb);
//		MultiframeFunctionalGroupsModule mfFuncGroups = 
//			seg.getMultiframeFunctionalGroupsModule();
//		sb = new StringBuilder("DICOM Dims:");
//		sb.append("\n* nDims= ").append(3)
//			.append("\n* nRows= ").append(protoDcm.getInt(Tag.Rows))
//			.append("\n* nCols= ").append(protoDcm.getInt(Tag.Columns))
//			.append("\n* nFrames= ").append(mfFuncGroups.getNumberOfFrames());
//		System.out.println(sb);
//		double[] niftiPixDims = nifti.getHeader().getPixelSpacing();
//		sb = new StringBuilder("Nifti PixelDims:");
//		sb.append("\n* PixelSpacing= [").append(niftiPixDims[1]).append(",")
//			.append(niftiPixDims[2]).append("]");
//		sb.append("\n* SliceThickness= ").append(niftiPixDims[3]);
//		System.out.println(sb);
//		PixelMeasures pixMeasures =
//			mfFuncGroups.getSharedFunctionalGroups().getPixelMeasures();
//		double[] pixDims = pixMeasures.getPixelSpacing();
//		sb = new StringBuilder("SEG PixelDims:");
//		sb.append("\n* PixelSpacing= [").append(pixDims[0]).append(",")
//			.append(pixDims[1]).append("]");
//		sb.append("\n* SliceThickness= ").append(pixMeasures.getSliceThickness());
//		System.out.println(sb);
//		
//	}

	private void checkNifti(Nifti nifti) throws ConversionException
	{
		if (nifti == null)
		{
			throw new ConversionException("Nifti must not be null");
		}
		long[] niftiDims = nifti.getHeader().getDimensions();
		if ((niftiDims[0] < 2) || (niftiDims[0] > 3))
		{
			throw new ConversionException(
				"NIfTI dimensions ("+niftiDims[0]+") not compatible with SEG");
		}
	}

	private void computeFrames(Segmentation seg) throws ConversionException
	{
		NiftiHeader hdr = nifti.getHeader();
		switch (hdr.getDataType())
		{
			case Nifti.Float32:
				computeFramesFloat32(seg);
				break;
			default:
				throw new ConversionException("Unsupported data type: "+
					NiftiUtils.getTypeName(hdr.getDataType()));
		}
	}

	private void computeFramesFloat32(Segmentation seg)
		throws ConversionException
	{
		float[] niftiPixels = nifti.getFloat32Data();
		// Find min and max
		float min = niftiPixels[0];
		float max = min;
		for (float pixel : niftiPixels)
		{
			if (pixel < min)
			{
				min = pixel;
				continue;
			}
			if (pixel > max)
			{
				max = pixel;
			}
		}
		logger.info("Min/Max: "+min+"/"+max);
		if ((min < 0.0) || (max > 1.0))
		{
			throw new ConversionException("Data outside probability range 0-1");
		}

		NiftiHeader header = nifti.getHeader();
		Image3DInfo protoI3d = i3dList.get(0);
		DicomObject protoDcm = protoI3d.dcm;
		// Overall matrix dimensions
		long[] niftiDims = header.getDimensions();
		int niftiRows = (int) niftiDims[1];
		int niftiCols = (int) niftiDims[2];
		int nRows = protoDcm.getInt(Tag.Rows);
		int nCols = protoDcm.getInt(Tag.Columns);
		int niftiFrames = (int) niftiDims[3];
		int nFrames = dcmMap.size();

		// Pixel dimensions
		double[] niftiPixDims = header.getPixelSpacing();
		double[] dcmPixDims = Arrays.copyOf(protoI3d.pixDims, 2);
		double dcmSliceThickness = protoI3d.thickness;

		// Is this even a 3D volume?
//		double dcmSliceSpacing = getSpacingBetweenSlices(protoDcm, isMultiFrame,
//			refFrameNumber);

		// 3D info
		double[] niftiPos = getNiftiPosition();
		double[] niftiOri = getNiftiOrientation();
		double[] niftiRow = Arrays.copyOfRange(niftiOri, 0, 2);
		double[] niftiCol = Arrays.copyOfRange(niftiOri, 3, 5);

		double xRatio = niftiCols / ((double) nCols);
		double yRatio = niftiRows / ((double) nRows);

		byte[] dcmPix = new byte[nRows*nCols*nFrames];
		logger.info("Interpolating "+dcmPix.length+" pixels");
		double invPixArea = 1.0/(niftiPixDims[1]*niftiPixDims[2]);
		int dcmFrameStride = nRows*nCols;
		int niftiFrameStride = niftiRows*niftiCols;
		int[] histogram = new int[256];
		for (int idx=0; idx<nFrames; idx++)
		{
			int dcmFrameOffset = idx*dcmFrameStride;
			int niftiFrameOffset = idx*niftiFrameStride;
			Image3DInfo i3d = i3dList.get(idx);
			for (int dcmRowIdx=0; dcmRowIdx<nRows; dcmRowIdx++)
			{
				double dcmY = dcmRowIdx*i3d.pixDims[0];
				int niftiRowIdx = (int) Math.floor(dcmRowIdx*yRatio);
				if (niftiRowIdx >= niftiRows-1)
					// Prevent overflow if DICOM pixel centre is past NIFTI pixel centre
					// ToDo: Fix outer border processing
					continue;
				int dcmRowOffset = dcmFrameOffset+dcmRowIdx*nCols;
				int niftiRowOffset = niftiFrameOffset+niftiRowIdx*niftiCols;
				double niftiYLo = niftiRowIdx*niftiPixDims[1];
				double niftiYHi = niftiYLo+niftiPixDims[1];
				double dy2y = niftiYHi-dcmY;
				double dyy1 = dcmY-niftiYLo;
				for (int dcmColIdx=0; dcmColIdx<nCols; dcmColIdx++)
				{
					double dcmX = dcmColIdx*i3d.pixDims[1];
					int niftiColIdx = (int) Math.floor(dcmColIdx*xRatio);
					if (niftiColIdx >= niftiCols-1)
						// Prevent overflow if DICOM pixel centre is past NIFTI pixel centre
						// ToDo: Fix outer border processing
						continue;
					double niftiXLo = niftiColIdx*niftiPixDims[2];
					double niftiXHi = niftiXLo+niftiPixDims[2];
					double dx2x = niftiXHi-dcmX;
					double dxx1 = dcmX-niftiXLo;
					int niftiOffset = niftiRowOffset+niftiColIdx;
//					System.out.println("Nifti: ("+niftiRowIdx+","+niftiColIdx+","+idx+")");
					try
					{
						double q11 = niftiPixels[niftiOffset];
						niftiOffset++;
						double q21 = niftiPixels[niftiOffset];
						niftiOffset += niftiCols;
						double q22 = niftiPixels[niftiOffset];
						niftiOffset--;
						double q12 = niftiPixels[niftiOffset];
						double value = invPixArea *
							(dx2x*dy2y*q11 + dxx1*dy2y*q21 + dx2x*dyy1*q12 + dxx1*dyy1*q22);
						int dcmOffset = dcmRowOffset+dcmColIdx;
//						System.out.println("DICOM: ("+dcmRowIdx+","+dcmColIdx+","+idx+")");
//						System.out.println("Qs: "+q11+", "+q12+", "+q21+", "+q22);
						byte byteValue = (byte) (Math.round(value*255) & 0x000000ff);
						dcmPix[dcmOffset] = byteValue;
//						histogram[byteValue+128]++;
					}
					catch (ArrayIndexOutOfBoundsException ex)
					{
						ex.printStackTrace();
						System.out.println("Placeholder line");
					}
				}
			}
		}
//		for (float f : niftiPixels)
//		{
//			int idx = (int) (Math.round(f*255));
//			histogram[idx]++;
//		}
//		for (int i : histogram)
//		{
//			System.out.println(Integer.toString(i));
//		}
		int x = 190;
		int y = 95;
		int z = 115;
		int offset = toOffset(x, y, z, niftiRows, niftiCols);
		System.out.println("["+offset+"] = "+niftiPixels[offset]);
		int[] idx = toIdx(offset, niftiRows, niftiCols);
		System.out.println("["+offset+"] = "+"("+idx[0]+","+idx[1]+","+idx[2]+")");
		System.out.println("("+x+","+y+","+z+") = "+
			getPixel(niftiPixels, niftiRows, niftiCols, nFrames, x, y, z));
		seg.getImagePixelModule().setPixelData(dcmPix, true);
	}

	private int[] toIdx(int offset, int nRows, int nCols)
	{
		int frameStride = nRows*nCols;
		int inFrame = offset % frameStride;
		int z = (offset-inFrame)/frameStride;
		int x = inFrame % nCols;
		int y = (inFrame - x) / nCols;
		return new int[] {x, y, z};
	}

	private int toOffset(int x, int y, int z, int nRows, int nCols)
	{
		return toOffset(new int[] {x, y, z}, nRows, nCols);
	}

	private int toOffset(int[] idx, int nRows, int nCols)
	{
		int frameStride = nRows*nCols;
		int offset = idx[2]*frameStride + idx[1]*nCols + idx[0];
		return offset;
	}

	private float getPixel(float[] array, int nRows, int nCols, int nFrames,
		int x, int y, int z)
	{
		float value = array[toOffset(x, y, z, nRows, nCols)];
		return value;
	}
	private void configClinicalTrialSubject(DicomObject dcm,
		Segmentation seg)
	{
		logger.debug("configClinicalTrialSubjectModule() not implemented yet");
	}

	private void configClinicalTrialStudy(DicomObject dcm,
		Segmentation seg)
	{
		logger.debug("configClinicalTrialStudyModule() not implemented yet");
	}

	private void configCommonInstRef(DicomObject dcm, Segmentation seg)
	{
		CommonInstanceReferenceModule cir = seg.getCommonInstanceReferenceModule();
		ReferencedSeries refSeries = new DefaultReferencedSeries();
		refSeries.setSeriesInstanceUid(dcm.getString(Tag.SeriesInstanceUID));
		cir.addReferencedSeries(refSeries);
		for (ContourImage ci : contourImages.values())
		{
			ReferencedInstance refInst = new DefaultReferencedInstance();
			ci.cloneTo(refInst);
			refSeries.addReferencedInstance(refInst);
		}
	}

	private void configEquipment(DicomObject dcm, Segmentation seg)
	{
		EnhancedGeneralEquipmentModule segEquip = seg.getEnhancedEquipmentModule();
		segEquip.setDeviceSerialNumber(
			dcm.getString(Tag.DeviceSerialNumber, "DeviceSerialNumber"));
		segEquip.setManufacturer(
			dcm.getString(Tag.Manufacturer, "Manufacturer"));
		segEquip.setManufacturersModelName(
			dcm.getString(Tag.ManufacturerModelName, "ManufacturerModelName"));
		segEquip.setSoftwareVersion(
			dcm.getString(Tag.SoftwareVersions, "SoftwareVersion"));
	}

	private void configFrameOfReference(DicomObject dcm, Segmentation seg)
	{
		FrameOfReferenceModule frameOfRef = seg.getFrameOfReferenceModule();
		frameOfRef.setFrameOfReferenceUid(
			dcm.getString(Tag.FrameOfReferenceUID, VR.UI, null));
	}

	private void configGeneralSeries(Segmentation seg, String date,
		String time)
	{
		SegmentationImageModule siModule = seg.getSegmentationImageModule();
		GeneralSeriesModule genSeries = seg.getGeneralSeriesModule();
		genSeries.setSeriesDate(date);
		genSeries.setSeriesTime(time);
		genSeries.setSeriesDescription(siModule.getContentLabel());
		genSeries.setSeriesNumber(99);
	}

	private void configGeneralStudy(DicomObject dcm, Segmentation seg)
	{
		GeneralStudyModule genStudy = seg.getGeneralStudyModule();
		genStudy.setStudyInstanceUid(dcm.getString(Tag.StudyInstanceUID));
		genStudy.setStudyDate(dcm.getString(Tag.StudyDate, ""));
		genStudy.setStudyTime(dcm.getString(Tag.StudyTime, ""));
		genStudy.setStudyId(dcm.getString(Tag.StudyID, ""));
		genStudy.setAccessionNumber(dcm.getString(Tag.AccessionNumber, ""));
		genStudy.setReferringPhysician(
			dcm.getString(Tag.ReferringPhysicianName, ""));
		genStudy.setStudyDescription(
			dcm.getString(Tag.StudyDescription, VR.LO, null));
	}

	private void configImagePixel(DicomObject protoDcm, Segmentation seg)
	{
		ImagePixelModule ipModule = seg.getImagePixelModule();
		long[] niftiDims = nifti.getHeader().getDimensions();
		int niftiRows = (int) niftiDims[1];
		int niftiCols = (int) niftiDims[2];
		int nRows = protoDcm.getInt(Tag.Rows);
		int nCols = protoDcm.getInt(Tag.Columns);
		ipModule.setRowCount(nRows);
		ipModule.setColumnCount(nCols);		
	}

	private void configModulesPreCompute(Segmentation seg, String date,
		String time) throws ConversionException
	{
		String ciUid = dcmMap.keySet().iterator().next();
		DicomObject protoDcm = dcmMap.get(ciUid);
		configPatient(protoDcm, seg);
		configClinicalTrialSubject(protoDcm, seg);
		configPatientStudy(protoDcm, seg);
		configClinicalTrialStudy(protoDcm, seg);
		configFrameOfReference(protoDcm, seg);
		configEquipment(protoDcm, seg);
		configSegmentationImage(seg);
		configImagePixel(protoDcm, seg);
		configMultiframeFuncGroups(seg, date, time);
		configSharedFuncGroups((SegmentationSharedFunctionalGroups)
			seg.getMultiframeFunctionalGroupsModule().getSharedFunctionalGroups());
		configDimensions(seg);
		configGeneralStudy(protoDcm, seg);
		configGeneralSeries(seg, date, time);
		configCommonInstRef(protoDcm, seg);
	}

	private void configModulesPostCompute(Segmentation seg)
		throws ConversionException
	{
		MultiframeFunctionalGroupsModule mfFuncGroups =
			seg.getMultiframeFunctionalGroupsModule();
//		mfFuncGroups.setNumberOfFrames(computeTotalFrames());
		configPerFrameFuncGroups((SegmentationPerFrameFunctionalGroups)
			mfFuncGroups.getPerFrameFunctionalGroups());

		// Add segments
		SegmentationImageModule siModule = seg.getSegmentationImageModule();
		segmentMap.values().forEach((x) -> siModule.addSegment(x));
	}

	private void configMultiframeFuncGroups(Segmentation seg, String date,
		String time)
	{
		MultiframeFunctionalGroupsModule mfFuncGroups = 
			seg.getMultiframeFunctionalGroupsModule();
		int nFrames = computeTotalFrames();
		mfFuncGroups.setInstanceNumber(1);
		mfFuncGroups.setContentDate(date);
		mfFuncGroups.setContentTime(time);
		mfFuncGroups.setNumberOfFrames(nFrames);
	}

	private void configPatient(DicomObject dcm, Segmentation seg)
	{
		PatientModule patient = seg.getPatientModule();
		patient.setPatientBirthDate(dcm.getString(Tag.PatientBirthDate, ""));
		patient.setPatientId(dcm.getString(Tag.PatientID, ""));
		patient.setPatientName(dcm.getString(Tag.PatientName, ""));
		patient.setPatientSex(dcm.getString(Tag.PatientSex));
	}

	private void configPatientStudy(DicomObject dcm, Segmentation seg)
	{
		logger.debug("configPatientStudy() not implemented yet");
	}

	private void configSegmentationImage(Segmentation seg)
	{
		SegmentationImageModule siModule = seg.getSegmentationImageModule();
		siModule.setContentLabel(createContentLabel());
		siModule.setImageComments("NOT FOR CLINICAL USE");
	}

	private Segmentation convertImpl(Nifti nifti, Map<String,DicomObject> dcmMap)
		throws ConversionException
	{
		checkNifti(nifti);
		this.nifti = nifti;
		setDicomMap(dcmMap);
		strict = true;

		// Cache the various items for quick search
		buildMaps();

		// Check the dimensions of each dataset.
		checkDimensions();

		Date now = new Date();
		String date = DicomUtils.formatDate(now);
		String time = DicomUtils.formatTime(now, true);

		Segmentation seg = Iods.segmentation(strict);
		configModulesPreCompute(seg, date, time);

		// Compute pixel data for new SEG
		computeFrames(seg);

		configModulesPostCompute(seg);

		return seg;
	}

	private boolean doubleEqual(double a, double b)
	{
		return doubleEqual(a, b, 0.0001);
	}

	private boolean doubleEqual(double a, double b, double tolerance)
	{
		if (a == 0.0)
		{
			//	Avoid a div-by-zero
			return Math.abs(b) < tolerance;
		}
		double scale = Math.abs((a-b)/a);
		return scale < tolerance;
	}

	private double[] getNiftiOrientation() throws ConversionException
	{
		double[] ipp = new double[] {Double.NaN, Double.NaN, Double.NaN,
			Double.NaN, Double.NaN, Double.NaN};
		NiftiHeader header = nifti.getHeader();
		if (header.getQFormCode() > 0)
		{
			throw new ConversionException("QForm orientation not supported");
		}
		if (header.getSFormCode() <= 0)
		{
			throw new ConversionException("QForm and SForm codes both not set");
		}
		double[] row = niftiNormalise(
			Arrays.copyOfRange(header.getAffineX(), 0, 3));
		double[] col = niftiNormalise(
			Arrays.copyOfRange(header.getAffineY(), 0, 3));
		System.arraycopy(row, 0, ipp, 0, 3);
		System.arraycopy(col, 0, ipp, 3, 3);

		return ipp;
	}

	private double[] getNiftiPosition() throws ConversionException
	{
		NiftiHeader header = nifti.getHeader();
		double[] niftiPos = new double[] {Double.NaN, Double.NaN, Double.NaN};
		if (header.getQFormCode() > 0)
		{
			// NIFTI x and y are inverted from DICOM
			niftiPos[0] = -header.getQuaternionXShift();
			niftiPos[1] = -header.getQuaternionYShift();
			niftiPos[2] = header.getQuaternionZShift();
			return niftiPos;
		}
		if (header.getSFormCode() <= 0)
		{
			throw new ConversionException("QForm and SForm codes both not set");
		}
		// SForm
		// NIFTI x and y are inverted from DICOM
		niftiPos[0] = -header.getAffineX()[3];
		niftiPos[1] = -header.getAffineY()[3];
		niftiPos[2] = header.getAffineZ()[3];
		return niftiPos;
	}

	private double[] niftiNormalise(double[] arr)
	{
		double length = Math.sqrt(arr[0]*arr[0]+arr[1]*arr[1]+arr[2]*arr[2]);
		// NIFTI x and y are inverted from DICOM
		double[] vec = new double[] {
			-arr[0]/length, -arr[1]/length, arr[2]/length
		};
		// Remove any -0.0 elements
		for (int i=0; i<2; i++)
		{
			vec[i] = (vec[i] != 0.0) ? vec[i] : 0.0;
		}
		return vec;
	}

	private class SegmentInfo
	{
		private final Segment segment;
		private final Map<Integer,BaseToSegmentationConverter.TargetFrame> targetFrames = new TreeMap<>();

		public SegmentInfo(Segment segment)
		{
			this.segment = segment;
		}

		public boolean addTargetFrame(BaseToSegmentationConverter.TargetFrame tf)
		{
			if (tf == null)
			{
				return false;
			}
			targetFrames.put(tf.getFrameInRoiIndex(), tf);
			return true;
		}

		public int getFrameSpan()
		{
			return targetFrames.size();
		}

		public Segment getSegment()
		{
			return segment;
		}

		public List<BaseToSegmentationConverter.TargetFrame> getTargetFrameList()
		{
			return ImmutableList.copyOf(targetFrames.values());
		}

		public boolean removeTargetFrame(BaseToSegmentationConverter.TargetFrame tf)
		{
			if (tf == null)
			{
				return false;
			}
			BaseToSegmentationConverter.TargetFrame removed = targetFrames.remove(tf.getFrameInRoiIndex());
			return (removed != null);
		}

	}

	private class Image3DInfo implements Comparable<Image3DInfo>
	{
		public final double[] column;
		public final DicomObject dcm;
		public final int frame;
		public final double[] ipp;
		public final double location;
		public final double[] pixDims;
		public final double[] row;
		public final double thickness;

		public Image3DInfo(DicomObject dcm, int frame) throws ConversionException
		{
			this.dcm = dcm;
			this.frame = frame;
			this.ipp = getImagePositionPatient(dcm, isMultiFrame, frame);
			double[] iop = getImageOrientationPatient(dcm, isMultiFrame, frame);
			row = Arrays.copyOfRange(iop, 0, 2);
			column = Arrays.copyOfRange(iop, 3, 5);
			location = DicomUtils.sliceLocation(ipp, iop);
			this.pixDims = getPixelSpacing(dcm, isMultiFrame, frame);
			this.thickness = getSliceThickness(dcm, isMultiFrame, frame);
		}

		@Override
		public int compareTo(Image3DInfo i3d)
		{
			if (location > i3d.location)
			{
				return 1;
			}
			return (location < i3d.location) ? -1 : 0;
		}
	}

	private class Image3DInfoComparator implements Comparator<Image3DInfo>
	{
		@Override
		public int compare(Image3DInfo a, Image3DInfo b)
		{
			return a.compareTo(b);
		}
	}
}
