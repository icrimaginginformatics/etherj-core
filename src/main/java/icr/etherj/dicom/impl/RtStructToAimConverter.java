/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import com.google.common.collect.ImmutableMap;
import icr.etherj.aim.AimToolkit;
import icr.etherj.aim.Code;
import icr.etherj.aim.DicomImageReference;
import icr.etherj.aim.Equipment;
import icr.etherj.aim.Image;
import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.aim.ImageReference;
import icr.etherj.aim.ImageSeries;
import icr.etherj.aim.ImageStudy;
import icr.etherj.aim.Markup;
import icr.etherj.aim.Person;
import icr.etherj.aim.TwoDimensionCoordinate;
import icr.etherj.aim.TwoDimensionGeometricShape;
import icr.etherj.aim.TwoDimensionPoint;
import icr.etherj.aim.TwoDimensionPolyline;
import icr.etherj.aim.User;
import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.Coordinate3D;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.Contour;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.ReferencedFrameOfReference;
import icr.etherj.dicom.iod.RoiContour;
import icr.etherj.dicom.iod.RtReferencedSeries;
import icr.etherj.dicom.iod.RtReferencedStudy;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.impl.DefaultContourImage;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.PatientModule;
import icr.etherj.dicom.iod.module.RoiContourModule;
import icr.etherj.dicom.iod.module.StructureSetModule;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
class RtStructToAimConverter extends BaseRoiConverter
{
	private static final Logger logger =
		LoggerFactory.getLogger(RtStructToAimConverter.class);

	private Map<String,DicomObject> dcmMap;
	private final double granularity = 20.0;
	private final Map<String,String> instanceToSeries = new HashMap<>();
	private final Lock lock = new ReentrantLock();
	private final SortedMap<Long,ContourImage> locatedImages = new TreeMap<>();
	private final Map<Integer,ImageAnnotation> roiToIa = new HashMap<>();
	private final Map<String,String> seriesToStudy = new HashMap<>();

	public ImageAnnotationCollection convert(RtStruct rtStruct,
		Map<String,DicomObject> dcmMap) throws ConversionException
	{
		// Run under a lock to allow instance variables
		try
		{
			lock.lock();
			return convertImpl(rtStruct, dcmMap);
		}
		finally
		{
			clear();
			lock.unlock();
		}
	}

	private void clear()
	{
		dcmMap = null;
		instanceToSeries.clear();
		locatedImages.clear();
		roiToIa.clear();
		seriesToStudy.clear();
	}

	private ImageAnnotationCollection convertImpl(RtStruct rtStruct,
		Map<String,DicomObject> dcmMap) throws ConversionException
	{
		this.dcmMap = ImmutableMap.copyOf(dcmMap);
		ImageAnnotationCollection iac = AimToolkit.getToolkit().createIac();
		try
		{
			processRtStruct(iac, rtStruct);
		}
		catch (IllegalArgumentException ex)
		{
			throw new ConversionException(ex);
		}
		return iac;
	}

	private TwoDimensionGeometricShape createMarkup(Contour contour, int shapeId)
		throws ConversionException
	{
		TwoDimensionGeometricShape markup;
		switch (contour.getContourGeometricType())
		{
			case Contour.ClosedPlanar:
				markup = new TwoDimensionPolyline();
				break;
			case Contour.Point:
				markup = new TwoDimensionPoint();
				break;
			default:
				throw new ConversionException(
					"Unsupported contour geometric type: "+
						contour.getContourGeometricType());
		}
		ContourImage ci = findContourImage(contour);
		DicomObject refDcm = findReferencedInstance(ci);
		populateMarkup(markup, shapeId, contour, ci, refDcm);

		return markup;
	}

	private double dot(double[] p, double[] q)
	{
		return p[0]*q[0]+p[1]*q[1]+p[2]*q[2];
	}

	private ContourImage findContourImage(Contour contour)
		throws ConversionException
	{
		List<ContourImage> ciList = contour.getContourImageList();
		switch (ciList.size())
		{
			case 1:
				return ciList.get(0);
			case 0:
				return findImageByLocation(contour);
			default:
				throw new ConversionException(
					String.format(
						"AIM requires exactly one referenced image per contour. Found %d",
						ciList.size()));
		}
	}

	private Coordinate3D findFurthestFromLine(Coordinate3D x1, Coordinate3D x2,
		List<Coordinate3D> coords)
	{
		// From http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html
		int coordCount = coords.size();
		Coordinate3D far = x1;
		double sqDistMax = 0.0;
		double[] dX1X0 = new double[3];
		double[] dX2X1 = new double[3];
		for (int i=0; i<coordCount; i++)
		{
			Coordinate3D x0 = coords.get(i);
			subtract(dX1X0, x1, x0);
			subtract(dX2X1, x2, x1);
			double sqX2X1 = square(dX2X1);
			double dot = dot(dX1X0, dX2X1);
			double sqDistX0 = (square(dX1X0)*sqX2X1 - dot*dot)/sqX2X1;
			if (sqDistX0 > sqDistMax)
			{
				sqDistMax = sqDistX0;
				far = x0;
			}
		}
		return far;
	}

	private Coordinate3D findFurthestFromPoint(Coordinate3D start, List<Coordinate3D> coords)
	{
		int coordCount = coords.size();
		Coordinate3D far = start;
		double sqDistMax = 0.0;
		for (int i=0; i<coordCount; i++)
		{
			Coordinate3D curr = coords.get(i);
			double dX = start.x-curr.x;
			double dY = start.y-curr.y;
			double dZ = start.z-curr.z;
			double sqDist = dX*dX+dY*dY+dZ*dZ;
			if (sqDist > sqDistMax)
			{
				sqDistMax = sqDist;
				far = curr;
			}
		}
		return far;
	}

	private ContourImage findImageByLocation(Contour contour)
		throws ConversionException
	{
		if (!Contour.ClosedPlanar.equals(contour.getContourGeometricType()))
		{
			// getContourLocation(contour) assumes co-planar triangles can be formed.
			throw new ConversionException(
				"Finding images by location only supported for CLOSED_PLANAR contours");
		}
		if (locatedImages.isEmpty())
		{
			locateAllImages();
		}
		double contourLoc = getContourLocation(contour);
		// Make integer location to within 1/granularity mm
		long loc = Math.round(granularity*contourLoc);
		ContourImage ci = locatedImages.get(loc);
		if (ci == null)
		{
			throw new ConversionException(
				String.format("Unable to locate image for contour at location: %.2f",
					contourLoc));
		}
		return ci;
	}

	private Coordinate3D[] findLargestTriangle(Contour contour)
		throws ConversionException
	{
		List<Coordinate3D> coords = contour.getContourData();
		int coordCount = coords.size();
		switch (coordCount)
		{
			case 0:
			case 1:
			case 2:
				throw new ConversionException(
					"Insufficient points to form planar contour");
			case 3:
				return new Coordinate3D[]
					{coords.get(0), coords.get(1), coords.get(2)};
			default:
		}
		Coordinate3D start = coords.get(0);
		Coordinate3D far = findFurthestFromPoint(start, coords);
		Coordinate3D wide = findFurthestFromLine(start, far, coords);

		return new Coordinate3D[] {start, far, wide};
	}

	private DicomObject findReferencedInstance(ContourImage ci)
		throws ConversionException
	{
		String refSopInstUid = ci.getReferencedSopInstanceUid();
		DicomObject refDcm = dcmMap.get(refSopInstUid);
		if (refDcm == null)
		{
			throw new ConversionException(
				"Referenced image not found for UID: "+
					ci.getReferencedSopInstanceUid());
		}
		if (!ci.getReferencedSopClassUid().equals(
				refDcm.getString(Tag.SOPClassUID)))
		{
			throw new ConversionException(
				String.format("SOP class UID mismatch. Expected: %s Found: %s",
					ci.getReferencedSopClassUid(),
					refDcm.getString(Tag.SOPClassUID)));
		}
		return refDcm;
	}

	private double getContourLocation(Contour contour) throws ConversionException
	{
		// Find a large triangle in the plane of the contour from the contour vertices
		Coordinate3D[] tri = findLargestTriangle(contour);
		// Slice location can be calculated from two vectors and point in the plane
		double[] ori = new double[]
			{tri[0].x-tri[1].x, tri[0].y-tri[1].y, tri[0].z-tri[1].z,
				tri[1].x-tri[2].x, tri[1].y-tri[2].y, tri[1].z-tri[2].z};
		double[] pos = new double[] {tri[1].x, tri[1].y, tri[1].z};
		return DicomUtils.sliceLocation(pos, ori);
	}

	private String getStructureSetDateTime(StructureSetModule ssm)
	{
		String ssDate = ssm.getStructureSetDate();
		String ssTime = ssm.getStructureSetTime();
		if (ssDate.isEmpty() || ssTime.isEmpty())
		{
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			return df.format(new Date());
		}
		return ssDate+ssTime;
	}

	private ImageAnnotation iaForRoi(Map<Integer,ImageAnnotation> roiToIa,
		RoiContour rc) throws ConversionException
	{
		ImageAnnotation ia = roiToIa.get(rc.getReferencedRoiNumber());
		if (ia == null)
		{
			throw new ConversionException(
				"Referenced ROI not found for ROI number: "+
					rc.getReferencedRoiNumber());
		}
		return ia;
	}

	private void locateAllImages() throws ConversionException
	{
		for (DicomObject dcm : dcmMap.values())
		{
			String sopClassUid = dcm.getString(Tag.SOPClassUID);
			if(DicomUtils.isMultiframeImageSopClass(sopClassUid))
			{
				locateFrames(dcm);
				continue;
			}
			ContourImage ci = new DefaultContourImage();
			ci.setReferencedSopClassUid(sopClassUid);
			ci.setReferencedSopInstanceUid(dcm.getString(Tag.SOPInstanceUID));
			double[] pos = getImagePositionPatient(dcm);
			double[] ori = getImageOrientationPatient(dcm);
			// Make integer location to within 1/granularity mm
			long location = Math.round(
				granularity*DicomUtils.sliceLocation(pos, ori));
			locatedImages.put(location, ci);
		}
	}

	private void locateFrames(DicomObject dcm) throws ConversionException
	{
		String sopClassUid = dcm.getString(Tag.SOPClassUID);
		String sopInstUid = dcm.getString(Tag.SOPInstanceUID);
		int frameCount = dcm.getInt(Tag.NumberOfFrames);
		for (int i=1; i<=frameCount; i++)
		{
			ContourImage ci = new DefaultContourImage();
			ci.setReferencedSopClassUid(sopClassUid);
			ci.setReferencedSopInstanceUid(sopInstUid);
			ci.setReferencedFrameNumber(i);
			double[] pos = getImagePositionPatient(dcm, true, i);
			double[] ori = getImageOrientationPatient(dcm, true, i);
			// Make integer location to within 1/granularity mm
			long location = Math.round(
				granularity*DicomUtils.sliceLocation(pos, ori));
			locatedImages.put(location, ci);
		}
	}

	private void populateMarkup(TwoDimensionGeometricShape markup, int shapeId,
		Contour contour, ContourImage ci, DicomObject refDcm)
		throws ConversionException
	{
		String refSopInstUid = ci.getReferencedSopInstanceUid();
		int refFrame = ci.getReferencedFrameNumber();
		boolean isMultiframe = DicomUtils.isMultiframeImageSopClass(
			refDcm.getString(Tag.SOPClassUID));
		double[] pos = getImagePositionPatient(refDcm, isMultiframe,
			refFrame);
		double[] ori = getImageOrientationPatient(refDcm, isMultiframe,
			refFrame);
		double[] row = Arrays.copyOfRange(ori, 0, 3);
		double[] col = Arrays.copyOfRange(ori, 3, 6);
		double[] pixDims = getPixelSpacing(refDcm, isMultiframe, refFrame);
		double[] patCoord = new double[3];

		markup.setIncludeFlag(true);
		markup.setShapeId(shapeId++);
		markup.setImageReferenceUid(refSopInstUid);
		markup.setReferencedFrameNumber(refFrame);
		int i = 0;
		for (Coordinate3D coord3D : contour.getContourData())
		{
			patCoord[0] = coord3D.x;
			patCoord[1] = coord3D.y;
			patCoord[2] = coord3D.z;
			double[] coord2D = DicomUtils.patientCoordToImageCoord(patCoord,
				pos, row, col, pixDims);
			// AIM uses TLHC of pixel instead of centre
			markup.addCoordinate(
				new TwoDimensionCoordinate(i++, coord2D[0]+0.5, coord2D[1]+0.5));
		}
	}

	private void processEquipment(ImageAnnotationCollection iac, RtStruct rtStruct)
	{
		Equipment equip = new Equipment();
		GeneralEquipmentModule gem = rtStruct.getGeneralEquipmentModule();
		equip.setManufacturerName(gem.getManufacturer());
		equip.setManufacturerModelName(gem.getManufacturersModelName());
		equip.setDeviceSerialNumber(gem.getDeviceSerialNumber());
		equip.setSoftwareVersion(gem.getSoftwareVersion());
		iac.setEquipment(equip);
	}

	private void processIaReferences(ImageAnnotation ia)
		throws ConversionException
	{
		Map<String,DicomImageReference> studyToRef = new HashMap<>();
		for (ImageReference imageRef : ia.getReferenceList())
		{
			if (imageRef instanceof DicomImageReference)
			{
				DicomImageReference dcmRef = (DicomImageReference) imageRef;
				studyToRef.put(dcmRef.getStudy().getInstanceUid(), dcmRef);
			}
		}
		for (Markup markup : ia.getMarkupList())
		{
			if (!(markup instanceof TwoDimensionPolyline))
			{
				continue;
			}
			TwoDimensionPolyline polyline = (TwoDimensionPolyline) markup;
			String instUid = polyline.getImageReferenceUid();
			String seriesUid = instanceToSeries.get(instUid);
			if (seriesUid == null)
			{
				throw new ConversionException(
					"No series found for instance UID: "+instUid);
			}
			String studyUid = seriesToStudy.get(seriesUid);
			if (studyUid == null)
			{
				throw new ConversionException(
					"No study found for series UID: "+seriesUid);
			}
			DicomObject refDcm = dcmMap.get(instUid);
			DicomImageReference dcmRef = studyToRef.get(studyUid);
			if (dcmRef == null)
			{
				dcmRef = new DicomImageReference();
				ImageStudy study = new ImageStudy(studyUid);
				dcmRef.setStudy(study);
				study.setStartDate(refDcm.getString(Tag.StudyDate, ""));
				study.setStartTime(refDcm.getString(Tag.StudyTime, ""));
				studyToRef.put(studyUid, dcmRef);
				ia.addReference(dcmRef);
			}
			ImageStudy study = dcmRef.getStudy();
			ImageSeries series = study.getSeries();
			if (series == null)
			{
				series = new ImageSeries(seriesUid);
				Code modality = new Code();
				modality.setCode(refDcm.getString(Tag.Modality));
				series.setModality(modality);
				study.setSeries(series);
			}
			Image image = series.getImage(instUid);
			if (image == null)
			{
				image = new Image(refDcm.getString(Tag.SOPClassUID), instUid);
				series.addImage(image);
			}
		}
	}

	private void processPerson(ImageAnnotationCollection iac, RtStruct rtStruct)
	{
		Person person = new Person();
		PatientModule pm = rtStruct.getPatientModule();
		person.setName(pm.getPatientName());
		person.setId(pm.getPatientId());
		person.setBirthDate(pm.getPatientBirthDate());
		person.setSex(pm.getPatientSex());
		iac.setPerson(person);
	}

	private void processRefFrameOfRef(StructureSetModule ssm)
	{
		for (ReferencedFrameOfReference refFoR : ssm.getReferencedFrameOfReferenceList())
		{
			for (RtReferencedStudy refStudy : refFoR.getRtReferencedStudyList())
			{
				String studyUid = refStudy.getReferencedSopInstanceUid();
				for (RtReferencedSeries refSeries : refStudy.getRtReferencedSeriesList())
				{
					String seriesUid = refSeries.getSeriesInstanceUid();
					seriesToStudy.putIfAbsent(seriesUid, studyUid);
					for (ContourImage ci : refSeries.getContourImageList())
					{
						instanceToSeries.putIfAbsent(ci.getReferencedSopInstanceUid(),
							seriesUid);
					}
				}
			}
		}
	}

	private void processRoiContours(RtStruct rtStruct)
		throws ConversionException
	{
		RoiContourModule rcm = rtStruct.getRoiContourModule();
		for (RoiContour rc : rcm.getRoiContourList())
		{
			String lineColour = "";
			int[] rgb = rc.getRoiDisplayColour();
			if (rgb != null && rgb.length == 3)
			{
				lineColour = Arrays.stream(rgb)
						.mapToObj(String::valueOf)
						.collect(Collectors.joining(","));
			}

			ImageAnnotation ia = iaForRoi(roiToIa, rc);
			int shapeId = 0;
			for (Contour contour : rc.getContourList())
			{
				try
				{
					TwoDimensionGeometricShape markup = createMarkup(contour,
						shapeId++);

					if (lineColour.length() > 0)
					{
						markup.setLineColour(lineColour);
					}

					ia.addMarkup(markup);
				}
				catch (ConversionException ex)
				{
					logger.warn("Error converting contour "+
						contour.getContourNumber()+": "+ex.getMessage());
				}
			}
			Code code = new Code();
			code.setCode("AnyClosedShape");
			ia.addTypeCode(code);
			processIaReferences(ia);
		}
	}

	private void processRtStruct(ImageAnnotationCollection iac, RtStruct rtStruct)
		throws ConversionException
	{
		StructureSetModule ssm = rtStruct.getStructureSetModule();
		iac.setDescription(ssm.getStructureSetLabel());
		iac.setDateTime(getStructureSetDateTime(ssm));
		processPerson(iac, rtStruct);
		processUser(iac);
		processEquipment(iac, rtStruct);
		processStructureSetRois(iac, rtStruct);
	}

	private void processStructureSetRois(ImageAnnotationCollection iac,
		RtStruct rtStruct) throws ConversionException
	{
		StructureSetModule ssm = rtStruct.getStructureSetModule();
		processRefFrameOfRef(ssm);
		List<StructureSetRoi> roiList = ssm.getStructureSetRoiList();
		for (StructureSetRoi roi : roiList)
		{
			ImageAnnotation ia = new ImageAnnotation();
			ia.setName(roi.getRoiName());
			ia.setDateTime(getStructureSetDateTime(ssm));
			ia.setComment(roi.getRoiDescription());
			roiToIa.put(roi.getRoiNumber(), ia);
			iac.addAnnotation(ia);
		}
		processRoiContours(rtStruct);
	}

	private void processUser(ImageAnnotationCollection iac)
	{
		User user = new User();
		user.setLoginName(System.getProperty("user.name"));
		iac.setUser(user);
	}

	private double square(double[] p)
	{
		return p[0]*p[0]+p[1]*p[1]+p[2]*p[2];
	}

	private void subtract(double[] dPQ, Coordinate3D p, Coordinate3D q)
	{
		dPQ[0] = p.x-q.x;
		dPQ[1] = p.y-q.y;
		dPQ[2] = p.z-q.z;
	}

}
