/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import icr.etherj.StringUtils;
import icr.etherj.aim.Equipment;
import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.aim.Markup;
import icr.etherj.aim.Person;
import icr.etherj.aim.TwoDimensionCoordinate;
import icr.etherj.aim.TwoDimensionGeometricShape;
import icr.etherj.aim.TwoDimensionPolyline;
import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.Segment;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.dicom.iod.impl.DefaultContourImage;
import icr.etherj.dicom.iod.impl.DefaultSegment;
import icr.etherj.dicom.iod.impl.DefaultSegmentation;
import icr.etherj.dicom.iod.module.EnhancedGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.FrameOfReferenceModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import icr.etherj.dicom.iod.module.PatientModule;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;

/**
 *
 * @author jamesd
 */
abstract class BaseAimToSegmentationConverter
	extends BaseToSegmentationConverter
	implements AimToSegmentationConverter
{	
	protected ImageAnnotationCollection iac;
	protected final Map<Integer,ImageAnnotationInfo> iaInfoMap = new TreeMap<>();
	protected final Map<Integer,Table<String,Integer,TargetFrame>> iaTargetFrames =
		new TreeMap<>();

	private final Lock lock = new ReentrantLock();

	@Override
	public final Segmentation convert(ImageAnnotationCollection iac,
		Map<String,DicomObject> dcmMap) throws ConversionException
	{
		try
		{
			lock.lock();
			return convertImpl(iac, dcmMap);
		}
		finally
		{
			clearInternal();
			lock.unlock();
		}
	}

	@Override
	protected final void clearInternal()
	{
		super.clearInternal();
		this.iac = null;
		iaInfoMap.clear();
		iaTargetFrames.clear();
	}

	@Override
	protected final int computeTotalFrames()
	{
		int nFrames = 0;
		for (ImageAnnotationInfo iai : iaInfoMap.values())
		{
			nFrames += iai.getFrameSpan();
		}
		return nFrames;
	}

	@Override
	protected final String createContentLabel()
	{
		return iac.getDescription();
	}
	
	protected abstract SourceFrame createSourceImage(DicomObject dcm,
		boolean isMultiFrame, ContourImage ci) throws ConversionException;

	private ContourImage buildContourImage(TwoDimensionGeometricShape shape)
	{
		String uid = shape.getImageReferenceUid();
		ContourImage ci = new DefaultContourImage();
		DicomObject dcm = dcmMap.get(uid);
		ci.setReferencedSopInstanceUid(uid);
		ci.setReferencedSopClassUid(dcm.getString(Tag.SOPClassUID));
		ci.setReferencedFrameNumber(shape.getReferencedFrameNumber());

		return ci;
	}

	private ImageAnnotationInfo buildImageAnnotationInfo(ImageAnnotation ia,
		int refSegNumber) throws ConversionException
	{
		if (ia.getTypeCode("AnyClosedShape") == null)
		{
			return null;
		}

		Table<String,Integer,SourceFrame> ciFrameTable = TreeBasedTable.create();
		int frameSpan = 0;
		for (Markup markup : ia.getMarkupList())
		{
			TwoDimensionGeometricShape shape = getShape(markup);
			if (shape == null)
			{
				// Only 2D polylines supported
				continue;
			}
			ContourImage ci = buildContourImage(shape);
			String uid = ci.getReferencedSopInstanceUid();
			contourImages.putIfAbsent(uid, ci);
			int refFrameIdx = ci.getReferencedFrameNumber();
			if (!ciFrameTable.contains(uid, refFrameIdx))
			{
				DicomObject dcm = dcmMap.get(uid);
				SourceFrame sf = createSourceImage(dcm, isMultiFrame, ci);
				ciFrameTable.put(uid, refFrameIdx, sf);
				frameSpan++;
			}
		}
		if (frameSpan == 0)
		{
			return null;
		}
		ImageAnnotationInfo iai = new ImageAnnotationInfo(ia, refSegNumber);
		iai.setFrameSpan(frameSpan);
		List<SourceFrame> sourceList = new ArrayList<>();
		sourceList.addAll(ciFrameTable.values());
		Collections.sort(sourceList);
		int idx = 1;
		for (SourceFrame sf : sourceList)
		{
			TargetFrame tf = new TargetFrame(refSegNumber, idx, sf);
			iai.addTargetFrame(tf);
			idx++;
		}
		
		return iai;
	}

	private void buildMaps() throws ConversionException
	{
		int number = 1;
		for (ImageAnnotation ia : iac.getAnnotationList())
		{
			// Store assorted info about each ImageAnnotation and its target frames
			ImageAnnotationInfo iai = buildImageAnnotationInfo(ia, number);
			// ia doesn't describe a valid segment
			if (iai == null)
			{
				continue;
			}
			iaInfoMap.put(number, iai);

			Segment segment = buildSegment(ia, number);
			segmentMap.put(number, segment);

			number++;
		}
		// Set the overall frame index for each target frame
		int frameIdx = 1;
		for (ImageAnnotationInfo iai : iaInfoMap.values())
		{
			Table<String,Integer,TargetFrame> table = TreeBasedTable.create();
			iaTargetFrames.put(iai.getReferencedSegmentNumber(), table);
			for (TargetFrame tf : iai.getTargetFrameList())
			{
				tf.setFrameIndex(frameIdx);
				frameIdx++;
				// Insert into table for later search when storing masks
				ContourImage ci = tf.getContourImage();
				String ciUid = ci.getReferencedSopInstanceUid();
				table.put(ciUid, ci.getReferencedFrameNumber(), tf);
				// Update overall set of ci UIDs
				contourImages.put(ciUid, ci);
			}
		}
	}

	private Segment buildSegment(ImageAnnotation ia, int number)
	{
		Segment segment = new DefaultSegment();
		segment.setSegmentNumber(number);
		String label = ia.getName();
		if (StringUtils.isNullOrEmpty(label))
		{
			label = String.format("Segment_%05d", number);
		}
		segment.setSegmentLabel(label);
		segment.setSegmentAlgorithmType(Constants.Manual);
		segment.setSegmentAlgorithmName(null);
		Code catCode = segment.getSegmentedPropertyCategoryCode();
		setSegPropDefaults(catCode);
		Code typeCode = segment.getSegmentedPropertyTypeCode();
		setSegPropDefaults(typeCode);

		return segment;
	}

	private LineSegmentInfo computeLineSegmentsAndRanges(
		List<TwoDimensionCoordinate> coords, int nRows, int nCols)
	{
		int nCoords = coords.size();
		double[] x = new double[nCoords];
		double[] y = new double[nCoords];
		int[] xRange = new int[] {nCols, 0};
		int[] yRange = new int[] {nRows, 0};
		for (int i=0; i<nCoords; i++)
		{
			TwoDimensionCoordinate coord = coords.get(i);
			// AIM uses TLHC of pixel not centre
			x[i] = coord.getX()-0.5;
			y[i] = coord.getY()-0.5;
			// Update the min/max ranges
			int value = (int) Math.floor(x[i]);
			xRange[0] = (value < xRange[0]) ? value : xRange[0];
			value = (int) Math.ceil(x[i]);
			xRange[1] = (value > xRange[1]) ? value : xRange[1];
			value = (int) Math.floor(y[i]);
			yRange[0] = (value < yRange[0]) ? value : yRange[0];
			value = (int) Math.ceil(y[i]);
			yRange[1] = (value > yRange[1]) ? value : yRange[1];
		}

		List<LineSegment> lines = new ArrayList<>();
		for (int i=0; i<(nCoords-1); i++)
		{
			lines.add(new LineSegment(x[i], y[i], x[i+1], y[i+1]));
		}
		if ((Math.abs(x[nCoords-1]-x[0]) < 1e-6) &&
			 (Math.abs(y[nCoords-1]-y[0]) < 1e-6))
		{
			// First and last points are close enough to be the same i.e.
			// closed polygon
			lines.add(new LineSegment(x[nCoords-2], y[nCoords-2], x[0], y[0]));
		}
		else
		{
			// Close the polygon
			lines.add(new LineSegment(x[nCoords-1], y[nCoords-1], x[0], y[0]));
		}

		return new LineSegmentInfo(lines, nRows, nCols, xRange, yRange);
	}

	private void configEquipment(Segmentation seg)
	{
		Equipment equip = iac.getEquipment();
		String serial = null;
		String manufacturer = null;
		String modelName = null;
		String software = null;
		if (equip != null)
		{
			serial = equip.getDeviceSerialNumber();
			manufacturer = equip.getManufacturerName();
			modelName = equip.getManufacturerModelName();
			software = equip.getSoftwareVersion();
		}
		EnhancedGeneralEquipmentModule module = seg.getEnhancedEquipmentModule();
		module.setDeviceSerialNumber(!StringUtils.isNullOrEmpty(serial)
			? serial : IodUtils.getDummyString(VR.LO));
		module.setManufacturer(!StringUtils.isNullOrEmpty(manufacturer)
			? manufacturer : IodUtils.getDummyString(VR.LO));
		module.setManufacturersModelName(!StringUtils.isNullOrEmpty(modelName)
			? modelName : IodUtils.getDummyString(VR.LO));
		module.setSoftwareVersion(!StringUtils.isNullOrEmpty(software)
			? software : IodUtils.getDummyString(VR.LO));
	}

	private void configFrameOfReference(Segmentation seg)
	{
		DicomObject dcm = dcmMap.values().iterator().next();
		FrameOfReferenceModule module = seg.getFrameOfReferenceModule();
		module.setFrameOfReferenceUid(dcm.getString(Tag.FrameOfReferenceUID));
	}

	private void configGeneralStudy(Segmentation seg)
	{
		DicomObject dcm = dcmMap.values().iterator().next();
		GeneralStudyModule module = seg.getGeneralStudyModule();
		module.setStudyInstanceUid(dcm.getString(Tag.StudyInstanceUID));
	}

	private void configModules(Segmentation seg)
	{
		configPatient(seg);
		configGeneralStudy(seg);
		configFrameOfReference(seg);
		configEquipment(seg);
	}

	private void configPatient(Segmentation seg)
	{
		Person person = iac.getPerson();
		if (person == null)
		{
			return;
		}
		PatientModule module = seg.getPatientModule();
		module.setPatientName(person.getName());
		module.setPatientId(person.getId());
		module.setPatientBirthDate(person.getBirthDate());
		module.setPatientSex(person.getSex());
	}

	private void convertAndStore(ImageAnnotationInfo iai,
		TwoDimensionGeometricShape shape, Segmentation seg, byte[] pixelData)
	{
		ImagePixelModule sipModule = seg.getImagePixelModule();
		int nRows = sipModule.getRowCount();
		int nCols = sipModule.getColumnCount();

		TargetFrame tf = iaTargetFrames.get(iai.getReferencedSegmentNumber())
			.get(shape.getImageReferenceUid(), shape.getReferencedFrameNumber());
		ContourImage ci = tf.getContourImage();

		// Build line segment data
		LineSegmentInfo lsi = computeLineSegmentsAndRanges(
			shape.getCoordinateList(), nRows, nCols);
		// Check each pixel for being inside the contour
		byte[] mask = computeMask(lsi);
		// Copy mask to correct part of pixelData
		copyMaskToPixelData(iai, ci, lsi, mask, pixelData);
	}

	private Segmentation convertImpl(ImageAnnotationCollection iac,
		Map<String,DicomObject> dcmMap) throws ConversionException
	{
		if (iac == null)
		{
			throw new ConversionException("ImageAnnotationCollection must not be null");
		}
		this.iac = iac;
		setDicomMap(dcmMap);
		strict = true;
		Segmentation seg = new DefaultSegmentation(strict);

		buildMaps();
		configModules(seg);

		// Fix up some more modules in the SEG and get the pixel data array ready
		byte[] pixelData = configAndPreAllocate(seg);
		// Convert polys to masks and store
		processIas(seg, pixelData);

		// Store all results
		seg.getImagePixelModule().setPixelData(pixelData, true);

		return seg;
	}

	private void copyMaskToPixelData(ImageAnnotationInfo iai, ContourImage ci,
		LineSegmentInfo lsi, byte[] mask, byte[] pixelData)
	{
		TargetFrame tf = iaTargetFrames.get(iai.getReferencedSegmentNumber())
			.get(ci.getReferencedSopInstanceUid(), ci.getReferencedFrameNumber());
		int frameOffset = lsi.nRows*lsi.nCols*(tf.getFrameIndex()-1);
		// Only copy within x and y min-max ranges
		for (int iY=lsi.yRange[0]; iY<lsi.yRange[1]; iY++)
		{
			for (int iX=lsi.xRange[0]; iX<lsi.xRange[1]; iX++)
			{
				int maskOffset = iY*lsi.nCols+iX;
				byte value = mask[maskOffset];
				if (value != 0)
				{
					pixelData[frameOffset+maskOffset] = value;
				}
			}
		}
	}

	private TwoDimensionGeometricShape getShape(Markup markup)
	{
		if (markup instanceof TwoDimensionPolyline)
		{
			return (TwoDimensionPolyline) markup;
		}
		return null;
	}

	private void processIas(Segmentation seg, byte[] pixelData)
	{
		for (ImageAnnotationInfo iai : iaInfoMap.values())
		{
			for (Markup markup : iai.getImageAnnotation().getMarkupList())
			{
				TwoDimensionGeometricShape shape = getShape(markup);
				if (shape != null)
				{
					convertAndStore(iai, shape, seg, pixelData);
				}
			}
		}
	}

	protected final class ImageAnnotationInfo
	{
		private int frameSpan;
		private final ImageAnnotation ia;
		private final int refSegNumber;
		private final Map<Integer,TargetFrame> targetFrames = new TreeMap<>();

		public ImageAnnotationInfo(ImageAnnotation ia, int refSegNumber)
		{
			this.ia = ia;
			this.refSegNumber = refSegNumber;
		}

		public boolean addTargetFrame(TargetFrame tf)
		{
			if (tf == null)
			{
				return false;
			}
			targetFrames.put(tf.getFrameInRoiIndex(), tf);
			return true;
		}

		public int getFrameSpan()
		{
			return frameSpan;
		}

		public ImageAnnotation getImageAnnotation()
		{
			return ia;
		}

		public int getReferencedSegmentNumber()
		{
			return refSegNumber;
		}

		public List<TargetFrame> getTargetFrameList()
		{
			return ImmutableList.copyOf(targetFrames.values());
		}

		public void setFrameSpan(int span)
		{
			frameSpan = span;
		}
	}

}
