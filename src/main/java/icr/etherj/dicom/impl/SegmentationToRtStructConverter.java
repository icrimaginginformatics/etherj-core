/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.Coordinate2D;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.RLE;
import icr.etherj.dicom.SegUtils;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.Contour;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.DerivationImage;
import icr.etherj.dicom.iod.FunctionalGroupsFrame;
import icr.etherj.dicom.iod.PerFrameFunctionalGroups;
import icr.etherj.dicom.iod.PixelMeasures;
import icr.etherj.dicom.iod.ReferencedFrameOfReference;
import icr.etherj.dicom.iod.RoiContour;
import icr.etherj.dicom.iod.RtReferencedSeries;
import icr.etherj.dicom.iod.RtReferencedStudy;
import icr.etherj.dicom.iod.RtRoiObservation;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.Segment;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.dicom.iod.SegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.SegmentationSharedFunctionalGroups;
import icr.etherj.dicom.iod.SharedFunctionalGroups;
import icr.etherj.dicom.iod.SourceImage;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.impl.DefaultContour;
import icr.etherj.dicom.iod.impl.DefaultContourImage;
import icr.etherj.dicom.iod.impl.DefaultReferencedFrameOfReference;
import icr.etherj.dicom.iod.impl.DefaultRoiContour;
import icr.etherj.dicom.iod.impl.DefaultRtReferencedSeries;
import icr.etherj.dicom.iod.impl.DefaultRtReferencedStudy;
import icr.etherj.dicom.iod.impl.DefaultRtRoiObservation;
import icr.etherj.dicom.iod.impl.DefaultRtStruct;
import icr.etherj.dicom.iod.impl.DefaultStructureSetRoi;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.RoiContourModule;
import icr.etherj.dicom.iod.module.RtRoiObservationsModule;
import icr.etherj.dicom.iod.module.SegmentationImageModule;
import icr.etherj.dicom.iod.module.StructureSetModule;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Uses Marching Squares to generate contours.
 * Bit arrangement:
 * +----+
 * |4  8|
 * |    |
 * |    |
 * |2  1|
 * +----+
 * 
 * Line segments for each config (Marching Squares Index):
 *  0 = +----+   1 = +----+   2 = +----+   3 = +----+
 *      |    |       |    |       |    |       |    |
 *      |    |       |    |       |    |       |____|
 *      |    |       |   /|       |\   |       |    |
 *      |    |       |  /#|       |#\  |       |#  #|
 *      +----+       +----+       +----+       +----+
 *
 *  4 = +----+   5 = +----+   6 = +----+   7 = +----+
 *      |#/  |       |# \ |       |#|  |       |# \ |
 *      |/   |       |   \|       | |  |       |   \|
 *      |    |       |\   |       | |  |       |    |
 *      |    |       | \ #|       |#|  |       |#  #|
 *      +----+       +----+       +----+       +----+
 *
 *  8 = +----+   9 = +----+  10 = +----+  11 = +----+
 *      |  \#|       |  |#|       | / #|       | / #|
 *      |   \|       |  | |       |/   |       |/   |
 *      |    |       |  | |       |   /|       |    |
 *      |    |       |  |#|       |# / |       |#  #|
 *      +----+       +----+       +----+       +----+
 *
 * 12 = +----+  13 = +----+  15 = +----+  15 = +----+
 *      |#  #|       |#  #|       |#  #|       |#  #|
 *      |____|       |    |       |    |       |    |
 *      |    |       |\   |       |   /|       |    |
 *      |    |       | \ #|       |# / |       |#  #|
 *      +----+       +----+       +----+       +----+
 *
 * All contours are considered to be clockwise. Searching from TL requires each
 * contour to start at MSI == 1. Algorithm for contour tracing prefers tightest
 * turns when faced with ambiguity.
 * 
 * @author jamesd
 */
final class SegmentationToRtStructConverter extends BaseRoiConverter
{
	private static final Logger logger =
		LoggerFactory.getLogger(SegmentationToRtStructConverter.class);

	private Map<String,DicomObject> dcmMap;
	private final Map<String,ContourImage> contourImages = new HashMap<>();
	private final Map<Integer,FrameInfo> frameInfoMap = new TreeMap<>();
	private boolean isMultiFrame = false;
	private final Lock lock = new ReentrantLock();
	private final Map<Integer,RoiContour> roiContourMap = new TreeMap<>();
	private final Map<Integer,RtRoiObservation> rtRoiObsMap = new TreeMap<>();
	private Segmentation seg;
	private final Map<Integer,Segment> segmentMap = new TreeMap<>();
	private final Map<Integer,StructureSetRoi> ssRoiMap = new TreeMap<>();
	private boolean strict = true;

	public RtStruct convert(Segmentation seg, Map<String, DicomObject> dcmMap,
		ExecutorService service) throws ConversionException
	{
		try
		{
			lock.lock();
			return convertImpl(seg, dcmMap, service);
		}
		finally
		{
			clearInternal();
			lock.unlock();
		}
	}

	private void buildFrameInfoAndRefImages()
		throws ConversionException
	{
		MultiframeFunctionalGroupsModule mfFuncGroups =
			seg.getMultiframeFunctionalGroupsModule();
		SegmentationSharedFunctionalGroups sharedFuncGroups =
			(SegmentationSharedFunctionalGroups)
				mfFuncGroups.getSharedFunctionalGroups();
		PerFrameFunctionalGroups pfFuncGroups =
			mfFuncGroups.getPerFrameFunctionalGroups();
		int frameIdx = 1;
		for (FunctionalGroupsFrame frame : pfFuncGroups.getFrameList())
		{
			if (!(frame instanceof SegmentationFunctionalGroupsFrame))
			{
				continue;
			}
			SegmentationFunctionalGroupsFrame sf =
				(SegmentationFunctionalGroupsFrame) frame;
			FrameInfo nativeInfo = buildFrameInfoNative(sharedFuncGroups, sf,
				frameIdx);
			if (nativeInfo != null)
			{
				frameInfoMap.put(frameIdx, nativeInfo);
			}
			for (DerivationImage di : sf.getDerivationImages())
			{
				for (SourceImage si : di.getSourceImageList())
				{
					ContourImage ci = new DefaultContourImage();
					si.cloneTo(ci);
					contourImages.putIfAbsent(
						ci.getReferencedSopInstanceUid(), ci);
					if (nativeInfo != null)
					{
						nativeInfo.setContourImage(ci);
					}
					else
					{
						if (!frameInfoMap.containsKey(frameIdx))
						{
							FrameInfo derivInfo = buildFrameInfoDeriv(frameIdx, ci);
							if (derivInfo != null)
							{
								frameInfoMap.put(frameIdx, derivInfo);
							}
						}
					}
				}
			}
			frameIdx++;
		}
	}

	private FrameInfo buildFrameInfoDeriv(int frameIdx, ContourImage ci)
		throws ConversionException
	{
		String uid = ci.getReferencedSopInstanceUid();
		int refFrame = ci.getReferencedFrameNumber();
		DicomObject dcm = dcmMap.get(uid);
		if (dcm == null)
		{
			return null;
		}
		double[] iop = getImageOrientationPatient(dcm, isMultiFrame, refFrame);
		double[] ipp = getImagePositionPatient(dcm, isMultiFrame, refFrame);
		double[] pixDims = getPixelSpacing(dcm, isMultiFrame, refFrame);
		if ((iop.length != 6) || (ipp.length != 3) || (pixDims.length != 2))
		{
			return null;
		}
		FrameInfo frameInfo = new FrameInfo(frameIdx, ipp, iop, pixDims);
		frameInfo.setContourImage(ci);
		return frameInfo;
	}

	private FrameInfo buildFrameInfoNative(
		SegmentationSharedFunctionalGroups sharedFuncGroups,
		SegmentationFunctionalGroupsFrame frame, int frameIdx)
	{
		PixelMeasures pm = sharedFuncGroups.getPixelMeasures();
		if (pm == null)
		{
			return null;
		}
		double[] pixDims = pm.getPixelSpacing();
		double[] iop = sharedFuncGroups.getImageOrientationPatient();
		double[] ipp = frame.getImagePositionPatient();
		if ((iop.length != 6) || (ipp.length != 3) || (pixDims.length != 2))
		{
			return null;
		}
		return new FrameInfo(frameIdx, ipp, iop, pixDims);
	}

	private void buildMaps() throws ConversionException
	{
		SegmentationImageModule sim = seg.getSegmentationImageModule();
		for (Segment segment : sim.getSegmentList())
		{
			int number = segment.getSegmentNumber();
			segmentMap.put(number, segment);

			// Create and store a structure set ROI and ROI contour per segment
			StructureSetRoi ssRoi = buildStructureSetRoi(segment);
			ssRoiMap.put(number, ssRoi);
			RoiContour rc = buildRoiContour(segment);
			roiContourMap.put(number, rc);
			RtRoiObservation rro = buildRtRoiObservation(segment);
			rtRoiObsMap.put(number, rro);
		}
		buildFrameInfoAndRefImages();
		if (contourImages.isEmpty())
		{
			throw new ConversionException("No source images found");
		}
		if (frameInfoMap.isEmpty())
		{
			throw new ConversionException("No frame info found");
		}
	}

	private RoiContour buildRoiContour(Segment segment)
	{
		int number = segment.getSegmentNumber();
		RoiContour rc = new DefaultRoiContour();
		rc.setReferencedRoiNumber(number);

		return rc;
	}

	private RtRoiObservation buildRtRoiObservation(Segment segment)
	{
		int number = segment.getSegmentNumber();
		RtRoiObservation rro = new DefaultRtRoiObservation();
		rro.setObservationNumber(number);
		rro.setReferencedRoiNumber(number);

		return rro;
	}

	private StructureSetRoi buildStructureSetRoi(Segment segment)
	{
		int number = segment.getSegmentNumber();
		StructureSetRoi ssRoi = new DefaultStructureSetRoi();
		ssRoi.setRoiNumber(number);
		ssRoi.setRoiName(segment.getSegmentLabel());
		ssRoi.setReferencedFrameOfReferenceUid(
			seg.getFrameOfReferenceModule().getFrameOfReferenceUid());
		ssRoi.setRoiGenerationAlgorithm(StructureSetRoi.Automatic);

		return ssRoi;
	}

	private void check3D() throws ConversionException
	{
	}

	private void clearInternal()
	{
		contourImages.clear();
		frameInfoMap.clear();
		roiContourMap.clear();
		rtRoiObsMap.clear();
		segmentMap.clear();
		ssRoiMap.clear();
		seg = null;
	}

	private void cloneCommonModules(RtStruct rts)
	{
		seg.getPatientModule().cloneTo(rts.getPatientModule());
		seg.getClinicalTrialSubjectModule().cloneTo(
			rts.getClinicalTrialSubjectModule());
		seg.getGeneralStudyModule().cloneTo(
			rts.getGeneralStudyModule());
		seg.getPatientStudyModule().cloneTo(
			rts.getPatientStudyModule());
		seg.getClinicalTrialStudyModule().cloneTo(
			rts.getClinicalTrialStudyModule());
		seg.getFrameOfReferenceModule().cloneTo(rts.getFrameOfReferenceModule());
	}

	private List<Contour> computeContours(SegmentationFunctionalGroupsFrame frame,
		int frameIdx, byte[] msIndices) throws ConversionException
	{
		FrameInfo frameInfo = frameInfoMap.get(frameIdx);
		List<Contour> contours = new ArrayList<>();
		int startIdx = findContourStart(msIndices);
		while (startIdx >= 0)
		{
			List<Coordinate2D> coords2D = traceContour(msIndices, startIdx);
			if (coords2D.isEmpty())
			{
				break;
			}
			Contour contour = new DefaultContour();
			contour.setContourGeometricType(Contour.ClosedPlanar);
			coords2D.forEach((x) -> contour.addCoordinate(
				DicomUtils.imageCoordToPatientCoord3D(
					frameInfo.getImagePositionPatient(),
					frameInfo.getRowVector(),
					frameInfo.getColumnVector(),
					frameInfo.getPixelDimensions(),
					x.column, x.row)));
			contours.add(contour);

			// Detect next contour, if any
			startIdx = findContourStart(msIndices);
		}

		return contours;
	}

	private void configModulesPreCompute(RtStruct rts, String date, String time)
	{
		cloneCommonModules(rts);
		configStructureSetModule(rts, date, time);
		configRoiContourModule(rts);
		configRtRoiObservationModule(rts);
	}

	private void configRoiContourModule(RtStruct rts)
	{
		RoiContourModule rcm = rts.getRoiContourModule();
		roiContourMap.values().forEach((x) -> rcm.addRoiContour(x));
	}

	private void configRtRoiObservationModule(RtStruct rts)
	{
		RtRoiObservationsModule rrom = rts.getRtRoiObservationsModule();
		rtRoiObsMap.values().forEach((x) -> rrom.addRtRoiObservation(x));
	}

	private void configStructureSetModule(RtStruct rts, String date, String time)
	{
		StructureSetModule ssm = rts.getStructureSetModule();
		ssm.setStructureSetDate(date);
		ssm.setStructureSetTime(time);
		SegmentationImageModule sim = seg.getSegmentationImageModule();
		ssm.setStructureSetLabel(sim.getContentLabel());
		ssm.setStructureSetDescription(sim.getContentDescription());
		for (StructureSetRoi ssRoi : ssRoiMap.values())
		{
			ssm.addStructureSetRoi(ssRoi);
		}
		ReferencedFrameOfReference rfor = new DefaultReferencedFrameOfReference();
		ssm.addReferencedFrameOfReference(rfor);
		rfor.setFrameOfReferenceUid(
			seg.getFrameOfReferenceModule().getFrameOfReferenceUid());
		RtReferencedStudy refStudy = new DefaultRtReferencedStudy();
		rfor.addRtReferencedStudy(refStudy);
		String siUid = contourImages.keySet().iterator().next();
		DicomObject protoDcm = dcmMap.get(siUid);
		refStudy.setReferencedSopInstanceUid(protoDcm.getString(
			Tag.StudyInstanceUID));
		RtReferencedSeries refSeries = new DefaultRtReferencedSeries();
		refStudy.addRtReferencedSeries(refSeries);
		refSeries.setSeriesInstanceUid(protoDcm.getString(
			Tag.SeriesInstanceUID));
		for (ContourImage ci : contourImages.values())
		{
			refSeries.addContourImage(ci);
		}
	}

	private RtStruct convertImpl(Segmentation seg, Map<String,DicomObject> dcmMap,
		ExecutorService service) throws ConversionException
	{
		if (seg == null)
		{
			throw new ConversionException("Segmentation must not be null");
		}
		this.seg = seg;
		setDicomMap(dcmMap);
		strict = seg.isStrict();

		// Ensure the input SEG has 3D info required by RTSTRUCT
		check3D();

		// Cache the various items for quick search
		buildMaps();

		Date now = new Date();
		String date = DicomUtils.formatDate(now);
		String time = DicomUtils.formatTime(now, true);

		RtStruct rts = new DefaultRtStruct(strict);
		configModulesPreCompute(rts, date, time);

		// Process frames to produce contours
		processFrames(rts, service);

		return rts;
	}

	private List<Coordinate2D> cullColinear(List<Coordinate2D> coords,
		List<Byte> path)
	{
		List<Coordinate2D> output = new ArrayList<>();
		Coordinate2D startCoord = coords.get(0);
		output.add(startCoord);
		byte curr = path.get(0);
		int idx = 1;
		int lastIdx = path.size();
		byte next;
		while (idx < lastIdx)
		{
			next = path.get(idx);
			if (!isColinear(next, curr))
			{
				// Not colinear with last segment
				output.add(coords.get(idx));
			}
			curr = next;
			idx++;
		}
		output.add(coords.get(coords.size()-1));

		return output;
	}

	private int enterFromAbove(byte[] msIndices, int currIdx, int rowStride,
		int startIdx, Position pos) throws ConversionException
	{
		// Down-Right
		int nextIdx = currIdx+rowStride+1;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromAbove(msIndices[nextIdx]))
		{
			pos.x += 1;
			pos.y += 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		// Down
		nextIdx = currIdx+rowStride;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromAbove(msIndices[nextIdx]))
		{
			pos.y += 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		// Down-Left
		nextIdx = currIdx+rowStride-1;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromAbove(msIndices[nextIdx]))
		{
			pos.x -= 1;
			pos.y += 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		throw new ConversionException(
			"Invalid direction from Marching Squares index: "+
				msIndices[currIdx]+
				" at ("+(currIdx % rowStride)+","+(currIdx / rowStride)+")");
	}

	private int enterFromBelow(byte[] msIndices, int currIdx, int rowStride,
		int startIdx, Position pos) throws ConversionException
	{
		// Up-Right
		int nextIdx = currIdx-rowStride+1;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromBelow(msIndices[nextIdx]))
		{
			pos.x += 1;
			pos.y -= 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		// Up
		nextIdx = currIdx-rowStride;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromBelow(msIndices[nextIdx]))
		{
			pos.y -= 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		// Up-Left
		nextIdx = currIdx-rowStride-1;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromBelow(msIndices[nextIdx]))
		{
			pos.x -= 1;
			pos.y -= 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		throw new ConversionException(
			"Invalid direction from Marching Squares index: "+
				msIndices[currIdx]+
				" at ("+(currIdx % rowStride)+","+(currIdx / rowStride)+")");
	}

	private int enterFromLeft(byte[] msIndices, int currIdx, int rowStride,
		int startIdx, Position pos) throws ConversionException
	{
		// Down-Right
		int nextIdx = currIdx+rowStride+1;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromLeft(msIndices[nextIdx]))
		{
			pos.x += 1;
			pos.y += 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		// Right
		nextIdx = currIdx+1;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromLeft(msIndices[nextIdx]))
		{
			pos.x += 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		// Up-Right
		nextIdx = currIdx-rowStride+1;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromLeft(msIndices[nextIdx]))
		{
			pos.x += 1;
			pos.y -= 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		throw new ConversionException(
			"Invalid direction from Marching Squares index: "+
				msIndices[currIdx]+
				" at ("+(currIdx % rowStride)+","+(currIdx / rowStride)+")");
	}

	private int enterFromRight(byte[] msIndices, int currIdx, int rowStride,
		int startIdx, Position pos) throws ConversionException
	{
		// Up-Left
		int nextIdx = currIdx-rowStride-1;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromRight(msIndices[nextIdx]))
		{
			pos.x -= 1;
			pos.y -= 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		// Left
		nextIdx = currIdx-1;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromRight(msIndices[nextIdx]))
		{
			pos.x -= 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		// Down-Left
		nextIdx = currIdx+rowStride-1;
		if ((nextIdx == startIdx) || isCompatibleEdgeFromRight(msIndices[nextIdx]))
		{
			pos.x -= 1;
			pos.y += 1;
			msIndices[currIdx] = 0;
			return nextIdx;
		}
		throw new ConversionException(
			"Invalid direction from Marching Squares index: "+
				msIndices[currIdx]+
				" at ("+(currIdx % rowStride)+","+(currIdx / rowStride)+")");
	}

	private int findContourStart(byte[] msIndices) throws ConversionException
	{
		/* TL -> BR search. Only four choices:
		 * 1) 0 - no contour, skip
		 * 2) 1 - TL corner of a contour, target found so return
		 * 3) 15 - internal pixel of previously processed contour, zero out
		 * 4) 2-14 - edge of internal contour of previously processed contour,
		 *    log and zero out
		 */
		for (int i=0; i<msIndices.length; i++)
		{
			switch (msIndices[i])
			{
				case 0:
					break;
				case 15:
					msIndices[i] = 0;
					break;
				case 1:
					return i;
				default:
					if (logger.isDebugEnabled())
					{
						logger.debug("Invalid Marching Squares index: "+msIndices[i]+
							" Leftover internal contour?");
					}
					msIndices[i] = 0;
			}
		}
		// No contour detected
		return -1;
	}

	private byte[] getFrameMask(int frameIdx)
	{
		String segType = seg.getSegmentationImageModule().getSegmentationType();
		ImagePixelModule ipm = seg.getImagePixelModule();
		int nRows = ipm.getRowCount();
		int nCols = ipm.getColumnCount();
		int frameBytes = nCols*nRows;
		byte[] frame;
		if (Constants.Binary.equals(segType))
		{
			frame = SegUtils.unpackFrame(ipm.getPixelData(), nRows, nCols,
				frameIdx);
		}
		else
		{
			byte[] pixelData = ipm.getPixelData();
			if (pixelData.length > 0)
			{
				int start = (frameIdx-1)*frameBytes;
				frame = Arrays.copyOfRange(pixelData, start, start+frameBytes);
			}
			else
			{
				frame = ipm.getPixelDataFragments().get(frameIdx);
				frame = RLE.decompressPixels(frame);
			}
		}
		int maskRowStride = nCols+1;
		// Need a 1 element padding to allow for filled pixels at right and bottom
		// edges of the frame
		byte[] mask = new byte[maskRowStride*(nRows+1)];
		for (int i=0; i<nRows; i++)
		{
			int pixRowOffset = i*nCols;
			int maskOffset = i*maskRowStride;
			for (int j=0; j<nCols; j++)
			{
				mask[maskOffset+j] = (frame[pixRowOffset+j] == 0)
					? (byte) 0 : (byte) 1;
			}
		}
		return mask;
	}

	/*
	 *	Four points on the square (BR=1, BL=2, TL=4, TR=8) determine the
	 * configuration index and thus where to draw line segments. Pixel under test
	 * is BR. Input mask is either 0 or 1.
	*/
	private byte[] getMarchingSquaresIndices(byte[] mask)
	{
		ImagePixelModule ipm = seg.getImagePixelModule();
		int nRows = ipm.getRowCount();
		int nCols = ipm.getColumnCount();
		// Need a 1 element padding to allow for filled pixels at right and bottom
		// edges of the frame
		int rowStride = nCols+1;
		byte[] msIndices = new byte[(nRows+1)*rowStride];

		// First row, TL&TR always 0
		msIndices[0] = mask[0];
		for (int i=1; i<nCols; i++)
		{
			msIndices[i] = (byte) (mask[i] + (mask[i-1] << 1));
		}
		msIndices[nCols] = (byte) (mask[nCols-1] << 1);

		// Middle rows, full compute unless first or last element
		int rowOffset;
		for (int i=1; i<nRows; i++)
		{
			rowOffset = i*rowStride;
			msIndices[rowOffset] = (byte)
				(mask[rowOffset] + (mask[rowOffset-rowStride] << 3));
			for (int j=1; j<nCols; j++)
			{
				msIndices[rowOffset+j] = (byte)
					((mask[rowOffset+j]) +
					 (mask[rowOffset+j-1] << 1) +
					 (mask[rowOffset+j-rowStride-1] << 2) +
					 (mask[rowOffset+j-rowStride] << 3));
			}
			msIndices[rowOffset+nCols] = (byte)
				((mask[rowOffset+nCols-1] << 1) +
				 (mask[rowOffset+nCols-rowStride-1] << 2));
		}

		// Last row, BL&BR always 0
		rowOffset = rowStride*nRows;
		msIndices[rowOffset] = (byte) (mask[rowOffset] << 3);
		for (int i=1; i<nCols; i++)
		{
			msIndices[rowOffset+i] = (byte)
				((mask[rowOffset-rowStride+i] << 3) +
				 (mask[rowOffset-rowStride+i-1] << 2));
		}
		msIndices[rowOffset+nCols] = (byte) (mask[rowOffset+nCols-1] << 2);

		return msIndices;
	}

	private int handleHorizEntrySaddle(List<Coordinate2D> coords,
		byte[] msIndices, int prevIdx, int currIdx, int rowStride, Position pos,
		List<Byte> path)
	{
		int nextIdx;
		if (prevIdx < currIdx)
		{
			// Entered from left
			logger.debug("MSI=10 saddle entered from left");
			// Add coordinate
			coords.add(new Coordinate2D(pos.x-0.5, pos.y));
			// Remove ambiguity for other side of saddle
			msIndices[currIdx] = 8;
			// Update position and set next pixel
			pos.y += 1;
			nextIdx = currIdx+rowStride;
			// Record path
			path.add((byte) 2);
		}
		else
		{
			// Entered from right
			logger.debug("MSI=10 saddle entered from right");
			// Add coordinate
			coords.add(new Coordinate2D(pos.x-0.5, pos.y-1.0));
			// Remove ambiguity for other side of saddle
			msIndices[currIdx] = 2;
			// Update position and set next pixel
			pos.y -= 1;
			nextIdx = currIdx-rowStride;
			// Record path
			path.add((byte) 8);
		}
		return nextIdx;
	}

	private int handleVertEntrySaddle(List<Coordinate2D> coords,
		byte[] msIndices, int prevIdx, int currIdx, Position pos, List<Byte> path)
	{
		int nextIdx;
		if (prevIdx < currIdx)
		{
			// Entered from above
			logger.debug("MSI=5 saddle entered from above");
			// Add coordinate
			coords.add(new Coordinate2D(pos.x-1.0, pos.y-0.5));
			// Remove ambiguity for other side of saddle
			msIndices[currIdx] = 1;
			// Update position and set next pixel
			pos.x -= 1;
			nextIdx = currIdx-1;
			// Record path
			path.add((byte) 4);
		}
		else
		{
			// Entered from below
			logger.debug("MSI=5 saddle entered from below");
			// Add coordinate
			coords.add(new Coordinate2D(pos.x, pos.y-0.5));
			// Remove ambiguity for other side of saddle
			msIndices[currIdx] = 4;
			// Update position and set next pixel
			pos.x += 1;
			nextIdx = currIdx+1;
			// Record path
			path.add((byte) 1);
		}
		return nextIdx;
	}

	private boolean isColinear(byte next, byte curr)
	{
		// Verticals and horizontals are simple
		if (next == curr)
		{
			return true;
		}
		// Diagonals alternate between pairs, one with 3 pixels inside then one
		// with 1 pixel inside.
		switch (curr)
		{
			case 1:
				return (next == 11);
			case 2:
				return (next == 7);
			case 4:
				return (next == 14);
			case 7:
				return (next == 2);
			case 8:
				return (next == 13);
			case 11:
				return (next == 1);
			case 13:
				return (next == 8);
			case 14:
				return (next == 4);
			default:
		}
		return false;
	}

	private boolean isCompatibleEdgeFromAbove(byte msIdx)
	{
		return (msIdx == 4) || (msIdx == 6) || (msIdx == 7) || (msIdx == 5);
	}

	private boolean isCompatibleEdgeFromBelow(byte msIdx)
	{
		return (msIdx == 1) || (msIdx == 9) || (msIdx == 13) || (msIdx == 5);
	}

	private boolean isCompatibleEdgeFromLeft(byte msIdx)
	{
		return (msIdx == 2) || (msIdx == 3) || (msIdx == 11) || (msIdx == 10);
	}

	private boolean isCompatibleEdgeFromRight(byte msIdx)
	{
		return (msIdx == 8) || (msIdx == 12) || (msIdx == 14) || (msIdx == 10);
	}

	private void processFrame(SegmentationFunctionalGroupsFrame frame,
		int frameIdx) throws ConversionException
	{
		int refSegNumber = frame.getReferencedSegmentNumber();
		byte[] mask = getFrameMask(frameIdx);
		byte[] msIndices = getMarchingSquaresIndices(mask);
		if (logger.isDebugEnabled())
		{
			logger.debug("Frame: "+frameIdx+" RoiContour: "+refSegNumber+
				" - "+segmentMap.get(refSegNumber).getSegmentLabel());
		}
		List<Contour> contours = computeContours(frame, frameIdx, msIndices);
		if (contours.isEmpty())
		{
			logger.info("Empty frame found. Frame: "+frameIdx+
				" RoiContour: "+refSegNumber);
			return;
		}

		FrameInfo frameInfo = frameInfoMap.get(frameIdx);
		ContourImage ci = null;
		if (frameInfo != null)
		{
			ci = frameInfo.getContourImage();
		}
		RoiContour rc = roiContourMap.get(refSegNumber);
		int contourNumber = rc.getContourCount()+1;
		for (Contour contour : contours)
		{
			contour.setContourNumber(contourNumber);
			contour.addContourImage(ci);
			rc.addContour(contour);
			contourNumber++;
		}
	}

	private void processFrames(RtStruct rts, ExecutorService service)
		throws ConversionException
	{
		if (service != null)
		{
			processFramesMT(rts, service);
			return;
		}
		PerFrameFunctionalGroups pfGroups =
			seg.getMultiframeFunctionalGroupsModule()
				.getPerFrameFunctionalGroups();
		int frameIdx = 1;
		for (FunctionalGroupsFrame frame : pfGroups.getFrameList())
		{
			if ((frame instanceof SegmentationFunctionalGroupsFrame))
			{
				processFrame((SegmentationFunctionalGroupsFrame) frame, frameIdx);
			}
			frameIdx++;
		}
	}

	private void processFramesMT(RtStruct rts, ExecutorService service)
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}
	
	private void setDicomMap(Map<String,DicomObject> dcmMap)
		throws ConversionException
	{
		if (dcmMap.isEmpty())
		{
			throw new ConversionException("Map of referenced instances is empty");
		}
		this.dcmMap = dcmMap;
		DicomObject dcm = dcmMap.values().iterator().next();
		isMultiFrame = DicomUtils.isMultiframeImageSopClass(
			dcm.getString(Tag.SOPClassUID));
	}

	private List<Coordinate2D> traceContour(byte[] msIndices, int startIdx)
		throws ConversionException
	{
		List<Coordinate2D> coords = new ArrayList<>();
		ImagePixelModule ipm = seg.getImagePixelModule();
		int nCols = ipm.getColumnCount();
		// Need a 1 element padding to allow for filled pixels at right and bottom
		// edges of the frame
		int rowStride = nCols+1;
		int x = startIdx % rowStride;
		int y = (startIdx-x) / rowStride;
		// MS index must be 1 i.e. BR corner.
		Coordinate2D start = new Coordinate2D(x-0.5, y);
		Position pos = new Position(x, y);
		coords.add(start);
		// Image indices
		int nextIdx = -1;
		int currIdx = startIdx;
		int prevIdx = -1;
		// MS index path for line compacting
		List<Byte> path = new ArrayList<>(); 
		while (nextIdx != startIdx)
		{
			byte currMsIdx = msIndices[currIdx];
			// Find next pixel, pos is updated inside enterFromX() methods
			switch (currMsIdx)
			{
				case 1:
				case 3:
				case 7:
					// Add coordinate at end of line segment
					coords.add(new Coordinate2D(pos.x, pos.y-0.5));
					// Find next edge voxel position
					nextIdx = enterFromLeft(msIndices, currIdx, rowStride, startIdx,
						pos);
					// Record path
					path.add(currMsIdx);
					break;
				case 2:
				case 6:
				case 14:
					// Add coordinate at end of line segment
					coords.add(new Coordinate2D(pos.x-0.5, pos.y));
					// Find next edge voxel position
					nextIdx = enterFromAbove(msIndices, currIdx, rowStride, startIdx,
						pos);
					// Record path
					path.add(currMsIdx);
					break;
				case 4:
				case 12:
				case 13:
					// Add coordinate at end of line segment
					coords.add(new Coordinate2D(pos.x-1.0, pos.y-0.5));
					// Find next edge voxel position
					nextIdx = enterFromRight(msIndices, currIdx, rowStride, startIdx,
						pos);
					// Record path
					path.add(currMsIdx);
					break;
				case 8:
				case 9:
				case 11:
					// Add coordinate at end of line segment
					coords.add(new Coordinate2D(pos.x-0.5, pos.y-1.0));
					// Find next edge voxel position
					nextIdx = enterFromBelow(msIndices, currIdx, rowStride, startIdx,
						pos);
					// Record path
					path.add(currMsIdx);
					break;
				case 5:
					// Clockwise contour can only enter an MSI=10 pixel from above or
					// below
					nextIdx = handleVertEntrySaddle(coords, msIndices, prevIdx,
						currIdx, pos, path);
					break;
				case 10:
					// Clockwise contour can only enter an MSI=10 pixel from the side
					nextIdx = handleHorizEntrySaddle(coords, msIndices, prevIdx,
						currIdx, rowStride, pos, path);
					break;
				default:
					throw new ConversionException(
						"Invalid Marching Squares index: "+currMsIdx);
			}
			prevIdx = currIdx;
			currIdx = nextIdx;
		}
		coords = cullColinear(coords, path);

		return coords;
	}

	private class FrameInfo
	{
		private ContourImage ci = null;
		private final double[] col;
		private final int idx;
		private final double[] ipp;
		private final double[] pixDims;
		private final double[] row;

		public FrameInfo(int frameIdx, double[] ipp, double[] iop,
			double[] pixDims)
		{
			idx = frameIdx;
			this.ipp = Arrays.copyOf(ipp, ipp.length);
			this.row = Arrays.copyOf(iop, 3);
			this.col = Arrays.copyOfRange(iop, 3, 6);
			this.pixDims = Arrays.copyOf(pixDims, pixDims.length);
		}

		public double[] getColumnVector()
		{
			return col;
		}

		public ContourImage getContourImage()
		{
			return ci;
		}

		public int getFrameIndex()
		{
			return idx;
		}

		public double[] getImagePositionPatient()
		{
			return ipp;
		}

		public double[] getPixelDimensions()
		{
			return pixDims;
		}

		public double[] getRowVector()
		{
			return row;
		}

		public void setContourImage(ContourImage ci)
		{
			this.ci = ci;
		}
	}

	private class Position
	{
		public int x;
		public int y;

		public Position(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
	}

}
