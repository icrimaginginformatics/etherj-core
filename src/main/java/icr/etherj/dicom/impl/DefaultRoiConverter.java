/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.RoiConverter;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.nifti.Nifti;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import org.dcm4che2.data.DicomObject;

/**
 *
 * @author jamesd
 */
final class DefaultRoiConverter implements RoiConverter
{
	@Override
	public ImageAnnotationCollection toIac(RtStruct rtStruct,
		Map<String,DicomObject> dcmMap) throws ConversionException
	{
		RtStructToAimConverter converter = new RtStructToAimConverter();
		return converter.convert(rtStruct, dcmMap);
	}

	@Override
	public Nifti toNifti(Segmentation seg, Map<String, DicomObject> dcmMap)
		throws ConversionException
	{
		SegToNiftiRoiConverter converter = new SegToNiftiRoiConverter();
		return converter.convert(seg, dcmMap);
	}

	@Override
	public RtStruct toRtStruct(ImageAnnotationCollection iac,
		Map<String,DicomObject> dcmMap)
		throws ConversionException
	{
		AimToRtStructConverter converter = new AimToRtStructConverter();
		return converter.convert(iac, dcmMap);
	}

	@Override
	public RtStruct toRtStruct(Segmentation seg, Map<String, DicomObject> dcmMap)
		throws ConversionException
	{
		SegmentationToRtStructConverter converter =
			new SegmentationToRtStructConverter();
		return converter.convert(seg, dcmMap, null);
	}

	@Override
	public Segmentation toSegmentation(ImageAnnotationCollection iac,
		Map<String,DicomObject> dcmMap) throws ConversionException
	{
		return toSegmentation(iac, dcmMap, true);
	}

	@Override
	public Segmentation toSegmentation(ImageAnnotationCollection iac,
		Map<String,DicomObject> dcmMap, boolean full3d)
		throws ConversionException
	{
		if (full3d)
		{
			AimToSegmentationConverter3D converter =
				new AimToSegmentationConverter3D();
			return converter.convert(iac, dcmMap);
		}
		AimToSegmentationConverter2D converter =
			new AimToSegmentationConverter2D();
		return converter.convert(iac, dcmMap);
	}

	@Override
	public Segmentation toSegmentation(Nifti nifti,
		Map<String,DicomObject> dcmMap)
		throws ConversionException
	{
		NiftiToSegmentationConverter converter =
			new NiftiToSegmentationConverter();
		return converter.convert(nifti, dcmMap);
	}
	
	@Override
	public Segmentation toSegmentation(RtStruct rtStruct,
		Map<String,DicomObject> dcmMap) throws ConversionException
	{
		return toSegmentation(rtStruct, dcmMap, null, false);
	}

	@Override
	public Segmentation toSegmentation(RtStruct rtStruct,
		Map<String,DicomObject> dcmMap, boolean useRle) throws ConversionException
	{
		return toSegmentation(rtStruct, dcmMap, null, useRle);
	}

	@Override
	public Segmentation toSegmentation(RtStruct rtStruct,
		Map<String,DicomObject> dcmMap, ExecutorService service)
		throws ConversionException
	{
		return toSegmentation(rtStruct, dcmMap, service, false);
	}

	@Override
	public Segmentation toSegmentation(RtStruct rtStruct,
		Map<String,DicomObject> dcmMap, ExecutorService service, boolean useRle)
		throws ConversionException
	{
		RtStructToSegmentationConverter converter =
			new RtStructToSegmentationConverter();
		return converter.convert(rtStruct, dcmMap, service, useRle);
	}

}
