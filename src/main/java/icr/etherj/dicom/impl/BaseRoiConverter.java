/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.DicomUtils;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
abstract class BaseRoiConverter
{
	protected double[] getImageOrientationPatient(DicomObject refDcm)
		throws ConversionException
	{
		return getImageOrientationPatient(refDcm, false, 1);
	}

	protected double[] getImageOrientationPatient(DicomObject refDcm,
		boolean isMultiframe, int frame) throws ConversionException
	{
		double[] ori;
		if (!isMultiframe)
		{
			ori = refDcm.getDoubles(Tag.ImageOrientationPatient);
		}
		else
		{
			frame = (frame > 0) ? frame : 1;
			ori = refDcm.getDoubles(new int[] {
				Tag.PerFrameFunctionalGroupsSequence, frame-1,
				Tag.PlaneOrientationSequence, 0,
				Tag.ImageOrientationPatient});
			if ((ori == null) || (ori.length != 6))
			{
				ori = refDcm.getDoubles(new int[] {
					Tag.SharedFunctionalGroupsSequence, 0,
					Tag.PlaneOrientationSequence, 0,
					Tag.ImageOrientationPatient});
			}
		}
		if ((ori == null) || (ori.length != 6))
		{
			String extra = (isMultiframe) ? " - Frame: "+frame : "";
			throw new ConversionException(
				"ImageOrientationPatient missing or invalid"+extra);
		}
		return ori;
	}

	protected double[] getImagePositionPatient(DicomObject refDcm)
		throws ConversionException
	{
		return getImagePositionPatient(refDcm, false, 1);
	}

	protected double[] getImagePositionPatient(DicomObject refDcm,
		boolean isMultiframe, int frame) throws ConversionException
	{
		double[] pos;
		if (!isMultiframe)
		{
			pos = refDcm.getDoubles(Tag.ImagePositionPatient);
		}
		else
		{
			pos = refDcm.getDoubles(new int[] {
				Tag.PerFrameFunctionalGroupsSequence, frame-1,
				Tag.PlanePositionSequence, 0,
				Tag.ImagePositionPatient});
		}
		if ((pos == null) || (pos.length != 3))
		{
			String extra = (isMultiframe) ? " - Frame: "+frame : "";
			throw new ConversionException(
				"ImagePositionPatient missing or invalid"+extra);
		}
		return pos;
	}

	protected double[] getPixelSpacing(DicomObject refDcm)
		throws ConversionException
	{
		return getPixelSpacing(refDcm, false, 1);
	}

	protected double[] getPixelSpacing(DicomObject refDcm, boolean isMultiframe,
		int frame) throws ConversionException
	{
		double[] spacing;
		if (!isMultiframe)
		{
			spacing = refDcm.getDoubles(Tag.PixelSpacing);
		}
		else
		{
			spacing = refDcm.getDoubles(new int[] {
				Tag.PerFrameFunctionalGroupsSequence, frame-1,
				Tag.PixelMeasuresSequence, 0,
				Tag.PixelSpacing});
			if ((spacing == null) || (spacing.length != 2))
			{
				spacing = refDcm.getDoubles(new int[] {
					Tag.SharedFunctionalGroupsSequence, 0,
					Tag.PixelMeasuresSequence, 0,
					Tag.PixelSpacing});
			}
		}
		if ((spacing == null) || (spacing.length != 2))
		{
			String extra = (isMultiframe) ? " - Frame: "+frame : "";
			throw new ConversionException(
				"PixelSpacing missing or invalid"+extra);
		}
		return spacing;
	}
	
	protected double getSliceThickness(DicomObject refDcm)
		throws ConversionException
	{
		return getSliceThickness(refDcm, false, 1);
	}

	protected double getSliceThickness(DicomObject refDcm, boolean isMultiframe,
		int frame) throws ConversionException
	{
		if (!isMultiframe)
		{
			return refDcm.getDouble(Tag.SliceThickness);
		}
		DicomObject dcm = DicomUtils.getSequenceObject(refDcm,
			new int[]
			{
				Tag.PerFrameFunctionalGroupsSequence, frame-1,
				Tag.PixelMeasuresSequence, 0
			});
		if (dcm == null)
		{
			dcm = DicomUtils.getSequenceObject(refDcm,
				new int[]
				{
					Tag.SharedFunctionalGroupsSequence, 0,
					Tag.PixelMeasuresSequence, 0
				});
		}
		if (dcm == null)
		{
			String extra = " - Frame: "+frame;
			throw new ConversionException("PixelMeasures missing or invalid"+extra);
		}
		return dcm.getDouble(Tag.SliceThickness);
	}
	
	protected double getSpacingBetweenSlices(DicomObject refDcm)
		throws ConversionException
	{
		return getSliceThickness(refDcm, false, 1);
	}

	protected double getSpacingBetweenSlices(DicomObject refDcm,
		boolean isMultiframe, int frame) throws ConversionException
	{
		if (!isMultiframe)
		{
			return refDcm.getDouble(Tag.SpacingBetweenSlices);
		}
		DicomObject dcm = DicomUtils.getSequenceObject(refDcm,
			new int[]
			{
				Tag.PerFrameFunctionalGroupsSequence, frame-1,
				Tag.PixelMeasuresSequence, 0
			});
		if (dcm == null)
		{
			dcm = DicomUtils.getSequenceObject(refDcm,
				new int[]
				{
					Tag.SharedFunctionalGroupsSequence, 0,
					Tag.PixelMeasuresSequence, 0
				});
		}
		if (dcm == null)
		{
			String extra = " - Frame: "+frame;
			throw new ConversionException("PixelMeasures missing or invalid"+extra);
		}
		return dcm.getDouble(Tag.SpacingBetweenSlices);
	}
	
}
