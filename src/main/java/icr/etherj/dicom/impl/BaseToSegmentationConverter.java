/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import com.google.common.collect.ImmutableList;
import icr.etherj.Uids;
import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.DerivationImage;
import icr.etherj.dicom.iod.DimensionIndex;
import icr.etherj.dicom.iod.FrameContent;
import icr.etherj.dicom.iod.PerFrameFunctionalGroups;
import icr.etherj.dicom.iod.ReferencedInstance;
import icr.etherj.dicom.iod.ReferencedSeries;
import icr.etherj.dicom.iod.Segment;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.dicom.iod.SegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SegmentationSharedFunctionalGroups;
import icr.etherj.dicom.iod.SharedFunctionalGroups;
import icr.etherj.dicom.iod.SourceImage;
import icr.etherj.dicom.iod.impl.DefaultCode;
import icr.etherj.dicom.iod.impl.DefaultDerivationImage;
import icr.etherj.dicom.iod.impl.DefaultDimensionIndex;
import icr.etherj.dicom.iod.impl.DefaultFrameContent;
import icr.etherj.dicom.iod.impl.DefaultReferencedInstance;
import icr.etherj.dicom.iod.impl.DefaultReferencedSeries;
import icr.etherj.dicom.iod.impl.DefaultSourceImage;
import icr.etherj.dicom.iod.module.CommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.GeneralSeriesModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import icr.etherj.dicom.iod.module.MultiframeDimensionModule;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.SegmentationImageModule;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
abstract class BaseToSegmentationConverter extends BaseRoiConverter
{
	private static final Logger logger =
		LoggerFactory.getLogger(BaseToSegmentationConverter.class);

	protected Map<String,ContourImage> contourImages = new HashMap<>();
	protected Map<String,DicomObject> dcmMap;
	protected boolean isMultiFrame = false;
	protected final Map<Integer,Segment> segmentMap = new TreeMap<>();
	protected boolean strict;

	protected final DerivationImage buildDerivationImage(TargetFrame tf)
	{
		DerivationImage di = new DefaultDerivationImage();
		Code derivCode = new DefaultCode();
		derivCode.setCodeValue("113076");
		derivCode.setCodingSchemeDesignator("DCM");
		derivCode.setCodeMeaning("Segmentation");
		di.addDerivationCode(derivCode);
		SourceImage si = new DefaultSourceImage(true);
		tf.getContourImage().cloneTo(si);
		Code purposeCode = new DefaultCode();
		purposeCode.setCodeValue("121322");
		purposeCode.setCodingSchemeDesignator("DCM");
		purposeCode.setCodeMeaning("Source image for image processing operation");
		si.setPurposeOfReferenceCode(purposeCode);
		di.addSourceImage(si);

		return di;
	}

	protected final FrameContent buildFrameContent(TargetFrame tf)
	{
		FrameContent fc = new DefaultFrameContent();
		int refRoi = tf.getReferencedRoiNumber();
		int inStackIdx = tf.getFrameInRoiIndex();
		// Indices are ReferencedSegmentNumber, StackID, InStackPositionNumber
		fc.setDimensionIndexValues(
			new int[] {refRoi, refRoi, inStackIdx});
		fc.setStackId(String.valueOf(refRoi));
		fc.setInStackPositionNumber(inStackIdx);
		return fc;
	}

	protected void clearInternal()
	{
		contourImages.clear();
		segmentMap.clear();
		dcmMap = null;
	}

	protected final byte[] computeMask(LineSegmentInfo lsi)
	{
		double xMax = Double.POSITIVE_INFINITY;
		byte[] mask = new byte[lsi.nRows*lsi.nCols];
		for (int iY=lsi.yRange[0]; iY<lsi.yRange[1]; iY++)
		{
			for (int iX=lsi.xRange[0]; iX<lsi.xRange[1]; iX++)
			{
				// Cast a horizontal ray from the centre of the pixel to infinity
				LineSegment ray = new LineSegment(iX, iY, xMax, iY);
				int nCrossings = 0;
				for (LineSegment line : lsi.lines)
				{
					if (line.intersectsXInfRay(ray))
					{
						nCrossings++;
					}
				}
				if (nCrossings % 2 == 1)
				{
					// Inside the contour if nCrossings is odd
					mask[iY*lsi.nCols+iX] = (byte) 255;
				}
			}
		}
		return mask;
	}

	protected abstract int computeTotalFrames();
	
	protected final byte[] configAndPreAllocate(Segmentation seg)
		throws ConversionException
	{
		Date now = new Date();
		String date = DicomUtils.formatDate(now);
		String time = DicomUtils.formatTime(now);

		SegmentationImageModule siModule = seg.getSegmentationImageModule();
		siModule.setContentLabel(createContentLabel());
		segmentMap.values().forEach((x) -> siModule.addSegment(x));
		siModule.setSegmentationType(Constants.Fractional);
		siModule.setSegmentationFractionalType(Constants.Probability);
		siModule.setBitsAllocated(8);
		siModule.setBitsStored(8);
		siModule.setHighBit(7);
		siModule.setImageComments("NOT FOR CLINICAL USE");

		if (contourImages.isEmpty())
		{
			throw new ConversionException("No contour images found");
		}
		ImagePixelModule ipModule = seg.getImagePixelModule();
		String ciUid = contourImages.keySet().iterator().next();
		DicomObject protoDcm = dcmMap.get(ciUid);
		int nRows = protoDcm.getInt(Tag.Rows);
		int nCols = protoDcm.getInt(Tag.Columns);
		ipModule.setRowCount(nRows);
		ipModule.setColumnCount(nCols);

		MultiframeFunctionalGroupsModule mfFuncGroups = 
			seg.getMultiframeFunctionalGroupsModule();
		int nFrames = computeTotalFrames();
		mfFuncGroups.setInstanceNumber(1);
		mfFuncGroups.setContentDate(date);
		mfFuncGroups.setContentTime(time);
		mfFuncGroups.setNumberOfFrames(nFrames);
		// Template method
		configPerFrameFuncGroups(
			(SegmentationPerFrameFunctionalGroups) mfFuncGroups.getPerFrameFunctionalGroups());
		configDimensions(seg);
		// Template method
		configSharedFuncGroups(
			(SegmentationSharedFunctionalGroups) mfFuncGroups.getSharedFunctionalGroups());

		configCommonInstRef(seg);

		// Misc items
		GeneralStudyModule genStudy = seg.getGeneralStudyModule();
		genStudy.setStudyDate(protoDcm.getString(Tag.StudyDate));
		genStudy.setStudyTime(protoDcm.getString(Tag.StudyTime));
		genStudy.setStudyId(protoDcm.getString(Tag.StudyID));
		GeneralSeriesModule genSeries = seg.getGeneralSeriesModule();
		genSeries.setSeriesDate(date);
		genSeries.setSeriesTime(time);
		genSeries.setSeriesDescription(siModule.getContentLabel());
		genSeries.setSeriesNumber(99);

		logger.info("Allocating "+nCols+"x"+nRows+"x"+nFrames+" array ("+
			(nRows*nCols*nFrames/1024)+"kB)");

		return new byte[nRows*nCols*nFrames];
	}

	protected void configDimensions(Segmentation seg)
	{
		MultiframeDimensionModule mfDims = seg.getMultiframeDimensionModule();
		String uid = Uids.generateDicomUid();
		mfDims.addDimensionOrganisationUid(uid);
		DimensionIndex dimIdx = new DefaultDimensionIndex();
		dimIdx.setDimensionOrganisationUid(uid);
		dimIdx.setFunctionalGroupPointer(Tag.SegmentIdentificationSequence);
		dimIdx.setIndexPointer(Tag.ReferencedSegmentNumber);
		dimIdx.setDescriptionLabel("ReferencedSegmentNumber");
		mfDims.addDimensionIndex(dimIdx);
		dimIdx = new DefaultDimensionIndex();
		dimIdx.setDimensionOrganisationUid(uid);
		dimIdx.setFunctionalGroupPointer(Tag.FrameContentSequence);
		dimIdx.setIndexPointer(Tag.StackID);
		dimIdx.setDescriptionLabel("StackID");
		mfDims.addDimensionIndex(dimIdx);
		dimIdx = new DefaultDimensionIndex();
		dimIdx.setDimensionOrganisationUid(uid);
		dimIdx.setFunctionalGroupPointer(Tag.FrameContentSequence);
		dimIdx.setIndexPointer(Tag.InStackPositionNumber);
		dimIdx.setDescriptionLabel("InStackPositionNumber");
		mfDims.addDimensionIndex(dimIdx);
	}

	protected abstract void configPerFrameFuncGroups(
		SegmentationPerFrameFunctionalGroups pfFuncGroups);

	protected abstract void configSharedFuncGroups(
		SegmentationSharedFunctionalGroups sharedFuncGroups)
		throws ConversionException;

	protected abstract String createContentLabel();

	protected final void setDicomMap(Map<String,DicomObject> dcmMap)
		throws ConversionException
	{
		if ((dcmMap == null) || dcmMap.isEmpty())
		{
			throw new ConversionException(
				"Map of referenced instances is null or empty");
		}
		// Filter out non-image objects
		this.dcmMap = new HashMap<>();
		for (String sopInstUid : dcmMap.keySet())
		{
			DicomObject dcm = dcmMap.get(sopInstUid);
			if (DicomUtils.isImageSopClass(dcm.getString(Tag.SOPClassUID)))
			{
				this.dcmMap.put(sopInstUid, dcm);
			}
		}
		DicomObject dcm = this.dcmMap.values().iterator().next();
		isMultiFrame = DicomUtils.isMultiframeImageSopClass(
			dcm.getString(Tag.SOPClassUID));
	}

	protected final void setSegPropDefaults(Code code)
	{
		code.setCodeValue("T-D0050");
		code.setCodeMeaning("Tissue");
		code.setCodingSchemeDesignator("SRT");
	}

	private void configCommonInstRef(Segmentation seg)
	{
		CommonInstanceReferenceModule cir = seg.getCommonInstanceReferenceModule();
		String uid = contourImages.keySet().iterator().next();
		DicomObject dcm = dcmMap.get(uid);
		ReferencedSeries refSeries = new DefaultReferencedSeries();
		refSeries.setSeriesInstanceUid(dcm.getString(Tag.SeriesInstanceUID));
		cir.addReferencedSeries(refSeries);
		for (ContourImage ci : contourImages.values())
		{
			ReferencedInstance refInst = new DefaultReferencedInstance();
			ci.cloneTo(refInst);
			refSeries.addReferencedInstance(refInst);
		}
	}

	protected final class LineSegment
	{
		private final double x1;
		private final double y1;
		private final double x2;
		private final double y2;
		private final double xMin;
		private final double xMax;
		private final double yMin;
		private final double yMax;
		private final double xDiff;
		private final double negXDiff;
		private final double yDiff;
		private final double negYDiff;

		public LineSegment(double x1, double y1, double x2, double y2)
		{
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
			// Precompute min, max and differences as intersectsXInfRay will get
			// called a LOT.
			if (x1 < x2)
			{
				xMin = x1;
				xMax = x2;
			}
			else
			{
				xMin = x2;
				xMax = x1;
			}
			if (y1 < y2)
			{
				yMin = y1;
				yMax = y2;
			}
			else
			{
				yMin = y2;
				yMax = y1;
			}
			xDiff = x2-x1;
			negXDiff=  -xDiff;
			yDiff = y2-y1;
			negYDiff = -yDiff;
		}

		// Assumes ray.xMax = Double.POSITIVE_INFINITY
		// https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
		public boolean intersectsXInfRay(LineSegment ray)
		{
			// Check overlaps in x and y for quick false result
			if ((xMax < ray.x1) ||
				 (yMax < ray.yMin) || (yMin > ray.yMax))
			{
				return false;
			}
			// Crosses exactly through end
			if (y2 == ray.y1)
			{
				return true;
			}
			// Overlaps in y, starts at lower x: must intersect
			if (ray.x1 <= xMin)
			{
				return true;
			}
			// Find orientation
			int o1 = orientationStartRayStart(ray);
			// Ray starts exactly on the line: no crossing
			if (o1 == 0)
			{
				return false;
			}
			int o2 = orientationStartRayEnd(ray);
			// For intersection o1 and o2 must oppose AND o3 and o4 must oppose
			// Minimise float maths
			if ((o1 != o2))
			{
				int o3 = orientationEndRayStart(ray);
				int o4 = orientationEndRayEnd(ray);
				if (o3 != o4)
				{
					return true;
				}
			}
			return false;
		}

		// Find orientation of p->q->r. p == line start, q == line end, r = ray
		// start. -1 == ACW, 0 == colinear, 1 == CW
		private int orientationEndRayStart(LineSegment ray)
		{
			return (int) Math.signum(yDiff*(ray.x1-x2) - xDiff*(ray.y1-y2));
		}

		// Find orientation of p->q->r. p == line start, q == line end, r = ray
		// end. -1 == ACW, 0 == colinear, 1 == CW
		private int orientationEndRayEnd(LineSegment ray)
		{
			return (int) Math.signum(yDiff*(ray.x2-x2) - xDiff*(ray.y2-y2));
		}

		// Find orientation of p->q->r. p == line start, q == line end, r = ray
		// start. -1 == ACW, 0 == colinear, 1 == CW
		private int orientationStartRayStart(LineSegment ray)
		{
			return (int) Math.signum(negYDiff*(ray.x1-x1) - negXDiff*(ray.y1-y1));
		}

		// Find orientation of p->q->r. p == line start, q == line end, r = ray
		// end. -1 == ACW, 0 == colinear, 1 == CW
		private int orientationStartRayEnd(LineSegment ray)
		{
			return (int) Math.signum(negYDiff*(ray.x2-x1) - negXDiff*(ray.y2-y1));
		}

	}

	protected final class LineSegmentInfo
	{
		public final List<LineSegment> lines;
		public final int nLines;
		public final int nRows;
		public final int nCols;
		public final int[] xRange;
		public final int[] yRange;

		public LineSegmentInfo(List<LineSegment> lines, int nRows, int nCols,
			int[] xRange, int[] yRange)
		{
			this.lines = ImmutableList.copyOf(lines);
			this.nLines = lines.size();
			this.nRows = nRows;
			this.nCols = nCols;
			this.xRange = xRange;
			this.yRange = yRange;
		}
	}

	protected final class SourceFrame implements Comparable<SourceFrame>
	{
		private final ContourImage ci;
		private final double[] ipp;
		private final int nRows;
		private final int nCols;
		private final double sliceLoc;

		public SourceFrame(double[] iop, double[] ipp, ContourImage ci, int nRows,
			int nCols)
		{
			this.ci = ci;
			if ((iop == null) || (ipp == null))
			{
				this.ipp = null;
				sliceLoc = Double.NaN;
			}
			else
			{
				this.ipp = Arrays.copyOf(ipp, 3);
				sliceLoc = DicomUtils.sliceLocation(ipp, iop);
			}
			this.nRows = nRows;
			this.nCols = nCols;
		}

		@Override
		public int compareTo(SourceFrame other)
		{
			if (sliceLoc < other.sliceLoc)
			{
				return -1;
			}
			return (sliceLoc > other.sliceLoc) ? 1 : 0;
		}

		public int getColumnCount()
		{
			return nCols;
		}

		public ContourImage getContourImage()
		{
			return ci;
		}

		public double[] getImagePositionPatient()
		{
			return ipp;
		}

		public int getRowCount()
		{
			return nRows;
		}

		public double getSliceLocation()
		{
			return sliceLoc;
		}
	}

	protected final class TargetFrame
	{
		private final ContourImage ci;
		private int frameIndex;
		private int frameInRoiIndex;
		private final double[] ipp;
		private final byte[] pixelData;
		private boolean pixelsSet = false;
		private final int refRoiNumber;

		public TargetFrame(int refRoiNumber, int frameInRoiIndex, SourceFrame sf)
		{
			this.refRoiNumber = refRoiNumber;
			this.frameInRoiIndex = frameInRoiIndex;
			this.ci = sf.getContourImage();
			double[] array = sf.getImagePositionPatient();
			this.ipp = (array != null) ? Arrays.copyOf(array, 3) : null;
			this.pixelData = new byte[sf.getColumnCount()*sf.getRowCount()];
		}

		public ContourImage getContourImage()
		{
			return ci;
		}

		public int getFrameInRoiIndex()
		{
			return frameInRoiIndex;
		}

		public int getFrameIndex()
		{
			return frameIndex;
		}

		public double[] getImagePositionPatient()
		{
			return ipp;
		}

		public byte[] getPixelData()
		{
			return pixelData;
		}

		public int getReferencedRoiNumber()
		{
			return refRoiNumber;
		}

		public boolean hasPixelsSet()
		{
			return pixelsSet;
		}

		public void setPixelsSet(boolean pixelsSet)
		{
			this.pixelsSet |= pixelsSet;
		}

		public void setFrameIndex(int frameIdx)
		{
			this.frameIndex = frameIdx;
		}

		public void setFrameInRoiIndex(int frameInRoiIndex)
		{
			this.frameInRoiIndex = frameInRoiIndex;
		}

	}

}
