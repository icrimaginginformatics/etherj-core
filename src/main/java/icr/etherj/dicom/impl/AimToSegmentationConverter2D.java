/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.SegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.SegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SegmentationSharedFunctionalGroups;
import icr.etherj.dicom.iod.impl.DefaultSegmentationFunctionalGroupsFrame;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

/**
 *
 * @author jamesd
 */
final class AimToSegmentationConverter2D extends BaseAimToSegmentationConverter
{
	@Override
	protected void configPerFrameFuncGroups(
		SegmentationPerFrameFunctionalGroups pfFuncGroups)
	{
		for (ImageAnnotationInfo iai : iaInfoMap.values())
		{
			int refSegNumber = iai.getReferencedSegmentNumber();
			for (TargetFrame tf : iai.getTargetFrameList())
			{
				SegmentationFunctionalGroupsFrame frame =
					new DefaultSegmentationFunctionalGroupsFrame();
				frame.addDerivationImage(buildDerivationImage(tf));
				frame.setFrameContent(buildFrameContent(tf));
				frame.setReferencedSegmentNumber(refSegNumber);
				pfFuncGroups.addFrame(frame);
			}
		}
	}

	@Override
	protected void configSharedFuncGroups(
		SegmentationSharedFunctionalGroups sharedFuncGroups)
		throws ConversionException
	{}

	@Override
	protected SourceFrame createSourceImage(DicomObject dcm, boolean isMultiFrame,
		ContourImage ci) throws ConversionException
	{
		SourceFrame sf = new SourceFrame(null, null, ci, dcm.getInt(Tag.Rows),
			dcm.getInt(Tag.Columns));
		return sf;
	}

}
