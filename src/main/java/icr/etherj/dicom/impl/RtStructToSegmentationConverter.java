/*********************************************************************
 * Copyright (c) 2020, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom.impl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import icr.etherj.StringUtils;
import icr.etherj.dicom.ConversionException;
import icr.etherj.dicom.Coordinate3D;
import icr.etherj.dicom.DicomUtils;
import icr.etherj.dicom.RLE;
import icr.etherj.dicom.SegUtils;
import icr.etherj.dicom.iod.Code;
import icr.etherj.dicom.iod.Constants;
import icr.etherj.dicom.iod.Contour;
import icr.etherj.dicom.iod.ContourImage;
import icr.etherj.dicom.iod.IodUtils;
import icr.etherj.dicom.iod.PixelMeasures;
import icr.etherj.dicom.iod.ReferencedInstance;
import icr.etherj.dicom.iod.ReferencedSeries;
import icr.etherj.dicom.iod.RoiContour;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.Segment;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.dicom.iod.SegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.SegmentationPerFrameFunctionalGroups;
import icr.etherj.dicom.iod.SegmentationSharedFunctionalGroups;
import icr.etherj.dicom.iod.StructureSetRoi;
import icr.etherj.dicom.iod.impl.DefaultPixelMeasures;
import icr.etherj.dicom.iod.impl.DefaultReferencedInstance;
import icr.etherj.dicom.iod.impl.DefaultReferencedSeries;
import icr.etherj.dicom.iod.impl.DefaultSegment;
import icr.etherj.dicom.iod.impl.DefaultSegmentation;
import icr.etherj.dicom.iod.impl.DefaultSegmentationFunctionalGroupsFrame;
import icr.etherj.dicom.iod.module.CommonInstanceReferenceModule;
import icr.etherj.dicom.iod.module.EnhancedGeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralEquipmentModule;
import icr.etherj.dicom.iod.module.GeneralSeriesModule;
import icr.etherj.dicom.iod.module.GeneralStudyModule;
import icr.etherj.dicom.iod.module.ImagePixelModule;
import icr.etherj.dicom.iod.module.MultiframeFunctionalGroupsModule;
import icr.etherj.dicom.iod.module.RoiContourModule;
import icr.etherj.dicom.iod.module.RtSeriesModule;
import icr.etherj.dicom.iod.module.SegmentationImageModule;
import icr.etherj.dicom.iod.module.SegmentationSeriesModule;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
final class RtStructToSegmentationConverter extends BaseToSegmentationConverter
{
	private static final Logger logger =
		LoggerFactory.getLogger(RtStructToSegmentationConverter.class);

	private final Lock lock = new ReentrantLock();
	private double[] pixDims;
	private final Map<Integer,Table<String,Integer,TargetFrame>> rcTargetFrames =
		new TreeMap<>();
	private final Map<Integer,RoiContourInfo> rcInfoMap = new TreeMap<>();
	private RtStruct rts;
	private final Map<Integer,StructureSetRoi> ssRoiMap = new TreeMap<>();
	private boolean useRle;

	public Segmentation convert(RtStruct rts, Map<String,DicomObject> dcmMap,
		ExecutorService service, boolean useRle) throws ConversionException
	{
		try
		{
			lock.lock();
			return convertImpl(rts, dcmMap, service, useRle);
		}
		finally
		{
			clearInternal();
			lock.unlock();
		}
	}

	@Override
	protected void clearInternal()
	{
		super.clearInternal();
		rcTargetFrames.clear();
		rcInfoMap.clear();
		ssRoiMap.clear();
		rts = null;
	}

	@Override
	protected int computeTotalFrames()
	{
		int nFrames = 0;
		for (RoiContourInfo rci : rcInfoMap.values())
		{
			nFrames += rci.getFrameSpan();
		}
		return nFrames;
	}

	@Override
	protected void configPerFrameFuncGroups(
		SegmentationPerFrameFunctionalGroups pfFuncGroups)
	{
		for (RoiContourInfo rci : rcInfoMap.values())
		{
			int refRoiNumber = rci.getRoiContour().getReferencedRoiNumber();
			for (TargetFrame tf : rci.getTargetFrameList())
			{
				SegmentationFunctionalGroupsFrame frame =
					new DefaultSegmentationFunctionalGroupsFrame();
				frame.addDerivationImage(buildDerivationImage(tf));
				frame.setFrameContent(buildFrameContent(tf));
				frame.setReferencedSegmentNumber(refRoiNumber);
				frame.setImagePositionPatient(tf.getImagePositionPatient());
				pfFuncGroups.addFrame(frame);
			}
		}
	}

	@Override
	protected void configSharedFuncGroups(
		SegmentationSharedFunctionalGroups sharedFuncGroups)
		throws ConversionException
	{
		ContourImage ci = contourImages.values().iterator().next();
		int refFrameNumber = ci.getReferencedFrameNumber();
		DicomObject dcm = dcmMap.get(ci.getReferencedSopInstanceUid());

		sharedFuncGroups.setImageOrientationPatient(
			getImageOrientationPatient(dcm, isMultiFrame, refFrameNumber));
		// Store pixDims as will need for coordinate conversion later
		pixDims = getPixelSpacing(dcm, isMultiFrame, refFrameNumber);
		PixelMeasures pm = new DefaultPixelMeasures();
		pm.setPixelSpacing(pixDims);
		pm.setSliceThickness(getSliceThickness(dcm, isMultiFrame, refFrameNumber));
		pm.setSpacingBetweenSlices(
			getSpacingBetweenSlices(dcm, isMultiFrame, refFrameNumber));
		sharedFuncGroups.setPixelMeasures(pm);
	}

	@Override
	protected String createContentLabel()
	{
		return rts.getStructureSetModule().getStructureSetLabel();
	}

	private void buildMaps() throws ConversionException
	{
		RoiContourModule rcModule = rts.getRoiContourModule();
		for (StructureSetRoi ssRoi :
			rts.getStructureSetModule().getStructureSetRoiList())
		{
			int number = ssRoi.getRoiNumber();
			ssRoiMap.put(number, ssRoi);

			// Create and store a segment per structure set ROI
			Segment segment = buildSegment(ssRoi);
			segmentMap.put(number, segment);

			// Store assorted info about each RoiContour and its target frames
			RoiContour rc = rcModule.getRoiContour(number);
			RoiContourInfo rci = buildRoiContourInfo(rc);
			rcInfoMap.put(number, rci);
		}
		// Set the overall frame index for each target frame
		int frameIdx = 1;
		for (RoiContourInfo rci : rcInfoMap.values())
		{
			Table<String,Integer,TargetFrame> table = TreeBasedTable.create();
			rcTargetFrames.put(rci.getRoiContour().getReferencedRoiNumber(), table);
			for (TargetFrame tf : rci.getTargetFrameList())
			{
				tf.setFrameIndex(frameIdx);
				frameIdx++;
				// Insert into table for later search when storing masks
				ContourImage ci = tf.getContourImage();
				String ciUid = ci.getReferencedSopInstanceUid();
				table.put(ciUid, ci.getReferencedFrameNumber(), tf);
				// Update overall set of ci UIDs
				contourImages.putIfAbsent(ciUid, ci);
			}
		}
		if (contourImages.isEmpty())
		{
			throw new ConversionException("No contour images found");
		}
	}

	private RoiContourInfo buildRoiContourInfo(RoiContour rc)
		throws ConversionException
	{
		RoiContourInfo rci = new RoiContourInfo(rc);

		Table<String,Integer,SourceFrame> ciFrameTable = TreeBasedTable.create();

		for (Contour contour : rc.getContourList())
		{
			if (!Contour.ClosedPlanar.equals(contour.getContourGeometricType()))
			{
				continue;
			}
			List<ContourImage> ciList = contour.getContourImageList();
			if (1 != ciList.size())
			{
				throw new ConversionException(
					"Unsupported number of images for this contour:"+ciList.size());
			}
			ContourImage ci = ciList.get(0);
			String uid = ci.getReferencedSopInstanceUid();
			int refFrameIdx = ci.getReferencedFrameNumber();
			if (!ciFrameTable.contains(uid, refFrameIdx))
			{
				DicomObject dcm = dcmMap.get(uid);
				SourceFrame sf = new SourceFrame(
					getImageOrientationPatient(dcm, isMultiFrame, refFrameIdx),
					getImagePositionPatient(dcm, isMultiFrame, refFrameIdx),
					ci, dcm.getInt(Tag.Rows), dcm.getInt(Tag.Columns));
				ciFrameTable.put(uid, refFrameIdx, sf);
			}
		}
		List<SourceFrame> sourceList = new ArrayList<>();
		sourceList.addAll(ciFrameTable.values());
		Collections.sort(sourceList);
		int idx = 1;
		for (SourceFrame sf : sourceList)
		{
			TargetFrame tf = new TargetFrame(rc.getReferencedRoiNumber(), idx, sf);
			rci.addTargetFrame(tf);
			idx++;
		}
		
		return rci;
	}

	private Segment buildSegment(StructureSetRoi ssRoi)
	{
		int number = ssRoi.getRoiNumber();
		Segment segment = new DefaultSegment();
		segment.setSegmentNumber(number);
		String label = ssRoi.getRoiName();
		if (StringUtils.isNullOrEmpty(label))
		{
			label = String.format("Segment_%05d", number);
		}
		segment.setSegmentLabel(label);
		String algo = ssRoi.getRoiGenerationAlgorithm();
		switch (algo)
		{
			case Constants.Manual:
				segment.setSegmentAlgorithmName(null);
				break;
			case Constants.Automatic:
			case Constants.SemiAutomatic:
				segment.setSegmentAlgorithmName("UNKNOWN");
				break;
			default:
				algo = Constants.Manual;
				segment.setSegmentAlgorithmName(null);
		}
		segment.setSegmentAlgorithmType(algo);
		Code catCode = segment.getSegmentedPropertyCategoryCode();
		setSegPropDefaults(catCode);
		Code typeCode = segment.getSegmentedPropertyTypeCode();
		setSegPropDefaults(typeCode);

		return segment;
	}

	private void cloneCommonModules(Segmentation seg)
	{
		rts.getPatientModule().cloneTo(seg.getPatientModule());
		rts.getClinicalTrialSubjectModule().cloneTo(
			seg.getClinicalTrialSubjectModule());
		rts.getGeneralStudyModule().cloneTo(
			seg.getGeneralStudyModule());
		rts.getPatientStudyModule().cloneTo(
			seg.getPatientStudyModule());
		rts.getClinicalTrialStudyModule().cloneTo(
			seg.getClinicalTrialStudyModule());
		rts.getFrameOfReferenceModule().cloneTo(seg.getFrameOfReferenceModule());
		rts.getCommonInstanceReferenceModule().cloneTo(
			seg.getCommonInstanceReferenceModule());
	}

	private LineSegmentInfo computeLineSegmentsAndRanges(
		List<Coordinate3D> contourData, double[] ipp, double[] iop, int nRows,
		int nCols)
	{
		int nCoords = contourData.size();
		double[] x = new double[nCoords];
		double[] y = new double[nCoords];
		int[] xRange = new int[] {nCols+1, -1};
		int[] yRange = new int[] {nRows+1, -1};
		double[] patCoord = new double[3];
		double[] row = Arrays.copyOf(iop, 3);
		double[] col = Arrays.copyOfRange(iop, 3, 6);
		for (int i=0; i<nCoords; i++)
		{
			Coordinate3D coord = contourData.get(i);
			patCoord[0] = coord.x;
			patCoord[1] = coord.y;
			patCoord[2] = coord.z;
			double[] xy = DicomUtils.patientCoordToImageCoord(patCoord, ipp, row,
				col, pixDims);
			x[i] = xy[0];
			y[i] = xy[1];
			// Update the min/max ranges
			int value = (int) Math.floor(x[i]);
			xRange[0] = (value < xRange[0]) ? value : xRange[0];
			value = (int) Math.ceil(x[i]);
			xRange[1] = (value > xRange[1]) ? value : xRange[1];
			value = (int) Math.floor(y[i]);
			yRange[0] = (value < yRange[0]) ? value : yRange[0];
			value = (int) Math.ceil(y[i]);
			yRange[1] = (value > yRange[1]) ? value : yRange[1];
		}
		List<LineSegment> lines = new ArrayList<>();
		for (int i=0; i<(nCoords-1); i++)
		{
			lines.add(new LineSegment(x[i], y[i], x[i+1], y[i+1]));
		}
		if ((Math.abs(x[nCoords-1]-x[0]) < 1e-6) &&
			 (Math.abs(y[nCoords-1]-y[0]) < 1e-6))
		{
			// First and last points are close enough to be the same i.e.
			// closed polygon
			lines.add(new LineSegment(x[nCoords-2], y[nCoords-2], x[0], y[0]));
		}
		else
		{
			// Close the polygon
			lines.add(new LineSegment(x[nCoords-1], y[nCoords-1], x[0], y[0]));
		}

		// Some regions may exist entirely outside the image
		if ((xRange[0] > nCols) || (xRange[1] < 0) ||
			 (yRange[0] > nRows) || (yRange[1] < 0))
		{
			return null;
		}
		// Some line segments may start or finish outside the image
		xRange[0] = (xRange[0] < 0) ? 0 : xRange[0];
		xRange[1] = (xRange[1] > nCols) ? nCols : xRange[1];
		yRange[0] = (yRange[0] < 0) ? 0 : yRange[0];
		yRange[1] = (yRange[1] > nRows) ? nRows : yRange[1];

		return new LineSegmentInfo(lines, nRows, nCols, xRange, yRange);
	}

	private void configCommonInstRef(Segmentation seg)
	{
		CommonInstanceReferenceModule cir = seg.getCommonInstanceReferenceModule();
		String uid = contourImages.keySet().iterator().next();
		DicomObject dcm = dcmMap.get(uid);
		ReferencedSeries refSeries = new DefaultReferencedSeries();
		refSeries.setSeriesInstanceUid(dcm.getString(Tag.SeriesInstanceUID));
		cir.addReferencedSeries(refSeries);
		for (ContourImage ci : contourImages.values())
		{
			ReferencedInstance refInst = new DefaultReferencedInstance();
			ci.cloneTo(refInst);
			refSeries.addReferencedInstance(refInst);
		}
	}

	private void configEquipment(Segmentation seg)
	{
		GeneralEquipmentModule rtEquip = rts.getGeneralEquipmentModule();
		EnhancedGeneralEquipmentModule segEquip = seg.getEnhancedEquipmentModule();
		String value = rtEquip.getDeviceSerialNumber();
		segEquip.setDeviceSerialNumber(!StringUtils.isNullOrEmpty(value)
			? value : IodUtils.getDummyString(VR.LO));
		value = rtEquip.getManufacturer();
		segEquip.setManufacturer(!StringUtils.isNullOrEmpty(value)
			? value : IodUtils.getDummyString(VR.LO));
		value = rtEquip.getManufacturersModelName();
		segEquip.setManufacturersModelName(!StringUtils.isNullOrEmpty(value)
			? value : IodUtils.getDummyString(VR.LO));
		value = rtEquip.getSoftwareVersion();
		segEquip.setSoftwareVersion(!StringUtils.isNullOrEmpty(value)
			? value : IodUtils.getDummyString(VR.LO));
		segEquip.setPixelPaddingValue(rtEquip.getPixelPaddingValue());
		segEquip.setSpatialResolution(rtEquip.getSpatialResolution());
	}

	private void configGeneralSeries(Segmentation seg, String date,
		String time)
	{
		SegmentationImageModule siModule = seg.getSegmentationImageModule();
		GeneralSeriesModule genSeries = seg.getGeneralSeriesModule();
		genSeries.setSeriesDate(date);
		genSeries.setSeriesTime(time);
		genSeries.setSeriesDescription(siModule.getContentLabel());
		genSeries.setSeriesNumber(99);
	}

	private void configGeneralStudy(Segmentation seg)
	{
		GeneralStudyModule genStudy = seg.getGeneralStudyModule();
		String ciUid = contourImages.keySet().iterator().next();
		DicomObject protoDcm = dcmMap.get(ciUid);
		genStudy.setStudyDate(protoDcm.getString(Tag.StudyDate));
		genStudy.setStudyTime(protoDcm.getString(Tag.StudyTime));
		genStudy.setStudyId(protoDcm.getString(Tag.StudyID));
	}

	private void configImagePixel(Segmentation seg)
	{
		ImagePixelModule ipModule = seg.getImagePixelModule();
		String ciUid = contourImages.keySet().iterator().next();
		DicomObject protoDcm = dcmMap.get(ciUid);
		int nRows = protoDcm.getInt(Tag.Rows);
		int nCols = protoDcm.getInt(Tag.Columns);
		ipModule.setRowCount(nRows);
		ipModule.setColumnCount(nCols);		
	}

	private void configModulesPostCompute(Segmentation seg)
		throws ConversionException
	{
		// Template method
		MultiframeFunctionalGroupsModule mfFuncGroups =
			seg.getMultiframeFunctionalGroupsModule();
		mfFuncGroups.setNumberOfFrames(computeTotalFrames());
		configPerFrameFuncGroups(
			(SegmentationPerFrameFunctionalGroups)
				mfFuncGroups.getPerFrameFunctionalGroups());
	}

	private void configModulesPreCompute(Segmentation seg, String date,
		String time) throws ConversionException
	{
		cloneCommonModules(seg);
		configSeries(seg);
		configEquipment(seg);
		configSegmentationImage(seg);
		configImagePixel(seg);
		configMultiframeFuncGroups(seg, date, time);
		configSharedFuncGroups(
			(SegmentationSharedFunctionalGroups)
				seg.getMultiframeFunctionalGroupsModule().getSharedFunctionalGroups());
		configDimensions(seg);
		configGeneralStudy(seg);
		configGeneralSeries(seg, date, time);
		configCommonInstRef(seg);
	}

	private void configMultiframeFuncGroups(Segmentation seg, String date,
		String time)
	{
		MultiframeFunctionalGroupsModule mfFuncGroups = 
			seg.getMultiframeFunctionalGroupsModule();
		int nFrames = computeTotalFrames();
		mfFuncGroups.setInstanceNumber(1);
		mfFuncGroups.setContentDate(date);
		mfFuncGroups.setContentTime(time);
		mfFuncGroups.setNumberOfFrames(nFrames);
	}

	private void configSegmentationImage(Segmentation seg)
	{
		SegmentationImageModule siModule = seg.getSegmentationImageModule();
		siModule.setContentLabel(createContentLabel());
		if (useRle)
		{
			siModule.setSegmentationType(Constants.Fractional);
			siModule.setSegmentationFractionalType(Constants.Probability);
			siModule.setBitsStored(8);
			siModule.setBitsAllocated(8);
			siModule.setHighBit(7);
		}
		else
		{
			siModule.setSegmentationType(Constants.Binary);
			siModule.setBitsStored(1);
			siModule.setBitsAllocated(1);
			siModule.setHighBit(0);
		}
		siModule.setImageComments("NOT FOR CLINICAL USE");
	}

	private void configSeries(Segmentation seg)
	{
		RtSeriesModule rtSeries = rts.getRtSeriesModule();
		SegmentationSeriesModule segSeries = seg.getSegmentationSeriesModule();
		segSeries.setSeriesDate(rtSeries.getSeriesDate());
		segSeries.setSeriesDescription(rtSeries.getSeriesDescription());
		segSeries.setSeriesNumber(rtSeries.getSeriesNumber());
		segSeries.setOperatorsName(rtSeries.getOperatorsName());
		segSeries.setSeriesTime(rtSeries.getSeriesTime());
	}

	private void convertAndStore(RoiContour rc, Contour contour, Segmentation seg)
		throws ConversionException
	{
		ImagePixelModule sipModule = seg.getImagePixelModule();
		int nRows = sipModule.getRowCount();
		int nCols = sipModule.getColumnCount();
		// buildMaps() has already tested each contour has exactly one
		// contour image
		ContourImage ci = contour.getContourImageList().get(0);
		int refFrameIdx = ci.getReferencedFrameNumber();
		DicomObject dcm = dcmMap.get(ci.getReferencedSopInstanceUid());
		double[] ipp = getImagePositionPatient(dcm, isMultiFrame, refFrameIdx);
		double[] iop = getImageOrientationPatient(dcm, isMultiFrame, refFrameIdx);

		// Build line segment data
		LineSegmentInfo lsi = computeLineSegmentsAndRanges(
			contour.getContourData(), ipp, iop, nRows, nCols);
		// Bail if no valid line segments
		if (lsi == null)
		{
			return;
		}
		// Check each pixel for being inside the contour
		byte[] mask = computeMask(lsi);
		// Copy mask to correct part of pixelData
		copyMaskToTargetFrame(rc, ci, lsi, mask);
	}

	private Segmentation convertImpl(RtStruct rts, Map<String,DicomObject> dcmMap,
		ExecutorService service, boolean useRle) throws ConversionException
	{
		if (rts == null)
		{
			throw new ConversionException("RtStruct must not be null");
		}
		this.rts = rts;
		setDicomMap(dcmMap);
		strict = rts.isStrict();
		this.useRle = useRle;

		// Cache the various items for quick search
		buildMaps();

		Date now = new Date();
		String date = DicomUtils.formatDate(now);
		String time = DicomUtils.formatTime(now, true);

		Segmentation seg = new DefaultSegmentation(strict);
		configModulesPreCompute(seg, date, time);

		// Convert contours to masks and store
		processContours(seg, service);
		// Post-process frames, removing empty items etc.
		postProcess(seg);
		// Config per frame groups now minimum set known
		configModulesPostCompute(seg);

		// Store all results
		storeResults(seg);

		return seg;
	}

	private void copyMaskToTargetFrame(RoiContour rc, ContourImage ci,
		LineSegmentInfo lsi, byte[] mask)
	{
		TargetFrame tf = rcTargetFrames.get(rc.getReferencedRoiNumber())
			.get(ci.getReferencedSopInstanceUid(), ci.getReferencedFrameNumber());
		byte[] pixelData = tf.getPixelData();
		// Only copy within x and y min-max ranges
		boolean pixelsSet = false;
		for (int iY=lsi.yRange[0]; iY<lsi.yRange[1]; iY++)
		{
			for (int iX=lsi.xRange[0]; iX<lsi.xRange[1]; iX++)
			{
				int maskOffset = iY*lsi.nCols+iX;
				byte value = mask[maskOffset];
				if (value != 0)
				{
					pixelData[maskOffset] = value;
					pixelsSet  = true;
				}
			}
		}
		tf.setPixelsSet(pixelsSet);
	}

	private boolean cullEmpty()
	{
		Set<Integer> rciKeys = rcInfoMap.keySet();
		Set<Integer> deadKeys = new TreeSet<>();
		boolean needReindex = false;
		for (Integer key : rciKeys)
		{
			RoiContourInfo rci = rcInfoMap.get(key);
			Segment segment = segmentMap.get(key);
			needReindex |= cullEmptyTargetFrames(rci);
			List<TargetFrame> tfList = rci.getTargetFrameList();
			if (tfList.isEmpty())
			{
				logger.debug("Empty segment: "+segment.getSegmentLabel());
				deadKeys.add(key);
			}
		}
		for (Integer key : deadKeys)
		{
			segmentMap.remove(key);
			rcInfoMap.remove(key);
		}
		return needReindex;
	}

	private boolean cullEmptyTargetFrames(RoiContourInfo rci)
	{
		List<TargetFrame> frames = rci.getTargetFrameList();
		List<TargetFrame> empties = new ArrayList<>();
		for (TargetFrame tf : frames)
		{
			if (!tf.hasPixelsSet())
			{
				empties.add(tf);
			}
		}
		if (empties.isEmpty())
		{
			return false;
		}
		logger.debug("RoiContour "+rci.getRoiContour().getReferencedRoiNumber()+
			" produced "+empties.size()+" empty frames of "+frames.size());
		for (TargetFrame tf : empties)
		{
			rci.removeTargetFrame(tf);
		}
		return true;
	}

	private void postProcess(Segmentation seg)
	{
		if (cullEmpty())
		{
			reindexTargetFrames();
		}

		// Add segments that still exist
		SegmentationImageModule siModule = seg.getSegmentationImageModule();
		segmentMap.values().forEach((x) -> siModule.addSegment(x));
	}

	private void processContours(Segmentation seg, ExecutorService service)
		throws ConversionException
	{
		if (service != null)
		{
			processContoursMT(seg, service);
			return;
		}
		List<RoiContour> rcList = rts.getRoiContourModule().getRoiContourList();
		int rcIdx = 1;
		for (RoiContour rc : rcList)
		{
			StructureSetRoi ssRoi = ssRoiMap.get(rc.getReferencedRoiNumber());
			long tic = 0;
			List<Contour> contourList = rc.getContourList();
			if (logger.isDebugEnabled())
			{
				tic = System.currentTimeMillis();
				logger.debug("Processing ROI: "+ssRoi.getRoiName()+" - "+rcIdx+
					" of "+rcList.size()+", "+contourList.size()+" contours");
			}
			for (Contour contour : contourList)
			{
				convertAndStore(rc, contour, seg);
			}
			if (logger.isDebugEnabled())
			{
				logger.debug("ROI "+ssRoi.getRoiName()+" processing time: "+
					(System.currentTimeMillis()-tic)+"ms");
			}
			rcIdx++;
		}
	}

	private void processContoursMT(Segmentation seg, ExecutorService service)
		throws ConversionException
	{
		List<Callable<Void>> tasks = new ArrayList();
		List<RoiContour> rcList = rts.getRoiContourModule().getRoiContourList();
		for (RoiContour rc : rcList)
		{
			List<Contour> contourList = rc.getContourList();
			for (Contour contour : contourList)
			{
				Callable<Void> task = new Callable<Void>()
				{
					@Override
					public Void call()
					{
						try
						{
							convertAndStore(rc, contour, seg);
						}
						catch (ConversionException ex)
						{
							logger.error("Conversion error!", ex);
						}
						return null;
					}
				};
				tasks.add(task);
			}
		}
		try
		{
			service.invokeAll(tasks);
		}
		catch (InterruptedException ex)
		{
			logger.error("RoiContour unpacking failed:"+ex.getMessage(), ex);
		}
	}

	private void reindexTargetFrames()
	{
		int overallIdx = 1;
		for (RoiContourInfo rci : rcInfoMap.values())
		{
			int inStackIdx = 1;
			for (TargetFrame tf : rci.getTargetFrameList())
			{
				tf.setFrameIndex(overallIdx);
				tf.setFrameInRoiIndex(inStackIdx);
				overallIdx++;
				inStackIdx++;
			}
		}
	}

	private void storeResults(Segmentation seg)
	{
		ImagePixelModule ipModule = seg.getImagePixelModule();
		int nRows = ipModule.getRowCount();
		int nCols = ipModule.getColumnCount();	
		MultiframeFunctionalGroupsModule mfFuncGroups = 
			seg.getMultiframeFunctionalGroupsModule();
		int nFrames = mfFuncGroups.getNumberOfFrames();
		int frameSize = nCols*nRows;

		if (useRle)
		{
			List<byte[]> frags = new ArrayList<>();
			frags.add(new byte[8]);
			for (RoiContourInfo rci : rcInfoMap.values())
			{
				List<TargetFrame> frames = rci.getTargetFrameList();
				for (TargetFrame tf : frames)
				{
					byte[] frag = RLE.compressPixels(tf.getPixelData(), nRows, nCols);
					frags.add(frag);
				}
			}
			seg.getImagePixelModule().setPixelDataFragments(frags, true);
		}
		else
		{
			byte[] pixelData = new byte[frameSize*nFrames];
			for (RoiContourInfo rci : rcInfoMap.values())
			{
				List<TargetFrame> frames = rci.getTargetFrameList();
				for (TargetFrame tf : frames)
				{
					System.arraycopy(tf.getPixelData(), 0,
						pixelData, (tf.getFrameIndex()-1)*frameSize,
						frameSize);
				}
			}
			seg.getImagePixelModule().setPixelData(SegUtils.pack(pixelData), true);
		}
	}

	private class RoiContourInfo
	{
		private final RoiContour rc;
		private final Map<Integer,TargetFrame> targetFrames = new TreeMap<>();

		public RoiContourInfo(RoiContour rc)
		{
			this.rc = rc;
		}

		public boolean addTargetFrame(TargetFrame tf)
		{
			if (tf == null)
			{
				return false;
			}
			targetFrames.put(tf.getFrameInRoiIndex(), tf);
			return true;
		}

		public int getFrameSpan()
		{
			return targetFrames.size();
		}

		public RoiContour getRoiContour()
		{
			return rc;
		}

		public List<TargetFrame> getTargetFrameList()
		{
			return ImmutableList.copyOf(targetFrames.values());
		}

		public boolean removeTargetFrame(TargetFrame tf)
		{
			if (tf == null)
			{
				return false;
			}
			TargetFrame removed = targetFrames.remove(tf.getFrameInRoiIndex());
			return (removed != null);
		}

	}

}
