/*********************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 *
 * @author jamesd
 */
public class RLE
{
	private static final int maxLen = 128;

	public static byte[] compress(byte[] bytes, int rows, int cols)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		compress(bytes, rows, cols, baos);
		return baos.toByteArray();
	}

	public static byte[] compress(byte[] bytes)
	{
		return compress(bytes, 1, bytes.length);
	}

	public static byte[] compressPixels(byte[] bytes, int rows, int cols)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		// Header is 64 bytes as 16 ints in LE ordering. First int is number of
		// segments, always 1. Remaining ints are offset to nth RLE segment. so
		// first int is always 64. Remaining ints are 0
		baos.write(1);
		pad(baos, 0, 3);
		baos.write(64);
		pad(baos, 0, 59);
		// Compress the segment
		compress(bytes, rows, cols, baos);

		return baos.toByteArray();
	}

	public static byte[] decompress(byte[] bytes)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		decompress(bytes, baos, 0);
		return baos.toByteArray();
	}

	public static byte[] decompressPixels(byte[] bytes)
	{
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		int nSeg = bais.read();
		skip(bais, 3);
		int offset = bais.read();
		skip(bais, 59);
		if ((nSeg != 1) || (offset != 64))
		{
			throw new IllegalArgumentException("RLE stream header invalid");
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		decompress(bytes, baos, 64);
		return baos.toByteArray();
	}

	private static void compress(byte[] array, int rows, int cols,
		ByteArrayOutputStream baos)
	{
		for (int i=0; i<rows; i++)
		{
			int idx = i*cols;
			int rowEnd = idx+cols;
			do
			{
				idx = encodeChunk(array, idx, rowEnd, baos);
			}
			while (idx < rowEnd);
		}
	}

	private static void decompress(byte[] array,
		ByteArrayOutputStream baos, int offset)
	{
		// Array must be even length as required by DICOM but last byte may be
		// zero packing rather than encoding data
		int maxIdx = array.length-1;
		for (int i=offset; i<maxIdx; i++)
		{
			int count = array[i];
			if (count >= 0)
			{
				if (count == 128)
				{
					continue;
				}
				count++;
				// Copy sequence of non-repeating bytes
				baos.write(array, i+1, count);
				i += count;
			}
			else
			{
				// Create run
				count = -count+1;
				byte b = array[i+1];
				for (int j=0; j<count; j++)
				{
					baos.write(b);
				}
				i++;
			}
		}
	}

	private static int encodeChunk(byte[] array, int offset, int rowEnd,
		ByteArrayOutputStream baos)
	{
		int last = rowEnd-1;
		// Last byte in row?
		if (offset == last)
		{
			baos.write(0);
			baos.write(array[offset]);
			return offset+1;
		}
		int count = 0;
		byte curr = array[offset];
		byte next = array[offset+1];
		int idx = offset;
		if (next == curr)
		{
			// Detect the run length
			count = 2;
			idx++;
			while ((idx < last) && (count < maxLen))
			{
				curr = next;
				next = array[idx+1];
				if (curr != next)
				{
					break;
				}
				count++;
				idx++;
			}
			// Encode the run
			baos.write(-(count-1));
			baos.write(curr);
			return offset+count;
		}
		// Detect the non-repeating sequence length;
		count++;
		idx++;
		while ((idx < last) && (count < maxLen))
		{
			curr = next;
			next = array[idx+1];
			if (curr == next)
			{
				break;
			}
			count++;
			idx++;
		}
		// Encode the sequence
		baos.write(count-1);
		baos.write(array, offset, count);
		return offset+count;
	}

	private static void pad(ByteArrayOutputStream baos, int value,
		int count)
	{
		for (int i=0; i<count; i++)
		{
			baos.write(value);
		}
	}

	private static void skip(ByteArrayInputStream bais, int count)
	{
		for (int i=0; i<count; i++)
		{
			bais.read();
		}
	}

}
