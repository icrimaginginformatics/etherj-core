/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom;

import icr.etherj.aim.ImageAnnotationCollection;
import icr.etherj.dicom.iod.RtStruct;
import icr.etherj.dicom.iod.Segmentation;
import icr.etherj.nifti.Nifti;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import org.dcm4che2.data.DicomObject;

/**
 * Converts ROI between different implementations.
 * @author jamesd
 */
public interface RoiConverter
{
	/** Closed planar ROI type for RTSTRUCT. */
	public static final String ClosedPlanar = "CLOSED_PLANAR";
	/** Open non-planar ROI type for RTSTRUCT. */
	public static final String OpenNonPlanar = "OPEN_NONPLANAR";
	/** Open planar ROI type for RTSTRUCT. */
	public static final String OpenPlanar = "OPEN_PLANAR";
	/** Point ROI type for RTSTRUCT. */
	public static final String Point = "POINT";

	/**
	 * Constructs an <code>ImageAnnotationCollection</code> from an
	 * <code>RtStruct</code> and <code>DicomObject</code>s.
	 * The map of referenced <code>DicomObject</code>s is required to provide
	 * image position/orientation etc.
	 * @param rtStruct the RTSTRUCT
	 * @param dcmMap the map of DICOM objects
	 * @return the image annotation collection
	 * @throws ConversionException if an error occurs during conversion
	 */
	ImageAnnotationCollection toIac(RtStruct rtStruct,
		Map<String,DicomObject> dcmMap) throws ConversionException;

	/**
	 * Constructs a <code>Nifti</code> from an
	 * <code>Segmentation</code> and <code>DicomObject</code>s.
	 * The map of referenced <code>DicomObject</code>s is required to provide
	 * frame of reference UID, image position/orientation etc.
	 * @param seg the segmentation
	 * @param dcmMap the map of DICOM objects
	 * @return the NIfTI object
	 * @throws ConversionException if an error occurs during conversion
	 */
	Nifti toNifti(Segmentation seg, Map<String,DicomObject> dcmMap)
		throws ConversionException;

	/**
	 * Constructs an <code>RtStruct</code> from an
	 * <code>ImageAnnotationCollection</code> and <code>DicomObject</code>s.
	 * The map of referenced <code>DicomObject</code>s is required to provide
	 * frame of reference UID, image position/orientation etc.
	 * @param iac the image annotation collection
	 * @param dcmMap the map of DICOM objects
	 * @return the RTSTRUCT
	 * @throws ConversionException if an error occurs during conversion
	 */
	RtStruct toRtStruct(ImageAnnotationCollection iac,
		Map<String,DicomObject> dcmMap)
		throws ConversionException;

	/**
	 * Constructs an <code>RtStruct</code> from an
	 * <code>Segmentation</code> and <code>DicomObject</code>s.
	 * The map of referenced <code>DicomObject</code>s is required to provide
	 * frame of reference UID, image position/orientation etc.
	 * @param seg the segmentation
	 * @param dcmMap the map of DICOM objects
	 * @return the RTSTRUCT
	 * @throws ConversionException if an error occurs during conversion
	 */
	RtStruct toRtStruct(Segmentation seg, Map<String,DicomObject> dcmMap)
		throws ConversionException;

	/**
	 * Constructs an <code>Segmentation</code> from an
	 * <code>ImageAnnotationCollection</code> and <code>DicomObject</code>s.
	 * The map of referenced <code>DicomObject</code>s is required to provide
	 * frame of reference UID, image position/orientation etc.
	 * @param iac the image annotation collection
	 * @param dcmMap the map of DICOM objects
	 * @return the RTSTRUCT
	 * @throws ConversionException if an error occurs during conversion
	 */
	Segmentation toSegmentation(ImageAnnotationCollection iac,
		Map<String,DicomObject> dcmMap)
		throws ConversionException;

	/**
	 * Constructs an <code>Segmentation</code> from an
	 * <code>ImageAnnotationCollection</code> and <code>DicomObject</code>s.
	 * The map of referenced <code>DicomObject</code>s is required to provide
	 * frame of reference UID, image position/orientation etc. If 
	 * <code>full3d</code> is false, SOP class and SOP instance references
	 * will be stored but not full 3D coordinates
	 * @param iac the image annotation collection
	 * @param dcmMap the map of DICOM objects
	 * @param full3d use full 3D coordinates
	 * @return the RTSTRUCT
	 * @throws ConversionException if an error occurs during conversion
	 */
	Segmentation toSegmentation(ImageAnnotationCollection iac,
		Map<String,DicomObject> dcmMap, boolean full3d)
		throws ConversionException;

	/**
	 * Constructs an <code>Segmentation</code> from an
	 * <code>Nifti</code> and <code>DicomObject</code>s.
	 * The map of referenced <code>DicomObject</code>s is required to provide
	 * frame of reference UID, image position/orientation etc.
	 * @param nifti the Nifti
	 * @param dcmMap the map of DICOM objects
	 * @return the Segmentation
	 * @throws ConversionException if an error occurs during conversion
	 */
	Segmentation toSegmentation(Nifti nifti, Map<String,DicomObject> dcmMap)
		throws ConversionException;

	/**
	 * Constructs an <code>Segmentation</code> from an
	 * <code>RtStruct</code> and <code>DicomObject</code>s.
	 * The map of referenced <code>DicomObject</code>s is required to provide
	 * frame of reference UID, image position/orientation etc.
	 * @param rtStruct the RTSTRUCT
	 * @param dcmMap the map of DICOM objects
	 * @return the Segmentation
	 * @throws ConversionException if an error occurs during conversion
	 */
	Segmentation toSegmentation(RtStruct rtStruct, Map<String,DicomObject> dcmMap)
		throws ConversionException;

	/**
	 * Constructs an <code>Segmentation</code> from an
	 * <code>RtStruct</code> and <code>DicomObject</code>s.
	 * The map of referenced <code>DicomObject</code>s is required to provide
	 * frame of reference UID, image position/orientation etc.
	 * @param rtStruct the RTSTRUCT
	 * @param dcmMap the map of DICOM objects
	 * @param useRle use RLE pixel compression
	 * @return the Segmentation
	 * @throws ConversionException if an error occurs during conversion
	 */
	Segmentation toSegmentation(RtStruct rtStruct, Map<String,DicomObject> dcmMap,
		boolean useRle) throws ConversionException;

	/**
	 * Constructs an <code>Segmentation</code> from an
	 * <code>RtStruct</code> and <code>DicomObject</code>s.
	 * The map of referenced <code>DicomObject</code>s is required to provide
	 * frame of reference UID, image position/orientation etc.
	 * @param rtStruct the RTSTRUCT
	 * @param dcmMap the map of DICOM objects
	 * @param service the executor service for parallelisation
	 * @return the Segmentation
	 * @throws ConversionException if an error occurs during conversion
	 */
	Segmentation toSegmentation(RtStruct rtStruct, Map<String,DicomObject> dcmMap,
		ExecutorService service) throws ConversionException;

	/**
	 * Constructs an <code>Segmentation</code> from an
	 * <code>RtStruct</code> and <code>DicomObject</code>s.
	 * The map of referenced <code>DicomObject</code>s is required to provide
	 * frame of reference UID, image position/orientation etc.
	 * @param rtStruct the RTSTRUCT
	 * @param dcmMap the map of DICOM objects
	 * @param service the executor service for parallelisation
	 * @param useRle use RLE pixel compression
	 * @return the Segmentation
	 * @throws ConversionException if an error occurs during conversion
	 */
	Segmentation toSegmentation(RtStruct rtStruct, Map<String,DicomObject> dcmMap,
		ExecutorService service, boolean useRle) throws ConversionException;

}
