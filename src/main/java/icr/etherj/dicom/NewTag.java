/*********************************************************************
 * Copyright (c) 2022, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom;

/**
 * DICOM tags that are not defined in DCM4CHE2.
 * @author jamesd
 */
public class NewTag
{
	/**
	 * LongCodeValue - 0x00080119. Not in Dcm4che v2.0.29
	 */
	public static final int LongCodeValue = 0x00080119;

	/**
	 * FloatingPointValue - 0x0040a161. Not in Dcm4che v2.0.29
	 */
	public static final int FloatingPointValue = 0x0040a161;
	/**
	 * RationalNumeratorValue - 0x0040a162. Not in Dcm4che v2.0.29
	 */
	public static final int RationalNumeratorValue = 0x0040a162;
	/**
	 * RationalDenominatorValue - 0x0040a163. Not in Dcm4che v2.0.29
	 */
	public static final int RationalDenominatorValue = 0x0040a163;
	/**
	 * WSM Image Frame Type SQ - 0x00400710. Not in Dcm4che v2.0.29
	 */
	public static final int WSMImageFrameTypeSequence = 0x00400710;

	/**
	 * NumberOfOpticalPaths - 0x00480302. Not in Dcm4che v2.0.29
	 */
	public static final int NumberOfOpticalPaths = 0x00480302;
	/**
	 * TotalPixelMatrixFocalPlanes - 0x00480303. Not in Dcm4che v2.0.29
	 */
	public static final int TotalPixelMatrixFocalPlanes = 0x00480303;

	/**
	 * SegmentedPropertyTypeModifierCodeSequence - 0x00620011.
	 * Not in Dcm4che v2.0.29
	 */
	public static final int SegmentedPropertyTypeModifierCodeSequence =
		0x00620011;

	/**
	 * DoublePointCoordinatesData - 0x00660022. Not in Dcm4che v2.0.29
	 */
	public static final int DoublePointCoordinatesData = 0x00660022;
	/**
	 * LongPrimitivePointIndexList - 0x00660040. Not in Dcm4che v2.0.29
	 */
	public static final int LongPrimitivePointIndexList = 0x00660040;
	/**
	 * MeasurementsSequence - 0x00660121. Not in Dcm4che v2.0.29
	 */
	public static final int MeasurementsSequence = 0x00660121;
	/**
	 * FloatingPointValues - 0x00660125. Not in Dcm4che v2.0.29
	 */
	public static final int FloatingPointValues = 0x00660125;
	/**
	 * MeasurementValuesSequence - 0x00660132. Not in Dcm4che v2.0.29
	 */
	public static final int MeasurementValuesSequence = 0x00660132;

	/**
	 * AnnotationCoordinateType - 0x006a0001. Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationCoordinateType = 0x006a0001;
	/**
	 * AnnotationGroupSequence - 0x006a0002. Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationGroupSequence = 0x006a0002;
	/**
	 * AnnotationGroupUID - 0x006a0003. Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationGroupUID = 0x006a0003;
	/**
	 * AnnotationGroupLabel - 0x006a0005. Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationGroupLabel = 0x006a0005;
	/**
	 * AnnotationGroupDescription - 0x006a0006. Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationGroupDescription = 0x006a0006;
	/**
	 * AnnotationGroupGenerationType - 0x006a0007. Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationGroupGenerationType = 0x006a0007;
	/**
	 * AnnotationGroupAlgorithmIdentificationSequence - 0x006a0008.
	 * Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationGroupAlgorithmIdentificationSequence =
		0x006a0008;
	/**
	 * AnnotationGroupPropertyCategoryCodeSequence - 0x006a0009.
	 * Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationGroupPropertyCategoryCodeSequence =
		0x006a0009;
	/**
	 * AnnotationGroupPropertyTypeCodeSequence - 0x006a000a.
	 * Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationGroupPropertyTypeCodeSequence = 0x006a000a;
	/**
	 * NumberOfAnnotations - 0x006a000c. Not in Dcm4che v2.0.29
	 */
	public static final int NumberOfAnnotations = 0x006a000c;
	/**
	 * AnnotationAppliesToAllOpticalPaths - 0x006a000d. Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationAppliesToAllOpticalPaths = 0x006a000d;
	/**
	 * ReferencedOpticalPathIdentifier - 0x006a000e. Not in Dcm4che v2.0.29
	 */
	public static final int ReferencedOpticalPathIdentifier = 0x006a000e;
	/**
	 * AnnotationAppliesToAllZPlanes - 0x006a000f. Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationAppliesToAllZPlanes = 0x006a000f;
	/**
	 * CommonZCoordinateValue - 0x006a0010. Not in Dcm4che v2.0.29
	 */
	public static final int CommonZCoordinateValue = 0x006a0010;
	/**
	 * AnnotationIndexList - 0x006a0011. Not in Dcm4che v2.0.29
	 */
	public static final int AnnotationIndexList = 0x006a0011;

	//	Prevent instantiation.
	private NewTag()
	{}
}
