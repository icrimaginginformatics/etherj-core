/*********************************************************************
 * Copyright (c) 2021, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.dicom;

/**
 *
 * @author jamesd
 */
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DicomComparison
{
	public static final int COMMON = 0;
	public static final int CHANGE = 1;
	public static final int INSERT = 2;
	public static final int DELETE = 3;

	private static final Logger logger =
		LoggerFactory.getLogger(DicomComparison.class);

	private int nestDepth = 0;
	private final List<DiffElement> diffList = new ArrayList<>();
	private DicomObject left = null;
	private DicomObject right = null;

	public DicomComparison(DicomObject left, DicomObject right)
	{
		if (left == null)
		{
			left = new BasicDicomObject();
		}
		this.left = left;
		if (right == null)
		{
			right = new BasicDicomObject();
		}
		this.right = right;

		diff(left, right);
	}

	/**
	 *
	 */
	public void dump()
	{
		dump(System.out);
	}

	/**
	 *
	 * @param ps
	 */
	public void dump(PrintStream ps)
	{
		dump(ps, 64);
	}

	/**
	 *
	 * @param ps
	 * @param width
	 */
	public void dump(PrintStream ps, int width)
	{
		StringBuffer sb = new StringBuffer();

		Iterator<DiffElement> iter = diffList.iterator();
		while (iter.hasNext())
		{
			sb.setLength(0);
			DiffElement dde = iter.next();
			DicomElement le = dde.getLeft();
			DicomElement re = dde.getRight();
			int ddeNestDepth = dde.getNestDepth();
			switch (dde.getType())
			{
				case CHANGE:
				{
					sb.append("c   ");
					for (int i=0; i<ddeNestDepth; i++)
					{
						sb.append("> ");
					}
					le.toStringBuffer(sb, width).append("   ");
					re.toStringBuffer(sb, width);
					break;
				}

				case INSERT:
				{
					sb.append("+   ");
					for (int i=0; i<ddeNestDepth; i++)
					{
						sb.append("> ");
					}
					re.toStringBuffer(sb, width);
					break;
				}

				case DELETE:
				{
					sb.append("-   ");
					for (int i=0; i<ddeNestDepth; i++)
					{
						sb.append("> ");
					}
					le.toStringBuffer(sb, width);
					break;
				}

				default:
				{
					sb.append("    ");
					for (int i=0; i<ddeNestDepth; i++)
					{
						sb.append("> ");
					}
					le.toStringBuffer(sb, width).append("   ");
					re.toStringBuffer(sb, width);
				}
			}
			sb.append(" ")
				.append(DicomUtils.tagName((le != null) ? le.tag() : re.tag()));
			ps.println(sb.toString());
		}
	}

	public DiffElement get(int index)
	{
		return diffList.get(index);
	}

	public DicomObject getLeft()
	{
		return left;
	}

	public DicomObject getRight()
	{
		return right;
	}

	public int size()
	{
		return diffList.size();
	}

	private void addMatchingTags(DicomElement leftNext, DicomElement rightNext)
	{
		if (leftNext.equals(rightNext) || (leftNext.vr() == VR.SQ))
		{
			diffList.add(new DiffElement(COMMON, nestDepth, leftNext,
				rightNext));
		}
		else
		{
			diffList.add(new DiffElement(CHANGE, nestDepth, leftNext,
				rightNext));
		}
		if (isContainer(leftNext))
		{
			processContainer(leftNext, rightNext);
		}
	}

	private ListIterator<DicomElement> createListIterator(
		Iterator<DicomElement> iter)
	{
		List<DicomElement> list = new ArrayList<>();
		while (iter.hasNext())
		{
			list.add(iter.next());
		}
		return list.listIterator();
	}

	private void deleteContainer(DicomElement de)
	{
		int nItems = de.countItems();
		if (de.hasDicomObjects())
		{
			for (int i=0; i<nItems; i++)
			{
				deleteObject(de.getDicomObject(i));
			}
		}
		else
		{
			for (int i=0; i<nItems; i++)
			{
				//onFragment(element, i, dcm);
				logger.debug("DicomDiff - Frag deletion");
			}
		}
	}

	private void deleteObject(DicomObject dcm)
	{
		Iterator<DicomElement> iter = dcm.iterator();
		while (iter.hasNext())
		{
			DicomElement next = iter.next();
			diffList.add(new DiffElement(DELETE, nestDepth, next, null));
			if (isContainer(next))
			{
				processContainer(next, null);
			}
		}
	}

	private void diff(DicomObject leftObject, DicomObject rightObject)
	{
		ListIterator<DicomElement> leftIter = createListIterator(leftObject.iterator());
		ListIterator<DicomElement> rightIter = createListIterator(rightObject.iterator());

		while (leftIter.hasNext())
		{
			DicomElement leftNext = leftIter.next();
			int leftNextTag = leftNext.tag();
			int rightNextTag = peekNextTag(rightIter);
			if (rightObject.contains(leftNextTag))
			{
				if (rightNextTag == leftNextTag)
				{
					DicomElement rightNext = rightIter.next();
					addMatchingTags(leftNext, rightNext);
				}
				else
				{
					handleDisparateTags(leftNext, rightIter);
					// Rewind to ensure we don't skip this unmatched tag from left.
					leftIter.previous();
				}
			}
			else
			{
				handleDisparateTags(leftNext, rightIter);
			}
		}
		// Handle anything remaining in the right object
		while (rightIter.hasNext())
		{
			DicomElement next = rightIter.next();
			diffList.add(new DiffElement(INSERT, nestDepth, null, next));
			if (isContainer(next))
			{
				processContainer(null, next);
			}
		}
	}

	private void handleDisparateTags(DicomElement leftNext, ListIterator<DicomElement> rightIter)
	{
		int leftNextTag = leftNext.tag();
		if (isTagBefore(leftNext.tag(), peekNextTag(rightIter)))
		{
			// Deletion
			diffList.add(new DiffElement(DELETE, nestDepth, leftNext,
				null));
			if (isContainer(leftNext))
			{
				processContainer(leftNext, null);
			}
		}
		else
		{
			// Insertion
			while (isTagBefore(peekNextTag(rightIter), leftNextTag))
			{
				DicomElement rightNext = rightIter.next();
				diffList.add(new DiffElement(INSERT, nestDepth, null,
					rightNext));
				if (isContainer(rightNext))
				{
					processContainer(null, rightNext);
				}
			}
		}
	}

	private void insertContainer(DicomElement de)
	{
		int nItems = de.countItems();
		if (de.hasDicomObjects())
		{
			for (int i=0; i<nItems; i++)
			{
				insertObject(de.getDicomObject(i));
			}
		}
		else
		{
			for (int i=0; i<nItems; i++)
			{
				//onFragment(element, i, dcm);
				logger.debug("DicomDiff - Frag insertion");
			}
		}
	}

	private void insertObject(DicomObject dcm)
	{
		Iterator<DicomElement> iter = dcm.iterator();
		while (iter.hasNext())
		{
			DicomElement next = iter.next();
			diffList.add(new DiffElement(INSERT, nestDepth, null, next));
			if (isContainer(next))
			{
				processContainer(null, next);
			}
		}
	}

	private boolean isContainer(DicomElement element)
	{
		return (element.length() == -1 || element.vr() == VR.SQ);
	}

	private boolean isTagBefore(int first, int second)
	{
		return ((first & 0xffffffffL) < (second & 0xffffffffL));
	}

	private int peekNextTag(ListIterator<DicomElement> iter)
	{
		int nextTag = 0xffffffff;
		if (iter.hasNext())
		{
			// Peek at next element then rewind to not disturb iterator state
			nextTag = iter.next().tag();
			iter.previous();
		}
		return nextTag;
	}

	private void processContainer(DicomElement left, DicomElement right)
	{
		nestDepth++;
		if (left == null)
		{
			insertContainer(right);
			nestDepth--;
			return;
		}
		if (right == null)
		{
			deleteContainer(left);
			nestDepth--;
			return;
		}

		int leftItems = left.countItems();
		int rightItems = right.countItems();
		int nItems = Math.min(leftItems, rightItems);
		if (left.hasDicomObjects())
		{
			for (int i=0; i<nItems; i++)
			{
				diff(left.getDicomObject(i), right.getDicomObject(i));
			}
		}
		else
		{
			for (int i=0; i<nItems; i++)
			{
				//onFragment(element, i, dcm);
				logger.debug("DicomDiff - Frag encountered");
			}
		}
		if (leftItems != rightItems)
		{
			if (leftItems > rightItems)
			{
				for (int i=nItems; i<leftItems; i++)
				{
					deleteObject(left.getDicomObject(i));
				}
			}
			else
			{
				for (int i=nItems; i<rightItems; i++)
				{
					insertObject(right.getDicomObject(i));
				}
			}
		}
		nestDepth--;
	}

	public static class DiffElement
	{
		DicomElement left;
		DicomElement right;
		int type;
		int nestDepth;

		public DiffElement(int type, int nestDepth, DicomElement left,
			DicomElement right)
		{
			this.type = type;
			this.nestDepth = nestDepth;
			this.left = left;
			this.right = right;
		}

		public DicomElement getLeft()
		{
			return left;
		}

		public int getNestDepth()
		{
			return nestDepth;
		}

		public DicomElement getRight()
		{
			return right;
		}

		public int getType()
		{
			return type;
		}
	}
}