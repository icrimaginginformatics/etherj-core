/** *******************************************************************
 * Copyright (c) 2018, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************** */
package icr.etherj.dicom;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jamesd
 */
public class Validation
{
	private static final Logger logger =
		LoggerFactory.getLogger(Validation.class);

	public static boolean type1(String clazz, String value, int tag)
	{
		boolean ok = (value != null) && !value.isEmpty();
		if (!ok)
		{
			logger.error(clazz+": "+getTagName(tag)+" is missing or empty");
		}
		return ok;
	}

	public static boolean type1(String clazz, String value, String[] allowable,
		int tag)
	{
		boolean ok = type1(clazz, value, tag);
		if (!ok)
		{
			return false;
		}
		return contains(allowable, value);
	}

	public static boolean type1(String clazz, int value, int min, int tag)
	{
		if (value < min)
		{
			logger.error(clazz+": "+getTagName(tag)+" < "+min);
			return false;
		}
		return true;
	}

	public static boolean type1(String clazz,
		Collection<? extends Validatable> collection, int min, int tag)
	{
		return type1(clazz, collection, min, Integer.MAX_VALUE, tag);
	}

	public static boolean type1(String clazz,
		Collection<? extends Validatable> collection, int min, int max, int tag)
	{
		if (collection.size() < min)
		{
			logger.error(clazz+": "+getTagName(tag)+" requires at least "+
				min+" elements.");
			return false;
		}
		if (collection.size() > max)
		{
			logger.error(clazz+": "+getTagName(tag)+" allows at most "+
				max+" elements.");
			return false;
		}
		boolean ok = true;
		for (Validatable item : collection)
		{
			ok &= item.validate();
		}
		return ok;
	}

	public static boolean type2(String clazz, String value, int tag)
	{
		boolean ok = (value != null);
		if (!ok)
		{
			logger.error(clazz+": "+getTagName(tag)+" is missing");
		}
		return ok;
	}

	public static boolean type2(String clazz, String value, String[] allowable,
		int tag)
	{
		if (value == null)
		{
			return false;
		}
		return value.isEmpty() || contains(allowable, value);
	}

	public static boolean type2Date(String clazz, String value, int tag)
	{
		boolean ok = (value != null);
		if (!ok)
		{
			logger.error(clazz+": "+getTagName(tag)+" is missing");
		}
		else
		{
			try
			{
				if (!value.isEmpty() && (DicomUtils.parseDate(value) == null))
				{
					ok = false;
					logger.error(clazz+": "+getTagName(tag)+" is not a valid DA: "+value);
				}
			}
			catch (IllegalArgumentException ex)
			{
				ok = false;
				logger.error(clazz+": "+getTagName(tag)+" is not a valid DA: "+value);
			}
		}
		return ok;
	}

	public static boolean type2Time(String clazz, String value, int tag)
	{
		boolean ok = (value != null);
		if (!ok)
		{
			logger.error(clazz+": "+getTagName(tag)+" is missing");
		}
		else
		{
			try
			{
				if (!value.isEmpty() && (DicomUtils.parseTime(value) == null))
				{
					ok = false;
					logger.error(clazz+": "+getTagName(tag)+" is not a valid TM: "+value);
				}
			}
			catch (IllegalArgumentException ex)
			{
				ok = false;
				logger.error(clazz+": "+getTagName(tag)+" is not a valid TM: "+value);
			}
		}
		return ok;
	}

	public static boolean type3(String clazz, String value, String[] allowable,
		int tag)
	{
		return (value == null) || contains(allowable, value);
	}

	public static boolean type3Date(String clazz, String value, int tag)
	{
		if (value == null)
		{
			return true;
		}
		else
		{
			try
			{
				if (!value.isEmpty() && (DicomUtils.parseDate(value) == null))
				{
					logger.error(clazz+": "+getTagName(tag)+" is not a valid DA: "+value);
					return false;
				}
			}
			catch (IllegalArgumentException ex)
			{
				logger.error(clazz+": "+getTagName(tag)+" is not a valid DA: "+value);
				return false;
			}
		}
		return true;
	}

	public static boolean type3Time(String clazz, String value, int tag)
	{
		if (value == null)
		{
			return true;
		}
		else
		{
			try
			{
				if (!value.isEmpty() && (DicomUtils.parseTime(value) == null))
				{
					logger.error(clazz+": "+getTagName(tag)+" is not a valid TM: "+value);
					return false;
				}
			}
			catch (IllegalArgumentException ex)
			{
				logger.error(clazz+": "+getTagName(tag)+" is not a valid TM: "+value);
				return false;
			}
		}
		return true;
	}

	private static boolean contains(String[] allowable, String value)
	{
		Set<String> set = new HashSet<>(Arrays.asList(allowable));
		return set.contains(value);
	}

	private static String getTagName(int tag)
	{
		String tagName = DicomUtils.tagName(tag);
		if (tagName == null)
		{
			int group = (tag >> 16) & 0x0000ffff;
			int element = tag & 0x0000ffff;
			tagName = String.format("(%04X,%04X)", group, element);
		}
		return tagName;
	}

	private Validation()
	{}
}
