/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.concurrent;

import java.beans.PropertyChangeListener;

/**
 *
 * @author jamesd
 */
public interface TaskMonitor
{
	/** Property name for property change support of cancellation. */
	public static final String CANCELLED = "cancelled";
	/** Property name for property change support of description. */
	public static final String DESCRIPTION = "description";
	/** Property name for property change support of indeterminism. */
	public static final String INDETERMINATE = "indeterminate";
	/** Property name for property change support of maximum value. */
	public static final String MAXIMUM = "maximum";
	/** Property name for property change support of minimum value. */
	public static final String MINIMUM = "minimum";
	/** Property name for property change support of title. */
	public static final String TITLE = "title";
	/** Property name for property change support of current value. */
	public static final String VALUE = "value";

	/**
	 * Adds a property change listener.
	 * @param pcl the property change listener
	 */
	void addPropertyChangeListener(PropertyChangeListener pcl);

	/**
	 * Returns the description text.
	 * @return the description
	 */
	String getDescription();

	/**
	 * Returns the maximum value.
	 * @return the maximum value
	 */
	int getMaximum();

	/**
	 * Returns the minimum value.
	 * @return the minimum value
	 */
	int getMinimum();

	/**
	 * Returns the title.
	 * @return the title
	 */
	String getTitle();

	/**
	 * Returns the current value.
	 * @return the value
	 */
	int getValue();

	/**
	 * Returns true if the task monitor can be cancelled.
	 * @return true if cancellable
	 */
	boolean isCancellable();

	/**
	 * Returns true if the task monitor has been cancelled.
	 * @return true if cancelled
	 */
	boolean isCancelled();

	/**
	 * Returns true if the task monitor is indeterminate.
	 * @return true if indeterminate
	 */
	boolean isIndeterminate();

	/**
	 * Removes a property change listener.
	 * @param pcl the property change listener
	 */
	void removePropertyChangeListener(PropertyChangeListener pcl);

	/**
	 * Sets the cancelled property.
	 * @param cancelled whether to cancel
	 */
	void setCancelled(boolean cancelled);

	/**
	 * Sets the description text.
	 * @param description the description
	 */
	void setDescription(String description);

	/**
	 * Sets the indeterminate property.
	 * @param indeterminate whether task monitor is indeterminate
	 */
	void setIndeterminate(boolean indeterminate);

	/**
	 * Sets the maximum value.
	 * @param max the value
	 */
	void setMaximum(int max);

	/**
	 * Sets the minimum value.
	 * @param min the value
	 */
	void setMinimum(int min);

	/**
	 * Sets the title.
	 * @param title the title
	 */
	void setTitle(String title);

	/**
	 * Sets the current value.
	 * @param value the value
	 */
	void setValue(int value);
}
