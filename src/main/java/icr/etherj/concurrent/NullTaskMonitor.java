/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.concurrent;

import java.beans.PropertyChangeListener;

/**
 * A no-op implementation of the <code>TaskMonitor</code> interface to provide
 * a minimal overhead fall back class when no full implementation is needed
 * @author jamesd
 */
class NullTaskMonitor implements TaskMonitor
{
	@Override
	public void addPropertyChangeListener(PropertyChangeListener pcl)
	{}

	@Override
	public String getDescription()
	{
		return "Null";
	}

	@Override
	public int getMaximum()
	{
		return 0;
	}

	@Override
	public int getMinimum()
	{
		return 0;
	}

	@Override
	public String getTitle()
	{
		return "Null";
	}

	@Override
	public int getValue()
	{
		return 0;
	}

	@Override
	public boolean isCancellable()
	{
		return false;
	}

	@Override
	public boolean isCancelled()
	{
		return false;
	}

	@Override
	public boolean isIndeterminate()
	{
		return false;
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener pcl)
	{}

	@Override
	public void setCancelled(boolean cancelled)
	{}

	@Override
	public void setDescription(String description)
	{}

	@Override
	public void setIndeterminate(boolean indeterminate)
	{}

	@Override
	public void setMaximum(int max)
	{}

	@Override
	public void setMinimum(int min)
	{}

	@Override
	public void setTitle(String title)
	{}

	@Override
	public void setValue(int value)
	{}
}
