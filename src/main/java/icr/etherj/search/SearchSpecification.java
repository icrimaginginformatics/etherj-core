/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.search;

import com.google.common.collect.ImmutableList;
import icr.etherj.AbstractDisplayable;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * A collection of {@link SearchCriterion}s making up a complete specification
 * of what to search for.
 * @author jamesd
 */
public class SearchSpecification extends AbstractDisplayable
{
	private final List<SearchCriterion> criteria = new ArrayList<>();

	/**
	 * Constructs a new <code>SearchSpecification</code>.
	 */
	public SearchSpecification()
	{}

	/**
	 * Add a list of criteria to the collection.
	 * @param criteria the criteria to add
	 */
	public void addCriteria(List<SearchCriterion> criteria)
	{
		this.criteria.addAll(criteria);
	}

	/**
	 * Add a criterion to the collection.
	 * @param criterion the criterion to add
	 */
	public void addCriterion(SearchCriterion criterion)
	{
		criteria.add(criterion);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		int nCriteria = criteria.size();
		ps.println(pad+"Criteria: "+nCriteria+" element"+
			(nCriteria != 1 ? "s" : ""));
		if (recurse)
		{
			for (SearchCriterion crit : criteria)
			{
				crit.display(ps, "  ");
			}
		}
	}

	/**
	 * Returns the list of criteria.
	 * @return the list of criteria
	 */
	public List<SearchCriterion> getCriteria()
	{
		return ImmutableList.copyOf(criteria);
	}

	/**
	 * Returns the number of criteria in the collection.
	 * @return the number of criteria
	 */
	public int getCriteriaCount()
	{
		return criteria.size();
	}
}
