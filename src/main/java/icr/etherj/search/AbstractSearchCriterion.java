/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.search;

import java.io.PrintStream;
import java.util.List;

/**
 * A skeletal implementation of {@link SearchCriterion} interface to reduce the
 * effort required for a full implementation.
 * @author jamesd
 */
public abstract class AbstractSearchCriterion implements SearchCriterion
{
	@Override
	public String createCombinatorSql()
	{
		String sql = "";
		switch (getCombinator())
		{
			case SearchCriterion.And:
				return "AND";
			case SearchCriterion.Or:
				return "OR";
			default:
				break;
		}
		return sql;
	}

	@Override
	public String createComparatorSql()
	{
		String sql = "";
		switch (getComparator())
		{
			case SearchCriterion.LessThan:
				return "<";
			case SearchCriterion.LessThanOrEqual:
				return "<=";
			case SearchCriterion.Equal:
				return "=";
			case SearchCriterion.GreaterThanOrEqual:
				return ">=";
			case SearchCriterion.GreaterThan:
				return ">";
			case SearchCriterion.NotEqual:
				return "!=";
			case SearchCriterion.Like:
				return "LIKE";
			default:
				break;
		}
		return sql;
	}

	@Override
	public void display()
	{
		display(System.out, "", false);
	}

	@Override
	public void display(boolean recurse)
	{
		display(System.out, "", recurse);
	}

	@Override
	public void display(String indent)
	{
		display(System.out, indent, false);
	}
	
	@Override
	public void display(String indent, boolean recurse)
	{
		display(System.out, indent, recurse);
	}

	@Override
	public void display(PrintStream ps)
	{
		display(ps, "", false);
	}

	@Override
	public void display(PrintStream ps, boolean recurse)
	{
		display(ps, "", recurse);
	}

	@Override
	public void display(PrintStream ps, String indent)
	{
		display(ps, indent, false);
	}

	/**
	 * Utility method for producing the SQL WHERE clause from the criterion
	 * and any children.
	 * @param sb the StringBuilder
	 * @param crit the criterion
	 * @return the StringBuilder
	 */
	protected final StringBuilder buildString(StringBuilder sb,
		SearchCriterion crit)
	{
		if (crit.hasCriteria())
		{
			sb.append("(");
			List<SearchCriterion> critList = crit.getCriteria();
			buildString(sb, critList.get(0));
			for (int i=1; i<critList.size(); i++)
			{
				SearchCriterion sc = critList.get(i);
				sb.append(" ").append(sc.createCombinatorSql()).append(" ");
				buildString(sb, sc);
			}
			sb.append(")");
		}
		else
		{
			sb.append("(");
			String typeStr = crit.getTypeString();
			if ((typeStr != null) && !typeStr.isEmpty())
			{
				sb.append(typeStr).append(".");
			}
			sb = sb.append(crit.getTagName())
				.append(" ").append(crit.createComparatorSql()).append(" ")
				.append(crit.getValue()).append(")");

		}
		return sb;
	}

}
