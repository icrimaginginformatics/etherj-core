/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import java.io.PrintStream;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author jamesd
 */
public class ImagingObservation extends Entity implements TypeCoded,
	QuestionTypeCoded
{
	private double annotatorConfidence = 0;
	private String comment = "";
	private boolean isPresent = false;
	private String label = "";
	private int questionIndex = -1;
	private final SortedMap<String,Code> questionTypeCodes = new TreeMap<>();
	private final SortedMap<String,Code> typeCodes = new TreeMap<>();

	@Override
	public Code addQuestionTypeCode(Code code)
	{
		return addCode(code, questionTypeCodes);
	}

	@Override
	public Code addTypeCode(Code code)
	{
		return addCode(code, typeCodes);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"UID: "+getUid());
		int nCodes = typeCodes.size();
		if (nCodes < 1)
		{
			ps.println(pad+"ERROR. ImagingObservation requires 1 or more type codes.");
			return;
		}
		ps.println(pad+"TypeCodes:");
		Set<String> keys = typeCodes.keySet();
		for (String key : keys)
		{
			Code code = typeCodes.get(key);
			code.display(ps, indent+"  ");
		}
		keys = questionTypeCodes.keySet();
		if (!keys.isEmpty())
		{
			ps.println(pad+"QuestionTypeCodes:");
		}
		for (String key : keys)
		{
			Code code = questionTypeCodes.get(key);
			code.display(ps, indent+"  ");
		}
		ps.println(pad+"IsPresent: "+isPresent);
		ps.println(pad+"AnnotatorConfidence: "+annotatorConfidence);
		if (!label.isEmpty())
		{
			ps.println(pad+"Label: "+label);
		}
		if (questionIndex >= 0)
		{
			ps.println(pad+"QuestionIndex: "+questionIndex);
		}
		if (!comment.isEmpty())
		{
			ps.println(pad+"Comment: "+comment);
		}
	}

	/**
	 * @return the annotatorConfidence
	 */
	public double getAnnotatorConfidence()
	{
		return annotatorConfidence;
	}

	/**
	 * @return the comment
	 */
	public String getComment()
	{
		return comment;
	}

	/**
	 * @return the isPresent
	 */
	public boolean getIsPresent()
	{
		return isPresent;
	}

	/**
	 * @return the label
	 */
	public String getLabel()
	{
		return label;
	}

	/**
	 * @return the questionIndex
	 */
	public int getQuestionIndex()
	{
		return questionIndex;
	}
	
	@Override
	public Code getQuestionTypeCode(String code)
	{
		return getCode(code, questionTypeCodes);
	}

	/**
	 * @return the questionTypeCodes
	 */
	@Override
	public SortedMap<String,Code> getQuestionTypeCodeMap()
	{
		return getCodeMap(questionTypeCodes);
	}

	@Override
	public Code getTypeCode(String code)
	{
		return getCode(code, typeCodes);
	}

	/**
	 * @return the typeCodes
	 */
	@Override
	public SortedMap<String,Code> getTypeCodeMap()
	{
		return getCodeMap(typeCodes);
	}

	@Override
	public Code removeQuestionTypeCode(String code)
	{
		return removeCode(code, questionTypeCodes);
	}

	@Override
	public Code removeTypeCode(String code)
	{
		return removeCode(code, typeCodes);
	}

	/**
	 * @param annotatorConfidence the annotatorConfidence to set
	 */
	public void setAnnotatorConfidence(double annotatorConfidence)
	{
		this.annotatorConfidence = annotatorConfidence;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment)
	{
		this.comment = comment;
	}

	/**
	 * @param isPresent the isPresent to set
	 */
	public void setIsPresent(boolean isPresent)
	{
		this.isPresent = isPresent;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label)
	{
		this.label = label;
	}

	/**
	 * @param questionIndex the questionIndex to set
	 */
	public void setQuestionIndex(int questionIndex)
	{
		this.questionIndex = questionIndex;
	}

}
