/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import com.google.common.collect.ImmutableList;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author jamesd
 */
public class Calculation extends Entity implements TypeCoded, QuestionTypeCoded
{
	private Algorithm algorithm = null;
	private String description = "";
	private String mathML = "";
	private int questionIndex = -1;
	private final SortedMap<String,Code> questionTypeCodes = new TreeMap<>();
	private final List<CalculationResult> results = new ArrayList<>();
	private final SortedMap<String,Code> typeCodes = new TreeMap<>();

	public boolean addCalculationResult(CalculationResult result)
	{
		return results.add(result);
	}

	@Override
	public Code addQuestionTypeCode(Code code)
	{
		return addCode(code, questionTypeCodes);
	}

	@Override
	public Code addTypeCode(Code code)
	{
		return addCode(code, typeCodes);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"UID: "+getUid());
		int nCodes = typeCodes.size();
		if (nCodes < 1)
		{
			ps.println(pad+"ERROR. ImagingPhysical requires 1 or more type codes.");
			return;
		}
		ps.println(pad+"TypeCodes:");
		Set<String> keys = typeCodes.keySet();
		for (String key : keys)
		{
			Code code = typeCodes.get(key);
			code.display(ps, indent+"  ");
		}
		keys = questionTypeCodes.keySet();
		if (!keys.isEmpty())
		{
			ps.println(pad+"QuestionTypeCodes:");
		}
		for (String key : keys)
		{
			Code code = questionTypeCodes.get(key);
			code.display(ps, indent+"  ");
		}
		ps.println(pad+"Description: "+description);
		if (!mathML.isEmpty())
		{
			ps.println(pad+"MathML: "+mathML);
		}
		if (questionIndex >= 0)
		{
			ps.println(pad+"QuestionIndex: "+questionIndex);
		}
		int nResults = results.size();
		ps.println(pad+"ResultList: "+nResults+" result"+
			(nResults != 1 ? "s" : ""));
		if (algorithm != null)
		{
			ps.println(pad+"Algorithm:");
			algorithm.display(ps, indent+"  ", recurse);
		}
		if (recurse)
		{
			if (!results.isEmpty())
			{
				ps.println(pad+"Results:");
			}
			for (CalculationResult result : results)
			{
				result.display(ps, indent+"  ", recurse);
			}
		}
	}

	/**
	 * @return the algorithm
	 */
	public Algorithm getAlgorithm()
	{
		return algorithm;
	}

	public CalculationResult getCalculationResult(int idx)
	{
		return results.get(idx);
	}

	public int getCalculationResultCount()
	{
		return results.size();
	}

	public List<CalculationResult> getCalculationResultList()
	{
		return ImmutableList.copyOf(results);
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return the mathML
	 */
	public String getMathML()
	{
		return mathML;
	}

	/**
	 * @return the questionIndex
	 */
	public int getQuestionIndex()
	{
		return questionIndex;
	}
	
	/**
	 * @return the questionTypeCodes
	 */
	@Override
	public SortedMap<String,Code> getQuestionTypeCodeMap()
	{
		return getCodeMap(questionTypeCodes);
	}

	@Override
	public Code getQuestionTypeCode(String code)
	{
		return questionTypeCodes.get(code);
	}

	@Override
	public Code getTypeCode(String code)
	{
		return typeCodes.get(code);
	}

	/**
	 * @return the typeCodes
	 */
	@Override
	public SortedMap<String,Code> getTypeCodeMap()
	{
		return getCodeMap(typeCodes);
	}

	@Override
	public Code removeTypeCode(String code)
	{
		return removeCode(code, typeCodes);
	}

	@Override
	public Code removeQuestionTypeCode(String code)
	{
		return removeCode(code, questionTypeCodes);
	}

	/**
	 * @param algorithm the algorithm to set
	 */
	public void setAlgorithm(Algorithm algorithm)
	{
		this.algorithm = algorithm;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @param mathML the mathML to set
	 */
	public void setMathML(String mathML)
	{
		this.mathML = mathML;
	}

	/**
	 * @param questionIndex the questionIndex to set
	 */
	public void setQuestionIndex(int questionIndex)
	{
		this.questionIndex = questionIndex;
	}

}
