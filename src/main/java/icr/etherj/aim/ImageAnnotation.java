/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import java.io.PrintStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

/**
 * ImageAnnotation from AIM v4r48.
 * @author jamesd
 */
public class ImageAnnotation extends Annotation
{
	private final Map<String,Markup> markups = new LinkedHashMap<>();
	private final Map<String,ImageReference> references = new LinkedHashMap<>();
	private final Map<String,DicomSegmentation> segmentations = new LinkedHashMap<>();

	/**
	 * Adds a Markup to the ImageAnnotation.
	 * @param markup the Markup to add
	 * @return the previous Markup with the same UID or null if no Markup
	 * exists with the same UID.
	 */
	public Markup addMarkup(Markup markup)
	{
		return addEntity(markup, markups);
	}

	/**
	 * Adds an ImageReference to the ImageAnnotation.
	 * @param reference the ImageReference to add
	 * @return the previous ImageAnnotation with the same UID or null if no
	 * ImageAnnotation exists with the same UID
	 */
	public ImageReference addReference(ImageReference reference)
	{
		return addEntity(reference, references);
	}

	/**
	 * Adds an DicomSegmentation to the ImageAnnotation.
	 * @param segmentation the DicomSegmentation to add
	 * @return the previous DicomSegmentation with the same UID or null if no
	 * DicomSegmentation exists with the same UID
	 */
	public DicomSegmentation addSegmentation(DicomSegmentation segmentation)
	{
		return addEntity(segmentation, segmentations);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"Comment: "+getComment());
		SortedMap<String,Code> codeMap = getTypeCodeMap();
		int nCodes = codeMap.size();
		if (nCodes < 1)
		{
			ps.println(pad+"ERROR. Annotation requires 1 or more type codes.");
			return;
		}
		Set<String> keys = codeMap.keySet();
		for (String key : keys)
		{
			Code code = codeMap.get(key);
			code.display(ps, indent+"  ");
		}
		ps.println(pad+"DateTime: "+getDateTime());
		ps.println(pad+"Name: "+getName());
		List<ImagingPhysical> ipList = getImagingPhysicalList();
		int nIP = ipList.size();
		ps.println(pad+"ImagingPhysicalList: "+nIP+" imagingphysical"+
			(nIP != 1 ? "s" : ""));
		List<Calculation> calcList = getCalculationList();
		int nCalcs = calcList.size();
		ps.println(pad+"CalculationList: "+nCalcs+" calculation"+
			(nCalcs != 1 ? "s" : ""));
		List<ImagingObservation> ioList = getImagingObservationList();
		int nIO = ioList.size();
		ps.println(pad+"ImagingObservationList: "+nIP+" imagingobervation"+
			(nIO != 1 ? "s" : ""));
		int nMarkup = markups.size();
		ps.println(pad+"MarkupList: "+nMarkup+" markup"+
			(nMarkup != 1 ? "s" : ""));
		int nRef = references.size();
		ps.println(pad+"ReferenceList: "+nRef+" reference"+
			(nRef != 1 ? "s" : ""));
		List<AnnotationRole> roleList = getAnnotationRoleList();
		int nRoles = roleList.size();
		ps.println(pad+"AnnotationRoleList: "+nIP+" annotationrole"+
			(nRoles != 1 ? "s" : ""));
		List<Inference> infList = getInferenceList();
		int nInf = infList.size();
		ps.println(pad+"InferenceList: "+nIP+" inference"+
			(nInf != 1 ? "s" : ""));
		List<TaskContext> tcList = getTaskContextList();
		int nTC = tcList.size();
		ps.println(pad+"TaskContextList: "+nTC+" taskcontext"+
			(nTC != 1 ? "s" : ""));
		List<DicomSegmentation> dsList = getDicomSegmentationList();
		int nSeg = dsList.size();
		ps.println(pad+"DicomSegmentationList: "+nSeg+" segmentation"+
			(nSeg != 1 ? "s" : ""));
		if (recurse)
		{
			ps.println(pad+"ImagingPhysicals:");
			ipList.forEach((ip) -> ip.display(ps, indent+"  ", true));
			ps.println(pad+"Calculations:");
			calcList.forEach((calc) -> calc.display(ps, indent+"  ", true));
			ps.println(pad+"ImagingObservations:");
			ioList.forEach((io) -> io.display(ps, indent+"  ", true));
			ps.println(pad+"Markups:");
			List<Markup> markupList = getMarkupList();
			markupList.forEach((markup) -> markup.display(ps, indent+"  ", true));
			ps.println(pad+"References:");
			List<ImageReference> refList = getReferenceList();
			refList.forEach((ref) -> ref.display(ps, indent+"  ", true));
			ps.println(pad+"AnnotationRoles:");
			roleList.forEach((role) -> role.display(ps, indent+"  ", true));
			ps.println(pad+"Inferences:");
			infList.forEach((inf) -> inf.display(ps, indent+"  ", true));
			ps.println(pad+"TaskContexts:");
			tcList.forEach((tc) -> tc.display(ps, indent+"  ", true));
			ps.println(pad+"DicomSegmentations:");
			dsList.forEach((ds) -> ds.display(ps, indent+"  ", true));
		}
	}

	/**
	 * Returns the Markup with the supplied UID or null if it does not exist.
	 * @param uid the UID of the Markup to be returned
	 * @return the Markup with the supplied UID or null if it does not exist
	 */
	public Markup getMarkup(String uid)
	{
		return getEntity(uid, markups);
	}

	/**
	 * Returns the number of Markups in the ImageAnnotation.
	 * @return the number of Markups
	 */
	public int getMarkupCount()
	{
		return markups.size();
	}

	/**
	 * Returns the list of Markups in the ImageAnnotation.
	 * @return the list of Markups
	 */
	public List<Markup> getMarkupList()
	{
		return getEntityList(markups);
	}

	/**
	 * Returns the ImageReference with the supplied UID or null if it does not
	 * exist.
	 * @param uid the UID of the ImageReference to be returned
	 * @return the ImageReference with the supplied UID or null if it does not
	 * exist
	 */
	public ImageReference getReference(String uid)
	{
		return getEntity(uid, references);
	}

	/**
	 * Returns the number of ImageReferences in the ImageAnnotation.
	 * @return the number of ImageReferences
	 */
	public int getReferenceCount()
	{
		return references.size();
	}

	/**
	 * Returns the list of ImageReferences in the ImageAnnotation.
	 * @return the list of ImageReferences
	 */
	public List<ImageReference> getReferenceList()
	{
		return getEntityList(references);
	}

	/**
	 * Returns the DicomSegmentation with the supplied UID or null if it does not
	 * exist.
	 * @param uid the UID of the DicomSegmentation to be returned
	 * @return the DicomSegmentation with the supplied UID or null if it does not
	 * exist
	 */
	public DicomSegmentation getDicomSegmentation(String uid)
	{
		return getEntity(uid, segmentations);
	}

	/**
	 * Returns the number of DicomSegmentations in the ImageAnnotation.
	 * @return the number of DicomSegmentations
	 */
	public int getDicomSegmentationCount()
	{
		return segmentations.size();
	}

	/**
	 * Returns the list of DicomSegmentations in the ImageAnnotation.
	 * @return the list of DicomSegmentations
	 */
	public List<DicomSegmentation> getDicomSegmentationList()
	{
		return getEntityList(segmentations);
	}

	/**
	 * Removes a Markup from the ImageAnnotation.
	 * @param uid the of the Markup to remove
	 * @return the removed Markup with the same UID or null if no Markup exists
	 * with the same UID
	 */
	public Markup removeMarkup(String uid)
	{
		return removeEntity(uid, markups);
	}

	/**
	 * Removes an ImageReference from the ImageAnnotation.
	 * @param uid the of the ImageReference to remove
	 * @return the removed ImageReference with the same UID or null if no
	 * ImageReference exists with the same UID
	 */
	public ImageReference removeReference(String uid)
	{
		return removeEntity(uid, references);
	}

	/**
	 * Removes a DicomSegmentation from the ImageAnnotation.
	 * @param uid the of the DicomSegmentation to remove
	 * @return the removed DicomSegmentation with the same UID or null if no
	 * DicomSegmentation exists with the same UID
	 */
	public DicomSegmentation removeDicomSegmentation(String uid)
	{
		return removeEntity(uid, segmentations);
	}

}
