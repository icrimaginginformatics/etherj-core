/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import java.util.HashMap;
import java.util.Map;

/**
 * Markup from AIM v4r48, base class for ROIs.
 * @author jamesd
 */
public abstract class Markup extends Entity
{
	public static final String TwoDimensionCircleClass = "icr.etherj.aim.TwoDimensionCircle";
	public static final String TwoDimensionEllipseClass = "icr.etherj.aim.TwoDimensionEllipse";
	public static final String TwoDimensionMultiPointClass = "icr.etherj.aim.TwoDimensionMultiPoint";
	public static final String TwoDimensionPointClass = "icr.etherj.aim.TwoDimensionPoint";
	public static final String TwoDimensionPolylineClass = "icr.etherj.aim.TwoDimensionPolyline";
	public static final int TwoDimensionCircle = 0;
	public static final int TwoDimensionEllipse = 1;
	public static final int TwoDimensionMultiPoint = 2;
	public static final int TwoDimensionPoint = 3;
	public static final int TwoDimensionPolyline = 4;
	private static final Map<Integer,String> intToName = new HashMap<>();
	private static final Map<String,Integer> nameToInt = new HashMap<>();

	protected int dimensionCount = 0;

	static
	{
		intToName.put(TwoDimensionCircle, TwoDimensionCircleClass);
		intToName.put(TwoDimensionEllipse, TwoDimensionEllipseClass);
		intToName.put(TwoDimensionMultiPoint, TwoDimensionMultiPointClass);
		intToName.put(TwoDimensionPoint, TwoDimensionPointClass);
		intToName.put(TwoDimensionPolyline, TwoDimensionPolylineClass);

		nameToInt.put(TwoDimensionCircleClass, TwoDimensionCircle);
		nameToInt.put(TwoDimensionEllipseClass, TwoDimensionEllipse);
		nameToInt.put(TwoDimensionMultiPointClass, TwoDimensionMultiPoint);
		nameToInt.put(TwoDimensionPointClass, TwoDimensionPoint);
		nameToInt.put(TwoDimensionPolylineClass, TwoDimensionPolyline);
	}

	/**
	 * Returns the integer class code for the class name.
	 * @param name
	 * @return the integer class code for the class name
	 */
	public static int getClassCode(String name)
	{
		Integer value = nameToInt.get(name);
		return (value == null) ? -1 : value;
	}

	/**
	 * Returns the class name for the integer class code.
	 * @param code
	 * @return the class name for the integer class code
	 */
	public static String getClassName(int code)
	{
		return intToName.get(code);
	}

	/**
	 * Returns the dimension count of the markup.
	 * @return the dimension count
	 */
	public int getDimensionCount()
	{
		return dimensionCount;
	}

}
