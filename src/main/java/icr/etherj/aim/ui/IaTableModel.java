/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim.ui;

import icr.etherj.aim.ImageAnnotation;
import icr.etherj.aim.ImageAnnotationCollection;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author jamesd
 */
public class IaTableModel extends AbstractTableModel
{
	String[] colNames = new String[] {"Person Name", "Person ID",
		"Lesion", "Scan", "ROI", ""};
	List<IaItem> itemList = new ArrayList<>();

	public IaTableModel()
	{
		this(new ArrayList<ImageAnnotationCollection>());
	}

	public IaTableModel(List<ImageAnnotationCollection> list)
	{
		for (ImageAnnotationCollection iac : list)
		{
			List<ImageAnnotation> annoList = iac.getAnnotationList();
			for (ImageAnnotation ia : annoList)
			{
				itemList.add(new DefaultIaItem(ia, iac));
			}
		}
		itemList.sort(new DefaultIaItem.Comparator());
	}

	@Override
	public int getColumnCount()
	{
		return colNames.length;
	}

	@Override
	public String getColumnName(int col)
	{
		return colNames[col];
	}

	@Override
	public int getRowCount()
	{
		return itemList.size();
	}

	public List<IaItem> getSelection(int[] rows)
	{
		List<IaItem> list = new ArrayList<>();
		for (int idx : rows)
		{
			list.add(itemList.get(idx));
		}
		return list;
	}

	public List<IaItem> getSelection(int startIdx, int endIdx)
	{
		return itemList.subList(startIdx, endIdx);
	}

	@Override
	public Object getValueAt(int row, int col)
	{
		IaItem item = itemList.get(row);
		switch (col)
		{
			case 0:
				return item.getPersonName();
			case 1:
				return item.getPersonId();
			case 2:
				return item.getLesionNumber();
			case 3:
				return item.getScan();
			case 4:
				return item.getRoiNumber();
			default:
		}
		return "";
	}

}
