/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************** */
package icr.etherj.aim.ui.impl;

import icr.etherj.aim.AimUnique;
import icr.etherj.aim.AimUtils;
import icr.etherj.aim.TwoDimensionGeometricShape;
import icr.etherj.aim.ui.AbstractAimTreeNode;

/**
 *
 * @author jamesd
 */
final class TwoDGeometricShapeNode extends AbstractAimTreeNode
{
	private final TwoDimensionGeometricShape shape2D;
	private final String key;

	public TwoDGeometricShapeNode(TwoDimensionGeometricShape shape2D)
	{
		super(false);
		this.shape2D = shape2D;
		String desc = shape2D.getDescription();
		if ((desc != null) && !desc.isEmpty())
		{
			setDisplayName(desc);
		}
		else
		{
			String label = shape2D.getLabel();
			if ((label != null) && !label.isEmpty())
			{
				setDisplayName(label);
			}
			else
			{
				setDisplayName(shape2D.getClass().getSimpleName());
			}
		}
		key = "01_"+getDisplayName()+"_"+shape2D.getUid();
	}

	@Override
	public String getKey()
	{
		return key;
	}

	@Override
	public AimUnique getAimEntity()
	{
		return shape2D;
	}
	
}
