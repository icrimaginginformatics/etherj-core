/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import icr.etherj.AbstractDisplayable;
import icr.etherj.Uids;
import java.util.List;

/**
 * AnnotationCollection from AIM v4r48.
 * @author jamesd
 */
public abstract class AnnotationCollection extends AbstractDisplayable
	implements AimUnique
{
	protected String aimVersion = "AIMv4_0";
	protected String dateTime = "";
	protected String description = "";
	protected Equipment equipment = null;
	protected String uid = Uids.generateDicomUid();
	protected User user = null;
	private String path = "";

	/**
	 *
	 * @param annotation
	 * @return
	 */
	public abstract ImageAnnotation addAnnotation(ImageAnnotation annotation);

	/**
	 *
	 * @return
	 */
	public String getAimVersion()
	{
		return aimVersion;
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public abstract ImageAnnotation getAnnotation(String uid);

	/**
	 *
	 * @return
	 */
	public abstract int getAnnotationCount();

	/**
	 *
	 * @return
	 */
	public abstract List<ImageAnnotation> getAnnotationList();

	/**
	 *
	 * @return
	 */
	public String getDateTime()
	{
		return dateTime;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return the equipment
	 */
	public Equipment getEquipment()
	{
		return equipment;
	}

	/**
	 * @return the path
	 */
	public String getPath()
	{
		return path;
	}

	@Override
	public String getUid()
	{
		return uid;
	}

	/**
	 *
	 * @return
	 */
	public User getUser()
	{
		return user;
	}

	/**
	 *
	 * @param uid
	 * @return
	 */
	public abstract ImageAnnotation removeAnnotation(String uid);

	/**
	 * @param aimVersion the aimVersion to set
	 */
	public void setAimVersion(String aimVersion) throws IllegalArgumentException
	{
		if ((aimVersion == null) || aimVersion.isEmpty())
		{
			throw new IllegalArgumentException("AIM version must not be null or empty");
		}
		this.aimVersion = aimVersion;
	}

	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(String dateTime)
	{
		this.dateTime = (dateTime == null) ? "" : dateTime;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = (description == null) ? "" : description;
	}

	/**
	 * @param equipment the equipment to set
	 */
	public void setEquipment(Equipment equipment)
	{
		this.equipment = equipment;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path)
	{
		this.path = (path == null) ? "" : path;
	}

	@Override
	public void setUid(String uid)
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException("UID must not be null or empty");
		}
		this.uid = uid;
	}

	/**
	 * @param user
	 */
	public void setUser(User user)
	{
		this.user = user;
	}

}
