/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import icr.etherj.AbstractDisplayable;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author jamesd
 */
public class ImageAnnotationCollection extends AnnotationCollection
{
	private final Map<String,ImageAnnotation> annotations = new LinkedHashMap<>();
	private Person person = null;

	@Override
	public ImageAnnotation addAnnotation(ImageAnnotation annotation)
	{
		return annotations.put(annotation.getUid(), annotation);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"AimVersion: "+aimVersion);
		ps.println(pad+"DateTime: "+dateTime);
		ps.println(pad+"Description: "+getDescription());
		if (user != null)
		{
			user.display(ps, indent+"  ");
		}
		if (equipment != null)
		{
			equipment.display(ps, indent+"  ");
		}
		if (person != null)
		{
			person.display(ps, indent+"  ");
		}
		ps.println(pad+"Uid: "+uid);
		int nAnnotations = annotations.size();
		ps.println(pad+"AnnotationList: "+nAnnotations+
			" Annotation"+((nAnnotations != 1) ? "s" : ""));
		if (recurse)
		{
			getAnnotationList().forEach((annotation) ->
				annotation.display(ps, indent+"  ", recurse));
		}
	}

	@Override
	public ImageAnnotation getAnnotation(String uid)
	{
		return annotations.get(uid);
	}

	@Override
	public int getAnnotationCount()
	{
		return annotations.size();
	}

	@Override
	public List<ImageAnnotation> getAnnotationList()
	{
		List<ImageAnnotation> list = new ArrayList<>();
		Set<Entry<String,ImageAnnotation>> entries = annotations.entrySet();
		Iterator<Entry<String,ImageAnnotation>> iter = entries.iterator();
		while (iter.hasNext())
		{
			Entry<String,ImageAnnotation> entry = iter.next();
			list.add(entry.getValue());
		}
		return list;
	}

	@Override
	public String getDescription()
	{
		if ((description != null) && !description.isEmpty())
		{
			return description;
		}
		if (person == null)
		{
			return "Description";
		}
		if (dateTime.isEmpty())
		{
			return person.getName();
		}
		StringBuilder sb = new StringBuilder(person.getName()).append("_")
			.append(AimUtils.getDate(dateTime)).append("_")
			.append(AimUtils.getTime(dateTime));
		return sb.toString();
	}

	/**
	 * @return the person
	 */
	public Person getPerson()
	{
		return person;
	}

	@Override
	public ImageAnnotation removeAnnotation(String uid)
	{
		return annotations.remove(uid);
	}

	/**
	 * @param person the person to set
	 */
	public void setPerson(Person person)
	{
		this.person = person;
	}

	public static class FileUidPair extends AbstractDisplayable
	{
		private final String path;
		private final String uid;

		public FileUidPair(String path, String uid)
		{
			this.path = path;
			this.uid = uid;
		}

		@Override
		public void display(PrintStream ps, String indent, boolean recurse)
		{
			ps.println(indent+getClass().getName());
			String pad = indent+"  * ";
			ps.println(pad+"Path: "+path);
			ps.println(pad+"Uid: "+uid);
		}

		/**
		 * @return the path
		 */
		public String getPath()
		{
			return path;
		}

		/**
		 * @return the uid
		 */
		public String getUid()
		{
			return uid;
		}
	}
}
