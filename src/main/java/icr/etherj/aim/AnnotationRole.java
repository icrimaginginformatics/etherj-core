/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import java.io.PrintStream;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author jamesd
 */
public class AnnotationRole extends Entity implements QuestionTypeCoded
{
	private Code roleCode = new Code();
	private final SortedMap<String,Code> questionTypeCodes = new TreeMap<>();
	private int roleSequenceCode = -1;

	@Override
	public Code addQuestionTypeCode(Code code)
	{
		return addCode(code, questionTypeCodes);
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"UID: "+getUid());
	}

	@Override
	public Code getQuestionTypeCode(String code)
	{
		return getCode(code, questionTypeCodes);
	}
	/**
	 * @return the questionTypeCodes
	 */
	@Override
	public SortedMap<String,Code> getQuestionTypeCodeMap()
	{
		return getCodeMap(questionTypeCodes);
	}

	/**
	 * @return the roleCode
	 */
	public Code getRoleCode()
	{
		return roleCode;
	}

	/**
	 * @return the roleSequenceCode
	 */
	public int getRoleSequenceCode()
	{
		return roleSequenceCode;
	}

	@Override
	public Code removeQuestionTypeCode(String code)
	{
		return removeCode(code, questionTypeCodes);
	}

	/**
	 * @param roleCode the roleCode to set
	 */
	public void setRoleCode(Code roleCode)
	{
		this.roleCode = roleCode;
	}

	/**
	 * @param roleSequenceCode the roleSequenceCode to set
	 */
	public void setRoleSequenceCode(int roleSequenceCode)
	{
		this.roleSequenceCode = roleSequenceCode;
	}
	
}
