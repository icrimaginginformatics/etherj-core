/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import icr.etherj.AbstractDisplayable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author jamesd
 */
public abstract class CalculationResult extends AbstractDisplayable
{
	private Code dataType = new Code();
	private final SortedMap<Integer,Dimension> dims = new TreeMap<>();
	private final CalculationResultId type;
	private String unitOfMeasure = "";

	protected CalculationResult(CalculationResultId type)
	{
		if (type == null)
		{
			throw new IllegalArgumentException("Type must not be null");
		}
		this.type = type;
	}

	public Dimension addDimension(Dimension dim)
	{
		return dims.put(dim.getIndex(), dim);
	}

	/**
	 * @return the dataType
	 */
	public Code getDataType()
	{
		return dataType;
	}

	public Dimension getDimension(int idx)
	{
		return dims.get(idx);
	}

	public List<Dimension> getDimensionList()
	{
		List<Dimension> list = new ArrayList<>();
		Set<Map.Entry<Integer,Dimension>> entries = dims.entrySet();
		Iterator<Map.Entry<Integer,Dimension>> iter = entries.iterator();
		while (iter.hasNext())
		{
			Map.Entry<Integer,Dimension> entry = iter.next();
			list.add(entry.getValue());
		}
		return list;
	}

	/**
	 * @return the type
	 */
	public CalculationResultId getType()
	{
		return type;
	}

	/**
	 * @return the unitOfMeasure
	 */
	public String getUnitOfMeasure()
	{
		return unitOfMeasure;
	}

	/**
	 * @param dataType the dataType to set
	 */
	public void setDataType(Code dataType)
	{
		this.dataType = dataType;
	}

	/**
	 * @param unitOfMeasure the unitOfMeasure to set
	 */
	public void setUnitOfMeasure(String unitOfMeasure)
	{
		this.unitOfMeasure = unitOfMeasure;
	}

}
