/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import icr.etherj.AbstractDisplayable;
import icr.etherj.Uids;
import java.io.PrintStream;

/**
 *
 * @author jamesd
 */
public class ImageStudy extends AbstractDisplayable
{
	private ImageSeries series;
	private String instanceUid;
	private String startDate = "";
	private String startTime = "";

	/**
	 *
	 */
	public ImageStudy()
	{
		instanceUid = Uids.generateDicomUid();
	}

	/**
	 *
	 * @param uid
	 * @throws IllegalArgumentException if the UID is null or empty
	 */
	public ImageStudy(String uid) throws IllegalArgumentException
	{
		if ((uid == null) || uid.isEmpty())
		{
			throw new IllegalArgumentException("UID must not be null or empty");
		}
		instanceUid = uid;
	}

	@Override
	public void display(PrintStream ps, String indent, boolean recurse)
	{
		ps.println(indent+getClass().getName());
		String pad = indent+"  * ";
		ps.println(pad+"InstanceUid: "+instanceUid);
		ps.println(pad+"StartDate: "+startDate);
		ps.println(pad+"StartTime: "+startTime);
		if (series != null)
		{
			series.display(ps, indent+"  ", recurse);
		}
	}

	/**
	 * @return the series
	 */
	public ImageSeries getSeries()
	{
		return series;
	}

	/**
	 * @return the instanceUid
	 */
	public String getInstanceUid()
	{
		return instanceUid;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime()
	{
		return startTime;
	}

	/**
	 * @param series the series to set
	 */
	public void setSeries(ImageSeries series)
	{
		this.series = series;
	}

	/**
	 * @param instanceUid the instanceUid to set
	 * @throws IllegalArgumentException if the UID is null or empty
	 */
	public void setInstanceUid(String instanceUid) throws IllegalArgumentException
	{
		if ((instanceUid == null) || instanceUid.isEmpty())
		{
			throw new IllegalArgumentException("UID must not be null or empty");
		}
		this.instanceUid = instanceUid;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate)
	{
		this.startDate = (startDate == null) ? "" : startDate;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(String startTime)
	{
		this.startTime = (startTime == null) ? "" : startTime;
	}

}
