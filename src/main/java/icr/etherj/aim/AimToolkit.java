/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import icr.etherj.Ether;
import icr.etherj.PathScan;
import icr.etherj.search.CompoundSearchCriterion;
import icr.etherj.search.SearchCriterion;
import icr.etherj.search.SearchSpecification;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory and factory locator class for <code>etherj.aim</code> package.
 * @author jamesd
 */
public class AimToolkit
{
	/** Key for default toolkit. */
	public static final String Default = "default";

	private static final Logger logger = LoggerFactory.getLogger(AimToolkit.class);
	private static final Map<String,AimToolkit> toolkitMap = new HashMap<>();

	static
	{
		toolkitMap.put(Default, new AimToolkit());
	}

	/**
	 *
	 * @return
	 */
	public static AimToolkit getToolkit()
	{
		return getToolkit(Default);
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	public static AimToolkit getToolkit(String key)
	{
		return toolkitMap.get(key);
	}

	/**
	 *
	 * @param key
	 * @param toolkit
	 * @return
	 */
	public static AimToolkit setToolkit(String key, AimToolkit toolkit)
	{
		AimToolkit tk = toolkitMap.put(key, toolkit);
		logger.info(toolkit.getClass().getName()+" set with key '"+key+"'");
		return tk;
	}

	/**
	 *
	 * @return
	 */
	public ImageAnnotationCollection createIac()
	{
		return new ImageAnnotationCollection();
	}

	/**
	 *
	 * @return
	 */
	public PathScan<ImageAnnotationCollection> createPathScan()
	{
		return new AimPathScan();
	}

	/**
	 *
	 * @param tag
	 * @param comparator
	 * @param value
	 * @return
	 */
	public SearchCriterion createSearchCriterion(int tag, int comparator,
		String value)
	{
		return new AimSearchCriterion(tag, comparator, value);
	}

	/**
	 *
	 * @param tag
	 * @param comparator
	 * @param value
	 * @param combinator
	 * @return
	 */
	public SearchCriterion createSearchCriterion(int tag, int comparator,
		String value, int combinator)
	{
		return new AimSearchCriterion(tag, comparator, value, combinator);
	}

	/**
	 *
	 * @param a
	 * @param b
	 * @return
	 */
	public SearchCriterion createSearchCriterion(SearchCriterion a,
		SearchCriterion b)
	{
		return new CompoundSearchCriterion(a, b);
	}

	/**
	 *
	 * @param a
	 * @param b
	 * @param combinator
	 * @return
	 */
	public SearchCriterion createSearchCriterion(SearchCriterion a,
		SearchCriterion b, int combinator)
	{
		return new CompoundSearchCriterion(a, b, combinator);
	}

	/**
	 *
	 * @param criteria
	 * @return
	 */
	public SearchCriterion createSearchCriterion(List<SearchCriterion> criteria)
	{
		return new CompoundSearchCriterion(criteria);
	}

	/**
	 *
	 * @param criteria
	 * @param combinator
	 * @return
	 */
	public SearchCriterion createSearchCriterion(List<SearchCriterion> criteria,
		int combinator)
	{
		return new CompoundSearchCriterion(criteria, combinator);
	}

	/**
	 *
	 * @return
	 */
	public SearchSpecification createSearchSpecification()
	{
		return new SearchSpecification();
	}

	/**
	 *
	 * @return
	 */
	public XmlParser createXmlParser()
	{
		return new DefaultXmlParser();
	}

	/**
	 *
	 * @return
	 */
	public XmlWriter createXmlWriter()
	{
		return new DefaultXmlWriter();
	}

	/*
	 *	Private constructor to prevent direct instantiation
	 */
	private AimToolkit()
	{}
}
