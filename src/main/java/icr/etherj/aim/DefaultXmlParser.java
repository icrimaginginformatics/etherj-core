/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj.aim;

import icr.etherj.Xml;
import icr.etherj.XmlException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author jamesd
 */
public class DefaultXmlParser implements XmlParser
{
	private static final org.slf4j.Logger logger =
		LoggerFactory.getLogger(DefaultXmlParser.class);

	@Override
	public ImageAnnotationCollection parse(String path)
		throws XmlException, IOException
	{
		return parse(new File(path));
	}

	@Override
	public ImageAnnotationCollection parse(File file)
		throws XmlException, IOException
	{
		return parse(new FileInputStream(file), file.getAbsolutePath());
	}

	@Override
	public ImageAnnotationCollection parse(InputStream stream)
		throws XmlException, IOException
	{
		return parse(stream, "");
	}

	@Override
	public ImageAnnotationCollection parse(InputStream stream,
		String path) throws XmlException, IOException, IllegalArgumentException
	{
		ImageAnnotationCollection iac = null;
		boolean hasUid = false;
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(stream);
			Element rootNode = doc.getDocumentElement();
			if (!rootNode.getNodeName().equals(AimXml.NODE_ANNOTATION_COLLECTION))
			{
				throw new IllegalArgumentException(
					"Incorrect doc type: "+rootNode.getNodeName());
			}
			rootNode.normalize();
			iac = new ImageAnnotationCollection();
			if ((path != null) && !path.isEmpty())
			{
				iac.setPath(path);
				logger.debug("Parsing IAC from: {}", path);
			}
			iac.setAimVersion(rootNode.getAttribute(AimXml.ATTR_AIM_VERSION));
			NodeList childNodes = rootNode.getChildNodes();
			for (int i=0; i<childNodes.getLength(); i++)
			{
				Node node = childNodes.item(i);
				switch (node.getNodeName())
				{
					case AimXml.NODE_TEXT:
						continue;

					case AimXml.NODE_UID:
						String uid = Xml.getAttrStr(node.getAttributes(), AimXml.ATTR_ROOT);
						if ((uid != null) && !uid.isEmpty())
						{
							iac.setUid(uid);
							hasUid = true;
						}
						break;

					case AimXml.NODE_DATE_TIME:
						iac.setDateTime(
							Xml.getAttrStr(node.getAttributes(), AimXml.ATTR_VALUE));
						break;

					case AimXml.NODE_DESCRIPTION:
						iac.setDescription(
							Xml.getAttrStr(node.getAttributes(), AimXml.ATTR_VALUE));
						break;

					case AimXml.NODE_USER:
						parseUser(node, iac);
						break;

					case AimXml.NODE_EQUIPMENT:
						parseEquipment(node, iac);
						break;

					case AimXml.NODE_PERSON:
						parsePerson(node, iac);
						break;

					case AimXml.NODE_ANNOTATIONS:
						NodeList annotations = node.getChildNodes();
						for (int j=0; j<annotations.getLength(); j++)
						{
							parseAnnotation(annotations.item(j), iac);
						}

					default:
				}
			}
		}
		catch (ParserConfigurationException | SAXException ex)
		{
			throw new XmlException(ex);
		}
		if (!hasUid)
		{
			iac = null;
			logger.warn("ImageAnnotationCollection has invalid UID");
		}
		return iac;
	}

	private void parseAlgorithm(Node algoNode, Calculation calc)
	{
		Algorithm algorithm = new Algorithm();
		NodeList childNodes = algoNode.getChildNodes();
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_NAME:
					algorithm.setName(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_TYPE:
					algorithm.addTypeCode(parseCode(attrs));
					break;

				case AimXml.NODE_UID:
					algorithm.setUid(Xml.getAttrStr(attrs, AimXml.ATTR_ROOT));
					break;

				case AimXml.NODE_VERSION:
					algorithm.setVersion(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_DESCRIPTION:
					algorithm.setDescription(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_PARAMETER_COLLECTION:
					NodeList data = node.getChildNodes();
					for (int j=0; j<data.getLength(); j++)
					{
						parseParameter(data.item(j), algorithm);
					}
					break;

				default:
			}
		}
		if (algorithm.getName() != null)
		{
			calc.setAlgorithm(algorithm);
		}
		else
		{
			logger.warn("Ignoring Algorithm with invalid name");
		}
	}

	private void parseAnnotation(Node annoNode, ImageAnnotationCollection iac)
		throws XmlException
	{
		if (annoNode.getNodeName().equals(AimXml.NODE_TEXT))
		{
			return;
		}
		ImageAnnotation annotation = new ImageAnnotation();
		NodeList childNodes = annoNode.getChildNodes();
		boolean hasUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						annotation.setUid(uid);
						hasUid = true;
					}
					break;

				case AimXml.NODE_TYPECODE:
					Code code = parseCode(attrs);
					annotation.addTypeCode(code);
					break;

				case AimXml.NODE_DATE_TIME:
					annotation.setDateTime(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_NAME:
					annotation.setName(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_COMMENT:
					annotation.setComment(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_IMAGING_PHYSICAL_COLLECTION:
					NodeList ips = node.getChildNodes();
					for (int j=0; j<ips.getLength(); j++)
					{
						parseImagingPhysical(ips.item(j), annotation);
					}
					break;

				case AimXml.NODE_CALCULATION_COLLECTION:
					NodeList calcs = node.getChildNodes();
					for (int j=0; j<calcs.getLength(); j++)
					{
						parseCalculation(calcs.item(j), annotation);
					}
					break;

				case AimXml.NODE_IMAGING_OBSERVATION_COLLECTION:
					NodeList ios = node.getChildNodes();
					for (int j=0; j<ios.getLength(); j++)
					{
						parseImagingObservation(ios.item(j), annotation);
					}
					break;

				case AimXml.NODE_MARKUP_COLLECTION:
					NodeList markups = node.getChildNodes();
					for (int j=0; j<markups.getLength(); j++)
					{
						parseMarkup(markups.item(j), annotation);
					}
					break;

				case AimXml.NODE_IMAGE_REFERENCE_COLLECTION:
					NodeList references = node.getChildNodes();
					for (int j=0; j<references.getLength(); j++)
					{
						parseReference(references.item(j), annotation);
					}
					break;

				case AimXml.NODE_ANNOTATION_ROLE_COLLECTION:
					NodeList roles = node.getChildNodes();
					for (int j=0; j<roles.getLength(); j++)
					{
						parseAnnotationRole(roles.item(j), annotation);
					}
					break;

				case AimXml.NODE_INFERENCE_COLLECTION:
					NodeList infs = node.getChildNodes();
					for (int j=0; j<infs.getLength(); j++)
					{
						parseInference(infs.item(j), annotation);
					}
					break;

				case AimXml.NODE_TASK_CONTEXT_COLLECTION:
					NodeList tcs = node.getChildNodes();
					for (int j=0; j<tcs.getLength(); j++)
					{
						parseTaskContext(tcs.item(j), annotation);
					}
					break;

				default:
			}
		}
		if (hasUid && !annotation.getTypeCodeMap().isEmpty())
		{
			iac.addAnnotation(annotation);
		}
		else
		{
			logger.warn("Ignoring ImageAnnotation with invalid UID or TypeCode map");
		}
	}

	private void parseAnnotationRole(Node roleNode, ImageAnnotation annotation)
	{
		AnnotationRole role = new AnnotationRole();
		NodeList childNodes = roleNode.getChildNodes();
		boolean hasUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						role.setUid(uid);
						hasUid = true;
					}
					break;

				case AimXml.NODE_ROLECODE:
					role.setRoleCode(parseCode(attrs));
					break;

				case AimXml.NODE_QUESTION_TYPECODE:
					role.addQuestionTypeCode(parseCode(attrs));
					break;

				case AimXml.NODE_ROLE_SEQUENCE_NUMBER:
					role.setRoleSequenceCode(Xml.getAttrInt(attrs, AimXml.ATTR_VALUE));
					break;

				default:
			}
		}
		if (hasUid)
		{
			annotation.addAnnotationRole(role);
		}
		else
		{
			logger.warn("Ignoring Role with invalid UID");
		}
	}

	private void parseCalculation(Node calcNode, ImageAnnotation annotation)
	{
		if (AimXml.NODE_TEXT.equals(calcNode.getNodeName()))
		{
			return;
		}
		Calculation calc = new Calculation();
		NodeList childNodes = calcNode.getChildNodes();
		boolean hasUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						calc.setUid(uid);
						hasUid = true;
					}
					break;

				case AimXml.NODE_TYPECODE:
					calc.addTypeCode(parseCode(attrs));
					break;

				case AimXml.NODE_QUESTION_TYPECODE:
					calc.addQuestionTypeCode(parseCode(attrs));
					break;

				case AimXml.NODE_DESCRIPTION:
					calc.setDescription(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_CALCULATION_RESULT_COLLECTION:
					NodeList results = node.getChildNodes();
					for (int j=0; j<results.getLength(); j++)
					{
						parseCalculationResult(results.item(j), calc);
					}
					break;

				case AimXml.NODE_ALGORITHM:
					parseAlgorithm(node, calc);
					break;

				default:
			}
		}
		if (hasUid && !calc.getTypeCodeMap().isEmpty())
		{
			annotation.addCalculation(calc);
		}
		else
		{
			logger.warn("Ignoring Calculation with invalid UID or TypeCode map");
		}
	}

	private void parseCalculationData(Node dataNode,
		ExtendedCalculationResult result)
	{
		if (AimXml.NODE_TEXT.equals(dataNode.getNodeName()))
		{
			return;
		}
		CalculationData calcData = new CalculationData();
		NodeList childNodes = dataNode.getChildNodes();
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_VALUE:
					calcData.setValue(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_COORDINATE_COLLECTION:
					NodeList data = node.getChildNodes();
					for (int j=0; j<data.getLength(); j++)
					{
						parseCoordinate(data.item(j), calcData);
					}
					break;

				default:
			}
		}
		if (calcData.getValue() != null)
		{
			result.addCalculationData(calcData);
		}
		else
		{
			logger.warn("Ignoring CalculationData with invalid value");
		}
	}

	private void parseCalculationResult(Node resultNode, Calculation calc)
	{
		ExtendedCalculationResult result = null;
		if (resultNode.getNodeName().equals(AimXml.NODE_TEXT))
		{
			return;
		}
		NamedNodeMap resultAttrs = resultNode.getAttributes();
		CalculationResultId resultId =
			CalculationResultId.getId(Xml.getAttrStr(resultAttrs, AimXml.ATTR_TYPE, null));
		if (resultId == null)
		{
			return;
		}
		String clazz = Xml.getAttrStr(resultAttrs, AimXml.ATTR_XSI_TYPE);
		switch (clazz)
		{
			case "ExtendedCalculationResult":
				result = new ExtendedCalculationResult(resultId);
				break;

			default: // ToDo: Handle CompactCalculationResult
				logger.warn("Unsupported XSI type {}", clazz);
		}
		if (result == null)
		{
			return;
		}
		NodeList childNodes = resultNode.getChildNodes();
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_UNIT_OF_MEASURE:
					result.setUnitOfMeasure(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_DATATYPE:
					result.setDataType(parseCode(attrs));
					break;

				case AimXml.NODE_DIMENSION_COLLECTION:
					NodeList results = node.getChildNodes();
					for (int j=0; j<results.getLength(); j++)
					{
						parseDimension(results.item(j), result);
					}
					break;

				case AimXml.NODE_CALCULATION_DATA_COLLECTION:
					NodeList data = node.getChildNodes();
					for (int j=0; j<data.getLength(); j++)
					{
						parseCalculationData(data.item(j), result);
					}
					break;

				default:
			}
		}
		if (result.getType() != null)
		{
			calc.addCalculationResult(result);
		}
		else
		{
			logger.warn("Ignoring CalculationResult with invalid data type");
		}
	}

	private void parseCoordinate(Node coordNode, CalculationData calcData)
	{
		if (AimXml.NODE_TEXT.equals(coordNode.getNodeName()))
		{
			return;
		}
		NodeList childNodes = coordNode.getChildNodes();
		int index = -1;
		int pos = -1;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_DIMENSION_INDEX:
					index = Xml.getAttrInt(attrs, AimXml.ATTR_VALUE);
					break;

				case AimXml.NODE_POSITION:
					pos = Xml.getAttrInt(attrs, AimXml.ATTR_VALUE);
					break;

				default:
			}
		}
		if ((index >= 0) && (pos >= 0))
		{
			calcData.addCoordinate(new Coordinate(index, pos));
		}
		else
		{
			logger.warn("Ignoring Coordinate with invalid index or position");
		}
	}

	private Code parseCode(NamedNodeMap attrs)
	{
		Code code = new Code();
		code.setCode(Xml.getAttrStr(attrs, AimXml.ATTR_CODE, ""));
		code.setCodeSystem(Xml.getAttrStr(attrs, AimXml.ATTR_CODE_SYSTEM, ""));
		code.setCodeSystemName(Xml.getAttrStr(attrs, 
			AimXml.ATTR_CODE_SYSTEM_NAME, ""));
		code.setCodeSystemVersion(Xml.getAttrStr(attrs,
			AimXml.ATTR_CODE_SYSTEM_VERSION, ""));

		return code;
	}

	private void parseDimension(Node dimNode, CalculationResult result)
	{
		if (AimXml.NODE_TEXT.equals(dimNode.getNodeName()))
		{
			return;
		}
		NodeList childNodes = dimNode.getChildNodes();
		int index = -1;
		int size = -1;
		String label = null;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_INDEX:
					index = Xml.getAttrInt(attrs, AimXml.ATTR_VALUE);
					break;

				case AimXml.NODE_SIZE:
					size = Xml.getAttrInt(attrs, AimXml.ATTR_VALUE);
					break;

				case AimXml.NODE_LABEL:
					label = Xml.getAttrStr(attrs, AimXml.ATTR_VALUE);
					break;

				default:
			}
		}
		if ((index >= 0) && (size > 0) && (label != null))
		{
			result.addDimension(new Dimension(index, size, label));
		}
		else
		{
			logger.warn("Ignoring Dimension with invalid index, size or label");
		}
	}

	private void parseEquipment(Node equipNode, ImageAnnotationCollection iac)
	{
		Equipment equipment = new Equipment();
		NodeList childNodes = equipNode.getChildNodes();
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_EQUIPMENT_MANUFACTURER_NAME:
					equipment.setManufacturerName(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_EQUIPMENT_MANUFACTURER_MODEL_NAME:
					equipment.setManufacturerModelName(
						Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_EQUIPMENT_DEVICE_SERIAL_NUMBER:
					equipment.setDeviceSerialNumber(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_EQUIPMENT_SOFTWARE_VERSION:
					equipment.setSoftwareVersion(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				default:
			}
		}
		if (!equipment.getManufacturerName().isEmpty())
		{
			iac.setEquipment(equipment);
		}
		else
		{
			logger.warn("Ignoring Equipment with no manufacturer name");
		}
	}

	private void parseImage(Node imageNode, ImageSeries series)
	{
		if (imageNode.getNodeName().equals(AimXml.NODE_TEXT))
		{
			return;
		}
		Image image = new Image();
		NodeList childNodes = imageNode.getChildNodes();
		boolean hasInstUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_SOP_CLASS_UID:
					image.setSopClassUid(Xml.getAttrStr(attrs, AimXml.ATTR_ROOT));
					break;

				case AimXml.NODE_SOP_INSTANCE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						image.setSopInstanceUid(uid);
						hasInstUid = true;
					}
					break;

				default:
			}
		}
		if (hasInstUid)
		{
			series.addImage(image);
		}
		else
		{
			logger.warn("Ignoring Image with no valid UID");
		}
	}

	private void parseImagingObservation(Node ioNode, ImageAnnotation annotation)
	{
		if (AimXml.NODE_TEXT.equals(ioNode.getNodeName()))
		{
			return;
		}
		ImagingObservation io = new ImagingObservation();
		NodeList childNodes = ioNode.getChildNodes();
		boolean hasUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						io.setUid(uid);
						hasUid = true;
					}
					break;

				case AimXml.NODE_TYPECODE:
					io.addTypeCode(parseCode(attrs));
					break;

				case AimXml.NODE_QUESTION_TYPECODE:
					io.addQuestionTypeCode(parseCode(attrs));
					break;

				case AimXml.NODE_LABEL:
					io.setLabel(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE, ""));
					break;

				case AimXml.NODE_COMMENT:
					io.setComment(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE, ""));
					break;

				default:
			}
		}
		if (hasUid && !io.getTypeCodeMap().isEmpty())
		{
			annotation.addImagingObservation(io);
		}
		else
		{
			logger.warn("Ignoring ImagingObservation with no valid UID or empty TypeCode map");
		}
	}

	private void parseImagingPhysical(Node ipNode, ImageAnnotation annotation)
	{
		if (AimXml.NODE_TEXT.equals(ipNode.getNodeName()))
		{
			return;
		}
		ImagingPhysical ip = new ImagingPhysical();
		NodeList childNodes = ipNode.getChildNodes();
		boolean hasUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						ip.setUid(uid);
						hasUid = true;
					}
					break;

				case AimXml.NODE_TYPECODE:
					ip.addTypeCode(parseCode(attrs));
					break;

				case AimXml.NODE_QUESTION_TYPECODE:
					ip.addQuestionTypeCode(parseCode(attrs));
					break;

				case AimXml.NODE_LABEL:
					ip.setLabel(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_COMMENT:
					ip.setComment(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				default:
			}
		}
		if (hasUid && !ip.getTypeCodeMap().isEmpty())
		{
			annotation.addImagingPhysical(ip);
		}
		else
		{
			logger.warn("Ignoring ImagingPhysical with no valid UID or empty TypeCode map");	
		}
	}

	private void parseInference(Node infNode, ImageAnnotation annotation)
	{
		Inference inf = new Inference();
		NodeList childNodes = infNode.getChildNodes();
		boolean hasUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						inf.setUid(uid);
						hasUid = true;
					}
					break;

				case AimXml.NODE_TYPECODE:
					inf.addTypeCode(parseCode(attrs));
					break;

				case AimXml.NODE_QUESTION_TYPECODE:
					inf.addQuestionTypeCode(parseCode(attrs));
					break;

				case AimXml.NODE_LABEL:
					inf.setLabel(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE, ""));
					break;

				case AimXml.NODE_COMMENT:
					inf.setComment(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE, ""));
					break;

				case AimXml.NODE_QUESTION_INDEX:
					inf.setQuestionIndex(Xml.getAttrInt(attrs, AimXml.ATTR_VALUE, -1));
					break;

				case AimXml.NODE_IMAGE_EVIDENCE:
					inf.setImageEvidence(
						"true".equals(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE)));
					break;

				case AimXml.NODE_IS_PRESENT:
					inf.setIsPresent(
						"true".equals(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE)));
					break;

				case AimXml.NODE_ANNOTATOR_CONFIDENCE:
					inf.setAnnotatorConfidence(
						Xml.getAttrDouble(attrs, AimXml.ATTR_VALUE, 0.0));
					break;

				default:
			}
		}
		if (hasUid && !inf.getTypeCodeMap().isEmpty())
		{
			annotation.addInference(inf);
		}
		else
		{
			logger.warn("Ignoring Inference with no valid UID or empty TypeCode map");
		}
	}

	private void parseMarkup(Node markupNode, ImageAnnotation annotation)
	{
		TwoDimensionGeometricShape shape = null;
		if (markupNode.getNodeName().equals(AimXml.NODE_TEXT))
		{
			return;
		}
		String clazz = Xml.getAttrStr(markupNode.getAttributes(), AimXml.ATTR_XSI_TYPE);
		switch (clazz)
		{
			case AimXml.TWO_DIMENSION_POLYLINE:
				shape = new TwoDimensionPolyline();
				break;

			case AimXml.TWO_DIMENSION_CIRCLE:
				shape = new TwoDimensionCircle();
				break;

			case AimXml.TWO_DIMENSION_MULTIPOINT:
				shape = new TwoDimensionMultiPoint();
				break;

			default:
				logger.warn("Unsupported XSI type: {}", clazz);
		}
		if (shape == null)
		{
			return;
		}
		NodeList childNodes = markupNode.getChildNodes();
		boolean hasUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			int number;
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						shape.setUid(uid);
						hasUid = true;
					}
					break;

				case AimXml.NODE_SHAPE_ID:
					number = Xml.getAttrInt(attrs, AimXml.ATTR_VALUE);
					if (number >= 0)
					{
						shape.setShapeId(number);
					}
					break;

				case AimXml.NODE_INCLUDE_FLAG:
					shape.setIncludeFlag("true".equals(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE)));
					break;

				case AimXml.NODE_LINE_COLOUR:
					shape.setLineColour(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE, ""));
					break;

				case AimXml.NODE_IMAGE_REFERENCE_UID:
					shape.setImageReferenceUid(Xml.getAttrStr(attrs, AimXml.ATTR_ROOT));
					break;

				case AimXml.NODE_REFERENCED_FRAME_NUMBER:
					number = Xml.getAttrInt(attrs, AimXml.ATTR_VALUE);
					if (number >= 0)
					{
						shape.setReferencedFrameNumber(number);
					}
					break;

				case AimXml.NODE_TWO_DIMENSION_COORDINATE_COLLECTION:
					NodeList coords = node.getChildNodes();
					for (int j=0; j<coords.getLength(); j++)
					{
						parseTwoDCoordinate(coords.item(j), shape);
					}
					break;

				default:
			}
		}
		if (hasUid)
		{
			annotation.addMarkup(shape);
		}
		else
		{
			logger.warn("Ignoring XSI type {} with no valid UID", clazz);
		}
	}

	private void parseParameter(Node paramNode, Algorithm algorithm)
	{
		NodeList childNodes = paramNode.getChildNodes();
		String name = null;
		String value = null;
		Code dataType = null;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_NAME:
					name = Xml.getAttrStr(attrs, AimXml.ATTR_VALUE);
					break;

				case AimXml.NODE_VALUE:
					value = Xml.getAttrStr(attrs, AimXml.ATTR_VALUE);
					break;

				case AimXml.NODE_DATATYPE:
					dataType = parseCode(attrs);
					break;

				default:
			}
		}
		if ((name != null) && (value != null) && (dataType != null))
		{
			algorithm.addParameter(new Parameter(name, value, dataType));
		}
		else
		{
			logger.warn("Ignoring Parameter with null name, value or data type");
		}
	}

	private void parsePerson(Node personNode, ImageAnnotationCollection iac)
	{
		Person person = new Person();
		NodeList childNodes = personNode.getChildNodes();
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_PERSON_BIRTHDATE:
					person.setBirthDate(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_PERSON_ETHNIC_GROUP:
					person.setEthnicGroup(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_PERSON_ID:
					person.setId(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_PERSON_NAME:
					person.setName(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_PERSON_SEX:
					person.setSex(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				default:
			}
		}
		if (!person.getName().isEmpty() && !person.getId().isEmpty())
		{
			iac.setPerson(person);
		}
		else
		{
			logger.warn("Ignoring Person with empty name or ID");
		}
	}

	private void parseReference(Node refNode, ImageAnnotation annotation)
	{
		DicomImageReference ref = null;
		if (refNode.getNodeName().equals(AimXml.NODE_TEXT))
		{
			return;
		}
		NamedNodeMap refAttrs = refNode.getAttributes();
		String clazz = Xml.getAttrStr(refAttrs, AimXml.ATTR_XSI_TYPE);
		switch (clazz)
		{
			case AimXml.DICOM_IMAGE_REFERENCE_ENTITY:
				ref = new DicomImageReference();
				break;

			default:
		}
		if (ref == null)
		{
			return;
		}
		NodeList childNodes = refNode.getChildNodes();
		boolean hasUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						ref.setUid(uid);
						hasUid = true;
					}
					break;

				case AimXml.NODE_IMAGE_STUDY:
					parseStudy(node, ref);
					break;

				default:
			}
		}
		if (hasUid)
		{
			annotation.addReference(ref);
		}
		else
		{
			logger.warn("Ignoring DicomImageReferenceEntity with no valid UID");
		}
	}

	private void parseSeries(Node seriesNode, ImageStudy study)
	{
		if (seriesNode.getNodeName().equals(AimXml.NODE_TEXT))
		{
			return;
		}
		ImageSeries series = new ImageSeries();
		NodeList childNodes = seriesNode.getChildNodes();
		boolean hasUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_INSTANCE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						series.setInstanceUid(uid);
						hasUid = true;
					}
					break;

				case AimXml.NODE_MODALITY:
					Code modality = series.getModality();
					modality.setCode(Xml.getAttrStr(attrs, AimXml.ATTR_CODE, ""));
					modality.setCodeSystem(Xml.getAttrStr(attrs, AimXml.ATTR_CODE_SYSTEM, ""));
					modality.setCodeSystemName(Xml.getAttrStr(attrs, 
						AimXml.ATTR_CODE_SYSTEM_NAME, ""));
					modality.setCodeSystemVersion(Xml.getAttrStr(attrs,
						AimXml.ATTR_CODE_SYSTEM_VERSION, ""));
					break;

				case AimXml.NODE_IMAGE_COLLECTION:
					NodeList images = node.getChildNodes();
					for (int j=0; j<images.getLength(); j++)
					{
						parseImage(images.item(j), series);
					}
					break;

				default:
			}
		}
		if (hasUid)
		{
			study.setSeries(series);
		}
		else
		{
			logger.warn("Ignoring ImageSeries with no valid UID");
		}
	}

	private void parseStudy(Node studyNode, DicomImageReference ref)
	{
		if (studyNode.getNodeName().equals(AimXml.NODE_TEXT))
		{
			return;
		}
		ImageStudy study = new ImageStudy();
		NodeList childNodes = studyNode.getChildNodes();
		boolean hasUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_INSTANCE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						study.setInstanceUid(uid);
						hasUid = true;
					}
					break;

				case AimXml.NODE_START_DATE:
					study.setStartDate(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_START_TIME:
					study.setStartTime(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_IMAGE_SERIES:
					parseSeries(node, study);
					break;

				default:
			}
		}
		if (hasUid)
		{
			ref.setStudy(study);
		}
		else
		{
			logger.warn("Ignoring ImageStudy with no valid UID");
		}
	}

	private TaskContext parseTaskContext(Node tcNode)
	{
		TaskContext tc = new TaskContext();
		NodeList childNodes = tcNode.getChildNodes();
		boolean hasUid = false;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_UID:
					String uid = Xml.getAttrStr(attrs, AimXml.ATTR_ROOT);
					if ((uid != null) && !uid.isEmpty())
					{
						tc.setUid(uid);
						hasUid = true;
					}
					break;

				case AimXml.NODE_WORKLIST_TASK_CATEGORY:
					tc.setWorklistTaskCategory(parseCode(attrs));
					break;

				case AimXml.NODE_WORKLIST_TASK_LEVEL:
					tc.setWorklistTaskLevel(parseCode(attrs));
					break;

				case AimXml.NODE_WORKLIST_TASK_TYPE:
					tc.setWorklistTaskType(parseCode(attrs));
					break;

				case AimXml.NODE_WORKLIST_TASK_REPEAT_TYPE:
					tc.setWorklistTaskRepeatType(parseCode(attrs));
					break;

				case AimXml.NODE_WORKLIST_TASK_VARIABILITY_TYPE:
					tc.setWorklistTaskVariabilityType(parseCode(attrs));
					break;

				case AimXml.NODE_WORKLIST_TASK_UID:
					tc.setWorklistTaskUid(Xml.getAttrStr(attrs, AimXml.ATTR_ROOT, ""));
					break;

				case AimXml.NODE_WORKLIST_TASK_NAME:
					tc.setWorklistTaskName(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE, ""));
					break;

				case AimXml.NODE_WORKLIST_TASK_DESCRIPTION:
					tc.setWorklistTaskDescription(
						Xml.getAttrStr(attrs, AimXml.ATTR_VALUE, ""));
					break;

				case AimXml.NODE_WORKLIST_SUBTASK_UID:
					tc.setWorklistSubtaskUid(Xml.getAttrStr(attrs, AimXml.ATTR_ROOT, ""));
					break;

				case AimXml.NODE_WORKLIST_SUBTASK_NAME:
					tc.setWorklistSubtaskName(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE, ""));
					break;

				case AimXml.NODE_WORKLIST_SUBTASK_START_DT:
					tc.setWorklistSubtaskStartDateTime(
						Xml.getAttrStr(attrs, AimXml.ATTR_VALUE, ""));
					break;

				case AimXml.NODE_WORKLIST_SUBTASK_CLOSED_DT:
					tc.setWorklistSubtaskClosedDateTime(
						Xml.getAttrStr(attrs, AimXml.ATTR_VALUE, ""));
					break;

				case AimXml.NODE_TASK_CONTEXT_COLLECTION:
					NodeList tcs = node.getChildNodes();
					for (int j=0; j<tcs.getLength(); j++)
					{
						parseTaskContext(tcs.item(j), tc);
					}
					break;

				default:
			}
		}
		if (!hasUid)
		{
			tc = null;
			logger.warn("Ignoring TaskContext with no valid UID");
		}
		return tc;
	}

	private void parseTaskContext(Node tcNode, ImageAnnotation annotation)
	{
		TaskContext tc = parseTaskContext(tcNode);
		if (tc != null)
		{
			annotation.addTaskContext(tc);
		}
	}

	private void parseTaskContext(Node tcNode, TaskContext parentTc)
	{
		TaskContext tc = parseTaskContext(tcNode);
		if (tc != null)
		{
			parentTc.addTaskContext(tc);
		}
	}

	private void parseTwoDCoordinate(Node coordNode,
		TwoDimensionGeometricShape shape)
	{
		if (coordNode.getNodeName().equals(AimXml.NODE_TEXT))
		{
			return;
		}
		NodeList childNodes = coordNode.getChildNodes();
		int idx = -1;
		double x = Double.NaN;
		double y = Double.NaN;
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_COORDINATE_INDEX:
					int value = Xml.getAttrInt(attrs, AimXml.ATTR_VALUE, -1);
					if (value >= 0)
					{
						idx = value;
					}
					break;

				case AimXml.NODE_X:
					x = Xml.getAttrDouble(attrs, AimXml.ATTR_VALUE);
					break;

				case AimXml.NODE_Y:
					y = Xml.getAttrDouble(attrs, AimXml.ATTR_VALUE);
					break;

				default:
			}
		}
		if (idx >= 0)
		{
			if (Double.isFinite(x) && Double.isFinite(y))
			{
				shape.addCoordinate(new TwoDimensionCoordinate(idx, x, y));
			}
			else
			{
				logger.warn(
					String.format("Ignoring non-finite TwoDimensionCoordinate: (%f,%f)",
					x, y));
			}
		}
		else
		{
			logger.warn("Ignoring TwoDimensionCoordinate with negative index: {}",
				idx);
		}
	}

	private void parseUser(Node userNode, ImageAnnotationCollection iac)
	{
		User user = new User();
		NodeList childNodes = userNode.getChildNodes();
		for (int i=0; i<childNodes.getLength(); i++)
		{
			Node node = childNodes.item(i);
			NamedNodeMap attrs = node.getAttributes();
			switch (node.getNodeName())
			{
				case AimXml.NODE_TEXT:
					continue;

				case AimXml.NODE_USER_NAME:
					user.setName(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_USER_LOGIN_NAME:
					user.setLoginName(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_USER_ROLE:
					user.setRoleInTrial(Xml.getAttrStr(attrs, AimXml.ATTR_VALUE));
					break;

				case AimXml.NODE_USER_NUMBER_IN_ROLE:
					user.setNumberWithinRoleOfClinicalTrial(
						Xml.getAttrInt(attrs, AimXml.ATTR_VALUE, 0));
					break;

				default:
			}
		}
		if (!user.getName().isEmpty() && !user.getLoginName().isEmpty())
		{
			iac.setUser(user);
		}
		else
		{
			logger.warn("Ignoring User with empty name or loginName");
		}
	}

	DefaultXmlParser()
	{}

}
