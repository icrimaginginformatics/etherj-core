/*********************************************************************
 * Copyright (c) 2017, Institute of Cancer Research
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * (3) Neither the name of the Institute of Cancer Research nor the
 *     names of its contributors may be used to endorse or promote
 *     products derived from this software without specific prior
 *     written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
package icr.etherj;

import com.google.common.collect.ImmutableList;
import icr.etherj.concurrent.Concurrent;
import icr.etherj.concurrent.TaskMonitor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A skeletal implementation of the PathScan interface. The subclass need only
 * implement scanFile(File).
 * @author jamesd
 * @param <T> the type of item to search for
 */
public abstract class AbstractPathScan<T> implements PathScan<T>
{
	private static final Logger logger =
		LoggerFactory.getLogger(AbstractPathScan.class);

	// CopyOnWriteArrayList to prevent concurrent modification errors during
	// iteration
	private final List<PathScanContext<T>> contexts = new CopyOnWriteArrayList<>();
	private volatile boolean stopScanning = false;

	@Override
	public boolean addContext(PathScanContext<T> context)
	{
		if (contexts.contains(context))
		{
			logger.warn(
				"PathScanContext #"+context.hashCode()+" is already registered.");
			return false;
		}
		return this.contexts.add(context);
	}

	@Override
	public List<PathScanContext<T>> getContextList()
	{
		return ImmutableList.copyOf(contexts);
	}

	@Override
	public boolean removeContext(PathScanContext<T> context)
	{
		return contexts.remove(context);
	}

	@Override
	public void scan(String path) throws IOException
	{
		scan(path, true, null);
	}

	@Override
	public void scan(String path, boolean recurse) throws IOException
	{
		scan(path, recurse, null);
	}

	@Override
	public void scan(String path, TaskMonitor taskMonitor) throws IOException
	{
		scan(path, true, taskMonitor);
	}

	@Override
	public void scan(String path, boolean recurse, TaskMonitor taskMonitor)
		throws IOException
	{
		Path jPath = Paths.get(path);
		if (taskMonitor == null)
		{
			taskMonitor = Concurrent.getTaskMonitor(true);
		}

		// Bail out if it's not a directory or not readable
		if (!Files.isDirectory(jPath) ||  !Files.isReadable(jPath))
		{
			logger.warn("Not a directory or cannot be read: {}",
				jPath);
			return;
		}

		stopScanning = false;
		taskMonitor.addPropertyChangeListener(new PropertyChangeListener()
		{
			@Override
			public void propertyChange(PropertyChangeEvent evt)
			{
				if (evt.getPropertyName().equals(TaskMonitor.CANCELLED))
				{
					stopScanning = ((TaskMonitor) evt.getSource()).isCancelled();
				}
			}
		});
		taskMonitor.setIndeterminate(true);
		taskMonitor.setDescription("Building import list...");
		logger.info(taskMonitor.getDescription());
		SortedMap<String,List<Path>> tree = new TreeMap<>();
		buildTree(tree, jPath, recurse, taskMonitor);
		int fileCount = getFileCount(tree);
		taskMonitor.setIndeterminate(false);
		taskMonitor.setDescription("Scanning "+Integer.toString(fileCount)+
			" files from "+jPath+"...");
		taskMonitor.setMinimum(0);
		taskMonitor.setMaximum(fileCount);
		taskMonitor.setValue(0);
		logger.info(taskMonitor.getDescription());
		ContextTreeWalker contextWalker = new ContextTreeWalker(contexts,
			taskMonitor);
		Iterator<PathScanContext<T>> iter = contexts.iterator();
		while (iter.hasNext())
		{
			iter.next().notifyScanStart();
		}
		walkTree(tree, contextWalker);
		iter = contexts.iterator();
		while (iter.hasNext())
		{
			iter.next().notifyScanFinish();
		}
		taskMonitor.setValue(taskMonitor.getMaximum());
		logger.info("Scan complete {}", jPath);
	}

	private void buildTree(SortedMap<String,List<Path>> tree, Path path,
		boolean recurse, TaskMonitor taskMonitor)
	{
		taskMonitor.setDescription("Listing directory "+path.toString()+"...");
		logger.debug("Listing directory {}...", path);
		List<Path> fileList = new ArrayList<>();
		tree.put(path.toString(), fileList);

		if (!Files.isReadable(path))
		{
			logger.warn("Directory cannot be read: {}", path.toString());
			return;
		}

		try {
			Files.walkFileTree(path,new SimpleFileVisitor<Path>(){
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
					if (!attrs.isDirectory()) {
						fileList.add(file);
					}
					if(stopScanning){
						return FileVisitResult.TERMINATE;
					}
					return FileVisitResult.CONTINUE;
				}
			});
		} catch (IOException e) {
			logger.error("",e);
		}
	}

	private int getFileCount(Map<String,List<Path>> tree) {
		int fileCount = 0;
		Set<String> keys = tree.keySet();
		Iterator<String> iter = keys.iterator();
		while (iter.hasNext()) {
			fileCount += tree.get(iter.next()).size();
		}
		return fileCount;
	}

	private void walkTree(SortedMap<String,List<Path>> tree, TreeWalker walker)
		throws IOException
	{
		Set<String> keys = tree.keySet();
		Iterator<String> iter = keys.iterator();
		while (iter.hasNext() && !stopScanning)
		{
			String path = iter.next();
			walker.onDirectory(Paths.get(path));
			List<Path> pathContents = tree.get(path);
			Iterator<Path> listIter = pathContents.iterator();
			while (listIter.hasNext() && !stopScanning)
			{
				Path file = listIter.next();
				walker.onFile(file);
			}
		}
	}

		interface TreeWalker
	{
		public void onDirectory(Path directory) throws IOException;

		public void onFile(Path file) throws IOException;
	}

	/**
	 *
	 */
	private class FileCountTreeWalker implements TreeWalker
	{
		int nFiles = 0;
		int nDirs = 0;

		public int getDirectoryCount()
		{
			return nDirs;
		}

		public int getFileCount()
		{
			return nFiles;
		}

		@Override
		public void onDirectory(Path directory) throws IOException
		{
			nDirs++;
		}

		@Override
		public void onFile(Path file) throws IOException
		{
			nFiles++;
		}
	}

	/**
	 *
	 */
	private class ContextTreeWalker implements TreeWalker
	{
		private List<PathScanContext<T>> contexts = null;
		private TaskMonitor taskMonitor = null;

		ContextTreeWalker(List<PathScanContext<T>> contexts, TaskMonitor taskMonitor)
		{
			this.contexts = contexts;
			this.taskMonitor = taskMonitor;
		}

		@Override
		public void onDirectory(Path directory) throws IOException
		{
			logger.trace("Scanning directory: {}", directory);
		}

		@Override
		public void onFile(Path file) throws IOException
		{
			T item = scanFile(file);
			if (item != null)
			{
				Iterator<PathScanContext<T>> iter = contexts.iterator();
				while (iter.hasNext())
				{
					// Notify contexts but protect ourselves if one throws
					try
					{
						iter.next().notifyItemFound(file.toFile(), item);
					}
					catch (Exception ex)
					{
						logger.warn("Exception in client PathScanContext", ex);
					}
				}
			}
			taskMonitor.setValue(taskMonitor.getValue()+1);
		}
	}
}
